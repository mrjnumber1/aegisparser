<?php

function in_arrayi($needle, array $haystack) {
	return in_array(strtolower($needle), array_map('strtolower', $haystack));
}

function array_keys_blacklist(array $array, array $blacklist) {
	foreach ($array as $key => $value)
		if (in_arrayi($key, $blacklist))
			unset($array[$key]);

	return $array;
}


$options = getopt("", ["server:", "uid::", "pwd::", "port::"]);

$server = $options["server"];
$uid = $options["uid"] ?? null;
$pwd = $options["pwd"] ?? null;
$port = $options["port"]??null;

$connection_info = null;

if (!is_null($uid) && !is_null($pwd)) {
	$connection_info = ["UID" => $uid, "PWD" => $pwd];
}

if (!is_null($port)) {
	$server .= ", ".$port;

$conn = null;

if (!is_null($connection_info) {
	$conn = sqlsrv_connect($server, $connection_info);
} else {
	$conn = sqlsrv_connect($server);



if (!$conn) {
	die(print_r(sqlsrv_errors(), true));
}

$data = [];
$tables = [];

$table_blacklist = [
	'charmotiontime', 'expparameter', 'expparameter2', 'expparameter3', 'firstjobexpparameter', 'firstjobexpparameter2',
	'firstjobexpparameter3', 'fsm', 'guest', 'guildexpparameter', 'jobexp', 'novicejobexpparameter',
	'novicejobexpparameter2',  'secondjobexpparameter', 'secondjobexpparameter2', 'ThirdJobExpParameter',
	'monmakingitem_exp', 'monparameter_exp', 'skillinfo', 'skilltypeinfo'
];
$column_blacklist = ['abuse_gauge', 'checkname', 'check_name', 'explan', 'explanation', 'sell', 'ect'];


$st = "SELECT TABLE_NAME FROM [Script].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_TYPE = 'BASE_TABLE'";
$query = sqlsrv_query($conn, $st);

if (!$query) {
	die(print_r(sqlsrv_errors(), true));
}

while (($row = sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC))) {
	$tables[] = $row['TABLE_NAME'];
}

sqlsrv_free_stmt($query);



foreach ($tables as $table)
{
	if (in_arrayi($table, $table_blacklist)) {
		continue;
	}


	$st = "SELECT * FROM [{$db}].[dbo].[{$table}]";
	$query = sqlsrv_query($conn, $st);
	if (!$query) {
		die(print_r(sqlsrv_errors(), true));
	}

	while (($row = sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC))) {
		$data[$table][] = array_keys_blacklist($row, $column_blacklist);
	}

	sqlsrv_free_stmt($query);

}

sqlsrv_close($conn);

echo serialize($data);




