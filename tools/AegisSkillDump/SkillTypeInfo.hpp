#pragma once

#include "library.hpp"

class CSkillTypeInfo {
public:
	struct vtable_t {
		void* (CSkillTypeInfo::*scalar_deleting_destructor)(uint32 flags);
		void (CSkillTypeInfo::*Init)(); //__purecall
		int32 (CSkillTypeInfo::*GetGNDRange)(int16 skLevel);
		int16 (CSkillTypeInfo::GetMaxLevel)();
		int32 (CSkillTypeInfo::OnMsg)(intptr sender, uint16 skid, int32 msg, int32 skLevel, int32 target, int32 par3, int32 par4);
		int16 (CSkillTypeInfo::GetSPCost)(int16 level);
		int32 (CSkillTypeInfo::IsAvailableJob)(int16 job);
		int16 (CSkillTypeInfo::GetAttackRange)(int32 level);
		int16 (CSkillTypeInfo::GetPreDelayTime)(int16 level);
		int16 (CSkillTypeInfo::GetPostDelayTM)(int16 level);
		uint32 (CSkillTypeInfo::GetProperty)();
		void (CSkillTypeInfo::SetProperty)(uint32);
		int16 (CSkillTypeInfo::GetVersion)();
		bool (CSkillTypeInfo::IsEnableIgnoreMagicImmune)(int32 skLevel);
		int32 (CSkillTypeInfo::IsSpellConditionGratify)(intptr, intptr, const int32 level, int32& out_USESKILL_FAIL);
	};
	
	CSkillTypeInfo() : { m_attackRange = 1; }
	virtual CSkillTypeInfo::~CSkillTypeInfo();
	virtual void SkillTypeInfo::Init() = 0;
	void SkillTypeInfo::SetName(const uchar* name);
	void SetSKID(int16 SKID);
	void SetType(int32 type);
	void SetFlag(uint32 flag); 
	void SetReferSKRNG(uint16 rng);
	void SetPattern(int32 pattern);
	uchar* GetName();
	uint16 GetSKID();
	int32 GetType();
	int32 GetPattern();
	virtual int GetGNDRange(int16 skLevel);
	uint32 GetFlag();
	uint16 GetReferSKRNG();
	int32 IsEventSkill();
	void SetEventSkill(int32 isEventSkill);
	bool IsNextLevelUpgradable(int16 level, int8 job);
	virtual int16 GetMaxLevel();
	virtual int32 OnMsg(intptr sender, uint16 skid, int32 msg, int32 skLevel, int32 targetID, int32 par3, int32 par4);
	virtual int16 GetSPCost(int16 level);
	virtual int32 IsAvailableJob(int16 job);
	virtual int16 GetAttackRange(int32 level);
	virtual int16 GetPreDelayTime(int16 level);
	virtual int16 GetPostDelayTM(int16 level);
	virtual uint32 GetProperty();
	virtual void SetProperty(uint32 property);
	int32 IsPossibleDispel();
	void SetAvailableJobLevel(int32 a, int32 b);
	int32 GetAvailableJobLevel();
	int32 GetAvailableJobLevel_ValidJob();
	virtual int16 GetVersion();
	virtual bool IsEnableIgnoreMagicImmune(int32 skLevel);
	int32 GetSinglePostDelay(int16 skLevel);
	int32 GetGlobalPostDelay(int16 skLevel);
	void SetSinglePostDelay(int16 skLevel, int32 amount);
	void SetGlobalPostDelay(int16 skLevel, int32 amount);
	void SetCastFixedDelay(int16 skLevel, int32 amount);
	int32 GetCastFixedDelay(int16 skLevel);
	void SetCastStatDelay(int16 skLevel, int32 amount);
	int32 GetCastStatDelay(int16 skLevel);
	virtual int32 IsSpellConditionGratify(intptr, intptr, const int32 level, int32& out_USESKILL_FAIL);
	
protected:
	int32 m_singlePostDelay[MAX_SKILL_LEVEL];
	int32 m_globalPostDelay[MAX_SKILL_LEVEL];
	int32 m_castFixedDelay[MAX_SKILL_LEVEL];
	int32 m_castStatDelay[MAX_SKILL_LEVEL];
	int32 m_property;
	int32 m_flag;
	int16 m_SKID;
	int16 m_referSKRNG;
	int32 m_pattern;
	int32 m_isEventSkill;
	int32 m_type;
	int16 m_attackRange; 
	uchar m_name[24];
	int32 m_dispel;
	int32 m_jobLevel;
	int32 m_jobLevelValidJob;
	int32 m_GNDRange;
};

class SK_NV_FIRSTAID : public CSkillTypeInfo {
	bool IsAvailableJob() { return true; };
}
