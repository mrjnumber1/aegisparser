#pragma once

// modified from czmq and rathena.
// rathena is licensed GPL 3.0 at http://rathena.org
// czmq is licensed MPL 2.0 at http://czmq.zeromq.org

#if (defined(__64BIT__) || defined(__x64_64__))
#define __IS_64BIT__
#else
#define __IS_32BIT__
#endif

#if (defined(WIN32) || defined(_WIN32))
#undef __WINDOWS__
#define __WINDOWS__
#undef __MSDOS__
#define __MSDOS__
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#endif

#if (defined(WINDOWS) || defined(_WINDOWS) || defined(__WINDOWS__))
#undef __WINDOWS__
#define __WINDOWS__
#undef __MSDOS__
#define __MSDOS__
#ifdef _MSC_VER
#if _MSC_VER >= 1500
#undef _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_DEPRECATE
#undef _WINSOCK_DEPRECATED_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma warning(disable : 4996)
#pragma warning(disable : 4018)
#endif
#endif
#endif

#if (defined(MSDOS) || defined(_MSC_VER))
#undef __MSDOS__
#define __MSDOS__
#if (defined(_DEBUG) && !defined(DEBUG))
#define DEBUG
#endif
#endif

#if (defined(__MSDOS__))
#if (defined(__WINDOWS__))
#if (defined(_WIN32_WINNT) && _WIN32_WINNT < 0x0600)
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif
#if (!defined(FD_SETSIZE))
#define FD_SETSIZE 1024 //	Max. filehandles/sockets
#endif
#include <direct.h>
#include <winsock2.h>
#include <windows.h>
#include <process.h>
#include <ws2tcpip.h> //	For getnameinfo ()
#include <iphlpapi.h> //	For GetAdaptersAddresses ()
#include <typeinfo>	  // for typeid
#define inet_ntop InetNtop
#endif
#include <malloc.h>
#include <dos.h>
#include <io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/utime.h>
#include <share.h>
#else
#define closesocket close
#define SOCKET_ERROR (-1)
#define INVALID_SOCKET (-1)
#endif

#if (defined(__UNIX__))
#include <fcntl.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <utime.h>
#include <inttypes.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/file.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <sys/uio.h>	//	Let CZMQ build with libzmq/3.x
#include <netinet/in.h> //	Must come before arpa/inet.h
#if (!defined(__UTYPE_ANDROID)) && (!defined(__UTYPE_IBMAIX)) && (!defined(__UTYPE_HPUX))
#include <ifaddrs.h>
#endif
#if defined(__UTYPE_SUNSOLARIS) || defined(__UTYPE_SUNOS)
#include <sys/sockio.h>
#endif
#if (!defined(__UTYPE_BEOS))
#include <arpa/inet.h>
#if (!defined(TCP_NODELAY))
#include <netinet/tcp.h>
#endif
#endif
#if (defined(__UTYPE_IBMAIX) || defined(__UTYPE_QNX))
#include <sys/select.h>
#endif
#if (defined(__UTYPE_BEOS))
#include <NetKit.h>
#endif
#if ((defined(_XOPEN_REALTIME) && (_XOPEN_REALTIME >= 1)) || (defined(_POSIX_VERSION) && (_POSIX_VERSION >= 199309L)))
#include <sched.h>
#endif
#if (defined(__UTYPE_OSX) || defined(__UTYPE_IOS))
#include <mach/clock.h>
#include <mach/mach.h> //	For monotonic clocks
#endif
#if (defined(__UTYPE_OSX))
#include <crt_externs.h> //	For _NSGetEnviron()
#endif
#if (defined(__UTYPE_ANDROID))
#include <android/log.h>
#endif
#if (defined(__UTYPE_LINUX) && defined(HAVE_LIBSYSTEMD))
#include <systemd/sd-daemon.h>
#endif
#endif

#if (defined(__VMS__))
#if (!defined(vaxc))
#include <fcntl.h> //	Not provided by Vax C
#endif
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include <unixio.h>
#include <unixlib.h>
#include <types.h>
#include <file.h>
#include <socket.h>
#include <dirent.h>
#include <time.h>
#include <pwd.h>
#include <stat.h>
#include <in.h>
#include <inet.h>
#endif

#if (defined(__OS2__))
#include <sys/types.h> //	Required near top
#include <fcntl.h>
#include <malloc.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <io.h>
#include <process.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/file.h>
#include <sys/wait.h>
#include <netinet/in.h> //	Must come before arpa/inet.h
#include <arpa/inet.h>
#include <utime.h>
#if (!defined(TCP_NODELAY))
#include <netinet/tcp.h>
#endif
#endif

// include whole crt and stl cuz why not of course

// C
#include <cassert>
#include <cctype>
#include <cerrno>
#include <cfloat>
#include <ciso646>
#include <climits>
#include <clocale>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cwchar>
#include <cwctype>
#include <ccomplex>
#include <cfenv>
#include <cinttypes>
#include <cstdalign>
#include <cstdbool>
#include <cstdint>
#include <ctgmath>
#include <cuchar>

// C++
#include <algorithm>
#include <bitset>
#include <complex>
#include <deque>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <new>
#include <numeric>
#include <ostream>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>
#include <vector>

#include <array>
#include <atomic>
#include <chrono>
#include <codecvt>
#include <condition_variable>
#include <forward_list>
#include <future>
#include <initializer_list>
#include <mutex>
#include <random>
#include <ratio>
#include <regex>
#include <scoped_allocator>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <shared_mutex>

// now all 3rd party libs

// grflib
#include <grfcrypt.h>

//mINI
#include <mINI/ini.h>

// zlib
#ifdef WIN32
#	define ZLIB_WINAPI
#endif
#include <zlib.h>


static_assert(UCHAR_MAX == 0xFF, "byte definition needs changed!!");
static_assert(USHRT_MAX == 0xFFFFU, "word definition needs changed!!");
static_assert(UINT_MAX == 0xFFFFFFFFU, "dword definition needs changed!!");
static_assert(ULLONG_MAX == 0xFFFFFFFFFFFFFFFFULL, "qword definition needs changed!!");

// required fixed width types for necessary inter-exe support (ex sending packets on a 32bit client to 64bit server)
typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;
typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8 byte;
typedef uint16 dbyte;
typedef uint32 qbyte;
typedef uint64 obyte;

typedef dbyte word;
typedef qbyte dword;
typedef obyte qword;

typedef uint8 uchar;
typedef uint16 ushort;
typedef uint32 uint;

// please only use these bools for packets..
typedef uint8 bool8;
typedef uint16 bool16;
typedef uint32 bool32;

typedef intptr_t intptr;
typedef uintptr_t uintptr;

typedef __time32_t time32;
typedef __time64_t time64;

typedef uint32 tick32;
typedef uint64 tick64;

tick32 GetTick32()
{
#if defined(__WINDOWS__)
	return GetTickCount();
#elif defined(ENABLE_RDTSC)
	return (tick32)((_rdtsc() - RDTSC_BEGINTICK) / RDTSC_CLOCK);
#elif defined(HAVE_MONOTONIC_CLOCK)
	struct timespec tval;
	clock_gettime(CLOCK_MONOTONIC, &tval);
	return tval.tv_sec * 1000 + tval.tv_nsec / 1000000;
#else
	struct timeval tval;
	gettimeofday(&tval, NULL);
	return tval.tv_sec * 1000 + tval.tv_usec / 1000;
#endif
}

tick64 GetTick64()
{
#if defined(__WINDOWS__)
	return GetTickCount64();
#elif defined(ENABLE_RDTSC)
	return (tick64)((_rdtsc() - RDTSC_BEGINTICK) / RDTSC_CLOCK);
#elif defined(HAVE_MONOTONIC_CLOCK)
	struct timespec tval;
	clock_gettime(CLOCK_MONOTONIC, &tval);
	return tval.tv_sec * 1000 + tval.tv_nsec / 1000000;
#else
	struct timeval tval;
	gettimeofday(&tval, NULL);
	return tval.tv_sec * 1000 + tval.tv_usec / 1000;
#endif
}

#define TICK64
#if defined(TICK64)
typedef tick64 tick;
tick GetTick() { return GetTick64(); }
#else
typedef tick32 tick;
tick GetTick() { return GetTick32(); };
#endif

#if defined(__WINDOWS__)
#define DIR_SEPARATOR "\\"
#define DIR_SEPARATOR_CHAR '\\'
#define PATH_SEPARATOR ";"
#define PATH_SEPARATOR_CHAR ';'
#else
#define DIR_SEPARATOR "/"
#define PATH_SEPARATOR ":"
#define DIR_SEPARATOR_CHAR '/'
#define PATH_SEPARATOR_CHAR ';'
#endif

inline void NormalizePathString(std::string &path)
{
	std::replace(path.begin(), path.end(), '\\', DIR_SEPARATOR_CHAR);
}

#if defined(__WINDOWS__)
#if !defined(inline)
#define inline __inline
#endif
#define strtoull _strtoui64
#define strtoll _strtoi64
#define atoll _atoi64
#define srandom srand
#define TIMEZONE _timezone
#if (!defined(__MINGW32__))
typedef int mode_t;
#if (!defined(_SSIZE_T_DEFINED))
typedef intptr_t ssize_t;
#define _SSIZE_T_DEFINED
#endif
#endif
#if (!defined(PRId8))
#define PRId8 "d"
#endif
#if (!defined(PRId16))
#define PRId16 "d"
#endif
#if (!defined(PRId32))
#define PRID32 "d"
#endif
#if (!defined(PRId64))
#define PRId64 "I64d"
#endif
#if (!defined(PRIu8))
#define PRIu8 "u"
#endif
#if (!defined(PRIu16))
#define PRIu16 "u"
#endif
#if (!defined(PRIu32))
#define PRIu32 "u"
#endif
#if (!defined(PRIu64))
#define PRIu64 "I64u"
#endif
#if (!defined(va_copy))
#define va_copy(dest, src) memcpy(dest, src, sizeof(src))
#endif
#endif

// newer gnuc does support #pragma pack, but we define this stuff here anyways
#ifdef ALIGNED
#undef ALIGNED
#endif
#if defined(_MSC_VER)
#define ALIGNED(x) __declspec(align(x))
#define PACKED
#elif defined(__GNUC__)
#define ALIGNED(x) __attribute__((aligned(x)))
#define PACKED __attribute__((packed))
#else
#define ALIGNED(x)
#define PACKED
#endif

#if (defined(_MSC_VER))
#define strcasecmp std::stricmp
#define strncasecmp std::strnicmp
#define strncmpi strncasecmp
#if (_MSC_VER < 1900)
#define snprintf _snprintf
#define vsnprintf _vsnprintf
#elif (_MSC_VER < 1400)
#define vsnprintf vsnprintf
#else
#define snprintf _snprintf_s
#define vsnprintf _vsnprintf_s
#endif
#else
#define strcmpi std::strcasecmp
#define stricmp std::strcasecmp
#define strncmpi std::strncasecmp
#define strnicmp std::strncasecmp
#endif

//	GCC supports validating format strings for functions that act like printf
#if defined(__GNUC__) && (__GNUC__ >= 2)
#define CHECK_PRINTF(a) __attribute__((format(printf, a, a + 1)))
#else
#define CHECK_PRINTF(a)
#endif

//	Lets us write code that compiles both on Windows and normal platforms
#if !defined(__WINDOWS__)
typedef int SOCKET;
#define closesocket close
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#if !defined O_BINARY
#define O_BINARY 0
#endif
#endif

#if defined(HAVE_LINUX_WIRELESS_H)
#include <linux/wireless.h>
//	This would normally come from net/if.h
unsigned int if_nametoindex(const char *ifname);
char *if_indextoname(unsigned int ifindex, char *ifname);
//	32 on Linux, 256 on Windows, pick largest to avoid overflows
#define IF_NAMESIZE 256
#else
#if defined(HAVE_NET_IF_H)
#include <net/if.h>
#endif
#if defined(HAVE_NET_IF_MEDIA_H)
#include <net/if_media.h>
#endif
#endif

// May overrun the buffer : strcpy, sprintf
// Sometimes null - terminates : strncpy, _snprintf, swprintf, wcsncpy, lstrcpy
// Always null - terminates : snprintf, strlcpy
// to solve a problem with a c api, we must use a c++ template !!!
// but it may actually be needed here cuz game development is fun

template <size_t char_count>
void strcpy_safe(char (&output)[char_count], const char *const src)
{
	std::strncpy(output, src, char_count);
	output[char_count - 1] = '\0';
}

#define streq(s1, s2) (std::strcmp((s1), (s2)) == 0)
#define strneq(s1, s2) (std::strcmp((s1), (s2)))
#define nstreq(s1, s2, len) (std::strncmp((s1), (s2), (size_t)(len)) == 0)
#define nstrneq(s1, s2, len) (std::strncmp((s1), (s2), (size_t)(len)))
#define streq_i(s1, s2) (strcasecmp((s1), (s2)) == 0)
#define strneq_i(s1, s2) (strcasecmp((s1), (s2)))
#define nstreq_i(s1, s2, len) (strncasecmp((s1), (s2), (size_t)(len)) == 0)
#define nstrneq_i(s1, s2, len) (strncasecmp((s1), (s2), (size_t)(len)))

#define QUOTE(x) #x
#define EXPAND_AND_QUOTE(x) QUOTE(x)

#define ISALNUM(c) (std::isalnum((uchar)(c)))
#define ISALPHA(c) (std::isalpha((uchar)(c)))
#define ISCNTRL(c) (std::iscntrl((uchar)(c)))
#define ISDIGIT(c) (std::isdigit((uchar)(c)))
#define ISGRAPH(c) (std::isgraph((uchar)(c)))
#define ISLOWER(c) (std::islower((uchar)(c)))
#define ISPRINT(c) (std::isprint((uchar)(c)))
#define ISPUNCT(c) (std::ispunct((uchar)(c)))
#define ISSPACE(c) (std::isspace((uchar)(c)))
#define ISUPPER(c) (std::isupper((uchar)(c)))
#define ISXDIGIT(c) (std::isxdigit((uchar)(c)))

#define TOASCII(c) (toascii((uchar)(c)))
#define TOLOWER(c) (std::tolower((uchar)(c)))
#define TOUPPER(c) (std::toupper((uchar)(c)))

#define ARRAYLENGTH(arr) (sizeof(arr) / sizeof(*arr))
#define OFFSETOF(type, member) ((size_t) & ((type *)0)->member)

//template <typename T> constexpr uint32 RGBA(T r, T g, T b, T a) { return ((uint32)((((a)&0xff)<<24)|(((b)&0xff)<<16)|(((g)&0xff)<<8)|((r)&0xff))); }

template <typename T>
void delete_ptr(T *&a)
{
	if (!a)
		return;
	delete a;
	a = nullptr;
}
