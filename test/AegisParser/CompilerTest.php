<?php

namespace AegisParser;

use PHPUnit\Framework\TestCase;

class CompilerTest extends TestCase
{

    public function testCmdGetSet() : void {
        $c = new Compiler();
        // SetCmd is private, ctor should set them all however
        self::assertSame(Cmd::BLOCKBREAK()->GetValue(), $c->GetCmdValue('__block'));
        self::assertSame(Cmd::END()->GetValue(), $c->GetCmdValue('end'));
        self::assertSame(Cmd::VAR()->GetValue(), $c->GetCmdValue('var'));
        self::assertSame(Cmd::IF()->GetValue(), $c->GetCmdValue('if'));
        self::assertSame(Cmd::ELSEIF()->GetValue(), $c->GetCmdValue('elseif'));
        self::assertSame(Cmd::ELSE()->GetValue(), $c->GetCmdValue('else'));
        self::assertSame(Cmd::ENDIF()->GetValue(), $c->GetCmdValue('endif'));
        self::assertSame(Cmd::DECLARE()->GetValue(), $c->GetCmdValue('declare'));
        self::assertSame(Cmd::DEFINE()->GetValue(), $c->GetCmdValue('define'));
        self::assertSame(Cmd::SWITCH()->GetValue(), $c->GetCmdValue('choose'));
        self::assertSame(Cmd::CASE()->GetValue(), $c->GetCmdValue('case'));
        self::assertSame(Cmd::BREAK()->GetValue(), $c->GetCmdValue('break'));
        self::assertSame(Cmd::DEFAULT()->GetValue(), $c->GetCmdValue('default'));
        self::assertSame(Cmd::ENDSWITCH()->GetValue(), $c->GetCmdValue('endchoose'));
        self::assertSame(Cmd::DEFCMD()->GetValue(), $c->GetCmdValue('defcmd'));
        self::assertSame(Cmd::WHILE()->GetValue(), $c->GetCmdValue('while'));
        self::assertSame(Cmd::ENDWHILE()->GetValue(), $c->GetCmdValue('endwhile'));
        self::assertSame(Cmd::EXITWHILE()->GetValue(), $c->GetCmdValue('exitwhile'));
    }

    public function testDefineGetSet() : void {
        $c = new Compiler();
        $defines = ["NV_BASIC" => 0, "SM_SWORD" => 1, "SM_TWOHAND" => 2, "SM_RECOVERY" => 3];

        foreach ($defines as $name => $skid) {
            $c->SetDefine($name, $skid);
        }
        foreach ($defines as $name => $skid) {
            self::assertSame($skid, $c->GetDefineValue($name));
        }
    }

    public function testLoadEnums() : void
    {
        $lines = [
            "20190314",
            "// ignored",
            "",
            "# 1",
            " OnStartHolding:",
            " OnStartEquip: // and ignored",
            "",
            "# 53",
            " REFINING_SUCCESS_EFFECT",
            " REFINING_FAIL_EFFECT",
            "# 6000",
            "MER_BEGIN, ",
            "MER_LIF, MER_AMISTR, MER_FILIR, MER_VANILMIRTH,",
            "MER_LIF2, MER_AMISTR2, MER_FILIR2, MER_VANILMIRTH2,",
            "# 0",
            "VAR_SPEED,",
            "VAR_EXP,",
            "VAR_JOBEXP,",
        ];
        $expected = ["OnStartHolding:" => 1, "OnStartEquip:" => 2, "REFINING_SUCCESS_EFFECT" => 53,
            "REFINING_FAIL_EFFECT" => 54,
            "MER_BEGIN" => 6000, "MER_LIF" => 6001, "MER_AMISTR" => 6002, "MER_LIF2" => 6005,
            "VAR_SPEED" => 0, "VAR_EXP" => 1, "VAR_JOBEXP" => 2
        ];
        $c = new Compiler();
        self::assertTrue($c->LoadEnums($lines, (int)$lines[0]));
        foreach ($expected as $k => $v) {
            self::assertSame($v, $c->GetDefineValue($k));
        }
    }

    public function testLoadDefs() : void {
        $data = ["NV_BASIC"=> 0, "SM_SWORD" => 1, "Skel_Prisoner_Card" => 4222, "Skel_Worker_Card" => 4092];
        $lines = [];
        foreach ($data as $str => $val) {
            $lines[] = $str.' '.$val;
        }


        $c = new Compiler();
        self::assertTrue($c->LoadDefs($lines));

        foreach ($data as $str => $val) {
            self::assertEquals($val, $c->GetDefineValue($str));
        }

    }


}
