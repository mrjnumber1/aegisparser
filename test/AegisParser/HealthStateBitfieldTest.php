<?php

namespace AegisParser;

use PHPUnit\Framework\TestCase;

class HealthStateBitfieldTest extends TestCase
{
    public function testBasic() : void {
        $f = new HealthStateBitfield();
        self::assertFalse($f->HasFlag(HealthStateFlag::POISON()));
        self::assertFalse($f->HasFlag(HealthStateFlag::ANGELUS()));
        self::assertFalse($f->HasFlag(HealthStateFlag::FEAR()));

        $f->FromNum(HealthStateFlag::POISON()->getValue());
        self::assertTrue($f->HasFlag(HealthStateFlag::POISON()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::CURSE()));
        self::assertCount(1, $f->GetFlags());

        $f->FromNum(HealthStateFlag::CURSE()->getValue());
        self::assertNotTrue($f->HasFlag(HealthStateFlag::POISON()));
        self::assertTrue($f->HasFlag(HealthStateFlag::CURSE()));

        $flags = (int)(HealthStateFlag::CURSE()->getValue() | HealthStateFlag::FEAR()->getValue());
        $f->FromNum($flags);
        self::assertNotTrue($f->HasFlag(HealthStateFlag::POISON()));
        self::assertTrue($f->HasFlag(HealthStateFlag::CURSE()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::SILENCE()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::CONFUSION()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::BLIND()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::ANGELUS()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::BLOODING()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::HEAVYPOISON()));
        self::assertTrue($f->HasFlag(HealthStateFlag::FEAR()));
        self::assertCount(2, $f->GetFlags());

        $f->AddFlag(HealthStateFlag::BLIND());
        self::assertNotTrue($f->HasFlag(HealthStateFlag::POISON()));
        self::assertTrue($f->HasFlag(HealthStateFlag::CURSE()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::SILENCE()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::CONFUSION()));
        self::assertTrue($f->HasFlag(HealthStateFlag::BLIND()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::ANGELUS()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::BLOODING()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::HEAVYPOISON()));
        self::assertTrue($f->HasFlag(HealthStateFlag::FEAR()));
        self::assertCount(3, $f->GetFlags());

        $f->ToggleFlag(HealthStateFlag::HEAVYPOISON());
        self::assertNotTrue($f->HasFlag(HealthStateFlag::POISON()));
        self::assertTrue($f->HasFlag(HealthStateFlag::CURSE()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::SILENCE()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::CONFUSION()));
        self::assertTrue($f->HasFlag(HealthStateFlag::BLIND()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::ANGELUS()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::BLOODING()));
        self::assertTrue($f->HasFlag(HealthStateFlag::HEAVYPOISON()));
        self::assertTrue($f->HasFlag(HealthStateFlag::FEAR()));
        self::assertCount(4, $f->GetFlags());

        $f->ClearFlags();
        self::assertNotTrue($f->HasFlag(HealthStateFlag::POISON()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::CURSE()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::SILENCE()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::CONFUSION()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::BLIND()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::ANGELUS()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::BLOODING()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::HEAVYPOISON()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::FEAR()));
        self::assertCount(0, $f->GetFlags());

        $f->AddFlag(HealthStateFlag::POISON());
        self::assertTrue($f->HasFlag(HealthStateFlag::POISON()));

        $f->FromNum(0);
        self::assertNotTrue($f->HasFlag(HealthStateFlag::POISON()));

        $flags = [HealthStateFlag::POISON(), HealthStateFlag::ANGELUS(), HealthStateFlag::FEAR()];
        $f->SetFlags($flags);
        self::assertTrue($f->HasFlag(HealthStateFlag::POISON()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::CURSE()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::BLIND()));
        self::assertTrue($f->HasFlag(HealthStateFlag::ANGELUS()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::BLOODING()));
        self::assertNotTrue($f->HasFlag(HealthStateFlag::HEAVYPOISON()));
        self::assertTrue($f->HasFlag(HealthStateFlag::FEAR()));
        self::assertCount(count($flags), $f->GetFlags());




    }
}
