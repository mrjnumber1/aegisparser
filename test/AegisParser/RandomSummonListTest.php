<?php

namespace AegisParser;

use PHPUnit\Framework\TestCase;

class RandomSummonListTest extends TestCase
{
    public function testItemIDs() : void
    {
        $l = new RandomSummonList(ItemID::Old_Card_Album(), ItemID::class);
        self::assertEquals(ItemID::Old_Card_Album(), $l->GetITID());
        self::assertEquals(0, $l->GetTotalEntryCount());
        self::assertEquals(0, $l->GetUniqueEntryCount());


        $l->AddEntry(ItemID::Poring_Card(), 3);
        self::assertEquals(3, $l->GetTotalEntryCount());
        self::assertEquals(1, $l->GetUniqueEntryCount());
        self::assertEquals(ItemID::Poring_Card(), $l->GetRandomEntry());
        for ($i=0; $i < $l->GetTotalEntryCount(); ++$i) {
            self::assertEquals(ItemID::Poring_Card(), $l->GetRandomEntry($i));
        }
        self::assertEquals(3, $l->GetEntryCount(ItemID::Poring_Card()));
        self::assertEquals(100, (int)$l->GetEntryPercent(ItemID::Poring_Card()));

        $l->AddEntry(ItemID::Drops_Card(), 3);
        self::assertEquals(6, $l->GetTotalEntryCount());
        self::assertEquals(2, $l->GetUniqueEntryCount());

        self::assertEquals(50, (int)$l->GetEntryPercent(ItemID::Poring_Card()));
        self::assertEquals(50, (int)$l->GetEntryPercent(ItemID::Drops_Card()));

        for ($i=0; $i < $l->GetTotalEntryCount(); ++$i) {
            if ($i < 3) {
                self::assertEquals(ItemID::Poring_Card(), $l->GetRandomEntry($i));
            } else {
                self::assertEquals(ItemID::Drops_Card(), $l->GetRandomEntry($i));
            }
        }

        $l->AddEntry(ItemID::Pupa_Card(), 1);
        $l->AddEntry(ItemID::Randgris_Card(), 1);
        $l->AddEntry(ItemID::Ghostring_Card(), 1);
        $l->AddEntry(ItemID::Hydra_Card(), 1);
        $l->AddEntry(ItemID::Poporing_Card(), 1);
        self::assertEquals(11, $l->GetTotalEntryCount());
        self::assertEquals(7, $l->GetUniqueEntryCount());

        self::assertLessThan(50, (int)$l->GetEntryPercent(ItemID::Poring_Card()));
        self::assertLessThan(50, (int)$l->GetEntryPercent(ItemID::Drops_Card()));

        for ($i=0; $i < $l->GetTotalEntryCount(); ++$i) {
            if ($i < 3) {
                self::assertEquals(ItemID::Poring_Card(), $l->GetRandomEntry($i));
            } else if ($i < 6) {
                self::assertEquals(ItemID::Drops_Card(), $l->GetRandomEntry($i));
            } else if ($i === 6) {
                self::assertEquals(ItemID::Pupa_Card(), $l->GetRandomEntry($i));
            } else if ($i === 7) {
                self::assertEquals(ItemID::Randgris_Card(), $l->GetRandomEntry($i));
            }else if ($i === 8) {
                self::assertEquals(ItemID::Ghostring_Card(), $l->GetRandomEntry($i));
            }else if ($i === 9) {
                self::assertEquals(ItemID::Hydra_Card(), $l->GetRandomEntry($i));
            }else if ($i === 10) {
                self::assertEquals(ItemID::Poporing_Card(), $l->GetRandomEntry($i));
            }
        }




    }
    public function testNonItemIDEntries() : void {
        $db = new RandomSummonList(ItemID::Branch_Of_Dead_Tree(), Jobtype::class);
        $db->AddEntry(JobType::WILOW(), 5);

        self::assertEquals(ItemID::Branch_Of_Dead_Tree(), $db->GetITID());
        self::assertEquals(5, $db->GetTotalEntryCount());
        self::assertEquals(1, $db->GetUniqueEntryCount());
        self::assertEquals(JobType::WILOW(), $db->GetRandomEntry());

        $db->AddEntry(JobType::LOLI_RURI(), 1);
        $db->AddEntry(JobType::OWL_DUKE(), 1);
        $db->AddEntry(JobType::PORCELLIO(), 1);
        $db->AddEntry(JobType::TEDDY_BEAR(), 1);
        $db->AddEntry(JobType::HILL_WIND(), 1);

        self::assertEquals(JobType::HILL_WIND(), $db->GetRandomEntry($db->GetTotalEntryCount()-1));
        self::assertEquals(10, $db->GetTotalEntryCount());
        self::assertEquals(6, $db->GetUniqueEntryCount());
    }

}
