<?php

namespace AegisParser;

use PHPUnit\Framework\TestCase;

class EnumBitfieldTest extends TestCase
{

    public function testConstruct() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $flag = MobAttrFlag::MVP();

        $bf->SetFlag($flag);

        self::assertEquals($flag->getValue(), $bf->GetNum());

    }

    // we disable this test because it's coded to assert and pass throw exception info up the stack
    /*
    public function testPassDifferentEnum() : void {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $flag = HealthStateFlag::BLIND();


        $bf->SetFlag($flag);
    }
    */

    public function testRemoveFlag() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $flag = MobAttrFlag::MVP();

        $bf->SetFlag($flag);
        self::assertEquals($flag->getValue(), $bf->GetNum());

        $bf->RemoveFlag($flag);
        self::assertEquals(0, $bf->GetNum());

    }

    public function testFromNum() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $value = MobAttrFlag::DAMAGE_REDUCTION_10()->getValue() | MobAttrFlag::MUST_DAMAGE_ONE_SPELLATK()->getValue();

        $bf->FromNum($value);
        self::assertEquals($value, $bf->GetNum());
        self::assertCount(2, $bf->GetFlags());
    }

    public function testAddFlags() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $flags = [MobAttrFlag::MUST_DAMAGE_ONE_SPELLATK(), MobAttrFlag::TELEPORT_BLOCK()];
        $count = count($flags);

        $bf->AddFlags($flags);
        self::assertCount($count, $bf->GetFlags());

        $bf->AddFlags([MobAttrFlag::DAMAGE_REDUCTION_1000()]);
        $count++;
        self::assertCount($count, $bf->GetFlags());

    }

    public function testClearFlags() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $flag = MobAttrFlag::MVP();
        $bf->SetFlag($flag);

        self::assertcount(1, $bf->GetFlags());
        $bf->ClearFlags();
        self::assertEquals(0, $bf->GetNum());
        self::assertCount(0, $bf->GetFlags());

        $flags = [MobAttrFlag::TELEPORT_BLOCK(), MobAttrFlag::MUST_DAMAGE_ONE_SPELLATK(), MobAttrFlag::MUST_DAMAGE_ONE_SHORT_RNG_PHYSICAL_ATK()];
        $count = count($flags);

        $bf->AddFlags($flags);
        self::assertCount($count, $bf->GetFlags());

        $bf->ClearFlags();
        self::assertCount(0, $bf->GetFlags());

    }

    public function testGetFlags() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $flags = [MobAttrFlag::TELEPORT_BLOCK(), MobAttrFlag::MUST_DAMAGE_ONE_SPELLATK(), MobAttrFlag::MUST_DAMAGE_ONE_SHORT_RNG_PHYSICAL_ATK()];
        sort($flags);
        $num = 0;
        foreach ($flags as $flag) {
            $num |= $flag->getValue();
        }
        $count = count($flags);

        $bf->AddFlags($flags);
        self::assertCount($count, $bf->GetFlags());
        self::assertEquals($num, $bf->GetNum());
        self::assertEquals($flags, $bf->GetFlags());
    }

    public function testEquals() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $other = new EnumBitfield(MobAttrFlag::class);
        $flag = MobAttrFlag::MVP();
        $bf->SetFlag($flag);
        $other->SetFlag($flag);

        self::assertTrue($bf->EqualsFlag($flag));
        self::assertTrue($bf->Equals($other));
    }

    public function testEqualsFlag() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $flags = [MobAttrFlag::TELEPORT_BLOCK(), MobAttrFlag::REFLECT_BLOCK()];

        $bf->SetFlag($flags[0]);
        self::assertTrue($bf->EqualsFlag($flags[0]));


        $bf->SetFlags($flags);
        self::assertFalse($bf->EqualsFlag($flags[0]));
    }

    public function testToggleFlag() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $flags = [MobAttrFlag::REFLECT_BLOCK(), MobAttrFlag::DAMAGE_REDUCTION_10()];
        $num = 0;
        foreach ($flags as $flag) {
            $num |= $flag->getValue();
        }
        $count = count($flags);
        $bf->SetFlags($flags);
        self::assertEquals($num, $bf->GetNum());

        $num ^= $flags[0]->getValue();
        $bf->ToggleFlag($flags[0]);
        self::assertEquals($num, $bf->GetNum());

        $num ^= $flags[0]->getValue();
        $bf->ToggleFlag($flags[0]);
        self::assertEquals($num, $bf->GetNum());
    }

    public function testRemoveFlags() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $flags = [MobAttrFlag::MUST_DAMAGE_ONE_SPELLATK(), MobAttrFlag::REFLECT_BLOCK(), MobAttrFlag::TELEPORT_BLOCK()];
        $num = 0;
        foreach ($flags as $flag) {
            $num |= $flag->getValue();
        }
        $count = count($flags);

        $bf->SetFlags($flags);
        self::assertEquals($num, $bf->GetNum());

        $bf->RemoveFlag($flags[0]);
        self::assertCount($count-1, $bf->GetFlags());
        $num ^= $flags[0]->getValue();
        self::assertEquals($num, $bf->GetNum());

    }

    public function testHasFlag() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $flags = [MobAttrFlag::MVP(), MobAttrFlag::ITEMDROP_BY_OPTION_BLOCK(), MobAttrFlag::DAMAGE_REDUCTION_1000()];
        $bf->SetFlags($flags);

        foreach ($flags as $flag) {
            self::assertTrue($bf->HasFlag($flag));
        }

        self::assertFalse($bf->HasFlag(MobAttrFlag::REFLECT_BLOCK()));
    }

    public function testContains() : void
    {
        $bf = new EnumBitfield(MobAttrFlag::class);
        $flags = [MobAttrFlag::TELEPORT_BLOCK(), MobAttrFlag::REFLECT_BLOCK(), MobAttrFlag::DAMAGE_REDUCTION_1000()];
        $bf->SetFlags($flags);
        $other = new EnumBitfield(MobAttrFlag::class);
        $other->SetFlag($flags[0]);

        self::assertTrue($bf->Contains($other));
        self::assertFalse($other->Contains($bf));
    }
}
