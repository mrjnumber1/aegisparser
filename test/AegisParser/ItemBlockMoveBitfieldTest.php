<?php

namespace AegisParser;

use PHPUnit\Framework\TestCase;

class ItemBlockMoveBitfieldTest extends TestCase
{

    public function testClass() : void {
        $b = new ItemBlockMoveBitfield(0);
        self::assertNotTrue($b->IsBlockDrop());
        self::assertNotTrue($b->IsBlockTrade());
        self::assertNotTrue($b->IsBlockStorage());
        self::assertNotTrue($b->IsBlockCart());
        self::assertNotTrue($b->IsBlockSellNPC());
        self::assertNotTrue($b->IsBlockMail());
        self::assertNotTrue($b->IsBlockAuction());
        self::assertNotTrue($b->IsBlockGuildStorage());
    }

    public function testSetFromItemMoveInfoString() : void {
        $b = new ItemBlockMoveBitfield(0);
        $txt = "5356	0	0	0	0	0	1	1";
        $txt_trimmed = "0 0 0 0 0 1 1";
        $b->SetFromItemMoveInfo($txt);
        self::assertNotTrue($b->IsBlockDrop());
        self::assertNotTrue($b->IsBlockTrade());
        self::assertNotTrue($b->IsBlockStorage());
        self::assertNotTrue($b->IsBlockCart());
        self::assertNotTrue($b->IsBlockSellNPC());
        self::assertTrue($b->IsBlockMail());
        self::assertTrue($b->IsBlockAuction());
        self::assertNotTrue($b->IsBlockGuildStorage());
        self::assertSame($txt_trimmed, $b->GetItemMoveInfoString(7));
        self::assertSame($txt_trimmed.' 0', $b->GetItemMoveInfoString(8));
    }

    public function testSetFromMuddiedItemMoveInfoString() : void {
        $b = new ItemBlockMoveBitfield(0);
        $txt = "1173	1	1	0	1	1	1	1	// Muramasa_C";
        $txt_trimmed = "1 1 0 1 1 1 1";
        $b->SetFromItemMoveInfo($txt);
        self::assertTrue($b->IsBlockDrop());
        self::assertTrue($b->IsBlockTrade());
        self::assertNotTrue($b->IsBlockStorage());
        self::assertTrue($b->IsBlockCart());
        self::assertTrue($b->IsBlockSellNPC());
        self::assertTrue($b->IsBlockMail());
        self::assertTrue($b->IsBlockAuction());
        self::assertNotTrue($b->IsBlockGuildStorage());
        self::assertSame($txt_trimmed, $b->GetItemMoveInfoString(7));
    }

    public function testFromIMIStringCommentAtFinalChar() : void {
        $b = new ItemBlockMoveBitfield(0);
        $txt = "18763	1	0	0	0	1	1	1	1//Remodel_Joker_Jester";
        $txt_trimmed = "1 0 0 0 1 1 1 1";
        $b->SetFromItemMoveInfo($txt);
        self::assertTrue($b->IsBlockDrop());
        self::assertNotTrue($b->IsBlockTrade());
        self::assertNotTrue($b->IsBlockStorage());
        self::assertNotTrue($b->IsBlockCart());
        self::assertTrue($b->IsBlockSellNPC());
        self::assertTrue($b->IsBlockMail());
        self::assertTrue($b->IsBlockAuction());
        self::assertTrue($b->IsBlockGuildStorage());
        self::assertSame($txt_trimmed, $b->GetItemMoveInfoString(8));
    }

    public function testFromIMIStringCommentZeroAtFinalChar() : void {
        $b = new ItemBlockMoveBitfield(0);
        // The same as before, but the last param is 0
        $txt = "18763	1	0	0	0	1	1	1	0//Remodel_Joker_Jester";
        $txt_trimmed = "1 0 0 0 1 1 1 0";

        $b->SetFromItemMoveInfo($txt);
        self::assertTrue($b->IsBlockDrop());
        self::assertNotTrue($b->IsBlockTrade());
        self::assertNotTrue($b->IsBlockStorage());
        self::assertNotTrue($b->IsBlockCart());
        self::assertTrue($b->IsBlockSellNPC());
        self::assertTrue($b->IsBlockMail());
        self::assertTrue($b->IsBlockAuction());
        self::assertNotTrue($b->IsBlockGuildStorage());
        self::assertSame($txt_trimmed, $b->GetItemMoveInfoString(8));
    }

    public function testFromIMIMultiByteString() : void {
        $b = new ItemBlockMoveBitfield(0);
        // now add multibyte characters
        $txt = "617	0	0	0	0	1	0	0	0	// 오래된보라상자";
        $txt_trimmed = "0 0 0 0 1 0 0 0";
        $b->SetFromItemMoveInfo($txt);
        self::assertNotTrue($b->IsBlockDrop());
        self::assertNotTrue($b->IsBlockTrade());
        self::assertNotTrue($b->IsBlockStorage());
        self::assertNotTrue($b->IsBlockCart());
        self::assertTrue($b->IsBlockSellNPC());
        self::assertNotTrue($b->IsBlockMail());
        self::assertNotTrue($b->IsBlockAuction());
        self::assertNotTrue($b->IsBlockGuildStorage());
        self::assertSame($txt_trimmed, $b->GetItemMoveInfoString(8));
    }

    public function testSetFromNum(): void {
        $b = new ItemBlockMoveBitfield(0);
        $b->SetFromNum(0x14);
        self::assertNotTrue($b->IsBlockDrop());
        self::assertNotTrue($b->IsBlockTrade());
        self::assertTrue($b->IsBlockStorage());
        self::assertNotTrue($b->IsBlockCart());
        self::assertTrue($b->IsBlockSellNPC());
        self::assertNotTrue($b->IsBlockMail());
        self::assertNotTrue($b->IsBlockAuction());
        self::assertNotTrue($b->IsBlockGuildStorage());
    }

    public function testIsMovableFlag() : void {
        $b = new ItemBlockMoveBitfield(NotMoveFlag::CART()->getValue());
        self::assertNotTrue($b->IsBlockDrop());
        self::assertNotTrue($b->IsBlockTrade());
        self::assertNotTrue($b->IsBlockStorage());
        self::assertTrue($b->IsBlockCart());
        self::assertNotTrue($b->IsBlockSellNPC());
        self::assertNotTrue($b->IsBlockMail());
        self::assertNotTrue($b->IsBlockAuction());
        self::assertNotTrue($b->IsBlockGuildStorage());

        self::assertNotTrue($b->IsFlagged(NotMoveFlag::DROP()));
        self::assertNotTrue($b->IsFlagged(NotMoveFlag::EXCHANGE()));
        self::assertNotTrue($b->IsFlagged(NotMoveFlag::STORE()));
        self::assertTrue($b->IsFlagged(NotMoveFlag::CART()));
        self::assertNotTrue($b->IsFlagged(NotMoveFlag::SELL_NPC()));
        self::assertNotTrue($b->IsFlagged(NotMoveFlag::MAIL()));
        self::assertNotTrue($b->IsFlagged(NotMoveFlag::AUCTION()));
        self::assertNotTrue($b->IsFlagged(NotMoveFlag::GUILD_STORE()));
    }
}
