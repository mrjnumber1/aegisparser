<?php

namespace AegisParser;

use PHPUnit\Framework\TestCase;

class EquipPosBitfieldTest extends TestCase
{

    public function testFromNum()
    {
        $l = new EquipPosBitfield([EquipPosFlag::RARM()]);

        self::assertEquals(EquipPosFlag::RARM()->getValue(), $l->GetPosNum());
        self::assertCount(1, $l->GetFlags());
        self::assertEquals([EquipPosFlag::RARM()], $l->GetFlags());
        self::assertEquals(EquipPosFlag::RARM(), $l->GetFlags()[0]);
        self::assertTrue($l->IsWeaponPos());
        self::assertNotTrue($l->IsArmorPos());
        self::assertNotTrue($l->IsShadowPos());
        self::assertNotTrue($l->IsTwoHandedPos());

        $l->SetFlags([EquipPosFlag::RARM(), EquipPosFlag::LARM()]);
        self::assertTrue($l->IsWeaponPos());
        self::assertTrue($l->IsTwoHandedPos());

        $num = (EquipPosFlag::ACCESSORY2()->getValue() | EquipPosFlag::ACCESSORY1()->getValue());
        $l->FromNum($num);
        self::assertEquals($num, $l->GetPosNum());
        self::assertCount(2, $l->GetFlags());
        self::assertTrue($l->HasFlag(EquipPosFlag::ACCESSORY1()));
        self::assertTrue($l->HasFlag(EquipPosFlag::ACCESSORY2()));
        self::assertNotTrue($l->EqualsFlag(EquipPosFlag::ACCESSORY1()));
        self::assertNotTrue($l->EqualsFlag(EquipPosFlag::ACCESSORY2()));
        self::assertTrue($l->IsAccessoryPos());
        self::assertNotTrue($l->IsSingleAccessoryPos());

        $num = (EquipPosFlag::HEAD()->getValue() | EquipPosFlag::HEAD2()->getValue() |EquipPosFlag::HEAD3()->getValue());
        $l->FromNum($num);
        self::assertEquals($num, $l->GetPosNum());
        self::assertCount(3, $l->GetFlags());
        self::assertTrue($l->IsHeadPos());
        self::assertNotTrue($l->IsSingleHeadPos());
        self::assertNotTrue($l->IsCostumeHeadPos());

        $l->SetFlag(EquipPosFlag::COSTUME_HEAD());
        self::assertEquals(EquipPosFlag::COSTUME_HEAD()->getValue(), $l->GetPosNum());
        self::assertCount(1, $l->GetFlags());
        self::assertTrue($l->IsCostumePos());
        self::assertTrue($l->IsCostumeHeadPos());
    }

    public function testPosCompare() : void {
        $a = new EquipPosBitfield([EquipPosFlag::RARM()]);
        $other = new EquipPosBitfield($a->GetFlags());

        self::assertTrue($a->Equals($other));
        self::assertTrue($other->Equals($a));
        self::assertTrue($a->Contains($other));
        self::assertTrue($other->Contains($a));

        $other->AddFlag(EquipPosFlag::LARM());
        self::assertFalse($a->Equals($other));
        self::assertFalse($other->Equals($a));
        self::assertTrue($a->Contains($other));
        self::assertTrue($other->Contains($a));

    }
}
