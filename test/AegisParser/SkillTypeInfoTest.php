<?php

namespace AegisParser;

use PHPUnit\Framework\TestCase;

class SkillTypeInfoTest extends TestCase
{
    public function testPassive() : void {
        $sk = new SkillTypeInfo(SkillID::NV_BASIC(), 9);
        self::assertEquals(SkillTargetType::PASSIVE(), $sk->GetTargetType());

        $sk = new SkillTypeInfo(SkillID::SM_SWORD(), 10);
        $sk->SetRequiredWeapon([WeaponType::SWORD(), WeaponType::SHORTSWORD(), WeaponType::TWOHANDSWORD()]);
        $sk->SetExtraData([range(4, 40, 4)]);

        self::assertTrue($sk->IsRequiredWeapon(WeaponType::SHORTSWORD()));
        self::assertTrue($sk->IsRequiredWeapon(WeaponType::SWORD()));
        self::assertTrue($sk->IsRequiredWeapon(WeaponType::TWOHANDSWORD()));
        self::assertFalse($sk->IsRequiredWeapon(WeaponType::SPEAR()));
        for ($i = 0; $i < SkillTypeInfo::MAX_LEVEL; ++$i) {
            self::assertEquals(4 * ($i+1), $sk->GetExtraDataAt(0, $i+1));
        }

        $sk = new SkillTypeInfo(SkillID::SM_TWOHAND(), 10);
        $sk->SetRequiredWeapon([ WeaponType::TWOHANDSWORD()]);
        $sk->SetExtraData([range(4, 40, 4)]);
        self::assertFalse($sk->IsRequiredWeapon(WeaponType::SHORTSWORD()));
        self::assertFalse($sk->IsRequiredWeapon(WeaponType::SWORD()));
        self::assertTrue($sk->IsRequiredWeapon(WeaponType::TWOHANDSWORD()));
        self::assertFalse($sk->IsRequiredWeapon(WeaponType::SPEAR()));
        for ($i = 0; $i < SkillTypeInfo::MAX_LEVEL; ++$i) {
            self::assertEquals(4 * ($i+1), $sk->GetExtraDataAt(0, $i+1));
        }

        $sk = new SkillTypeInfo(SkillID::SM_RECOVERY(), 10);
        $sk->SetExtraData([range(10, 100, 10), range(2, 20, 2), range(5, 50, 5)]);
        for ($i = 0; $i < SkillTypeInfo::MAX_LEVEL; ++$i) {
            self::assertEquals(10 * ($i+1), $sk->GetExtraDataAt(0, $i+1));
            self::assertEquals(2 * ($i+1), $sk->GetExtraDataAt(1, $i+1));
            self::assertEquals(5 * ($i+1), $sk->GetExtraDataAt(2, $i+1));
        }
    }
    public function testActiveSkills() : void {
        $sk = new SkillTypeInfo(SkillID::SM_BASH(), 10);
        $sk->SetRange([1]);
        $sk->SetTargetType(SkillTargetType::TOCHARACTER());
        $sk->SetPattern(SkillPattern::DIRECTATTACK());
        $sk->SetAtkPercent(range(130, 400, 30));
        $sk->SetHitPercent(range(105, 150, 5));
        // this info would be under fatal blow, but it's here for testing
        $sk->SetHandicap(Condition::BODY_STUN(), [0, 0, 0, 0, 0, 50, 100, 150, 200, 250]);
        $sk->SetSPCost([8, 8, 8, 8, 8, 15, 15, 15, 15, 15]);

        for ($i=0; $i < $sk->GetMaxLevel(); ++$i) {
            $lv = $i+1;
            self::assertEquals(SkillTargetType::TOCHARACTER(), $sk->GetTargetType());
            self::assertEquals(1, $sk->GetRangeAt($lv));
            self::assertEquals(SkillPattern::DIRECTATTACK(), $sk->GetPattern());
            self::assertEquals(100+30*$lv, $sk->GetAtkPercentAt($lv));
            self::assertEquals(100+5*$lv, $sk->GetHitPercentAt($lv));
            self::assertEquals(Condition::BODY_STUN(), $sk->GetHandicapType());
            if ($lv < 6) {
                self::assertEquals(8, $sk->GetSPCostAt($lv));
            } else {
                self::assertEquals(15, $sk->GetSPCostAt($lv));
                self::assertEquals(50 *($lv-5), $sk->GetHandicapRateAt($lv));
            }
        }
    }

    public function testMultiExtraData() : void {
        $sk = new SkillTypeInfo(SkillID::SM_PROVOKE(), 10);
        $sk->SetSPCost([10]);
        $sk->SetRange([9]);
        $sk->SetTargetType(SkillTargetType::TOCHARACTER());
        $sk->SetExtraData([
            range(73, 100, 3), // base success rate
            range(105, 132, 3), // atk%
            range(90, 45, -5), // def%
            [30000] // time
        ]);

        /*
         * $sk->SetEFST(EFST::PROVOKE(), [30000], range(105, 132, 3), range(90, 45, -5))
         */

        self::assertequals(SkillTargetType::TOCHARACTER(), $sk->GetTargetType());
        for ($i=0; $i < $sk->GetMaxLevel(); ++$i) {
            $lv = $i+1;
            self::assertEquals(10, $sk->GetSPCostAt($lv));
            self::assertEquals(9, $sk->GetRangeAt($lv));
            self::assertEquals(70+3*$lv, $sk->GetExtraDataAt(0, $lv));
            self::assertEquals(102+3*$lv, $sk->GetExtraDataAt(1, $lv));
            self::assertEquals(95-5*$lv, $sk->GetExtraDataAt(2, $lv));
            self::assertEquals(30*1000, $sk->GetExtraDataAt(3, $lv));
        }
    }

}
