<?php

namespace AegisParser;

use PHPUnit\Framework\TestCase;

class ScriptTest extends TestCase
{

    public function testClass() : void
    {
        $text = "a nice string with no quotes or comments";
        $s = new Script();
        $s->LoadText($text);
        self::assertEquals(1, $s->GetLineNum());
        self::assertSame($text, $s->GetLine(0));

        $s->Clear();
        $text = "this is fine //but this is all gone";
        $s->LoadText($text);
        self::assertEquals(1, $s->GetLineNum());
        self::assertSame("this is fine ", $s->GetLine(0));

        // false if loading blank text
        $s->Clear();
        self::assertFalse($s->LoadText(""));

        // true if loading fully commented text
        $s->Clear();
        $text = "// all commented out";
        self::assertTrue($s->LoadText($text));
        self::assertSame('', $s->GetLine(0));

        // Add new text on the next line
        $s->Clear();
        $text.= PHP_EOL."2nd line not commented";
        self::assertTrue($s->LoadText($text));
        self::assertEquals(2, $s->GetLineNum());
        self::assertSame('', $s->GetLine(0));
        self::assertSame("2nd line not commented", $s->GetLine(1));

        $s->Clear();
        $text = "/* onto the scary stuff */";
        self::assertTrue($s->LoadText($text));
        self::assertEquals(1, $s->GetLineNum());
        self::assertSame('', $s->GetLine(0));

        $s->Clear();
        $text = "appears /* does not appear */ also appears";
        self::assertTrue($s->LoadText($text));
        self::assertEquals(1, $s->GetLineNum());
        self::assertSame("appears  also appears", $s->GetLine(0));

        $s->Clear();
        $text = "multiline time /* please work".PHP_EOL."thanks */ dude";
        self::assertTrue($s->LoadText($text));
        self::assertEquals(2, $s->GetLineNum());
        self::assertSame("multiline time ", $s->GetLine(0));
        self::assertSame(" dude", $s->GetLine(1));

        $s->Clear();
        $text = 'now a "quoted /*comment for some reason*/" supported';
        self::assertTrue($s->LoadText($text));
        self::assertEquals(1, $s->GetLineNum());
        self::assertSame($text, $s->GetLine(0));

        // so, what to do on multi line quotes??
        // the ZoneServer handler just seems to silently disregard it ...
    }
}
