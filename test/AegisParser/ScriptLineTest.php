<?php

namespace AegisParser;

use PHPUnit\Framework\TestCase;

class ScriptLineTest extends TestCase
{

    public function testGetBaseMatchesInput() : void
    {
        $str = "define ENUM 0";
        $line = new ScriptLine($str);

        self::assertEquals($str, $line->GetBase());
    }


    public function testGetOperator() : void
    {
        $operators = "+-/*=";
        $str = "5 = 5";
        $line = new ScriptLine($str);

        $value = $line->GetOperator($operators);
        self::assertEquals('', $value);

        $line->Skip($str[0]);
        $line->Skip($str[1]);

        $value = $line->GetOperator($operators);
        self::assertSame('=', $value);

        // now let's try on a 2 char operator
        $line = new ScriptLine('++v');
        $value = $line->GetOperator($operators);
        self::assertEquals(2, strlen($value));
        self::assertSame('++', $value);

        // a 2 char operator with different chars
        $line = new ScriptLine('v += 5');
        $var = $line->GetWord(' ');
        self::assertSame('v', $var);
        $line->Skip(' ');

        $op = $line->GetOperator($operators);
        self::assertSame('+=', $op);
    }

    public function testSkip() : void
    {
        $str = " define ENUM 0";
        $whitespace = " \t\n";
        $line = new ScriptLine($str);

        $line->Skip($whitespace);
        self::assertEquals(ltrim($str), $line->GetCurrent());

        $value = $line->GetWord($whitespace);
        self::assertEquals("define", $value);

        $line->Skip($whitespace);

        $value = $line->GetWord($whitespace);
        self::assertEquals("ENUM", $value, 'Consecutive word failure');


        $line = new ScriptLine(substr($str, 1));
        $current = $line->GetCurrent();

        // skipping nothing should not alter the current at all
        $line->Skip($whitespace);
        self::assertEquals($current, $line->GetCurrent());
    }

    public function testGetParse() : void
    {
        $str = 'dialog "This is a line"';
        $line = new ScriptLine($str);

        $word = $line->GetWord(" ");
        self::assertSame('dialog', $word);

        $line->Skip(" ");
        [$parsed,] = $line->GetParse('"');
        self::assertSame('This is a line', $parsed);

        // now with a new separator later into the line
        $line = new ScriptLine("/just using other shit/ 'just bec/aus/e' ");
        [$parsed,] = $line->GetParse('/');
        self::assertSame('just using other shit', $parsed);

        $line->Skip(' ');
        [$parsed,] = $line->GetParse('\'');
        self::assertSame('just bec/aus/e', $parsed);

        // separators separated 1 char apart
        $line = new ScriptLine('"first str" "and second"');
        [$parsed,] = $line->GetParse('"');
        self::assertSame('first str', $parsed);

        // returns blank and does not advance the current
        [$parsed,] = $line->GetParse('"');
        self::assertSame('', $parsed);

        $line->Skip(' ');

        [$parsed,] = $line->GetParse('"');
        self::assertSame('and second', $parsed);


        // now there's consecutive separators
        $line = new ScriptLine('"please work""thanks"');
        [$parsed,] = $line->GetParse('"');
        self::assertSame('please work', $parsed);
        [$parsed,] = $line->GetParse('"');
        self::assertSame('thanks', $parsed);

    }

    public function testGetWord() : void
    {
        $str = "define ENUM 0";
        $words = explode(' ', $str);
        $line = new ScriptLine($str);

        foreach ($words as $expected)
        {
            // a pre-skip as well should be okay
            /** @noinspection DisconnectedForeachInstructionInspection */
            $line->Skip(' ');
            $word = $line->GetWord(' ');
            self::assertSame($expected, $word);
        }

        // Reading one too many should return a blank
        $word = $line->GetWord(' ');
        self::assertSame('', $word);
    }
}
