<?php

namespace AegisParser;

use PHPUnit\Framework\TestCase;

class ParsingTest extends TestCase
{

    public function testRun() : void
    {
        $good_str = 'regular ordinary string';

        $p = new Parsing();
        self::assertTrue($p->Run($good_str), "safe string returns true");
        self::assertSame($good_str, $p->Get());

        $p = new Parsing();
        $ugly_str = 'this has "unclosed quote';
        self::assertTrue($p->Run($ugly_str), "unclosed string returns true also for some reason");
        self::assertSame($ugly_str, $p->Get());

        $p = new Parsing();
        $closed_quote = 'now "closed quotes" for u';
        self::assertTrue($p->Run($closed_quote), "closed string returns true");
        self::assertSame($closed_quote, $p->Get(), "closed quotes are OK");

        $p = new Parsing();
        $front_quote = '"first" x';
        self::assertTrue($p->Run($front_quote));
        self::assertSame($front_quote, $p->Get());

        $p = new Parsing();
        $line = "if (v[VAR_STR] == 5)";
        self::assertTrue($p->Run($line));
        self::assertSame('if  $0', $p->Get());
        self::assertSame('$0=v[VAR_STR] == 5', $p->Get());
        self::assertSame('', $p->Get());

        $line = "if ((v[VAR_STR] == 20) && (v[VAR_AGI] == 10))";
        $p = new Parsing();
        self::assertTrue($p->Run($line));
        self::assertSame('if  $0', $p->Get());
        self::assertSame('$0= $1 &&  $2', $p->Get());
        self::assertSame('$2=v[VAR_AGI] == 10', $p->Get());
        self::assertSame('$1=v[VAR_STR] == 20', $p->Get());
        self::assertSame('', $p->Get());

    }
}
