<?php

namespace AegisParser;

use Ds\Pair;
use PHPUnit\Framework\TestCase;

class SkillSplashTest extends TestCase
{

    public function testSquare() : void {
        $s = new SkillSplash(SkillSplashType::SQUARE(), range(1, 5), 5);

        for ($i = 1; $i <= 5; ++$i) {
            self::assertEquals(new Pair($i, $i), $s->GetSplash($i));
        }

        // fill to the end with base value
        $s = new SkillSplash(SkillSplashType::SQUARE(), [1], 5);
        for ($i = 1; $i <= 5; ++$i) {
            self::assertEquals(new Pair(1, 1), $s->GetSplash($i));
        }
    }
    public function testLine() : void {
        $s = new SkillSplash(SkillSplashType::LINE(), [3, 14, 3, 14, 3, 14, 3, 14, 3, 14], 5);
        for ($i = 1; $i <= 5; ++$i) {
            self::assertEquals(new Pair(3, 14), $s->GetSplash($i));
        }

        // fill to the end with base pair value
        $s = new SkillSplash(SkillSplashType::LINE(), [3, 14], 5);
        for ($i = 1; $i <= 5; ++$i) {
            self::assertEquals(new Pair(3, 14), $s->GetSplash($i));
        }
    }
}
