<?php


namespace AegisParser;


final class SkillFlagBitfield
{
    public EnumBitfield $bf;
    public function __construct() {
        $this->bf = new EnumBitfield(SkillFlag::class);
    }
}