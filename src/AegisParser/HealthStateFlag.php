<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;


use MyCLabs\Enum\Enum;

/**
 * @method static HealthStateFlag NORMAL()
 * @method static HealthStateFlag POISON()
 * @method static HealthStateFlag CURSE()
 * @method static HealthStateFlag SILENCE()
 * @method static HealthStateFlag CONFUSION()
 * @method static HealthStateFlag BLIND()
 * @method static HealthStateFlag ANGELUS()
 * @method static HealthStateFlag BLOODING()
 * @method static HealthStateFlag HEAVYPOISON()
 * @method static HealthStateFlag FEAR()
 */
final class HealthStateFlag extends Enum
{
    private const NORMAL = 0;
    private const POISON = 1;
    private const CURSE = 2;
    private const SILENCE = 4;
    private const CONFUSION = 8;
    private const BLIND = 0x10;
    private const ANGELUS = 0x20;
    private const BLOODING = 0x40;
    private const HEAVYPOISON = 0x80;
    private const FEAR = 0x100;
}