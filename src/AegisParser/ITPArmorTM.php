<?php


namespace AegisParser;


final class ITPArmorTM extends ITPArmor
{
    public function __construct() { parent::__construct(); $this->SetType(ItemType::ARMORTM()); }
}