<?php


namespace AegisParser;


use MyCLabs\Enum\Enum;

/**
 * @method static SkillTargetType PASSIVE()
 * @method static SkillTargetType TOCHARACTER()
 * @method static SkillTargetType TOGROUND()
 * @method static SkillTargetType TOME()
 * @method static SkillTargetType TOITEM()
 * @method static SkillTargetType TOALL()
 * @method static SkillTargetType TOSKILL()
 */
final class SkillTargetType extends Enum
{
    private const PASSIVE = 0;
    private const TOCHARACTER = 1;
    private const TOGROUND = 2;
    private const TOME = 4;
    private const TOITEM = 8;
    private const TOALL = 0x10;
    private const TOSKILL = 0x20;
}