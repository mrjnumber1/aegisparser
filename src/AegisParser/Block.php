<?php

namespace AegisParser;

use Ds\Map;
use Ds\Stack;

final class Block
{
    private int $alloc;
    private int $pos;
    private int $step;
    private int $depth;
    private Stack $posStack;
    private Stack $stepStack;
    private Map $infoMap;

    public function __construct() {
        $this->alloc = $this->pos = $this->step = $this->depth = 0;
        $this->posStack = new Stack();
        $this->stepStack = new Stack();
        $this->infoMap = new Map();
    }
    public function IsComplete() : bool {
        return $this->depth === 0;
    }
    public function GetStrInfo() : string {
        return "index $this->pos-$this->step depth $this->depth";
    }

    public function GetStartId() : int {
        return $this->pos << 8;
    }

    public function GetCurId() : int {
        return $this->step | ($this->pos << 8);
    }

    public function GetNextId() : int {
        return ($this->step+1) | ($this->pos << 8);
    }

    public function GetEndId() : int {
        return ($this->pos << 8) | 0xFF;
    }

    public function GetInfo(int $id) : ?int {
        return $this->infoMap->get($id, null);
    }

    private function Label(int $id, int $info) : void {
        $this->infoMap->put($id, $info);
    }

    public function Start(int $info) : bool {
        if ($this->alloc >= 0xFFFFFF) {
            return false;
        }

        $this->stepStack->push($this->step);
        $this->posStack->push($this->pos);
        ++$this->depth;
        $this->pos = $this->alloc;
        ++$this->alloc;

        $this->step = 0;
        $this->Label($this->GetCurId(), $info);

        return true;
    }

    public function Link(int $info) : bool {
        if ($this->step >= 0xFF) {
            return false;
        }

        $this->step++;
        $this->Label($this->GetCurId(), $info);
        return true;
    }

    public function End(int $info) : bool {
        $this->Label($this->GetEndId(), $info);

        if (!$this->depth) {
            return false;
        }

        $this->step = $this->stepStack->pop();
        $this->pos = $this->posStack->pop();
        --$this->depth;
        return $this->depth >= 0;
    }
}