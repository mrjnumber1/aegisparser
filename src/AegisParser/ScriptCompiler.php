<?php


namespace AegisParser;

use Application\Log;
use RuntimeException;

final class ScriptCompiler
{
    private string $zoneDir;
    private string $binDir;
    private string $dataDir;

    private Compiler $compiler;

    public function __construct(string $zone_path) {
        $this->zoneDir = $zone_path;
        $this->dataDir = $zone_path."scriptdata".DIRECTORY_SEPARATOR;
        $this->binDir = $zone_path."scriptbin".DIRECTORY_SEPARATOR;
        $this->compiler = new Compiler();
    }

    public function __destruct() {
        $this->reset();
    }

    public function Init() : bool {
        $dirs = [$this->dataDir, $this->binDir];
        foreach ($dirs as $dir) {
            if (!file_exists($dir) && !mkdir($concurrentDirectory = $dir) && !is_dir($concurrentDirectory)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }

        $ok = (
            $this->initTokenMap()
            && $this->initItemScript()
            && $this->initSkillScript()
            && $this->initBuffScript()
        );
        assert($ok, "ScriptCompiler::Init fail!");

        $this->reset();

        return true;
    }

    private function initTokenMap() : bool {
        Log::info("\n\n ----- Begin: Init Token Map ------");

        if (
            $this->setAsm($this->binDir."common.asm") &&
            $this->setBin($this->binDir."common.bin") &&
            $this->loadEnum($this->dataDir."enum_v2.sc", 20030414) &&
            $this->loadDef($this->dataDir."itp.def") &&
            $this->loadDef($this->dataDir."npcspr.def") &&
            $this->loadDef($this->dataDir."mobname.def") && // this is npcspr.def but with char classes added ??
            $this->loadDef($this->dataDir."skill.def") &&
            $this->loadV2()
        ) {
            $this->complete();
            Log::info("\n ----- End : Init Token Map ------ \n");
            return true;
        }

        return false;
    }

    private function initItemScript() : bool {
        Log::info("\n\n ----- Begin : Init Item Script ------ ");

        $ok = (
            $this->setAsm($this->binDir."item.asm") &&
            $this->setBin($this->binDir."item.bin") &&
            $this->run($this->zoneDir."itemdata".DIRECTORY_SEPARATOR."special.sc")
        );
        assert($ok, "item load error");

        $this->complete();
        Log::info("\n ----- End : Init Item Script ------ \n");

        return true;
    }

    private function initSkillScript() : bool {
        Log::info("\n\n ---- Begin : Init Skill Script ----- ");

        $ok = (
            $this->setAsm($this->binDir."skill.asm") &&
            $this->setBin($this->binDir."skill.bin") &&
            $this->run($this->zoneDir."SkillSpecial.sc")
			// && this->run($this->zoneDir."SkillSpecialExtra.sc")
        );
        assert($ok, "initskillscript fail !");

        $this->complete();
        Log::info("\n ----- End : Init Skill Script ------ \n\n");
        return true;
    }

    private function initBuffScript() : bool {
        Log::info("\n\n ---- Begin : Init BUFF Script ----- ");
        $ok = (
            $this->setAsm($this->binDir."buff.asm") &&
            $this->setBin($this->binDir."buff.bin") &&
            $this->run($this->zoneDir."BuffSpecial.sc")
        );
        assert($ok, "initBuffScript fail");

        $this->complete();
        Log::info("\n ----- End : Init BUFF Script ------ \n\n");
        return true;
    }

    private function loadEnum(string $path, int $date=0) : bool {
        Log::info("LoadEnum $path");
        if ($this->compiler->LoadEnum($path, $date)) {
            return true;
        }
        Log::error("Can't load enum or old version!");
        return false;
    }

    private function loadDef(string $path) : bool {
        Log::info("LoadDef $path");
        if ($this->compiler->LoadDef($path)) {
            return true;
        }

        Log::error("Can't load script def");
        return false;
    }


    private function complete() : void {
        $this->compiler->Optimize();
        $this->compiler->Release();
    }

    private function setAsm(string $path) : bool {
        return $this->compiler->SetAsm($path);
    }

    private function setBin(string $path) : bool {
        return $this->compiler->SetBin($path);
    }

    private function stdDefine(string $define, int $value) : bool {
        $str = "$define $value";
        $line = new ScriptLine($str);

        return $this->compiler->OnCommand($line, Cmd::DEFINE());
    }

    private function stdDeclare(string $func, string $param, ScriptFunc $id, bool $isBlockCheck=false) : bool {
        $str = "$func $param {$id->getValue()}";
        if ($isBlockCheck) {
            $str .= " blockcheck";
        }
        $line = new ScriptLine($str);

        return $this->compiler->OnCommand($line, Cmd::DECLARE());
    }

    private function loadV2() : bool {

        $defines = [
            "ENUM" => 0,
            "MALE" => Sex::MALE()->getValue(), "FEMALE" => Sex::FEMALE()->getValue(),
            "DEFAULT_WIDTH" => 10, "DEFAULT_HEIGHT" => 10, "DEFAULT_DIRECTION" => 0, "DEFAULT_SPEED" => 300,
            "on" => 1, "off" => 0,
            "FW_DONTCARE" => 0, "FW_THIN" => 100, "FW_EXTRALIGHT" => 200, "FW_LIGHT" => 300, "FW_NORMAL" => 400, "FW_MEDIUM" => 500, "FW_SEMIBOLD" => 600, "FW_BOLD" => 700, "FW_EXTRABOLD" => 800, "FW_HEAVY" => 900,
            "CELLINFO_DEFAULT" => 0, "CELLINFO_RED" => 1, "CELLINFO_BLUE" => 5,
        ];

        foreach ($defines as $str => $val)
        {
            $ok = $this->stdDefine($str, $val);
            assert($ok, "definition fail for `$str` = `$val`.");
        }

        $declares = [
            "return" => [".", ScriptFunc::RETURN()],
            "item" => ["n", ScriptFunc::ITEM(), true],
            "event" => ["n", ScriptFunc::EVENT(), true],
            "get" => ["n", ScriptFunc::GET()],
            "inc" => ["nn", ScriptFunc::INC()],
            "dec" => ["nn", ScriptFunc::DEC()],
            "EnableSkill" => ["nn", ScriptFunc::ENABLESKILL()],
            "DisableSkill" => ["nn", ScriptFunc::DISABLESKILL()],
            "Summon" => [".", ScriptFunc::SUMMON()],
            "Skill" => ["nn", ScriptFunc::SKILL()],
            "CreateItem" => [".", ScriptFunc::CREATEITEM()],
            "LostItem" => ["n", ScriptFunc::LOSTITEM()],
            "HealHP" => ["n", ScriptFunc::HEALHP()],
            "HealSP" => ["n", ScriptFunc::HEALSP()],
            "HealAll" => [".", ScriptFunc::HEALALL()],
            "Condition" => ["nnn", ScriptFunc::CONDITION()],
            "Cure" => ["nn", ScriptFunc::CURE()],
            "AddExtParam" => ["nnn", ScriptFunc::ADDEXTPARAM()],
            "SubExtParam" => ["nnn", ScriptFunc::SUBEXTPARAM()],
            "SetMakableWpnItemList" => [".", ScriptFunc::SETMAKABLE_WPNLIST()],
            "SetMakableMtlItemList" => [".", ScriptFunc::SETMAKABLE_MTLLIST()],
            "RaceAddDamage" => ["nn", ScriptFunc::RACE_ADD_DAMAGE()],
            "RaceSubDamage" => ["nn", ScriptFunc::RACE_SUB_DAMAGE()],
            "AddAttrTolerace" => ["n?", ScriptFunc::ATTR_ADD_TOLERACE()],
            "SubAttrTolerace" => ["n?", ScriptFunc::ATTR_SUB_TOLERACE()],
            "AddRaceTolerace" => ["nn", ScriptFunc::RACE_ADD_TOLERACE()],
            "SubRaceTolerace" => ["nn", ScriptFunc::RACE_SUB_TOLERACE()],
            "AddDamage_Property" => ["nnn", ScriptFunc::ADDDAMAGE_PROPERTY()],
            "SubDamage_Property" => ["nnn", ScriptFunc::SUBDAMAGE_PROPERTY()],
            "AddDamage_Size" => ["nnn", ScriptFunc::ADDDAMAGE_SIZE()],
            "SubDamage_Size" => ["nnn", ScriptFunc::SUBDAMAGE_SIZE()],
            "AddDamage_Name" => ["nsn", ScriptFunc::ADDDAMAGE_NAME()],
            "SubDamage_Name" => ["nsn", ScriptFunc::SUBDAMAGE_NAME()],
            "AddDamage_SKID" => ["nnn", ScriptFunc::ADDDAMAGE_SKID()],
            "SubDamage_SKID" => ["nnn", ScriptFunc::SUBDAMAGE_SKID()],
            "AddDamage_CRI" => ["nn", ScriptFunc::ADDDAMAGE_CRI()],
            "SubDamage_CRI" => ["nn", ScriptFunc::SUBDAMAGE_CRI()],
            "AddState_MLEATK" => ["nnn", ScriptFunc::ADDSTATE_MLEATK()],
            "SubState_MLEATK" => ["nnn", ScriptFunc::SUBSTATE_MLEATK()],
            "AddState_MLEAttacked" => ["nnn", ScriptFunc::ADDSTATE_MLEATTACKED()],
            "SubState_MLEAttacked" => ["nnn", ScriptFunc::SUBSTATE_MLEATTACKED()],
            "AddHealPercent_ITEM" => ["nsn", ScriptFunc::ADDHEALPERCENT_ITEM()],
            "SubHealPercent_ITEM" => ["nsn", ScriptFunc::SUBHEALPERCENT_ITEM()],
            "AddHealAmount_Kill" => ["nn", ScriptFunc::ADDHEALAMOUNT_KILL()],
            "SubHealAmount_Kill" => ["nn", ScriptFunc::SUBHEALAMOUNT_KILL()],
            "IndestructibleArmor" => ["n", ScriptFunc::INDESTRUCTIBLE_ARMOR()],
            "IndestructibleWeapon" => ["n", ScriptFunc::INDESTRUCTIBLE_WEAPON()],
            "AddStateTolerace" => ["nn", ScriptFunc::ADD_STATE_TOLERACE()],
            "SubStateTolerace" => ["nn", ScriptFunc::SUB_STATE_TOLERACE()],
            "BodyAttribute" => ["n", ScriptFunc::BODY_ATTRIBUTE()],
            "SubRangeAttackDamage" => ["nn", ScriptFunc::SUB_RANGE_ATTACKDAMAGE()],
            "AddRangeAttackDamage" => ["nn", ScriptFunc::ADD_RANGE_ATTACKDAMAGE()],
            "NoDispell" => ["n", ScriptFunc::NODISPELL()],
            "AddHPdrain" => ["nn", ScriptFunc::ADD_HPDRAIN()],
            "SubHPdrain" => ["nn", ScriptFunc::SUB_HPDRAIN()],
            "Magicimmune" => ["n", ScriptFunc::MAGICIMMUNE()],
            "NoJamstone" => ["n", ScriptFunc::NOJAMSTONE()],
            "AddSPdrain" => ["nn", ScriptFunc::ADD_SPDRAIN()],
            "SubSPdrain" => ["nn", ScriptFunc::SUB_SPDRAIN()],
            "AddMeleeAttackReflect" => ["n", ScriptFunc::ADD_MELEEATTACK_REFLECT()],
            "SubMeleeAttackReflect" => ["n", ScriptFunc::SUB_MELEEATTACK_REFLECT()],
            "PerfectDamage" => ["n", ScriptFunc::PERFECT_DAMAGE()],
            "Reincarnation" => ["n", ScriptFunc::REINCARNATION()],
            "SubSpellCastTime" => ["n", ScriptFunc::SUB_SPELLCASTTIME()],
            "AddSpellCastTime" => ["n", ScriptFunc::ADD_SPELLCASTTIME()],
            "SplashAttack" => ["n", ScriptFunc::SPLASH_ATTACK()],
            "SubSPconsumption" => ["n", ScriptFunc::SUB_SPCONSUMPTION()],
            "AddSPconsumption" => ["n", ScriptFunc::ADD_SPCONSUMPTION()],
            "StartCapture" => ["s", ScriptFunc::START_CAPTURE()],
            "Incubation" => [".", ScriptFunc::INCUBATION()],
            "AddAtk_DamageWeapon" => ["nn", ScriptFunc::ADDATK_DAMAGEWEAPON()],
            "SubAtk_DamageWeapon" => ["nn", ScriptFunc::SUBATK_DAMAGEWEAPON()],
            "AddAtk_DamageArmor" => ["nn", ScriptFunc::ADDATK_DAMAGEARMOR()],
            "SubAtk_DamageArmor" => ["nn", ScriptFunc::SUBATK_DAMAGEARMOR()],
            "AddReceiveItem_Race" => ["nsn", ScriptFunc::ADDRECEIVEITEM_RACE()],
            "SubReceiveItem_Race" => ["nsn", ScriptFunc::SUBRECEIVEITEM_RACE()],
            "SetIgnoreDEFRace" => ["n", ScriptFunc::SETIGNOREDEFRACE()],
            "ResetIgnoreDEFRace" => ["n", ScriptFunc::RESETIGNOREDEFRACE()],
            "SetIgnoreDEFClass" => ["n", ScriptFunc::SETIGNOREDEFCLASS()],
            "ResetIgnoreDEFClass" => ["n", ScriptFunc::RESETIGNOREDEFCLASS()],
            "SetDisappearHPAmount" => ["snn", ScriptFunc::SET_DISAPPEARHPAMOUNT()],
            "ResetDisappearHPAmount" => ["s", ScriptFunc::RESET_DISAPPEARHPAMOUNT()],
            "SetDisappearSPAmount" => ["snn", ScriptFunc::SET_DISAPPEARSPAMOUNT()],
            "ResetDisappearSPAmount" => ["s", ScriptFunc::RESET_DISAPPEARSPAMOUNT()],
            "SetAutoSpell" => ["n", ScriptFunc::SETAUTOSPELL()],
            "AddAtk_ComaRace" => ["nn", ScriptFunc::ADDATK_COMARACE()],
            "SubAtk_ComaRace" => ["nn", ScriptFunc::SUBATK_COMARACE()],
            "AddAtk_Coma" => ["n", ScriptFunc::ADDATK_COMA()],
            "SubAtk_Coma" => ["n", ScriptFunc::SUBATK_COMA()],
            "SubSPAmount_Action" => ["nn", ScriptFunc::SUBSPAMOUNT_ACTION()],
            "AddSPAmount_Action" => ["nn", ScriptFunc::ADDSPAMOUNT_ACTION()],
            "SubSPAmount_Attack" => ["nn", ScriptFunc::SUBSPAMOUNT_ATTACK()],
            "AddSPAmount_Attack" => ["nn", ScriptFunc::ADDSPAMOUNT_ATTACK()],
            "AddSPAmount_AtkRace" => ["nnn", ScriptFunc::ADDSPAMOUNT_ATKRACE()],
            "SubSPAmount_AtkRace" => ["nnn", ScriptFunc::SUBSPAMOUNT_ATKRACE()],
            "AddDestroySP_Attack" => ["nnn", ScriptFunc::ADDDESTROYSP_ATTACK()],
            "SubDestroySP_Attack" => ["nnn", ScriptFunc::SUBDESTROYSP_ATTACK()],
            "AddSPAmount_KillRace" => ["nn", ScriptFunc::ADDSPAMOUNT_KILLRACE()],
            "SubSPAmount_KillRace" => ["nn", ScriptFunc::SUBSPAMOUNT_KILLRACE()],
            "AddEXPPercent_KillRace" => ["nn", ScriptFunc::ADDEXPPERCENT_KILLRACE()],
            "SubEXPPercent_KillRace" => ["nn", ScriptFunc::SUBEXPPERCENT_KILLRACE()],
            "ClassAddDamage" => ["nnn", ScriptFunc::CLASSADDDAMAGE()],
            "ClassSubDamage" => ["nnn", ScriptFunc::CLASSSUBDAMAGE()],
            "RaceAddDamageSelf" => ["nn", ScriptFunc::RACEADDDAMAGESELF()],
            "RaceSubDamageSelf" => ["nn", ScriptFunc::RACESUBDAMAGESELF()],
            "SetInvestigate" => [".", ScriptFunc::SETINVESTIGATE()],
            "ResetInvestigate" => [".", ScriptFunc::RESETINVESTIGATE()],
            "ModifyDEF_Fraction" => ["nn", ScriptFunc::MODIFYDEF_FRACTION()],
            "AddGuideAttack" => ["n", ScriptFunc::ADDGUIDEATTACK()],
            "SubGuideAttack" => ["n", ScriptFunc::SUBGUIDEATTACK()],
            "AddChangeMonster" => ["n", ScriptFunc::ADDCHANGEMONSTER()],
            "SubChangeMonster" => ["n", ScriptFunc::SUBCHANGEMONSTER()],
            "AddCRIPercent_Race" => ["nn", ScriptFunc::ADDCRIPERCENT_RACE()],
            "SubCRIPercent_Race" => ["nn", ScriptFunc::SUBCRIPERCENT_RACE()],
            "SetPushingSkill" => ["nn", ScriptFunc::SETPUSHINGSKILL()],
            "ResetPushingSkill" => ["n", ScriptFunc::RESETPUSHINGSKILL()],
            "GetZeny_Range" => ["nn", ScriptFunc::GETZENY_RANGE()],
            "MulticastEffect" => ["n", ScriptFunc::MULTICASTEFFECT()],
            "SetExtParamTime" => ["nnn", ScriptFunc::SETEXTPARAMTIME()],
            "SetAutoSpellTarget" => ["nnn", ScriptFunc::SETAUTOSPELL_TARGET()],
            "SetAutoSpellSelf" => ["nnn", ScriptFunc::SETAUTOSPELL_SELF()],
            "SetReceiveZENY_Kill" => ["nnn", ScriptFunc::SETRECEIVEZENY_KILL()],
            "AddCRI_RANGEATK" => ["n", ScriptFunc::ADDCRI_RANGEATK()],
            "SubCRI_RANGEATK" => ["n", ScriptFunc::SUBCRI_RANGEATK()],
            "AddState_RANGEATK" => ["nn", ScriptFunc::ADDSTATE_RANGEATK()],
            "SubState_RANGEATK" => ["nn", ScriptFunc::SUBSTATE_RANGEATK()],
            "AddGuildEXP" => ["nn", ScriptFunc::ADDGUILDEXP()],
            "SetReceiveItem_Group" => ["n", ScriptFunc::SETRECEIVEITEM_GROUP()],
            "AddReflectMagic" => ["n", ScriptFunc::ADDREFLECT_MAGIC()],
            "SubReflectMagic" => ["n", ScriptFunc::SUBREFLECT_MAGIC()],
            "ItemCreate" => ["sn", ScriptFunc::ITEMCREATE()],
            "IsCompleteCombination" => ["s", ScriptFunc::ISCOMPLETECOMBINATION()],
            "IsBreakCombination" => ["s", ScriptFunc::ISBREAKCOMBINATION()],
            "SetAutoSpell_MLEATKED" => ["nnnn", ScriptFunc::SETAUTOSPELL_MLEATKED()],
            "GetRefineLevel" => ["n", ScriptFunc::GETREFINELEVEL()],
            "GetSkillLevel" => ["n", ScriptFunc::GETSKILLLEVEL()],
            "GetPureJob" => [".", ScriptFunc::GETPUREJOB()],
            "AddGetItem_Kill" => ["sn", ScriptFunc::ADDGETITEM_KILL()],
            "SubGetItem_Kill" => ["sn", ScriptFunc::SUBGETITEM_KILL()],
            "AddParameterValue" => ["nn", ScriptFunc::ADDPARAMETERVALUE()],
            "EvolutionHomun" => [".", ScriptFunc::EVOLUTION_HOMUN()],
            "AddParamTime" => ["n?", ScriptFunc::ADDPARAMTIME()],
            "SubParamTime" => ["nnn", ScriptFunc::SUBPARAMTIME()],
            "AddDamageTM_Prop" => ["nnnn", ScriptFunc::ADDDAMAGETM_PROP()],
            "SubDamageTM_Prop" => ["nnnn", ScriptFunc::SUBSAMAGETM_PROP()],
            "Cooking" => ["n", ScriptFunc::COOKING()],
            "SummonNPC" => ["n", ScriptFunc::SUMMONNPC()],
            "trace" => ["s", ScriptFunc::TRACE()],
            "v" => ["n", ScriptFunc::VAR()],
            "lv" => ["s", ScriptFunc::LOCALVAR()],
            "npcv" => ["sn", ScriptFunc::NPCVAR()],
            "IncLocalVar" => ["sn", ScriptFunc::INCLOCALVAR()],
            "DecLocalVar" => ["sn", ScriptFunc::DECLOCALVAR()],
            "SetLocalVar" => ["sn", ScriptFunc::SETLOCALVAR()],
            "GetEquipIsIdentify" => ["n", ScriptFunc::GETEQUIP_IS_IDENTIFY()],
            "GetEquipRefineryCnt" => ["n", ScriptFunc::GETEQUIP_REFINERYCNT()],
            "GetEquipPercentRefinery" => ["n", ScriptFunc::GETEQUIP_PERCENTREFINERY()],
            "GetEquipRefineryCost" => ["n", ScriptFunc::GETEQUIP_GETEQUIPREFINERYCOST()],
            "GetEquipIsSuccessRefinery" => ["n", ScriptFunc::GETEQUIP_IS_SUCCESSREFINERY()],
            "GetEquipName" => ["n", ScriptFunc::GETEQUIP_NAME()],
            "GetEquipItemIdx" => ["n", ScriptFunc::GETEQUIP_ITEMIDX()],
            "GetEquipWeaponLv" => ["n", ScriptFunc::GETEQUIP_WEAPONLV()],
            "GetEquipIsEnableRef" => ["n", ScriptFunc::GETEQUIP_IS_ENABLEREF()],
            "LastNpcName" => ["s", ScriptFunc::LASTNPCNAME()],
            "PcName" => [".", ScriptFunc::PCNAME()],
            "OnInit:" => [".", ScriptFunc::ONINIT(), true],
            "OnClick:" => [".", ScriptFunc::ONCLICK(), true],
            "OnTouch:" => [".", ScriptFunc::ONTOUCH(), true],
            "OnMyMobDead:" => [".", ScriptFunc::ONMYMOBDEAD(), true],
            "OnTimer:" => ["n", ScriptFunc::ONTIMER(), true],
            "OnCommand:" => ["s", ScriptFunc::ONCOMMAND(), true],
            "OnStartArena:" => [".", ScriptFunc::ONSTARTARENA(), true],
            "rand" => ["nn", ScriptFunc::RAND()],
            "lot" => ["nn", ScriptFunc::LOT()],
            "GetPCCount" => ["s", ScriptFunc::GETPCCOUNT()],
            "OnTouchNPC:" => [".", ScriptFunc::ONTOUCHNPC(), true],
            "OnTouch2:" => [".", ScriptFunc::ONTOUCH2(), true],
            "guide" => ["ssnnnn", ScriptFunc::GUIDE(), true],
            "npc" => ["ssnnnnnn", ScriptFunc::NPC(), true],
            "mob" => ["nsnnnnn", ScriptFunc::MOB(), true],
            "warp" => ["ssnnnn", ScriptFunc::GATE(), true],
            "trader" => ["ssnnnn", ScriptFunc::TRADER(), true],
            "arenaguide" => ["ssnnnnnn", ScriptFunc::ARENAGUIDE(), true],
            "hiddenwarp" => ["ssnnnn", ScriptFunc::HIDDENWARP(), true],
            "effect" => ["snnnnnn", ScriptFunc::EFFECT(), true],
            "dialog" => ["s", ScriptFunc::DIALOG()],
            "putmob" => ["snnnnnnnnn", ScriptFunc::PUTMOB()],
            "moveto" => ["snn", ScriptFunc::MOVETO()],
            "say" => ["s", ScriptFunc::SAY()],
            "menu" => ["s?", ScriptFunc::MENU()],
            "getgold" => ["n", ScriptFunc::GETMONEY()],
            "dropgold" => ["n", ScriptFunc::DROPMONEY()],
            "getitem" => ["nn", ScriptFunc::GETITEM()],
            "dropitem" => ["nn", ScriptFunc::DROPITEM()],
            "setitem" => ["nn", ScriptFunc::SETITEM()],
            "wait" => [".", ScriptFunc::WAITDIALOG()],
            "close" => [".", ScriptFunc::CLOSEDIALOG()],
            "hpfullheal" => [".", ScriptFunc::HPFULLHEAL()],
            "spfullheal" => [".", ScriptFunc::SPFULLHEAL()],
            "hpheal" => ["n", ScriptFunc::HPHEAL()],
            "spheal" => ["n", ScriptFunc::SPHEAL()],
            "poisonheal" => [".", ScriptFunc::POISONHEAL()],
            "stoneeal" => [".", ScriptFunc::STONEHEAL()],
            "curseheal" => [".", ScriptFunc::CURSEHEAL()],
            "freezingheal" => [".", ScriptFunc::FREEZINGHEAL()],
            "silenceheal" => [".", ScriptFunc::SILENCEHEAL()],
            "confusionheal" => [".", ScriptFunc::CONFUSIONHEAL()],
            "sellitem" => ["n", ScriptFunc::SELLITEM()],
            "buyitem" => ["n", ScriptFunc::BUYITEM()],
            "jobchange" => ["n", ScriptFunc::JOBCHANGE()],
            "exchange" => ["nnnn", ScriptFunc::EXCHANGEITEM()],
            "error" => [".", ScriptFunc::ERROR()],
            "checkpoint" => ["snn", ScriptFunc::CHECKPOINT()],
            "store" => [".", ScriptFunc::STORE()],
            "cart" => ["n", ScriptFunc::CART()],
            "dlgwrite" => ["nn", ScriptFunc::DLGWRITE()],
            "input" => [".", ScriptFunc::INPUT()],
            "inputstr" => [".", ScriptFunc::INPUTSTR()],
            "compass" => ["nnnns", ScriptFunc::COMPASS()],
            "nude" => [".", ScriptFunc::NUDE()],
            "showimage" => ["sn", ScriptFunc::SHOW_IMAGE()],
            "changepallete" => ["nn", ScriptFunc::CHANGE_PALLETE()],
            "callmonster" => ["snsnn?", ScriptFunc::CALLMONSTER()],
            "addskill" => ["nnnn", ScriptFunc::ADDSKILL()],
            "cmdothernpc" => ["ss", ScriptFunc::OTHERNPC_CMD()],
            "strlocalvar" => ["ns", ScriptFunc::STRLOCALVAR()],
            "InitTimer" => [".", ScriptFunc::INITTIMER()],
            "setarenaeventsize" => ["n", ScriptFunc::SETARENAEVENTSIZE()],
            "makewaitingroom" => ["sn", ScriptFunc::MAKEWAITINGROOM()],
            "enablearena" => [".", ScriptFunc::ENABLEARENA()],
            "disablearena" => [".", ScriptFunc::DISABLEARENA()],
            "warpwaitingpctoarena" => ["snn", ScriptFunc::WARPWAITINGPCTOARENA()],
            "resetmymob" => [".", ScriptFunc::RESETMYMOB()],
            "warpallpcinthemap" => ["snn", ScriptFunc::WARPALLPCINTHEMAP()],
            "broadcastinmap" => ["s", ScriptFunc::BROADCASTINTHEMAP()],
            "stoptimer" => [".", ScriptFunc::STOPTIMER()],
            "addnpctimer" => ["sn", ScriptFunc::ADDNPCTIMER()],
            "subnpctimer" => ["sn", ScriptFunc::SUBNPCTIMER()],
            "enablenpc" => ["s", ScriptFunc::ENABLENPC()],
            "disablenpc" => ["s", ScriptFunc::DISABLENPC()],
            "callnpc" => ["snsnnn", ScriptFunc::CALLNPC()],
            "SetFeeZeny" => ["n", ScriptFunc::SETFEEZENY()],
            "SetFeeItem" => ["nn", ScriptFunc::SETFEEITEM()],
            "SetReqLevel" => ["nn", ScriptFunc::SETREQLEVEL()],
            "SetTexJob" => ["n", ScriptFunc::SETTEXJOB()],
            "DisableItemMove" => [".", ScriptFunc::DISABLEITEMMOVE()],
            "EnableItemMove" => [".", ScriptFunc::ENABLEITEMMOVE()],
            "SuccessRefItem" => ["n", ScriptFunc::SUCCESSREFITEM()],
            "FailedRefItem" => ["n", ScriptFunc::FAILEDREFITEM()],
            "SetEffectStatus" => ["n", ScriptFunc::SETEFFECTSTATUS()],
            "ResetStat" => [".", ScriptFunc::RESETSTAT()],
            "ResetSkill" => [".", ScriptFunc::RESETSKILL()],
            "menu2" => ["s?", ScriptFunc::MENU2()],
            "ShowDigit" => ["n", ScriptFunc::SHOWDIGIT()],
            "EventAddSkill" => ["nn", ScriptFunc::EVENT_ADD_SKILL()],
            "EventDelSkill" => ["n", ScriptFunc::EVENT_DEL_SKILL()],
            "SetParameter" => ["nn", ScriptFunc::SETPARAMETER()],
            "Emotion" => ["sn", ScriptFunc::EMOTION()],
            "ChangeSpr" => ["sn", ScriptFunc::CHANGESPR()],
            "GetEquipCount" => ["n", ScriptFunc::GETEQUIPCOUNT()],
            "AgitGet" => ["n", ScriptFunc::AGITGET()],
            "AgitGet2" => ["sn", ScriptFunc::AGITGET2()],
            "AgitSet" => ["nn", ScriptFunc::AGITSET()],
            "AgitFunc" => ["nnn", ScriptFunc::AGITFUNC()],
            "AgitNpcGet" => ["nn", ScriptFunc::AGITNPCGET()],
            "AgitNpcSet" => ["nnn", ScriptFunc::AGITNPCSET()],
            "IsGuildMaster" => ["n", ScriptFunc::ISGUILDMASTER()],
            "putmob2" => ["snnnnnnnnns", ScriptFunc::PUTMOB2()],
            "AgitRegister" => ["s", ScriptFunc::AGITREGISTER()],
            "broadcastinmap2" => ["snnnns", ScriptFunc::BROADCASTINMAP2()],
            "broadcastserver" => ["snnnns", ScriptFunc::BROADCASTSERVER()],
            "sound" => ["snnn", ScriptFunc::SOUND()],
            "dlgwritestr" => [".", ScriptFunc::DLGWRITESTR()],
            "GetDamagedItemNum" => [".", ScriptFunc::GETDAMAGEDITEMNUM()],
            "RepairDamagedItem" => [".", ScriptFunc::REPAIRDAMAGEDITEM()],
            "SavePPL" => ["n", ScriptFunc::SAVEPPL()],
            "AgitEmblemFlag" => ["s", ScriptFunc::AGITEMBLEMFLAG()],
            "hpdrain" => ["n", ScriptFunc::HPDRAIN()],
            "spdrain" => ["n", ScriptFunc::SPDRAIN()],
            "getexp" => ["n", ScriptFunc::GETEXP()],
            "dropexp" => ["n", ScriptFunc::DROPEXP()],
            "ServerTime" => [".", ScriptFunc::SERVERTIME()],
            "GetLocalVarName" => ["n", ScriptFunc::GETLOCALVARNAME()],
            "GetMarried" => [".", ScriptFunc::GETMARRIED()],
            "countdown" => ["n", ScriptFunc::COUNTDOWN()],
            "GuildName" => [".", ScriptFunc::GETGUILDNAME()],
            "GetNeighborPcNumber" => ["n", ScriptFunc::GETNEIGHBORPCNUMBER()],
            "getnameditem" => ["nn", ScriptFunc::GETNAMEDITEM()],
            "dropnameditem" => ["nn", ScriptFunc::DROPNAMEDITEM()],
            "GetCountOfMyNameItem" => ["n", ScriptFunc::GETCOUNTFOMYNAMEITEM()],
            "ChangeHairStyle" => ["n", ScriptFunc::CHANGE_HAIRSTYLE()],
            "ShowEffect" => ["sn", ScriptFunc::SHOWEFFECT()],
            "falcon" => ["n", ScriptFunc::FALCON()],
            "peco" => ["n", ScriptFunc::PECO()],
            "CheckMaxWeight" => ["nn", ScriptFunc::CHECK_MAXWEIGHT()],
            "CheckMaxCount" => ["nn", ScriptFunc::CHECK_MAXCOUNT()],
            "CheckMaxZeny" => ["n", ScriptFunc::CHECK_MAXZENY()],
            "GetMEventZenyName" => ["n", ScriptFunc::MGETEVENT_ZENYNAME()],
            "GetMEventItemName" => ["n", ScriptFunc::MGETEVENT_ITEMNAME()],
            "GetMEventZeny" => ["n", ScriptFunc::MGETEVENT_ZENY()],
            "GetMEventItem" => ["n", ScriptFunc::MGETEVENT_ITEM()],
            "CreateGuild" => ["s", ScriptFunc::CREATE_GUILD()],
            "CreateGlobalVar" => ["sn", ScriptFunc::CREATEGLOBALVAR()],
            "GetGlobalVar" => ["s", ScriptFunc::GETGLOBALVAR()],
            "SetGlobalVar" => ["sn", ScriptFunc::SETGLOBALVAR()],
            "StrCmp" => ["ss", ScriptFunc::STRCMP()],
            "StrStr" => ["ss", ScriptFunc::STRSTR()],
            "UpgradeGuildLevel" => [".", ScriptFunc::UPGRADEGUILDLEVEL()],
            "CreateGlobalStr" => ["ss", ScriptFunc::CREATEGLOBALSTR()],
            "GetGlobalStr" => ["s", ScriptFunc::GETGLOBALSTR()],
            "SetGlobalStr" => ["ss", ScriptFunc::SETGLOBALSTR()],
            "divorce" => [".", ScriptFunc::DIVORCE()],
            "GetTablePoint" => [".", ScriptFunc::GET_TABLEPOINT()],
            "AddTablePoint" => ["n", ScriptFunc::ADD_TABLEPOINT()],
            "DelTablePoint" => ["n", ScriptFunc::DEL_TABLEPOINT()],
            "DataMoveToPVP" => [".", ScriptFunc::PCDATA_MOVE_TO_PVP()],
            "GetPVPPoint" => [".", ScriptFunc::GETPVPPOINT()],
            "SetPVPPoint" => ["n", ScriptFunc::SETPVPPOINT()],
            "GetMEventItemID" => ["n", ScriptFunc::MGETEVENT_ITEMID()],
            "getJexp" => ["n", ScriptFunc::GETJOBEXP()],
            "dropJexp" => ["n", ScriptFunc::DROPJOBEXP()],
            "ChkSkill" => ["n", ScriptFunc::CHKSKILL()],
            "GetBODYSTATE" => ["nn", ScriptFunc::GETBODYSTATE()],
            "GetHEALTHSTATE" => ["nn", ScriptFunc::GETHEALTHSTATE()],
            "UseSkillToPC" => ["nnnn", ScriptFunc::USESKILL_TO_PC()],
            "IsBaby" => [".", ScriptFunc::ISBABY()],
            "GetLotto" => ["n", ScriptFunc::GETLOTTO()],
            "SetLotto" => ["nnnnnn", ScriptFunc::SETLOTTO()],
            "InitLotto" => [".", ScriptFunc::INITLOTTO()],
            "OpenAuction" => [".", ScriptFunc::OPENAUCTION()],
            "OpenMailing" => [".", ScriptFunc::OPENMAILING()],
            "StripPC" => ["n", ScriptFunc::STRIPTPC()],
            "PushPC" => ["nn", ScriptFunc::PUSHPC()],
            "ItemDown" => ["nnnn", ScriptFunc::ITEMDOWN()],
            "SetNumArray" => ["nn", ScriptFunc::SETNUMARRAY()],
            "GetNumArray" => ["n", ScriptFunc::GETNUMARRAY()],
            "ShuffleNumbers" => ["nn", ScriptFunc::SHUFFLENUMBERS()],
            "PartyName" => [".", ScriptFunc::PARTYNAME()],
            "ChangeSpeed" => ["nn", ScriptFunc::CHANGESPEED()],
            "IsSiegeTime" => [".", ScriptFunc::ISSIEGETIME()],
            "ConsumeSpecialItem" => ["n", ScriptFunc::CONSUMESPECIALITEM()],
            "SetItemPartyInMap" => ["nn", ScriptFunc::SETITEMPARTYINMAP()],
            "IsHuntingListFull" => [".", ScriptFunc::ISHUNTINGLISTFULL()],
            "FindHuntingList" => ["n", ScriptFunc::FINDHUNTINGLIST()],
            "AddHuntingList" => ["n", ScriptFunc::ADDHUNTINGLIST()],
            "DeleteHuntingList" => ["n", ScriptFunc::DELETEHUNTINGLIST()],
            "DisplayHuntingList" => [".", ScriptFunc::DISPLAYHUNTINGLIST()],
            "DisplayHuntingList2" => [".", ScriptFunc::DISPLAYHUNTINGLIST2()],
            "SetHuntingList" => ["nn", ScriptFunc::SETHUNTINGLIST()],
            "ClearHuntingList" => [".", ScriptFunc::CLEARHUNTINGLIST()],
            "IsTimeListFull" => [".", ScriptFunc::ISTIMELISTFULL()],
            "FindTimeList" => ["n", ScriptFunc::FINDTIMELIST()],
            "AddTimeList" => ["n", ScriptFunc::ADDTIMELIST()],
            "DeleteTimeList" => ["n", ScriptFunc::DELETETIMELIST()],
            "DisplayTimeList" => [".", ScriptFunc::DISPLAYTIMELIST()],
            "SetTimeList" => ["nn", ScriptFunc::SETTIMELIST()],
            "ClearTimeList" => [".", ScriptFunc::CLEARTIMELIST()],
            "ChangeCellType" => ["nnn", ScriptFunc::CHANGECELLTYPE()],
            "GetCellType" => ["nn", ScriptFunc::GETCELLTYPE()],
            "IsPcCafe" => [".", ScriptFunc::ISPCCAFE()],
            "GetPayType" => [".", ScriptFunc::GETPAYTYPE()],
            "GetConnectionArea" => [".", ScriptFunc::GETCONNECTIONAREA()],
            "GetConnectionState" => [".", ScriptFunc::GETCONNECTIONSTATE()],
            "GetPetEgg" => ["n", ScriptFunc::GETPETEGG()],
            "CashTrader" => ["ssnnnn", ScriptFunc::CASHTRADER(), true],
            "CashShop" => ["s", ScriptFunc::CASHSHOP()],
            "AddexpTime" => ["nn", ScriptFunc::ADDEXPTIME()],
            "SubexpTime" => ["n", ScriptFunc::SUBEXPTIME()],
            "AddReceiveItem" => ["nn", ScriptFunc::ADDRECEIVEITEM()],
            "GetAdvEquipIsSuccessRefinery" => ["n", ScriptFunc::ADVEQUIP_ISSUCCESSREFINERY()],
            "Partycall" => [".", ScriptFunc::PARTYCALL()],
            "putboss" => ["snnnnnnnnn?", ScriptFunc::PUTBOSS()],
            "UseBossPerception" => ["n", ScriptFunc::USEBOSSPERCEPTION()],
            "SkillToMe" => ["nn", ScriptFunc::SKILLTOME()],
            "UseHuntingList" => [".", ScriptFunc::UseHuntingList()],
            "CheckHuntingList" => ["n", ScriptFunc::CheckHuntingList()],
            "item2" => ["n", ScriptFunc::ITEM2(), true],
            "WeaponProperty" => ["nn", ScriptFunc::WEAPONPROPERTY()],
            "ReadBook" => ["nn", ScriptFunc::READBOOK()],
            "Mercenary_Summon" => ["n", ScriptFunc::MERCENARY_SUMMON()],
            "Mercenary_HealHP" => ["n", ScriptFunc::MERCENARY_HEALHP()],
            "Mercenary_HealSP" => ["n", ScriptFunc::MERCENARY_HEALSP()],
            "GetMercenaryTotalSummonNum" => ["n", ScriptFunc::GETMERCENARYTOTALSUMMONNUM()],
            "GetMercenaryFaith" => ["n", ScriptFunc::GETMERCENARYFAITH()],
            "SubMercenaryFaith" => ["nn", ScriptFunc::SUBMERCENARYFAITH()],
            "AddMercenaryFaith" => ["nn", ScriptFunc::ADDMERCENARYFAITH()],
            "npc2" => ["ssnnnnnnn", ScriptFunc::NPC2(), true],
            "OnMoveNpcCmd:" => [".", ScriptFunc::ONMOVENPCCMD(), true],
            "MovePos" => ["nn", ScriptFunc::MOVEPOS()],
            "MoveWait" => ["n", ScriptFunc::MOVEWAIT()],
            "Refinery" => ["n", ScriptFunc::REFINERY()],
            "Mercenary_Condition" => ["nnn", ScriptFunc::MERCENARY_CONDITION()],
            "AddNeverknockback" => ["n", ScriptFunc::ADDNEVERKNOCKBACK()],
            "GetAdvEquipPercentRefinery" => ["n", ScriptFunc::GETADVEQUIPPERCENTREFINERY()],
            "ReLoadMobileEvent" => [".", ScriptFunc::RELOAD_MOBILE_EVENT()],
            "SetTempTime" => ["n", ScriptFunc::SETTEMPTIME()],
            "ElapseTempTime" => ["n", ScriptFunc::ELAPSETEMPTIME()],
            "SubNeverknockback" => ["n", ScriptFunc::SUBNEVERKNOCKBACK()],
            "GetWeaponClass" => ["n", ScriptFunc::GETWEAPONCLASS()],
            "GetEquipSlotAmount" => ["n", ScriptFunc::GETEQUIPSLOTAMOUNT()],
            "AddHealValue" => ["n", ScriptFunc::ADDHEALVALUE()],
            "SubHealValue" => ["n", ScriptFunc::SUBHEALVALUE()],
            "SetAutoSpell_MAGATKED" => ["nnnn", ScriptFunc::SETAUTOSPELL_MAGATKED()],
            "AddHPAmount_MAGKill" => ["n", ScriptFunc::ADDHPAMOUNT_MAGKILL()],
            "SubHPAmount_MAGKill" => ["n", ScriptFunc::SUBHPAMOUNT_MAGKILL()],
            "AddSPAmount_MAGKill" => ["n", ScriptFunc::ADDSPAMOUNT_MAGKILL()],
            "SubSPAmount_MAGKill" => ["n", ScriptFunc::SUBSPAMOUNT_MAGKILL()],
            "AddSpellDelay" => ["n", ScriptFunc::ADDSPELLDELAY()],
            "SubSpellDelay" => ["n", ScriptFunc::SUBSPELLDELAY()],
            "AddSpecificSpellCastTime" => ["nn", ScriptFunc::ADDSPECIFICSPELLCASTTIME()],
            "SubSpecificSpellCastTime" => ["nn", ScriptFunc::SUBSPECIFICSPELLCASTTIME()],
            "AddMdamage_Race" => ["nn", ScriptFunc::ADDMDAMAGE_RACE()],
            "SubMdamage_Race" => ["nn", ScriptFunc::SUBMDAMAGE_RACE()],
            "AddMdamage_Class" => ["nn", ScriptFunc::ADDMDAMAGE_CLASS()],
            "SubMdamage_Class" => ["nn", ScriptFunc::SUBMDAMAGE_CLASS()],
            "SetIgnoreMdefRace" => ["nn", ScriptFunc::SETIGNOREMDEFRACE()],
            "ResetIgnoreMdefRace" => ["nn", ScriptFunc::RESETIGNOREMDEFRACE()],
            "SetIgnoreMdefClass" => ["nn", ScriptFunc::SETIGNOREMDEFCLASS()],
            "ResetIgnoreMdefClass" => ["nn", ScriptFunc::RESETIGNOREMDEFCLASS()],
            "SetHPAmountTime" => ["nnn", ScriptFunc::SETHPAMOUNTTIME()],
            "ResetHPAmountTime" => ["nnn", ScriptFunc::RESETHPAMOUNTTIME()],
            "SetSPAmountTime" => ["nnn", ScriptFunc::SETSPAMOUNTTIME()],
            "ResetSPAmountTime" => ["nnn", ScriptFunc::RESETSPAMOUNTTIME()],
            "SetIgnoreDefRace_Percent" => ["nn", ScriptFunc::SETIGNOREDEFRACE_PERCENT()],
            "ResetIgnoreDefRace_Percent" => ["nn", ScriptFunc::RESETIGNOREDEFRACE_PERCENT()],
            "SetIgnoreDefClass_Percent" => ["nn", ScriptFunc::SETIGNOREDEFCLASS_PERCENT()],
            "ResetIgnoreDefClass_Percent" => ["nn", ScriptFunc::RESETIGNOREDEFCLASS_PERCENT()],
            "GetInventoryRemainCount" => ["nn", ScriptFunc::GETINVENTORYREMAINCOUNT()],
            "SetAutoWeapon" => ["nnn", ScriptFunc::SETAUTOWEAPON()],
            "ResetAutoWeapon" => ["nnn", ScriptFunc::RESETAUTOWEAPON()],
            "SetAutoMweapon" => ["nnn", ScriptFunc::SETAUTOMWEAPON()],
            "ResetAutoMweapon" => ["nnn", ScriptFunc::RESETAUTOMWEAPON()],
            "SetAutoAttacked" => ["nnn", ScriptFunc::SETAUTOATTACKED()],
            "ResetAutoAttacked" => ["nnn", ScriptFunc::RESETAUTOATTACKED()],
            "SetAutoMAttacked" => ["nnn", ScriptFunc::SETAUTOMATTACKED()],
            "ResetAutoMAttacked" => ["nnn", ScriptFunc::RESETAUTOMATTACKED()],
            "ResetReceiveItem_Group" => ["n", ScriptFunc::RESETRECEIVEITEM_GROUP()],
            "Buff" => ["s", ScriptFunc::BUFF(), true],
            "SetBuff" => ["snn", ScriptFunc::SETBUFF()],
            //"SetBuff" => ["sn?", ScriptFunc::SETBUFF()],
            "GetBuffValue" => ["s", ScriptFunc::GETBUFFVALUE()],
            "AddHealModifyPercent" => ["n", ScriptFunc::ADDHEALMODIFYPERCENT()],
            "SubHealModifyPercent" => ["n", ScriptFunc::SUBHEALMODIFYPERCENT()],
            "SetHPPercentTime" => ["nnn", ScriptFunc::SETHPPERCENTTIME()],
            "ResetHPPercentTime" => ["nnn", ScriptFunc::RESETHPPERCENTTIME()],
            "SetSPPercentTime" => ["nnn", ScriptFunc::SETSPPERCENTTIME()],
            "ResetSPPercentTime" => ["nnn", ScriptFunc::RESETSPPERCENTTIME()],
            "LoudSpeaker" => ["snnnns", ScriptFunc::LOUDSPEAKER()],
            "SetTargetAttacked_Buff" => ["nnsnn", ScriptFunc::SETTARGETATTACKED_BUFF()],
            "ResetTargetAttacked_Buff" => ["nnsnn", ScriptFunc::RESETTARGETATTACKED_BUFF()],
            "GetMapName" => [".", ScriptFunc::GETMAPNAME()],
            "setquest" => ["n", ScriptFunc::SETQUEST()],
            "changequest" => ["nn", ScriptFunc::CHANGQUEST()],
            "erasequest" => ["n", ScriptFunc::ERASEQUEST()],
            "completequest" => ["n", ScriptFunc::COMPLATEQUEST()],
            "completequest_between" => ["nn", ScriptFunc::COMPLETEQUEST_BETWEEN()],
            "recall_completequest" => ["n", ScriptFunc::RECALL_COMPLETEQUEST()],
            "isbegin_quest" => ["n", ScriptFunc::ISBEGINQUEST()],
            "checkquest_hunting" => ["n", ScriptFunc::CHECKQUEST_HUNTING()],
            "checkquest_playtime" => ["n", ScriptFunc::CHECKQUEST_PLAYTIME()],
            "GetLastSiegeMsg" => ["s", ScriptFunc::GETLASTSIEGEMSG()],
            "DataMoveTo_Sakray" => [".", ScriptFunc::PCDATA_MOVE_TO_SAKRAY()],
            "callguardian" => ["snsnn", ScriptFunc::CALLGUARDIAN()],
            "IsPremiumPcCafe" => [".", ScriptFunc::ISPREMIUMPCCAFE()],
            "MD_Subscription" => ["s", ScriptFunc::MDUNGEON_SUBSCRIPTION()],
            "MD_Enter" => ["s", ScriptFunc::MDUNGEON_ENTER()],
            "CampCode" => ["snn", ScriptFunc::CAMPCODE()],
            "GetCampCode" => [".", ScriptFunc::GETCAMPCODE()],
            "CallCampMob" => ["nnnss", ScriptFunc::CALLCAMPMOB()],
            "SetRP2" => ["nn", ScriptFunc::SETRP2()],
            "MassSetRP2" => ["snn", ScriptFunc::MASSSETRP2()],
            "PlayerToRP" => ["n", ScriptFunc::PLAYERTORP()],
            "GetMEventZenyName_Exculusive" => ["n", ScriptFunc::MGETEVENT_ZENYNAME_EXCULUSEIVE()],
            "GetMEventItemName_Exculusive" => ["n", ScriptFunc::MGETEVENT_ITEMNAME_EXCULUSEIVE()],
            "GetMEventZeny_Exculusive" => ["n", ScriptFunc::MGETEVENT_ZENY_EXCULUSEIVE()],
            "GetMEventItem_Exculusive" => ["n", ScriptFunc::MGETEVENT_ITEM_EXCULUSEIVE()],
            "GetMEventItemID_Exculusive" => ["n", ScriptFunc::MGETEVENT_ITEMID_EXCULUSEIVE()],
            "ReLoadMobileEvent_Exculusive" => [".", ScriptFunc::MGETEVENT_RELOAD_EXCULUSEIVE()],
            "MD_List" => ["sss", ScriptFunc::MDUNGEON_LIST()],
            "MD_Destroy" => ["s", ScriptFunc::MDUNGEON_DESTROY()],
            "PlusCampPoint" => ["sn", ScriptFunc::PLUSCAMPPOINT()],
            "MinusCampPoint" => ["sn", ScriptFunc::MINUSCAMPPOINT()],
            "GetCampPoint" => ["s", ScriptFunc::GETCAMPPOINT()],
            "ResetCampPoint" => [".", ScriptFunc::RESETCAMPPOINT()],
            "CheckSiegeTime" => ["n", ScriptFunc::CHECKSIEGETIME()],
            "AddState_MATK" => ["nnn", ScriptFunc::ADDSTATE_MATK()],
            "SubState_MATK" => ["nnn", ScriptFunc::SUBSTATE_MATK()],
            "AddState_MAttacked" => ["nnn", ScriptFunc::ADDSTATE_MATTACKED()],
            "SubState_MAttacked" => ["nnn", ScriptFunc::SUBSTATE_MATTACKED()],
            "GetMDMapName" => ["s", ScriptFunc::MDUNGEON_MAPNAME()],
            "GetMDNpcName" => ["s", ScriptFunc::MDUNGEON_NPCNAME()],
            "MD_Npc" => ["ssnnnnnn", ScriptFunc::MDUNGEON_NPC(), true],
            "MD_Warp" => ["ssnnnn", ScriptFunc::MDUNGEON_WAP(), true],
            "MD_Hiddenwarp" => ["ssnnnn", ScriptFunc::MDUNGEON_HIDDENWAP(), true],
            "MD_Putmob" => ["snnnnnnnnn", ScriptFunc::MDUNGEON_PUTMOB()],
            "Mymobskilluse" => ["snnnn", ScriptFunc::MYMOBSKILLUSE()],
            "WhereMymobX" => ["s", ScriptFunc::WHEREMYMOB_X()],
            "WhereMymobY" => ["s", ScriptFunc::WHEREMYMOB_Y()],
            "WideMobskilluse" => ["nnnnnnn", ScriptFunc::WIDEMOBSKILLUSE()],
            "WideMobskilluse2" => ["nnnsnnnn", ScriptFunc::WIDEMOBSKILLUSE2()],
            "MD_OpenState" => ["n", ScriptFunc::MDUNGEON_OPENSTATE()],
            "AddHPDrain_100" => ["n", ScriptFunc::ADDHPDRAIN_100()],
            "SubHPDrain_100" => ["n", ScriptFunc::SUBHPDRAIN_100()],
            "CreatePackage" => [".", ScriptFunc::CREATEPACKAGE()],
            "IsBender" => [".", ScriptFunc::ISBENDER()],
            "MoveResume" => [".", ScriptFunc::MOVERESUME()],
            "MoveReturn" => [".", ScriptFunc::MOVERETURN()],
            "MoveSpeed" => ["n", ScriptFunc::MOVESPEED()],
            "GetNpcPosition" => ["s", ScriptFunc::GETNPCPOSITION()],
            "AddReceiveItem_Equip" => ["n", ScriptFunc::ADDRECEIVEITEM_EQUIP()],
            "SubReceiveItem_Equip" => ["n", ScriptFunc::SUBRECEIVEITEM_EQUIP()],
            "AddOnlyJobExpTime" => ["nn", ScriptFunc::ADDONLYJOBEXPTIME()],
            "progress_bar" => ["sn", ScriptFunc::PROGRESS_BAR()],
            "MercenaryCheck" => [".", ScriptFunc::MERCENARYCHECK()],
            "MercenaryType" => [".", ScriptFunc::MERCENARYTYPE()],
            "IsFreeServer" => [".", ScriptFunc::ISFREESERVER()],
            "SetSkillAutoSpell" => ["nnnnn", ScriptFunc::SETSKILLAUTOSPELL()],
            "ResetSkillAutoSpell" => ["nnnnn", ScriptFunc::RESETSKILLAUTOSPELL()],
            "SetSkillActivate" => ["nnnn", ScriptFunc::SETSKILLACTIVATE()],
            "ResetSkillActivate" => ["nnnn", ScriptFunc::RESETSKILLACTIVATE()],
            "GetActivated_Skill" => [".", ScriptFunc::GETACTIVATED_SKILL()],
            "GetDeactivated_Skill" => [".", ScriptFunc::GETDEACTIVATED_SKILL()],
            "AddState_SKID" => ["nnnn", ScriptFunc::ADDSTATE_SKID()],
            "SubState_SKID" => ["nnnn", ScriptFunc::SUBSTATE_SKID()],
            "SetAutoSpellMagic" => ["nnnn", ScriptFunc::SETAUTOSPELLMAGIC()],
            "ResetAutoSpellMagic" => ["nnnn", ScriptFunc::RESETAUTOSPELLMAGIC()],
            "run_npc" => ["ssnnnnnn", ScriptFunc::RUN_NPC()],
            "run_npctrader" => ["ssnnnnnn", ScriptFunc::RUN_NPCTRADER()],
            "OnCampCommand:" => [".", ScriptFunc::ONCAMPCOMMAND(), true],
            "check_partyjob" => [".", ScriptFunc::CHECKPARTYJOB()],
            "clearbuff" => [".", ScriptFunc::CLEARBUFF()],
            "check_alive" => ["s", ScriptFunc::CHECKALIVE()],
            "kvm_npc" => ["ssnnnnnn", ScriptFunc::KVM_NPC(), true],
            "kvm_start_alivecheck" => [".", ScriptFunc::KVM_START_ALIVECHECK()],
            "pvprankcheck" => [".", ScriptFunc::PVPRANKCHECK()],
            "SetAutoSpell_MLEATK" => ["nnnn", ScriptFunc::SETAUTOSPELL_MLEATK()],
            "SetAutoSpell_RANGEATK" => ["nnnn", ScriptFunc::SETAUTOSPELL_RANGEATK()],
            "kvm_npc_noti" => ["s", ScriptFunc::KVM_NPC_NOTI()],
            "getarenaeventsize" => [".", ScriptFunc::GETARENAEVENTSIZE()],
            "SetMakableRuneItemList" => ["n", ScriptFunc::SETMAKABLERUNEITEMLIST()],
            "IsSuccessRuneUse" => ["n", ScriptFunc::ISSUCCESSRUNEUSE()],
            "SetMakableNewPoisonList" => [".", ScriptFunc::SETMAKABLENEWPOISONLIST()],
            "GetItemSocket" => ["nnnnn", ScriptFunc::GETITEMSOCKET()],
            "GetPetRelationship" => [".", ScriptFunc::GETPETRELATIONSHIP()],
            "AddMDamage_Name" => ["nsn", ScriptFunc::ADDMDAMAGE_NAME()],
            "SubMDamage_Name" => ["nsn", ScriptFunc::SUBMDAMAGE_NAME()],
            "GetNonSlotItemSock" => ["nnnnn", ScriptFunc::GETNONSLOTITEMSOCK()],
            "IsEffectStatus" => ["n", ScriptFunc::ISEFFECTSTATUS()],
            "GetMyMercenary" => [".", ScriptFunc::GETMYMERCENARY()],
            "ErrorLog" => ["s", ScriptFunc::ERRORLOG()],
            "AddReceiveItem_Name" => ["ssn", ScriptFunc::ADDRECEIVEITEM_NAME()],
            "SubReceiveItem_Name" => ["ssn", ScriptFunc::SUBRECEIVEITEM_NAME()],
            "IsMadogear" => [".", ScriptFunc::ISMADOGEAR()],
            "SetMadogear" => ["s", ScriptFunc::SETMADOGEAR()],
            "AddSFCTConsumeAmount" => ["nnn", ScriptFunc::ADD_SFCT_CONSUME_AMOUNT()],
            "SubSFCTConsumeAmount" => ["nnn", ScriptFunc::SUB_SFCT_CONSUME_AMOUNT()],
            "AddSFCTConsumePermill" => ["nnn", ScriptFunc::ADD_SFCT_CONSUME_PERMILL()],
            "SubSFCTConsumePermill" => ["nnn", ScriptFunc::SUB_SFCT_CONSUME_PERMILL()],
            "AddSFCTEquipAmount" => ["nnn", ScriptFunc::ADD_SFCT_EQUIP_AMOUNT()],
            "SubSFCTEquipAmount" => ["nnn", ScriptFunc::SUB_SFCT_EQUIP_AMOUNT()],
            "AddSFCTEquipPermill" => ["nnn", ScriptFunc::ADD_SFCT_EQUIP_PERMILL()],
            "SubSFCTEquipPermill" => ["nnn", ScriptFunc::SUB_SFCT_EQUIP_PERMILL()],
            "AddAttrTolerace_Disregard" => ["n?", ScriptFunc::ADDATTRTOLERACE_DISREGARD()],
            "SubAttrTolerace_Disregard" => ["n?", ScriptFunc::SUBATTRTOLERACE_DISREGARD()],
            "GetExdEquipIsSuccessRefinery" => ["n", ScriptFunc::GETEXDEQUIPISSUCCESSREFINERY()],
            "GetExdAdvEquipIsSuccessRefinery" => ["n", ScriptFunc::GETEXDADVEQUIPISSUCCESSREFINERY()],
            "DownRefItem" => ["n", ScriptFunc::DOWNREFITEM()],
            "DownAdvRefItem" => ["n", ScriptFunc::DOWNADVREFITEM()],
            "SuccessRandomRefItem" => ["nnn", ScriptFunc::SUCCESSRANDREFITEM()],
            "SuccessRandomAdvRefItem" => ["nnn", ScriptFunc::SUCCESSRANDADVREFITEM()],
            "FinalBattlePlay" => ["s", ScriptFunc::FINAL_BATTLE_PLAY()],
            "OnCampCommand2:" => ["s", ScriptFunc::ONCAMPCOMMAND2(), true],
            "InterOther" => ["nnnn", ScriptFunc::INTEROTHER()],
            "SetAutoSpell_MLEATKED_V2" => ["nnnn", ScriptFunc::SETAUTOSPELL_MLEATKED_V2()],
            "SetAutoSpell_RANGEATKED_V2" => ["nnnn", ScriptFunc::SETAUTOSPELL_RANGEATKED_V2()],
            "AddSkillSP" => ["nn", ScriptFunc::ADDSKILLSP()],
            "SubSkillSP" => ["nn", ScriptFunc::SUBSKILLSP()],
            "AddSkillDelay" => ["nn", ScriptFunc::ADDSKILLDELAY()],
            "SubSkillDelay" => ["nn", ScriptFunc::SUBSKILLDELAY()],
            "PlayBGM" => ["s", ScriptFunc::PLAYBGM()],
            "StripPC2" => ["n", ScriptFunc::STRIPTPC2()],
            "AddStateCastTime" => ["nn", ScriptFunc::ADDSTATECASTTIME()],
            "SubStateCastTime" => ["nn", ScriptFunc::SUBSTATECASTTIME()],
            "AddSkillMDamage" => ["nn", ScriptFunc::ADDSKILLMDAMAGE()],
            "SubSkillMDamage" => ["nn", ScriptFunc::SUBSKILLMDAMAGE()],
            "GetLocation" => [".", ScriptFunc::GETLOCATION()],
            "BroadcastInZone" => ["nss", ScriptFunc::BROADCAST_IN_ZONE()],
            "BroadcastInMap" => ["nss", ScriptFunc::BROADCAST_IN_MAP()],
            "SetEffect" => ["sn", ScriptFunc::SET_ITEM_EFFECT()],
            "ResetEffect" => ["s", ScriptFunc::RESET_ITEM_EFFECT()],
            "SetReadyMutationHomun" => [".", ScriptFunc::SETREADYMUTATIONHOMUN()],
            "MutationHomun" => ["n", ScriptFunc::MUTATIONHOMUN()],
            "ModifyMDEF_Fraction" => ["nn", ScriptFunc::MODIFYMDEF_FRACTION()],
            "AddAllDEF_Fluctuation" => ["n", ScriptFunc::ADDALLDEF_FLUCTUATION()],
            "SubAllDEF_Fluctuation" => ["n", ScriptFunc::SUBALLDEF_FLUCTUATION()],
            "AddAllMDEF_Fluctuation" => ["n", ScriptFunc::ADDALLMDEF_FLUCTUATION()],
            "SubAllMDEF_Fluctuation" => ["n", ScriptFunc::SUBALLMDEF_FLUCTUATION()],
            "OpenSearchStoreInfo" => ["nn", ScriptFunc::OPEN_SEARCH_STORE_INFO()],
            "ChangeRP" => ["snn", ScriptFunc::CHANGERP()],
            "IsAllianceGuild" => ["n", ScriptFunc::ISALLIANCEGUILD()],
            "OpenHour_SiegeMode" => ["nn", ScriptFunc::OPENHOUR_SIEGEMODE()],
            "CheckHomunLevel" => [".", ScriptFunc::CHECKHOMUNLEVEL()],
            "CheckHomunMutation" => [".", ScriptFunc::CHECKHOMUNMUTATION()],
            "CheckHomunCall" => [".", ScriptFunc::CHECKHOMUNCALL()],
            "SetBattleFieldMovePosition" => ["ssnn", ScriptFunc::SETBATTLEFIELDMOVEPOSTION()],
            "GetEquipcompositionType" => ["nn", ScriptFunc::GETEQUIPCOMPOSITIONTYPE()],
            "GetEquipDBName" => ["n", ScriptFunc::GETEQUIPDBNAME()],
            "GetEquipcardID" => ["nn", ScriptFunc::GETEQUIPCARDID()],
            "dropequipitem" => ["n", ScriptFunc::DROPEQUIPITEM()],
            "getrefitem" => ["ns", ScriptFunc::GETREFITEM()],
            "ResetBuff" => ["s?", ScriptFunc::RESETBUFF()],
            "AddState_EFSTATK" => ["nsnnn", ScriptFunc::ADDSTATE_EFSTATK()],
            "SubState_EFSTATK" => ["nsnn", ScriptFunc::SUBSTATE_EFSTATK()],
            "GetNonSlotItemSock2" => ["nnnnnn", ScriptFunc::GETNONSLOTITEMSOCK2()],
            "getitem2" => ["nnnn", ScriptFunc::GETITEM2()],
            "ShowScript" => ["ss", ScriptFunc::SHOWSCRIPT()],
            "ShowEmotion" => ["sn", ScriptFunc::SHOWEMOTION()],
            "StartCollection" => ["n", ScriptFunc::START_COLLECTION()],
            "Montransform" => ["nns", ScriptFunc::MONTRANSFORM()],
            "DeleteAllBodyItem" => [".", ScriptFunc::DELETE_ALL_BODY_ITEM()],
            "OnAgitInvest:" => [".", ScriptFunc::ONAGITINVEST(), true],
            "IncGlobalVar" => ["sn", ScriptFunc::INC_GLOBALVAR()],
            "DecGlobalVar" => ["sn", ScriptFunc::DEC_GLOBALVAR()],
            "GetAgitInvestMsg" => [".", ScriptFunc::GET_AGITINVEST_MSG()],
            "AddHandicapState_Race" => ["nnnn", ScriptFunc::ADD_HANDICAPSTATE_RACE()],
            "SubHandicapState_Race" => ["nnnn", ScriptFunc::SUB_HANDICAPSTATE_RACE()],
            "SetNumDEF_Race" => ["nnnn", ScriptFunc::SET_NUM_DEF_RACE()],
            "ResetNumDEF_Race" => ["nnnn", ScriptFunc::RESET_NUM_DEF_RACE()],
            "SetNumMDEF_Race" => ["nnnn", ScriptFunc::SET_NUM_MDEF_RACE()],
            "ResetNumMDEF_Race" => ["nnnn", ScriptFunc::RESET_NUM_MDEF_RACE()],
            "SetPerDEF_Self" => ["nn", ScriptFunc::SET_PER_DEF_SELF()],
            "SetPerMDEF_Self" => ["nn", ScriptFunc::SET_PER_MDEF_SELF()],
            "SetMHPPerDamage_Race" => ["nn", ScriptFunc::SET_MHP_PER_DAMAGE_RACE()],
            "ReSetMHPPerDamage_Race" => ["nn", ScriptFunc::RESET_MHP_PER_DAMAGE_RACE()],
            "SetMSPPerDamage_Race" => ["nn", ScriptFunc::SET_MSP_PER_DAMAGE_RACE()],
            "ReSetMSPPerDamage_Race" => ["nn", ScriptFunc::RESET_MSP_PER_DAMAGE_RACE()],
            "GetAgitEconomy" => ["s", ScriptFunc::GET_AGIT_ECONOMY()],
            "IncAgitEconomy" => ["sn", ScriptFunc::INC_AGIT_ECONOMY()],
            "DecAgitEconomy" => ["sn", ScriptFunc::DEC_AGIT_ECONOMY()],
            "EQ_Make" => ["snnnn", ScriptFunc::EQ_MAKE()],
            "EQ_CheckUser" => ["sn", ScriptFunc::EQ_CHECK_USER()],
            "EQ_NotifyAdmission" => ["s", ScriptFunc::EQ_NOTIFY_ADMISSION()],
            "EQ_IsReady" => ["s", ScriptFunc::EQ_IS_READY()],
            "EQ_MoveToRoom" => ["ssnn", ScriptFunc::EQ_MOVE_TO_ROOM()],
            "EQ_ReturnToPreSpace" => ["s", ScriptFunc::EQ_RETURN_TO_PRE_SPACE()],
            "EQ_MoveToLobby" => ["ssn", ScriptFunc::EQ_MOVE_TO_LOBBY()],
            "Lobby_Make" => ["s", ScriptFunc::LOBBY_MAKE()],
            "Lobby_UserCount" => ["s", ScriptFunc::LOBBY_USER_COUNT()],
            "Lobby_NotifyAdmission" => ["s", ScriptFunc::LOBBY_NOTIFY_ADMISSION()],
            "Lobby_IsReady" => ["sn", ScriptFunc::LOBBY_IS_READY()],
            "Lobby_MoveToRoom" => ["ssnn", ScriptFunc::LOBBY_MOVE_TO_ROOM()],
            "Lobby_ReturnToPreSpace" => ["s", ScriptFunc::LOBBY_RETURN_TO_PRE_SPACE()],
            "Lobby_Init" => ["s", ScriptFunc::LOBBY_INIT()],
            "clear" => [".", ScriptFunc::CLEARDIALOG()],
            "Navigation_Active" => ["snnnnn?", ScriptFunc::NAVIGATION_ACTIVE()],
            "Active_Montransform" => ["n", ScriptFunc::ACTIVE_MONTRANSFORM()],
            "DeActive_Montransform" => ["n", ScriptFunc::DEACTIVE_MONTRANSFORM()],
            "SetAutoSpell2" => ["nnnn", ScriptFunc::SETAUTOSPELL2()],
            "SetAutoSpell2_MLEATKED" => ["nnnn", ScriptFunc::SETAUTOSPELL2_MLEATKED()],
            "SetAutoSpell2_MLEATKED_Short" => ["nnnn", ScriptFunc::SETAUTOSPELL2_MLEATKED_SHORT()],
            "SetAutoSpell2_MLEATKED_Long" => ["nnnn", ScriptFunc::SETAUTOSPELL2_MLEATKED_LONG()],
            "SetAutoSpell2_MAGATKED" => ["nnnn", ScriptFunc::SETAUTOSPELL2_MAGATKED()],
            "SetAutoSpell2_MLEATK" => ["nnnn", ScriptFunc::SETAUTOSPELL2_MLEATK()],
            "SetAutoSpell2_RANGEATK" => ["nnnn", ScriptFunc::SETAUTOSPELL2_RANGEATK()],
            "ResetAutoSpell2" => ["nn", ScriptFunc::RESETAUTOSPELL2()],
            "ResetAutoSpell2_MLEATKED" => ["nn", ScriptFunc::RESETAUTOSPELL2_MLEATKED()],
            "ResetAutoSpell2_MLEATKED_Short" => ["nn", ScriptFunc::RESETAUTOSPELL2_MLEATKED_SHORT()],
            "ResetAutoSpell2_MLEATKED_Long" => ["nn", ScriptFunc::RESETAUTOSPELL2_MLEATKED_LONG()],
            "ResetAutoSpell2_MAGATKED" => ["nn", ScriptFunc::RESETAUTOSPELL2_MAGATKED()],
            "ResetAutoSpell2_MLEATK" => ["nn", ScriptFunc::RESETAUTOSPELL2_MLEATK()],
            "ResetAutoSpell2_RANGEATK" => ["nn", ScriptFunc::RESETAUTOSPELL2_RANGEATK()],
            "PauseAutoSpell2" => ["nn", ScriptFunc::PAUSEAUTOSPELL2()],
            "PauseAutoSpell2_MLEATKED" => ["nn", ScriptFunc::PAUSEAUTOSPELL2_MLEATKED()],
            "PauseAutoSpell2_MLEATKED_Short" => ["nn", ScriptFunc::PAUSEAUTOSPELL2_MLEATKED_SHORT()],
            "PauseAutoSpell2_MLEATKED_Long" => ["nn", ScriptFunc::PAUSEAUTOSPELL2_MLEATKED_LONG()],
            "PauseAutoSpell2_MAGATKED" => ["nn", ScriptFunc::PAUSEAUTOSPELL2_MAGATKED()],
            "PauseAutoSpell2_MLEATK" => ["nn", ScriptFunc::PAUSEAUTOSPELL2_MLEATK()],
            "PauseAutoSpell2_RANGEATK" => ["nn", ScriptFunc::PAUSEAUTOSPELL2_RANGEATK()],
            "ResumeAutoSpell2" => ["nn", ScriptFunc::RESUMEAUTOSPELL2()],
            "ResumeAutoSpell2_MLEATKED" => ["nn", ScriptFunc::RESUMEAUTOSPELL2_MLEATKED()],
            "ResumeAutoSpell2_MLEATKED_Short" => ["nn", ScriptFunc::RESUMEAUTOSPELL2_MLEATKED_SHORT()],
            "ResumeAutoSpell2_MLEATKED_Long" => ["nn", ScriptFunc::RESUMEAUTOSPELL2_MLEATKED_LONG()],
            "ResumeAutoSpell2_MAGATKED" => ["nn", ScriptFunc::RESUMEAUTOSPELL2_MAGATKED()],
            "ResumeAutoSpell2_MLEATK" => ["nn", ScriptFunc::RESUMEAUTOSPELL2_MLEATK()],
            "ResumeAutoSpell2_RANGEATK" => ["nn", ScriptFunc::RESUMEAUTOSPELL2_RANGEATK()],
            "CheckJobGroup" => ["n", ScriptFunc::CHECK_JOB_GROUP()],
            "GetPremiumCampaign_Grade" => ["n", ScriptFunc::GETPREMIUMCAMPAIGN_GRADE()],
            "GetBaseJob" => [".", ScriptFunc::GET_BASE_JOB()],
            "GetCategoryJob" => ["n", ScriptFunc::GET_CATEGORY_JOB()],
            "EQ_JobControl" => ["sn", ScriptFunc::EQ_JOB_CONTROL()],
            "AddMDamage_Property" => ["nnn", ScriptFunc::ADDMDAMAGE_PROPERTY()],
            "SubMDamage_Property" => ["nnn", ScriptFunc::SUBMDAMAGE_PROPERTY()],
            "Lobby_CampCode" => ["ssnn", ScriptFunc::LOBBY_CAMPCODE()],
            "InsertAliveMember" => [".", ScriptFunc::INSERT_ALIVE_MEMBER()],
            "EQ_UserInfo_Init" => [".", ScriptFunc::EQ_USERINFO_INIT()],
            "AddQuestInfo" => ["nnn", ScriptFunc::ADDQUESTINFO()],
            "AddQuestInfo2" => ["nnnsnn", ScriptFunc::ADDQUESTINFO2()],
            "SetQuestJob" => ["nn", ScriptFunc::SETQUESTJOB()],
            "SetQuestSex" => ["nn", ScriptFunc::SETQUESTSEX()],
            "SetQuestLevel" => ["nnn", ScriptFunc::SETQUESTLEVEL()],
            "SetQuestJobLevel" => ["nnn", ScriptFunc::SETQUESTJOBLEVEL()],
            "SetQuestItem" => ["nnn", ScriptFunc::SETQUESTITEM()],
            "SetQuestHomunLevel" => ["nnn", ScriptFunc::SETQUESTHOMUNLEVEL()],
            "SetQuestHomunType" => ["nn", ScriptFunc::SETQUESTHOMUNTYPE()],
            "SetQuestQuestItem" => ["nnns?", ScriptFunc::SETQUESTQUESTITEM()],
            "IsSetQuest" => ["n", ScriptFunc::IS_SET_QUEST()],
            "IsLowLevelSiegeJob" => [".", ScriptFunc::IS_LOWLEVEL_SIEGE_JOB()],
            "ResetBuffLowLevelSiege" => [".", ScriptFunc::RESET_BUFF_LOWLEVEL_SIEGE()],
            "EQ_MoveToLobbyJobMatching" => ["sss", ScriptFunc::EQ_MOVE_TO_LOBBY_JOB_MATCHING()],
            "GetYear" => [".", ScriptFunc::GET_YEAR()],
            "GetMonth" => [".", ScriptFunc::GET_MONTH()],
            "GetDay" => [".", ScriptFunc::GET_DAY()],
            "GetWeekDay" => [".", ScriptFunc::GET_WEEK_DAY()],
            "MergeItem" => [".", ScriptFunc::MERGE_ITEM()],
            "dialog2" => ["sn", ScriptFunc::DIALOG2()],
            "wait2" => ["n", ScriptFunc::WAITDIALOG2()],
            "store_V2" => ["ns", ScriptFunc::STORE_V2()],
            "CallFalcon" => [".", ScriptFunc::CALLFALCON()],
            "SetPRNpcWinner" => ["nn", ScriptFunc::SETPRNPCWINNER()],
            "PD_SubScription" => ["s", ScriptFunc::PD_SUBSCRIPTION()],
            "PD_Enter" => ["s", ScriptFunc::PD_ENTER()],
            "SetQuestQuest" => ["nnn", ScriptFunc::SETQUESQUEST()],
            "TalkShow" => ["ss", ScriptFunc::TALK_SHOW()],
            "RentItem" => ["nn", ScriptFunc::RENT_ITEM()],
            "ProvideExp" => ["nnnn", ScriptFunc::PROVIDE_EXP()],
            "GetMonsterHP" => ["s", ScriptFunc::GET_MONSTER_HP()],
            "MonsterTalkShow" => ["ss", ScriptFunc::MONSTER_TALK_SHOW()],
            "CheckJobGroup2" => [".", ScriptFunc::CHECKJOBGROUP2()],
            "JoinClan" => [".", ScriptFunc::JOIN_CLAN()],
            "LeaveClan" => [".", ScriptFunc::LEAVE_CLAN()],
            "SetMonsterHP" => ["sn", ScriptFunc::SET_MONSTER_HP()],
            "ItemDown2" => ["nnnnnnnn", ScriptFunc::ITEMDOWN2()],
            "GetElapsedTimer" => [".", ScriptFunc::GET_ELAPSED_TIMER()],
            "IsEnableNpc" => ["s", ScriptFunc::IS_ENABLE_NPC()],
            "GetNpcMonster_X" => ["ss", ScriptFunc::GET_NPC_MONSTER_X()],
            "GetNpcMonster_Y" => ["ss", ScriptFunc::GET_NPC_MONSTER_Y()],
            "DelayTime" => ["n", ScriptFunc::DELAY_TIME()],
            "SetClanMaster" => [".", ScriptFunc::SET_CLAN_MASTER()],
            "SavePosition" => [".", ScriptFunc::SAVEPOSITION()],
            "MovotoSavePosition" => [".", ScriptFunc::MOVETOSAVEPOSITION()],
            "MD_Trader" => ["ssnnnn", ScriptFunc::MDUNGEON_TRADER(), true],
            "SetMonsterHP2" => ["ssn", ScriptFunc::SET_MONSTER_HP2()],
            "GetRankingPoint" => ["n", ScriptFunc::GET_RANKING_POINT()],
            "IncRankingPoint" => ["nn", ScriptFunc::INC_RANKING_POINT()],
            "DecRankingPoint" => ["nn", ScriptFunc::DEC_RANKING_POINT()],
            "NPC_Montransform" => ["nn", ScriptFunc::NPC_MONTRANSFORM()],
            "PartyMapMove" => ["nsnn", ScriptFunc::PARTY_MAP_MOVE()],
            "IsPremium" => [".", ScriptFunc::IS_PREMIUM()],
        ];

        foreach ($declares as $name => $data)
        {
            [$params, $func_num] = $data;
            $block_check = false;
            if (count($data) > 2) {
                $block_check = $data[2];
            }
            $ok = $this->stdDeclare($name, $params, $func_num, $block_check);
            assert($ok, "ScriptCompiler::LoadV2: invalid declare for `$name` p:`$params` num:`{$func_num->getValue()}`, b:`$block_check`!");
        }

        return true;
    }

    private function reset() : void {
        if ($this->compiler)
        {
            $this->compiler->Release();
            unset($this->compiler);
        }
    }

    private function run(string $filename, int $date=0) : bool {
        Log::info("ScriptCompiler::Run: Loading... $filename");
        $ok = $this->compiler->Run($filename, $date);
        assert($ok, "ScriptCompiler::Run: Can't run or old version $filename $date");

        return true;
    }
}