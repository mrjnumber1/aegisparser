<?php


namespace AegisParser;


final class ITPEvent extends ITP
{
    public function __construct() { parent::__construct(); $this->type = ItemType::EVENT(); }
}