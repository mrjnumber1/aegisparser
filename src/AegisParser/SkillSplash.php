<?php
/** @noinspection PhpNonStrictObjectEqualityInspection */
/** @noinspection TypeUnsafeComparisonInspection */


namespace AegisParser;


use Ds\Pair;

final class SkillSplash
{
    private SkillSplashType $type;
    private array $range;
    private int $max_lv;

    public function __construct(SkillSplashType $type, array $values, int $max_level = SkillTypeInfo::MAX_LEVEL)
    {
        $this->max_lv = $max_level;
        $this->SetType($type);
        if ($this->type != SkillSplashType::NONE()) {
            $this->SetRange($values);
        }
    }

    public function SetType(SkillSplashType $type) : void {
        $this->type = $type;
    }

    public function GetType() : SkillSplashType {
        return $this->type;
    }

    public function SetRange(array $values) : void {
        assert($this->GetType() != SkillSplashType::NONE(), "SkillSplash::SetRange: Used with no splash data set!");
        $count = count($values);
        assert($count, "SkillSplash::SetRange: Received empty array!");

        if ($this->GetType() == SkillSplashType::SQUARE()) {
            assert($count <= $this->max_lv,"SkillSplash::SetRange: Square splash got too many entries (got $count)");
            $this->populateRangeArray($values);
            return;
        }

        assert($count <= 2*$this->max_lv,"SkillSplash::SetRange: Line splash got too many entries (got $count)");
        $this->populateRangeArray($values);
    }

    private function populateRangeArray(array $values) : void {
        $this->range = [];
        $i = 0;
        foreach ($values as $value) {
            assert(is_int($value), "SkillSplash::SetRange: Received non-numeric splash value!");
            $this->range[$i] = $value;
            $i++;
        }

        // square, populate the entire array
        if ($i === $this->max_lv || $i === 2*$this->max_lv) {
            return;
        }

        assert($i === 1 || $i === 2, "SkillSplash::SetRange: Need more data! got $i");
        // only got 1 entry, so fill the entire thing the rest of the way
        $step = 1;
        if ($i === 2) {
            $step = 2;
        }

        for (; $i < $this->max_lv*$step; $i += $step) {
            $this->range[$i] = $this->range[$i - $step];
            if ($step === 2) {
                $this->range[$i+1] = $this->range[$i-1];
            }
        }

    }

    public function GetSplash(int $level) : Pair {
        assert ($level <= SkillTypeInfo::MAX_LEVEL, "SkillSplash::GetSplash: Invalid skill level $level");

        if ($this->GetType() == SkillSplashType::NONE()) {
            return new Pair(0, 0);
        }
        assert(count($this->range), "SkillSplash::GetSplash: Splash has not been populated yet!");

        if ($this->GetType() == SkillSplashType::SQUARE()) {
            return new Pair($this->range[$level-1], $this->range[$level-1]);
        }

        return new Pair($this->range[2*($level-1)], $this->range[2*($level-1)+1]);
    }
}