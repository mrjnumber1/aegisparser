<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static EquipPosFlag HEAD()
 * @method static EquipPosFlag RARM()
 * @method static EquipPosFlag ROBE()
 * @method static EquipPosFlag ACCESSORY1()
 * @method static EquipPosFlag BODY()
 * @method static EquipPosFlag LARM()
 * @method static EquipPosFlag SHOES()
 * @method static EquipPosFlag ACCESSORY2()
 * @method static EquipPosFlag HEAD2()
 * @method static EquipPosFlag HEAD3()
 * @method static EquipPosFlag COSTUME_HEAD2()
 * @method static EquipPosFlag COSTUME_HEAD3()
 * @method static EquipPosFlag COSTUME_HEAD()
 * @method static EquipPosFlag COSTUME_ROBE()
 * @method static EquipPosFlag COSTUME_FLOOR()
 * @method static EquipPosFlag AMMO()
 * @method static EquipPosFlag ARMOR_SHADOW()
 * @method static EquipPosFlag WEAPON_SHADOW()
 * @method static EquipPosFlag SHIELD_SHADOW()
 * @method static EquipPosFlag SHOES_SHADOW()
 * @method static EquipPosFlag R_ACCESSORY_SHADOW()
 * @method static EquipPosFlag L_ACCESSORY_SHADOW()
 */
final class EquipPosFlag extends Enum
{
    protected const HEAD = 1;
    protected const RARM = 2;
    protected const ROBE = 4;
    protected const ACCESSORY1 = 8;
    protected const BODY = 0x10;
    protected const LARM = 0x20;
    protected const SHOES = 0x40;
    protected const ACCESSORY2 = 0x80;
    protected const HEAD2 = 0x100;
    protected const HEAD3 = 0x200;
    protected const COSTUME_HEAD2 = 0x400;
    protected const COSTUME_HEAD3 = 0x800;
    protected const COSTUME_HEAD = 0x1000;
    protected const COSTUME_ROBE = 0x2000;
    protected const COSTUME_FLOOR = 0x4000;
    protected const AMMO = 0x8000;
    protected const ARMOR_SHADOW = 0x10000;
    protected const WEAPON_SHADOW = 0x20000;
    protected const SHIELD_SHADOW = 0x40000;
    protected const SHOES_SHADOW = 0x80000;
    protected const R_ACCESSORY_SHADOW = 0x1000000;
    protected const L_ACCESSORY_SHADOW = 0x2000000;
}