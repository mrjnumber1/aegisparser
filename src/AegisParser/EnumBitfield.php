<?php


namespace AegisParser;


use bitfield\Bitfield;
use MyCLabs\Enum\Enum;

class EnumBitfield implements IEnumBitfield
{
    protected Bitfield $field;
    protected array $keys;
    protected string $class;
    protected Enum $class_type;
    protected bool $zero_start;


    public function __construct(string $enum_class, bool $is_zero_start = false)
    {
        $this->class = $enum_class;
        $this->zero_start = $is_zero_start;

        $this->class_type = new $this->class($this->zero_start ? 0 : 1);

        assert(is_subclass_of($this->class_type, Enum::class, false), "Passed a non-Enum type $this->class");

        $this->keys = $this->class_type::keys();
        assert(count($this->keys) > 0, "Passed a blank enum to Bitfield!");

        if ($this->zero_start) {
            array_shift($this->keys);
        }

        $this->field = new Bitfield($this->keys);
    }

    private function checkFlagType(Enum $flag, string $func) : void
    {
        assert(is_a($flag, $this->class_type::class, false), "EnumBitfield::$func wrong flag type passed `".$flag::class."`");
        assert(in_array($flag->getKey(), $this->keys, false), "EnumBitfield::$func flag passed has invalid key {$flag->getKey()}");
    }

    public function AddFlag(Enum $flag): void
    {
        $this->checkFlagType($flag, __FUNCTION__);
        $this->field->on($flag->getKey());
    }

    public function AddFlags(array $flags): void
    {
        foreach ($flags as $flag) {
            $this->checkFlagType($flag, __FUNCTION__);
            $this->AddFlag($flag);
        }
    }

    public function ClearFlags() : void
    {
        $this->field->setValue(0);
    }

    public function GetFlags(): array
    {
        if ($this->field->getValue() === 0) {
            return [];
        }

        $fields = $this->keys;
        if ($this->zero_start) {
            array_shift($fields);
        }
        $flags = [];
        foreach ($fields as $i => $k) {
            if ($this->field->isOn($k)) {
                $n = 1 << $i;
                if ($this->zero_start) {
                    $n = 1 << ($i-1);
                }
                $flags[] = $this->class_type::from($n);
            }
        }

        return $flags;
    }

    public function RemoveFlag(Enum $flag): void
    {
        $key = $flag->getKey();
        $this->checkFlagType($flag, __FUNCTION__);
        assert(in_array($key, $this->keys, false), "EnumBitfield::RemoveFlag Mismatched flag type!");

        $this->field->off($key);
    }

    public function RemoveFlags(array $flags): void
    {
        foreach ($flags as $flag) {
            $this->checkFlagType($flag, __FUNCTION__);
            $this->RemoveFlag($flag);
        }
    }

    public function ToggleFlag(Enum $flag): void
    {
        $this->checkFlagType($flag, __FUNCTION__);
        if ($this->Hasflag($flag)) {
            $this->RemoveFlag($flag);
        } else {
            $this->AddFlag($flag);
        }
    }

    public function SetFlag(Enum $flag) : void {
        $this->ClearFlags();
        $this->checkFlagType($flag, __FUNCTION__);
        $this->AddFlag($flag);
    }

    public function SetFlags(array $flags) : void {
        $this->ClearFlags();
        $this->AddFlags($flags);
    }

    public function HasFlag(Enum $flag): bool
    {
        $this->checkFlagType($flag, __FUNCTION__);
        return $this->field->isOn($flag->getKey());
    }

    public function FromNum(int $num): void
    {
        $this->ClearFlags();

        if ($num === 0) {
            return;
        }

        assert($num < (1<<count($this->keys)), "EnumBitfield::FromNum num `$num` received was too high!");

        foreach ($this->keys as $i => $name) {
            if (((1<<$i) & $num) !== 0) {
                $this->field->on($name);
            }
        }

        assert($this->GetNum() === $num, "EnumBitfield::FromNum mismatch! requested `$num` got `{$this->GetNum()}`");
    }

    public function GetNum() : int {
        return $this->field->getValue();
    }

    public function Equals(IEnumBitfield $rhs): bool
    {
        return $this->GetNum() === $rhs->GetNum();
    }

    public function EqualsFlag(Enum $flag): bool
    {
        $this->checkFlagType($flag, __FUNCTION__);
        return $this->GetNum() === $flag->getValue();
    }

    public function Contains(IEnumBitfield $rhs): bool
    {
        return ($this->GetNum() & $rhs->GetNum()) === $rhs->GetNum();
    }
}