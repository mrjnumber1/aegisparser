<?php


namespace AegisParser;


final class ITPBow extends ITPWeapon
{
    public function __construct() { parent::__construct(); $this->type = ItemType::BOW(); }
}
