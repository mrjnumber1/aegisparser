<?php
namespace AegisParser;


final class MonParameter extends NpcParameter
{

    public int $Level = 0;
    public int $HP = 0;
    public int $SP = 0;
    public int $ARan = 0;
    public int $STR=0;
    public int $INT=0;
    public int $VIT=0;
    public int $DEX=0;
    public int $AGI=0;
    public int $LUK=0;
    public int $ATK1=0;
    public int $ATK2=0;
    public int $DEF=0;
    public int $MDEF=0;
    public int $BaseExp=0;
    public int $JobEXP=0;
    public int $MVPExp = 0;
    //public int $inc=0;
    public int $AS=0;
    public int $ES=0;
    public int $RechargeTime=0;
    public int $AttackMT=0;
    public int $AttackedMT=0;
    public int $MSpeed=0;
    public CharScale $Size;
    public Race $Race;
    public ElementProperty $Property;
    public MobClass $Class;
    public NpcAI $AI;
    public MobAttrBitfield $Attr;
    public MonMakingItem $ItemInfo;
    public MonMakingItem $MVPItemInfo;
    public MonSpawnInfo $SlaveSpawn;
    public SummonMobList $SlaveSummon;
    private array $skillList;

    public function SetSkillList(array $list) : void {
        foreach ($list as $item) {
            assert(is_a($item, MobSkillInfo::class, false), "MonParameter::SetSkillList invalid entry passed!");
        }
        $this->skillList = $list;
    }
    public function GetSkillList() : array {
        return $this->skillList;
    }

    public function __construct()  {
        $this->Size = CharScale::SMALL();
        $this->Race = Race::NOTHING();
        $this->Property = ElementProperty::NOTHING();
        $this->Class = MobClass::NONE();
        $this->AI = NpcAI::WARP_NPC();
        $this->Attr = new MobAttrBitfield();
        $this->ItemInfo = new MonMakingItem();
        $this->MVPItemInfo = new MonMakingItem();

        $this->skillList = [];
    }

}