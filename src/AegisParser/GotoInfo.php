<?php


namespace AegisParser;


final class GotoInfo
{
    private int $pos;
    private int $id;

    public function __construct(int $pos, int $id) {
        $this->SetPos($pos);
        $this->SetId($id);
    }
    public function SetPos(int $pos) : void {
        $this->pos = $pos;
    }
    public function SetId(int $id) : void {
        $this->id = $id;
    }
    public function GetPos() : int {
        return $this->pos;
    }
    public function GetId() : int {
        return $this->id;
    }
}