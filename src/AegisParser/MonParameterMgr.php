<?php
namespace AegisParser;

use Application\Log;
use Ds\Map;
use Ds\Set;

final class MonParameterMgr
{
    private Map $mobList; // map<mob_id, MonParameter>
    private Map $mobChat; // map<chat_id, MonChat>
    // We back up the string of SPRITE_NAME that isn't a valid php enum name
    private Map $sprite2IDList; // map<SPRITE_NAME, mob_id>
    private Set $classChangeList; // Set<JobType>

    private const SPAWNSETINFO_DATE = 20020627;

    public function __construct(string $zone_path, array $DB) {
        $this->sprite2IDList = new Map();
        $this->mobList = new Map();
        $this->mobChat = new Map();
        $this->classChangeList = new Set();

        $this->init($zone_path, $DB);
    }

    private function init(string $zone_path, array $DB) : void {
        Log::info("MonParameterMgr::init start...");

        $this->loadMonParameter($DB['monparameter']);
        Log::info("MonParameterMgr::loadMonParameter complete. Added {$this->mobList->count()} monsters!");

        $num = $this->loadMonMakingItem($DB['monmakingitem']);
        Log::info("MonParameterMgr::loadMonMakingItem complete for $num monsters)!");

        $num = $this->loadMvpInfo($DB['mvp']);
        Log::info("MonParameterMgr::loadMvpInfo complete for $num monsters!");

        $num = $this->loadSpawnInfo($zone_path);
        Log::info("MonParameterMgr::loadSpawnInfo complete for $num monsters!");

        $num = $this->loadSummonInfo($zone_path);
        Log::info("MonParameterMgr::loadSummonInfo complete for $num monsters!");

        $this->loadMobChat($zone_path);
        Log::info("MonParameterMgr::loadMobChat complete! {$this->mobChat->count()} messages!");

        $num = $this->loadNpcAddSkillInfo($zone_path);
        Log::info("MonParameterMgr::loadNpcAddSkillInfo complete for $num monsters!");

        $this->loadClassChangeList($zone_path);
        Log::info("MonParameterMgr::loadClassChangeList complete with {$this->classChangeList->count()} entries!");

    }

    // Some event monsters are a waste of entries, so we blacklist them
    private function isBlackListMob(string $name) : bool {
        $blacklist = [
            // BULLET mobs are sprites used for ranged monsters, no data associated
            "contain_str" => [
                "_BULLET_"
            ],
            // EVENT_ mobs just clog up the list
            // META_ are NPC_METAMORPHOSIS abandoned mobs
            // M_ and MER_ are usually mercenaries
            // A_ are unknown event mobs
            "start_str" => [
                "A_",
                "E_",
                "EASTER_EGG",
                "EVENT_",
                "L_",
                "META_",
                "M_",
                "MER_",
            ],
            "end_str" => [
                    "_BULLET",
            ],
            "job" => [
                "_4_F_SIGN1",
                "CONCEIVE_PECOPECO",
                "EFFECT_NPC",
                "FILAMENTOUS",
                "G_DETALE",
                "G_INCANTATION_SAMURA",
                "HUGELING",
                "MIME_MONKEY",
                "PROVOKE_YOYO",
                "SMOKING_ORC",
                "SOLDIER_ANDRE",
                "SOLDIER_DENIRO",
                "SOLDIER_PIERE",
                "TURTLE",
            ],
        ];
        if (in_array($name, $blacklist["job"], false)) {
            return true;
        }
        foreach ($blacklist["contain_str"] as $badstr) {
            if (str_contains($name, $badstr)) {
                return true;
            }
        }
        foreach ($blacklist["start_str"] as $badstr) {
            if (str_starts_with($name, $badstr)) {
                return true;
            }
        }
        foreach ($blacklist["end_str"] as $badstr) {
            if (str_ends_with($name, $badstr)) {
                return true;
            }
        }
        return false;
    }

    private function loadMonParameter(array $entries) : void {

        foreach ($entries as $entry) {
            $name = JobType::MakeValidName($entry['Name']);

            if ($this->isBlackListMob($name)) {
                //Log::info("MonParameterMgr::loadMonParameter: Blacklisted mob {$name} skipped.");
                continue;
            }


            if (!JobType::HasKey($name)) {
                Log::warning("Invalid Job Type in monparameter name:`$name`. Discarding...");
                continue;
            }

            $jt = JobType::$name();
            if (!JobType::IsMob($jt)) {
                $npc = new NpcParameter();
                $npc->ID = $jt->getValue();
                $npc->Name = substr($entry['Name'], 0, 23);
                $this->sprite2IDList->put($npc->Name, $npc->ID);
                $this->mobList->put($npc->ID, $npc);
                continue;
            }

            $mob = new MonParameter();
            $mob->ID = $jt->getValue();
            $mob->Name = substr($entry['Name'], 0, 23);
            $mob->Level = $entry['LV'];
            $mob->HP = $entry['HP'];
            $mob->SP = $entry['SP'];
            $mob->STR = $entry['str'];
            $mob->AGI = $entry['agi'];
            $mob->VIT = $entry['vit'];
            $mob->INT = $entry['int'];
            $mob->DEX = $entry['dex'];
            $mob->LUK = $entry['luk'];
            $mob->ATK1 = $entry['atk1'];
            $mob->ATK2 = $entry['atk2'];
            $mob->DEF = $entry['def'];
            $mob->MDEF = $entry['mdef'];
            $mob->BaseExp = $entry['exp'];
            $mob->JobEXP = $entry['jexp'];
            $mob->ARan = $entry['aRan'];
            $mob->AS = $entry['as'];
            $mob->ES = $entry['es'];
            $mob->MSpeed = $entry['Mspeed'];
            $mob->RechargeTime = $entry['rechargeTime'];
            $mob->AttackedMT = $entry['attackedMT'];
            $mob->AttackMT = $entry['attackMT'];
            $mob->Property = ElementProperty::from($entry['property']);
            $mob->Size = CharScale::from($entry['scale']);
            $mob->Race = Race::from($entry['race']);

            $this->sprite2IDList->put($mob->Name, $mob->ID);
            $this->mobList->put($mob->ID, $mob);
        }
    }

    private function loadMonMakingItem(array $entries) : int {
        $count = 0;
        foreach ($entries as $entry) {
            $name = JobType::MakeValidName($entry['Name']);

            if ($this->isBlackListMob($name)) {
                //Log::info("MonParameterMgr::loadMonParameter: Blacklisted mob {$name} skipped.");
                continue;
            }

            if (!$this->sprite2IDList->hasKey($entry['Name'])) {
                Log::warning("Invalid monster `$name` in monmakingitem . Discarding...");
                continue;
            }
            $id = $this->sprite2IDList[$entry['Name']];
            if (!$this->mobList->hasKey($id)) {
                Log::warning("MonParameterMgr::loadMonMakingItem: Monster `$name` ($id) found but no param data loaded! Discarding...");
                continue;
            }

            $param = $this->mobList[$id];

            $list = [];
            for ($i=1; $i <= MonMakingItem::MAX_DROPS; ++$i)
            {
                $item = $entry["item$i"];
                $rate = $entry["percent$i"];
                $group = $entry["group$i"] ?? MonItemInfo::DEFAULT_GROUP;
                if ($rate < 1) {
                    continue;
                }

                if (!ItemID::HasKey($item)) {
                    Log::warning("MonParameterMgr::loadMonMakingItem: Monster `$name` ($id) has invalid item `$item`! Discarding...");
                    continue;
                }

                $drop = new MonItemInfo();
                $drop->ITID = ItemID::Get($item);
                $drop->Rate = $rate;
                $drop->Group = $group;

                $list[] = $drop;
            }

            $param->ItemInfo = new MonMakingItem();
            $param->ItemInfo->SetDrops($list);

            $this->mobList->put($id, $param);
            $count++;
        }

        return $count;
    }

    private function loadMvpInfo(array $entries) : int {
        $count = 0;

        foreach ($entries as $entry) {
            $name = JobType::MakeValidName($entry['name']);

            if ($this->isBlackListMob($name)) {
                //Log::info("MonParameterMgr::loadMvpInfo: Blacklisted mob {$name} skipped.");
                continue;
            }
            if (!$this->sprite2IDList->hasKey($entry['name'])) {
                Log::warning("Invalid monster `$name` in mvp . Discarding...");
                continue;
            }

            $id = $this->sprite2IDList[$entry['name']];
            if (!$this->mobList->hasKey($id)) {
                Log::warning("MonParameterMgr::loadMvpInfo: Monster `$name` ($id) found but no param data loaded! Discarding...");
                continue;
            }

            $param = $this->mobList[$id];

            $param->MVPExp = (int)($param->BaseExp * $entry['expPercent'] / 10000);

            $list = [];
            for ($i=1; $i <= MonMakingItem::MAX_MVP_DROPS; ++$i)
            {
                $item = $entry["itemName$i"];
                $rate = $entry["itemPercent$i"];

                if ($rate < 1) {
                    continue;
                }

                if (!ItemID::HasKey($item)) {
                    Log::warning("MonParameterMgr::loadMvpInfo: MVP `$name` ($id) has invalid item `$item`! Discarding...");
                    continue;
                }

                $drop = new MonItemInfo();
                $drop->ITID = ItemID::Get($item);
                $drop->Rate = $rate;

                $list[] = $drop;
            }

            $param->MVPItemInfo = new MonMakingItem();
            $param->MVPItemInfo->SetDrops($list);

            $count++;
            $this->mobList->put($id, $param);
        }

        return $count;
    }

    private function loadFileContents(string $filename) : array {
        $file = $filename;
        if (!file_exists($file)) {
            assert(false, "MonParameterMgr::loadFileContents: required file `$filename` not found!");
        }

        $contents = file_get_contents($file);
        assert ($contents !== false, "MonParameterMgr::loadFileContents: Failed to read '$filename'.");

        $lines = [];
        foreach (explode("\n", $contents) as $line) {
            // remove all comments and trailing/leading whitespace
            $line = trim($line);
            if ($line === '' || str_starts_with($line, "//")) {
                continue;
            }
            if (str_contains($line, "//")) {
                $line = substr($line, 0, strpos($line, "//"));
            }

            // replace all tabs with normal spaces
            $line = str_replace("\t", " ", $line);
            // remove multiple whitespace with single whitespace
            $line = preg_replace("/\s+/", " ", $line);
            // trim starting/ending whitespace again for the end
            $line = trim($line);

            // add to our return :)
            $lines[] = $line;
        }

        return $lines;
    }

    private function loadClassChangeList(string $zone_path) : void {
        $lines = $this->loadFileContents($zone_path."ClassChange.txt");
        if (empty ($lines)) {
            return;
        }

        foreach ($lines as $line) {
            [$skid, $name] = explode(" ", $line);

            assert ($skid === SkillID::SA_CLASSCHANGE()->getKey(), "MonParameterMgr::loadClassChangeList invalid skill $skid");

            //$boss_id = $this->sprite2IDList[substr($name, 0, 23)];
            $boss = JobType::MakeValidName($name);

            if ($this->isBlackListMob($boss)) {
                //Log::info("MonParameterMgr::loadClassChangeList: Blacklisted boss $boss found. Skipping...");
                continue;
            }

            $this->classChangeList->add(JobType::$boss());
        }
    }

    private function loadSpawnInfo(string $zone_path) : int {
        $lines = $this->loadFileContents($zone_path."SpawnSetInfo.scp");
        if (empty($lines)) {
            return 0;
        }

        $count = 0;
        $date = 0;
        foreach ($lines as $line) {
            $line = trim($line);

            if (!$date) {
                $date = (int)$line;
                assert($date === self::SPAWNSETINFO_DATE, "MonParameterMgr::loadSpawnInfo: SpawnSetInfo.scp date mismatch! Got $date");
                continue;
            }

            [$master, $slave, $ai, $slave_count] = explode(" ", $line);

            assert($ai === "MONSTER_TYPE_24", "MonParameterMgr::loadSpawnInfo: unexpected AI received `$ai`.");

            $master_id = $this->sprite2IDList[substr($master, 0, 23)];

            $master = JobType::MakeValidName($master);
            $slave = JobType::MakeValidName($slave);
            if ($this->isBlackListMob($master) || $this->isBlackListMob($slave)) {
                Log::info("MonParameterMgr::loadSpawnInfo: Blacklisted master/slave $master/$slave found. Skipping...");
                continue;
            }

            $boss = $this->mobList[$master_id];
            $boss->SlaveSpawn = new MonSpawnInfo(JobType::$master(), JobType::$slave(), $slave_count);
            $this->mobList->put($master_id, $boss);
            $count++;
        }

        return $count;
    }

    private function loadSummonInfo(string $path) : int {
        $lines = $this->loadFileContents($path."SlaveSummonInfo.scp");
        if (empty($lines)) {
            return 0;
        }

        $count = 0;
        foreach ($lines as $line) {
            $data = preg_split("/\s/", $line);
            $master = array_shift($data);

            $master_sanitized = JobType::MakeValidName($master);
            if ($this->isBlackListMob($master_sanitized)) {
                //Log::info("MonParameterMgr::loadSummonInfo: Blacklisted master {$master} found. Skipping...");
                continue;
            }

            $master_id = $this->sprite2IDList[substr($master, 0, 23)];
            $master = $master_sanitized;

            $boss = $this->mobList[$master_id];

            $max_slaves = 0;
            $slaves = [];

            foreach ($data as $slave_name) {
                if (is_numeric($slave_name)) {
                    $max_slaves = $slave_name;
                    break;
                }
                $slave_id = $this->sprite2IDList[substr($slave_name, 0, 23)];
                assert($this->mobList->hasKey($slave_id), "MonParameterMgr::loadSummonInfo: `$master` has slave `$slave_name` with no parameters!");

                $slave_name = JobType::MakeValidName($slave_name);
                if ($this->isBlackListMob($slave_name)) {
                    //Log::info("MonParameterMgr::loadSummonInfo: `{$master}` has blacklisted slave `{$slave_name}`. Discarding!")
                    continue;
                }
                assert(JobType::HasKey($slave_name), "MonParameterMgr::loadSummonInfo: `$master` has invalid slave `$slave_name`");

                $slaves[] = JobType::$slave_name();
            }

            $boss->SlaveSummon = new SummonMobList(JobType::$master(), $max_slaves, $slaves);

            $this->mobList->put($master_id, $boss);
            $count++;
        }
        return $count;
    }

    private function loadMobChat(string $path) : void {
        $filename = $path."MobChat.txt";
        if (!file_exists($filename)) {
            return;
        }

        $lines = $this->loadFileContents($filename);
        if (empty($lines)) {
            return;
        }

        foreach ($lines as $line) {
            preg_match("/(\d+) ([\dA-Fa-f]+) (.*)/", $line, $matches);
            [, $id, $color, $text] = $matches;
            $id = (int)$id;
            $color = intval($color, 16);
            assert($color <= 0xFFFFFF, "Invalid color $color received!");

            $chat = new MonChat($color, $text);
            $this->mobChat->put($id, $chat);
        }
    }

    private function loadNpcAddSkillInfo(string $path) : int
    {
        // TODO: they converted this to lua by now, probably.
        $filename = $path . "NpcAddSkillInfo.txt";
        if (!file_exists($filename)) {
            return 0;
        }

        $lines = $this->loadFileContents($filename);
        if (empty($lines)) {
            return 0;
        }

        $count = 0;
        $last_mob = 0;
        $entry = $param = null;
        $list = [];

        foreach ($lines as $line) {
            $data = preg_split("/\s/", $line);

            $mob = array_shift($data);
            $mob_sanitized = JobType::MakeValidName($mob);
            if ($this->isBlacklistMob($mob_sanitized)) {
                //Log::info("Skipped loading skill data for blacklisted mob.");
                continue;
            }

            assert($this->sprite2IDList->hasKey($mob), "MonParameterMgr::loadNpcAddSkillInfo: Unknown mob `$mob` requested.");
            $mob_id = $this->sprite2IDList[$mob];

            if ($last_mob && $mob_id !== $last_mob) {
                // put the prior mob into the list
                $param = $this->mobList[$last_mob];
                $current_list = $param->GetSkillList();
                $merged = array_merge($current_list, $list);
                $param->SetSkillList($merged);
                $this->mobList->put($last_mob, $param);
                $count++;
                // and carry on..
                $param = null;
                $list = [];
            }

            $last_mob = $mob_id;

            $st_name = array_shift($data);
            assert(FSMState::isValidKey($st_name), "MonParameterMgr::loadNpcAddSkillInfo: `$mob` invalid FSM state `$st_name`!");
            $state = FSMState::$st_name();

            $sk_name = array_shift($data);
            assert(SkillID::isValidKey($sk_name), "MonParameterMgr::loadNpcAddSkillInfo: `$mob` in $st_name invalid skill ID `$sk_name`!");
            $skill = SKillID::$sk_name();

            $level = (int)array_shift($data);
            assert($level <= 10 && $level > 0, "MonParameterMgr::loadNpcAddSkillInfo: $mob@$sk_name ($st_name) invalid skill level $level!!");

            $rate = (int)array_shift($data);
            $cast_time = (int)array_shift($data);
            $cooldown = (int)array_shift($data);

            $cond_value = $cond = $emotion = $mob_chat = $new_ai = null;
            $breakable = true;

            if (count($data)) {
                $last = count($data);

                for ($i = 0; $i < $last; ++$i) {
                    if (str_starts_with($data[$i], "MONSTER_TYPE")) {
                        $key = $data[$i];
                        $new_ai = NpcAI::$key();
                        continue;
                    }

                    if ($data[$i] === "NO_DISPEL") {
                        $breakable = false;
                        continue;
                    }
                    if ($data[$i] === "SEND_CHAT") {
                        $i++;
                        $num = (int)$data[$i];
                        assert($this->mobChat->hasKey($num), "MonParameterMgr::loadNpcAddSkillInfo: $mob@$sk_name ($st_name) Requested invalid chat number $num!");
                        $mob_chat = $this->mobChat[$num];
                        continue;
                    }
                    if ($data[$i] === "SEND_EMOTICON") {
                        if ($mob === "AMON_RA" && $state === FSMState::RMOVE_ST()) {
                            Log::info("MonParameterMgr::loadNpcAddSkillInfo: Ignoring Gravity's broken AMON_RA@xxx RMOVE_ST SEND_EMOTICON entry...");
                            continue;
                        }
                        $i++;
                        $num = (int)$data[$i];
                        assert(Emotion::isValid($num), "MonParameterMgr::loadNpcAddSkillInfo: $mob@$sk_name ($st_name) Requested invalid emotion number $num!");
                        $emotion = Emotion::from($num);
                        continue;
                    }

                    $key = $data[$i];
                    assert(MobSkillCondition::isValidKey($key), "MonParameterMgr::loadNpcAddSkillInfo: $mob@$sk_name ($st_name) invalid condition key $data[$i]!");
                    $cond = MobSkillCondition::$key();

                    if (!MobSkillCondition::HasParameter($cond)) {
                        continue;
                    }
                    ++$i;
                    $cond_value = MobSkillCondition::ValidateParameter($cond, $data[$i]);
                }
            }

            $entry = new MobSkillInfo();
            $entry->SKID = $skill;
            $entry->State = $state;
            $entry->Level = $level;
            $entry->Rate = $rate;
            $entry->CastTime = $cast_time;
            $entry->Cooldown = $cooldown;
            $entry->Breakable = $breakable;
            $entry->NewAI = $new_ai;
            $entry->Chat = $mob_chat;
            $entry->Emotion = $emotion;
            $entry->Condition = $cond;
            $entry->Condition_Value = $cond_value;

            $list[] = $entry;
        }

        if (count($list)) {
            // put the prior mob into the list
            $param = $this->mobList[$last_mob];
            $current_list = $param->GetSkillList();
            $merged = array_merge($current_list, $list);
            $param->SetSkillList($merged);
            $this->mobList->put($last_mob, $param);
            $count++;
        }

        return $count;
    }

    public function Save(string $path) : void
    {
        assert(!$this->sprite2IDList->isEmpty(), "MonParameterMgr::Save: empty sprite list!");

        $this->sprite2IDList->sort();
        $it = $this->sprite2IDList->getIterator();

        $str = "";

        foreach ($it as $name => $id) {
            $str .= $name." ".$id."\r\n";
        }
        file_put_contents($path, $str);
    }


}