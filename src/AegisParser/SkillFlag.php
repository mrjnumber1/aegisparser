<?php


namespace AegisParser;


use MyCLabs\Enum\Enum;

/**
 * @method static SkillFlag NODISPEL()
 * @method static SkillFlag QUESTSKILL()
 * @method static SkillFlag NOREDUCT()
 * @method static SkillFlag CASTING_DISTANCE()
 * @method static SkillFlag UNABLE_TOBOSS()
 * @method static SkillFlag APPLY_PLUSRNG()
 * @method static SkillFlag IGNORE_SAFETYWALL()
 * @method static SkillFlag PLUS_TRAP_RANGE_RA_RESEARCHTRAP()
 * @method static SkillFlag DISTINCT_DAMAGE_FORMULA()
 * @method static SkillFlag CANCELCASTING_WHEN_EQUIP_CHANGED()
 * @method static SkillFlag EF_WARNING_PLANE()
 * @method static SkillFlag DISABLE_FIXEDCASTING_REDUCTION()
 */
final class SkillFlag extends Enum
{
    private const NODISPEL = 0x1;
    private const QUESTSKILL = 0x2;
    private const NOREDUCT = 0x4;
    private const CASTING_DISTANCE = 0x8;
    private const UNABLE_TOBOSS = 0x10;
    private const APPLY_PLUSRNG = 0x20;
    private const IGNORE_SAFETYWALL = 0x40;
    private const PLUS_TRAP_RANGE_RA_RESEARCHTRAP = 0x80;
    private const DISTINCT_DAMAGE_FORMULA = 0x100;
    private const UNKNOWNSKFLAGFUCKSSAKE = 0x200;
    private const CANCELCASTING_WHEN_EQUIP_CHANGED = 0x400;
    private const EF_WARNING_PLANE = 0x800;
    private const DISABLE_FIXEDCASTING_REDUCTION = 0x1000;
}