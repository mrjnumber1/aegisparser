<?php


namespace AegisParser;


use MyCLabs\Enum\Enum;

/**
 * Class SkillPattern
 * @package AegisParser
 * @method static SkillPattern NONE()
 * @method static SkillPattern DIRECTATTACK()
 * @method static SkillPattern ATTACKSPELL()
 */
final class SkillPattern extends Enum
{
    private const NONE = 0;
    private const DIRECTATTACK = 1; // physical
    private const ATTACKSPELL = 2; // magical
}