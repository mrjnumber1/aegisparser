<?php


namespace AegisParser;


class ITPWeapon extends ITPEquippable
{
    protected int $attackValue;
    protected int $MATK;
    protected int $attRange;
    protected int $itemLevel;
    protected WeaponType $class;
    protected ElementProperty $property;


    public function __construct() { parent::__construct(); $this->type = ItemType::WEAPON(); }

    public function IsRangedWeapon() : bool {
        switch ($this->GetType()) {
            case ItemType::GUN():
            case ItemType::BOW():
                return true;
        }
        switch ($this->GetClass()) {
            case WeaponType::INSTRUMENT():
            case WeaponType::WHIP():
                return true;
        }
        return false;
    }

    public function IsBothHandWeapon() : bool {
        return $this->GetLocation()->IsTwoHandedPos();
    }

    /**
     * @return int
     */
    public function GetAttackValue(): int
    {
        return $this->attackValue;
    }

    /**
     * @param int $attackValue
     */
    public function SetAttackValue(int $attackValue): void
    {
        $this->attackValue = $attackValue;
    }

    /**
     * @return int
     */
    public function GetMATK(): int
    {
        return $this->MATK;
    }

    /**
     * @param int $MATK
     */
    public function SetMATK(int $MATK): void
    {
        $this->MATK = $MATK;
    }

    /**
     * @return int
     */
    public function GetAttRange(): int
    {
        return $this->attRange;
    }

    /**
     * @param int $attRange
     */
    public function SetAttRange(int $attRange): void
    {
        $this->attRange = $attRange;
    }

    /**
     * @return int
     */
    public function GetItemLevel(): int
    {
        return $this->itemLevel;
    }

    /**
     * @param int $itemLevel
     */
    public function SetItemLevel(int $itemLevel): void
    {
        $this->itemLevel = $itemLevel;
    }

    /**
     * @return WeaponType
     */
    public function GetClass(): WeaponType
    {
        return $this->class;
    }

    /**
     * @param WeaponType $class
     */
    public function SetClass(WeaponType $class): void
    {
        $this->class = $class;
    }
    /**
     * @return ElementProperty
     */
    public function GetProperty(): ElementProperty
    {
        return $this->property;
    }

    /**
     * @param ElementProperty $property
     */
    public function SetProperty(ElementProperty $property): void
    {
        $this->property = $property;
    }

}