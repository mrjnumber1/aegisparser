<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static RETURN()
 * @method static ITEM()
 * @method static EVENT()
 * @method static GET()
 * @method static INC()
 * @method static DEC()
 * @method static ENABLESKILL()
 * @method static DISABLESKILL()
 * @method static SUMMON()
 * @method static SKILL()
 * @method static CREATEITEM()
 * @method static LOSTITEM()
 * @method static HEALHP()
 * @method static HEALSP()
 * @method static HEALALL()
 * @method static CONDITION()
 * @method static CURE()
 * @method static ADDEXTPARAM()
 * @method static SUBEXTPARAM()
 * @method static SETMAKABLE_WPNLIST()
 * @method static SETMAKABLE_MTLLIST()
 * @method static RACE_ADD_DAMAGE()
 * @method static RACE_SUB_DAMAGE()
 * @method static ATTR_ADD_TOLERACE()
 * @method static ATTR_SUB_TOLERACE()
 * @method static RACE_ADD_TOLERACE()
 * @method static RACE_SUB_TOLERACE()
 * @method static ADDDAMAGE_PROPERTY()
 * @method static SUBDAMAGE_PROPERTY()
 * @method static ADDDAMAGE_SIZE()
 * @method static SUBDAMAGE_SIZE()
 * @method static ADDDAMAGE_NAME()
 * @method static SUBDAMAGE_NAME()
 * @method static ADDDAMAGE_SKID()
 * @method static SUBDAMAGE_SKID()
 * @method static ADDDAMAGE_CRI()
 * @method static SUBDAMAGE_CRI()
 * @method static ADDSTATE_MLEATK()
 * @method static SUBSTATE_MLEATK()
 * @method static ADDSTATE_MLEATTACKED()
 * @method static SUBSTATE_MLEATTACKED()
 * @method static ADDHEALPERCENT_ITEM()
 * @method static SUBHEALPERCENT_ITEM()
 * @method static ADDHEALAMOUNT_KILL()
 * @method static SUBHEALAMOUNT_KILL()
 * @method static INDESTRUCTIBLE_ARMOR()
 * @method static INDESTRUCTIBLE_WEAPON()
 * @method static ADD_STATE_TOLERACE()
 * @method static SUB_STATE_TOLERACE()
 * @method static BODY_ATTRIBUTE()
 * @method static SUB_RANGE_ATTACKDAMAGE()
 * @method static ADD_RANGE_ATTACKDAMAGE()
 * @method static NODISPELL()
 * @method static ADD_HPDRAIN()
 * @method static SUB_HPDRAIN()
 * @method static MAGICIMMUNE()
 * @method static NOJAMSTONE()
 * @method static ADD_SPDRAIN()
 * @method static SUB_SPDRAIN()
 * @method static ADD_MELEEATTACK_REFLECT()
 * @method static SUB_MELEEATTACK_REFLECT()
 * @method static PERFECT_DAMAGE()
 * @method static REINCARNATION()
 * @method static SUB_SPELLCASTTIME()
 * @method static ADD_SPELLCASTTIME()
 * @method static SPLASH_ATTACK()
 * @method static SUB_SPCONSUMPTION()
 * @method static ADD_SPCONSUMPTION()
 * @method static START_CAPTURE()
 * @method static INCUBATION()
 * @method static ADDATK_DAMAGEWEAPON()
 * @method static SUBATK_DAMAGEWEAPON()
 * @method static ADDATK_DAMAGEARMOR()
 * @method static SUBATK_DAMAGEARMOR()
 * @method static ADDRECEIVEITEM_RACE()
 * @method static SUBRECEIVEITEM_RACE()
 * @method static SETIGNOREDEFRACE()
 * @method static RESETIGNOREDEFRACE()
 * @method static SETIGNOREDEFCLASS()
 * @method static RESETIGNOREDEFCLASS()
 * @method static SET_DISAPPEARHPAMOUNT()
 * @method static RESET_DISAPPEARHPAMOUNT()
 * @method static SET_DISAPPEARSPAMOUNT()
 * @method static RESET_DISAPPEARSPAMOUNT()
 * @method static SETAUTOSPELL()
 * @method static ADDATK_COMARACE()
 * @method static SUBATK_COMARACE()
 * @method static ADDATK_COMA()
 * @method static SUBATK_COMA()
 * @method static SUBSPAMOUNT_ACTION()
 * @method static ADDSPAMOUNT_ACTION()
 * @method static SUBSPAMOUNT_ATTACK()
 * @method static ADDSPAMOUNT_ATTACK()
 * @method static ADDSPAMOUNT_ATKRACE()
 * @method static SUBSPAMOUNT_ATKRACE()
 * @method static ADDDESTROYSP_ATTACK()
 * @method static SUBDESTROYSP_ATTACK()
 * @method static ADDSPAMOUNT_KILLRACE()
 * @method static SUBSPAMOUNT_KILLRACE()
 * @method static ADDEXPPERCENT_KILLRACE()
 * @method static SUBEXPPERCENT_KILLRACE()
 * @method static CLASSADDDAMAGE()
 * @method static CLASSSUBDAMAGE()
 * @method static RACEADDDAMAGESELF()
 * @method static RACESUBDAMAGESELF()
 * @method static SETINVESTIGATE()
 * @method static RESETINVESTIGATE()
 * @method static MODIFYDEF_FRACTION()
 * @method static ADDGUIDEATTACK()
 * @method static SUBGUIDEATTACK()
 * @method static ADDCHANGEMONSTER()
 * @method static SUBCHANGEMONSTER()
 * @method static ADDCRIPERCENT_RACE()
 * @method static SUBCRIPERCENT_RACE()
 * @method static SETPUSHINGSKILL()
 * @method static RESETPUSHINGSKILL()
 * @method static GETZENY_RANGE()
 * @method static MULTICASTEFFECT()
 * @method static SETEXTPARAMTIME()
 * @method static SETAUTOSPELL_TARGET()
 * @method static SETAUTOSPELL_SELF()
 * @method static SETRECEIVEZENY_KILL()
 * @method static ADDCRI_RANGEATK()
 * @method static SUBCRI_RANGEATK()
 * @method static ADDSTATE_RANGEATK()
 * @method static SUBSTATE_RANGEATK()
 * @method static ADDGUILDEXP()
 * @method static SETRECEIVEITEM_GROUP()
 * @method static ADDREFLECT_MAGIC()
 * @method static SUBREFLECT_MAGIC()
 * @method static ITEMCREATE()
 * @method static ISCOMPLETECOMBINATION()
 * @method static ISBREAKCOMBINATION()
 * @method static SETAUTOSPELL_MLEATKED()
 * @method static GETREFINELEVEL()
 * @method static GETSKILLLEVEL()
 * @method static GETPUREJOB()
 * @method static ADDGETITEM_KILL()
 * @method static SUBGETITEM_KILL()
 * @method static ADDPARAMETERVALUE()
 * @method static EVOLUTION_HOMUN()
 * @method static ADDPARAMTIME()
 * @method static SUBPARAMTIME()
 * @method static ADDDAMAGETM_PROP()
 * @method static SUBSAMAGETM_PROP()
 * @method static COOKING()
 * @method static SUMMONNPC()
 * @method static TRACE()
 * @method static VAR()
 * @method static LOCALVAR()
 * @method static NPCVAR()
 * @method static INCLOCALVAR()
 * @method static DECLOCALVAR()
 * @method static SETLOCALVAR()
 * @method static GETEQUIP_IS_IDENTIFY()
 * @method static GETEQUIP_REFINERYCNT()
 * @method static GETEQUIP_PERCENTREFINERY()
 * @method static GETEQUIP_GETEQUIPREFINERYCOST()
 * @method static GETEQUIP_IS_SUCCESSREFINERY()
 * @method static GETEQUIP_NAME()
 * @method static GETEQUIP_ITEMIDX()
 * @method static GETEQUIP_WEAPONLV()
 * @method static GETEQUIP_IS_ENABLEREF()
 * @method static LASTNPCNAME()
 * @method static PCNAME()
 * @method static ONINIT()
 * @method static ONCLICK()
 * @method static ONTOUCH()
 * @method static ONMYMOBDEAD()
 * @method static ONTIMER()
 * @method static ONCOMMAND()
 * @method static ONSTARTARENA()
 * @method static RAND()
 * @method static LOT()
 * @method static GETPCCOUNT()
 * @method static ONTOUCHNPC()
 * @method static ONTOUCH2()
 * @method static GUIDE()
 * @method static NPC()
 * @method static MOB()
 * @method static GATE()
 * @method static TRADER()
 * @method static ARENAGUIDE()
 * @method static HIDDENWARP()
 * @method static EFFECT()
 * @method static DIALOG()
 * @method static PUTMOB()
 * @method static MOVETO()
 * @method static SAY()
 * @method static MENU()
 * @method static GETMONEY()
 * @method static DROPMONEY()
 * @method static GETITEM()
 * @method static DROPITEM()
 * @method static SETITEM()
 * @method static WAITDIALOG()
 * @method static CLOSEDIALOG()
 * @method static HPFULLHEAL()
 * @method static SPFULLHEAL()
 * @method static HPHEAL()
 * @method static SPHEAL()
 * @method static POISONHEAL()
 * @method static STONEHEAL()
 * @method static CURSEHEAL()
 * @method static FREEZINGHEAL()
 * @method static SILENCEHEAL()
 * @method static CONFUSIONHEAL()
 * @method static SELLITEM()
 * @method static BUYITEM()
 * @method static JOBCHANGE()
 * @method static EXCHANGEITEM()
 * @method static ERROR()
 * @method static CHECKPOINT()
 * @method static STORE()
 * @method static CART()
 * @method static DLGWRITE()
 * @method static INPUT()
 * @method static INPUTSTR()
 * @method static COMPASS()
 * @method static NUDE()
 * @method static SHOW_IMAGE()
 * @method static CHANGE_PALLETE()
 * @method static CALLMONSTER()
 * @method static ADDSKILL()
 * @method static OTHERNPC_CMD()
 * @method static STRLOCALVAR()
 * @method static INITTIMER()
 * @method static SETARENAEVENTSIZE()
 * @method static MAKEWAITINGROOM()
 * @method static ENABLEARENA()
 * @method static DISABLEARENA()
 * @method static WARPWAITINGPCTOARENA()
 * @method static RESETMYMOB()
 * @method static WARPALLPCINTHEMAP()
 * @method static BROADCASTINTHEMAP()
 * @method static STOPTIMER()
 * @method static ADDNPCTIMER()
 * @method static SUBNPCTIMER()
 * @method static ENABLENPC()
 * @method static DISABLENPC()
 * @method static CALLNPC()
 * @method static SETFEEZENY()
 * @method static SETFEEITEM()
 * @method static SETREQLEVEL()
 * @method static SETTEXJOB()
 * @method static DISABLEITEMMOVE()
 * @method static ENABLEITEMMOVE()
 * @method static SUCCESSREFITEM()
 * @method static FAILEDREFITEM()
 * @method static SETEFFECTSTATUS()
 * @method static RESETSTAT()
 * @method static RESETSKILL()
 * @method static MENU2()
 * @method static SHOWDIGIT()
 * @method static EVENT_ADD_SKILL()
 * @method static EVENT_DEL_SKILL()
 * @method static SETPARAMETER()
 * @method static EMOTION()
 * @method static CHANGESPR()
 * @method static GETEQUIPCOUNT()
 * @method static AGITGET()
 * @method static AGITGET2()
 * @method static AGITSET()
 * @method static AGITFUNC()
 * @method static AGITNPCGET()
 * @method static AGITNPCSET()
 * @method static ISGUILDMASTER()
 * @method static PUTMOB2()
 * @method static AGITREGISTER()
 * @method static BROADCASTINMAP2()
 * @method static BROADCASTSERVER()
 * @method static SOUND()
 * @method static DLGWRITESTR()
 * @method static GETDAMAGEDITEMNUM()
 * @method static REPAIRDAMAGEDITEM()
 * @method static SAVEPPL()
 * @method static AGITEMBLEMFLAG()
 * @method static HPDRAIN()
 * @method static SPDRAIN()
 * @method static GETEXP()
 * @method static DROPEXP()
 * @method static SERVERTIME()
 * @method static GETLOCALVARNAME()
 * @method static GETMARRIED()
 * @method static COUNTDOWN()
 * @method static GETGUILDNAME()
 * @method static GETNEIGHBORPCNUMBER()
 * @method static GETNAMEDITEM()
 * @method static DROPNAMEDITEM()
 * @method static GETCOUNTFOMYNAMEITEM()
 * @method static CHANGE_HAIRSTYLE()
 * @method static SHOWEFFECT()
 * @method static FALCON()
 * @method static PECO()
 * @method static CHECK_MAXWEIGHT()
 * @method static CHECK_MAXCOUNT()
 * @method static CHECK_MAXZENY()
 * @method static MGETEVENT_ZENYNAME()
 * @method static MGETEVENT_ITEMNAME()
 * @method static MGETEVENT_ZENY()
 * @method static MGETEVENT_ITEM()
 * @method static CREATE_GUILD()
 * @method static CREATEGLOBALVAR()
 * @method static GETGLOBALVAR()
 * @method static SETGLOBALVAR()
 * @method static STRCMP()
 * @method static STRSTR()
 * @method static UPGRADEGUILDLEVEL()
 * @method static CREATEGLOBALSTR()
 * @method static GETGLOBALSTR()
 * @method static SETGLOBALSTR()
 * @method static DIVORCE()
 * @method static GET_TABLEPOINT()
 * @method static ADD_TABLEPOINT()
 * @method static DEL_TABLEPOINT()
 * @method static PCDATA_MOVE_TO_PVP()
 * @method static GETPVPPOINT()
 * @method static SETPVPPOINT()
 * @method static MGETEVENT_ITEMID()
 * @method static GETJOBEXP()
 * @method static DROPJOBEXP()
 * @method static CHKSKILL()
 * @method static GETBODYSTATE()
 * @method static GETHEALTHSTATE()
 * @method static USESKILL_TO_PC()
 * @method static ISBABY()
 * @method static GETLOTTO()
 * @method static SETLOTTO()
 * @method static INITLOTTO()
 * @method static OPENAUCTION()
 * @method static OPENMAILING()
 * @method static STRIPTPC()
 * @method static PUSHPC()
 * @method static ITEMDOWN()
 * @method static SETNUMARRAY()
 * @method static GETNUMARRAY()
 * @method static SHUFFLENUMBERS()
 * @method static PARTYNAME()
 * @method static CHANGESPEED()
 * @method static ISSIEGETIME()
 * @method static CONSUMESPECIALITEM()
 * @method static SETITEMPARTYINMAP()
 * @method static ISHUNTINGLISTFULL()
 * @method static FINDHUNTINGLIST()
 * @method static ADDHUNTINGLIST()
 * @method static DELETEHUNTINGLIST()
 * @method static DISPLAYHUNTINGLIST()
 * @method static DISPLAYHUNTINGLIST2()
 * @method static SETHUNTINGLIST()
 * @method static CLEARHUNTINGLIST()
 * @method static ISTIMELISTFULL()
 * @method static FINDTIMELIST()
 * @method static ADDTIMELIST()
 * @method static DELETETIMELIST()
 * @method static DISPLAYTIMELIST()
 * @method static SETTIMELIST()
 * @method static CLEARTIMELIST()
 * @method static CHANGECELLTYPE()
 * @method static GETCELLTYPE()
 * @method static ISPCCAFE()
 * @method static GETPAYTYPE()
 * @method static GETCONNECTIONAREA()
 * @method static GETCONNECTIONSTATE()
 * @method static GETPETEGG()
 * @method static CASHTRADER()
 * @method static CASHSHOP()
 * @method static ADDEXPTIME()
 * @method static SUBEXPTIME()
 * @method static ADDRECEIVEITEM()
 * @method static ADVEQUIP_ISSUCCESSREFINERY()
 * @method static PARTYCALL()
 * @method static PUTBOSS()
 * @method static USEBOSSPERCEPTION()
 * @method static SKILLTOME()
 * @method static UseHuntingList()
 * @method static CheckHuntingList()
 * @method static ITEM2()
 * @method static WEAPONPROPERTY()
 * @method static READBOOK()
 * @method static MERCENARY_SUMMON()
 * @method static MERCENARY_HEALHP()
 * @method static MERCENARY_HEALSP()
 * @method static GETMERCENARYTOTALSUMMONNUM()
 * @method static GETMERCENARYFAITH()
 * @method static SUBMERCENARYFAITH()
 * @method static ADDMERCENARYFAITH()
 * @method static NPC2()
 * @method static ONMOVENPCCMD()
 * @method static MOVEPOS()
 * @method static MOVEWAIT()
 * @method static REFINERY()
 * @method static MERCENARY_CONDITION()
 * @method static ADDNEVERKNOCKBACK()
 * @method static GETADVEQUIPPERCENTREFINERY()
 * @method static RELOAD_MOBILE_EVENT()
 * @method static SETTEMPTIME()
 * @method static ELAPSETEMPTIME()
 * @method static SUBNEVERKNOCKBACK()
 * @method static GETWEAPONCLASS()
 * @method static GETEQUIPSLOTAMOUNT()
 * @method static ADDHEALVALUE()
 * @method static SUBHEALVALUE()
 * @method static SETAUTOSPELL_MAGATKED()
 * @method static ADDHPAMOUNT_MAGKILL()
 * @method static SUBHPAMOUNT_MAGKILL()
 * @method static ADDSPAMOUNT_MAGKILL()
 * @method static SUBSPAMOUNT_MAGKILL()
 * @method static ADDSPELLDELAY()
 * @method static SUBSPELLDELAY()
 * @method static ADDSPECIFICSPELLCASTTIME()
 * @method static SUBSPECIFICSPELLCASTTIME()
 * @method static ADDMDAMAGE_RACE()
 * @method static SUBMDAMAGE_RACE()
 * @method static ADDMDAMAGE_CLASS()
 * @method static SUBMDAMAGE_CLASS()
 * @method static SETIGNOREMDEFRACE()
 * @method static RESETIGNOREMDEFRACE()
 * @method static SETIGNOREMDEFCLASS()
 * @method static RESETIGNOREMDEFCLASS()
 * @method static SETHPAMOUNTTIME()
 * @method static RESETHPAMOUNTTIME()
 * @method static SETSPAMOUNTTIME()
 * @method static RESETSPAMOUNTTIME()
 * @method static SETIGNOREDEFRACE_PERCENT()
 * @method static RESETIGNOREDEFRACE_PERCENT()
 * @method static SETIGNOREDEFCLASS_PERCENT()
 * @method static RESETIGNOREDEFCLASS_PERCENT()
 * @method static GETINVENTORYREMAINCOUNT()
 * @method static SETAUTOWEAPON()
 * @method static RESETAUTOWEAPON()
 * @method static SETAUTOMWEAPON()
 * @method static RESETAUTOMWEAPON()
 * @method static SETAUTOATTACKED()
 * @method static RESETAUTOATTACKED()
 * @method static SETAUTOMATTACKED()
 * @method static RESETAUTOMATTACKED()
 * @method static RESETRECEIVEITEM_GROUP()
 * @method static BUFF()
 * @method static SETBUFF()
 * @method static GETBUFFVALUE()
 * @method static ADDHEALMODIFYPERCENT()
 * @method static SUBHEALMODIFYPERCENT()
 * @method static SETHPPERCENTTIME()
 * @method static RESETHPPERCENTTIME()
 * @method static SETSPPERCENTTIME()
 * @method static RESETSPPERCENTTIME()
 * @method static LOUDSPEAKER()
 * @method static SETTARGETATTACKED_BUFF()
 * @method static RESETTARGETATTACKED_BUFF()
 * @method static GETMAPNAME()
 * @method static SETQUEST()
 * @method static CHANGQUEST()
 * @method static ERASEQUEST()
 * @method static COMPLATEQUEST()
 * @method static COMPLETEQUEST_BETWEEN()
 * @method static RECALL_COMPLETEQUEST()
 * @method static ISBEGINQUEST()
 * @method static CHECKQUEST_HUNTING()
 * @method static CHECKQUEST_PLAYTIME()
 * @method static GETLASTSIEGEMSG()
 * @method static PCDATA_MOVE_TO_SAKRAY()
 * @method static CALLGUARDIAN()
 * @method static ISPREMIUMPCCAFE()
 * @method static MDUNGEON_SUBSCRIPTION()
 * @method static MDUNGEON_ENTER()
 * @method static CAMPCODE()
 * @method static GETCAMPCODE()
 * @method static CALLCAMPMOB()
 * @method static SETRP2()
 * @method static MASSSETRP2()
 * @method static PLAYERTORP()
 * @method static MGETEVENT_ZENYNAME_EXCULUSEIVE()
 * @method static MGETEVENT_ITEMNAME_EXCULUSEIVE()
 * @method static MGETEVENT_ZENY_EXCULUSEIVE()
 * @method static MGETEVENT_ITEM_EXCULUSEIVE()
 * @method static MGETEVENT_ITEMID_EXCULUSEIVE()
 * @method static MGETEVENT_RELOAD_EXCULUSEIVE()
 * @method static MDUNGEON_LIST()
 * @method static MDUNGEON_DESTROY()
 * @method static PLUSCAMPPOINT()
 * @method static MINUSCAMPPOINT()
 * @method static GETCAMPPOINT()
 * @method static RESETCAMPPOINT()
 * @method static CHECKSIEGETIME()
 * @method static ADDSTATE_MATK()
 * @method static SUBSTATE_MATK()
 * @method static ADDSTATE_MATTACKED()
 * @method static SUBSTATE_MATTACKED()
 * @method static MDUNGEON_MAPNAME()
 * @method static MDUNGEON_NPCNAME()
 * @method static MDUNGEON_NPC()
 * @method static MDUNGEON_WAP()
 * @method static MDUNGEON_HIDDENWAP()
 * @method static MDUNGEON_PUTMOB()
 * @method static MYMOBSKILLUSE()
 * @method static WHEREMYMOB_X()
 * @method static WHEREMYMOB_Y()
 * @method static WIDEMOBSKILLUSE()
 * @method static WIDEMOBSKILLUSE2()
 * @method static MDUNGEON_OPENSTATE()
 * @method static ADDHPDRAIN_100()
 * @method static SUBHPDRAIN_100()
 * @method static CREATEPACKAGE()
 * @method static ISBENDER()
 * @method static MOVERESUME()
 * @method static MOVERETURN()
 * @method static MOVESPEED()
 * @method static GETNPCPOSITION()
 * @method static ADDRECEIVEITEM_EQUIP()
 * @method static SUBRECEIVEITEM_EQUIP()
 * @method static ADDONLYJOBEXPTIME()
 * @method static PROGRESS_BAR()
 * @method static MERCENARYCHECK()
 * @method static MERCENARYTYPE()
 * @method static ISFREESERVER()
 * @method static SETSKILLAUTOSPELL()
 * @method static RESETSKILLAUTOSPELL()
 * @method static SETSKILLACTIVATE()
 * @method static RESETSKILLACTIVATE()
 * @method static GETACTIVATED_SKILL()
 * @method static GETDEACTIVATED_SKILL()
 * @method static ADDSTATE_SKID()
 * @method static SUBSTATE_SKID()
 * @method static SETAUTOSPELLMAGIC()
 * @method static RESETAUTOSPELLMAGIC()
 * @method static RUN_NPC()
 * @method static RUN_NPCTRADER()
 * @method static ONCAMPCOMMAND()
 * @method static CHECKPARTYJOB()
 * @method static CLEARBUFF()
 * @method static CHECKALIVE()
 * @method static KVM_NPC()
 * @method static KVM_START_ALIVECHECK()
 * @method static PVPRANKCHECK()
 * @method static SETAUTOSPELL_MLEATK()
 * @method static SETAUTOSPELL_RANGEATK()
 * @method static KVM_NPC_NOTI()
 * @method static GETARENAEVENTSIZE()
 * @method static SETMAKABLERUNEITEMLIST()
 * @method static ISSUCCESSRUNEUSE()
 * @method static SETMAKABLENEWPOISONLIST()
 * @method static GETITEMSOCKET()
 * @method static GETPETRELATIONSHIP()
 * @method static ADDMDAMAGE_NAME()
 * @method static SUBMDAMAGE_NAME()
 * @method static GETNONSLOTITEMSOCK()
 * @method static ISEFFECTSTATUS()
 * @method static GETMYMERCENARY()
 * @method static ERRORLOG()
 * @method static ADDRECEIVEITEM_NAME()
 * @method static SUBRECEIVEITEM_NAME()
 * @method static ISMADOGEAR()
 * @method static SETMADOGEAR()
 * @method static ADD_SFCT_CONSUME_AMOUNT()
 * @method static SUB_SFCT_CONSUME_AMOUNT()
 * @method static ADD_SFCT_CONSUME_PERMILL()
 * @method static SUB_SFCT_CONSUME_PERMILL()
 * @method static ADD_SFCT_EQUIP_AMOUNT()
 * @method static SUB_SFCT_EQUIP_AMOUNT()
 * @method static ADD_SFCT_EQUIP_PERMILL()
 * @method static SUB_SFCT_EQUIP_PERMILL()
 * @method static ADDATTRTOLERACE_DISREGARD()
 * @method static SUBATTRTOLERACE_DISREGARD()
 * @method static GETEXDEQUIPISSUCCESSREFINERY()
 * @method static GETEXDADVEQUIPISSUCCESSREFINERY()
 * @method static DOWNREFITEM()
 * @method static DOWNADVREFITEM()
 * @method static SUCCESSRANDREFITEM()
 * @method static SUCCESSRANDADVREFITEM()
 * @method static FINAL_BATTLE_PLAY()
 * @method static ONCAMPCOMMAND2()
 * @method static INTEROTHER()
 * @method static SETAUTOSPELL_MLEATKED_V2()
 * @method static SETAUTOSPELL_RANGEATKED_V2()
 * @method static ADDSKILLSP()
 * @method static SUBSKILLSP()
 * @method static ADDSKILLDELAY()
 * @method static SUBSKILLDELAY()
 * @method static PLAYBGM()
 * @method static STRIPTPC2()
 * @method static ADDSTATECASTTIME()
 * @method static SUBSTATECASTTIME()
 * @method static ADDSKILLMDAMAGE()
 * @method static SUBSKILLMDAMAGE()
 * @method static GETLOCATION()
 * @method static BROADCAST_IN_ZONE()
 * @method static BROADCAST_IN_MAP()
 * @method static SET_ITEM_EFFECT()
 * @method static RESET_ITEM_EFFECT()
 * @method static SETREADYMUTATIONHOMUN()
 * @method static MUTATIONHOMUN()
 * @method static MODIFYMDEF_FRACTION()
 * @method static ADDALLDEF_FLUCTUATION()
 * @method static SUBALLDEF_FLUCTUATION()
 * @method static ADDALLMDEF_FLUCTUATION()
 * @method static SUBALLMDEF_FLUCTUATION()
 * @method static OPEN_SEARCH_STORE_INFO()
 * @method static CHANGERP()
 * @method static ISALLIANCEGUILD()
 * @method static OPENHOUR_SIEGEMODE()
 * @method static CHECKHOMUNLEVEL()
 * @method static CHECKHOMUNMUTATION()
 * @method static CHECKHOMUNCALL()
 * @method static SETBATTLEFIELDMOVEPOSTION()
 * @method static GETEQUIPCOMPOSITIONTYPE()
 * @method static GETEQUIPDBNAME()
 * @method static GETEQUIPCARDID()
 * @method static DROPEQUIPITEM()
 * @method static GETREFITEM()
 * @method static RESETBUFF()
 * @method static ADDSTATE_EFSTATK()
 * @method static SUBSTATE_EFSTATK()
 * @method static GETNONSLOTITEMSOCK2()
 * @method static GETITEM2()
 * @method static SHOWSCRIPT()
 * @method static SHOWEMOTION()
 * @method static START_COLLECTION()
 * @method static MONTRANSFORM()
 * @method static DELETE_ALL_BODY_ITEM()
 * @method static ONAGITINVEST()
 * @method static INC_GLOBALVAR()
 * @method static DEC_GLOBALVAR()
 * @method static GET_AGITINVEST_MSG()
 * @method static ADD_HANDICAPSTATE_RACE()
 * @method static SUB_HANDICAPSTATE_RACE()
 * @method static SET_NUM_DEF_RACE()
 * @method static RESET_NUM_DEF_RACE()
 * @method static SET_NUM_MDEF_RACE()
 * @method static RESET_NUM_MDEF_RACE()
 * @method static SET_PER_DEF_SELF()
 * @method static SET_PER_MDEF_SELF()
 * @method static SET_MHP_PER_DAMAGE_RACE()
 * @method static RESET_MHP_PER_DAMAGE_RACE()
 * @method static SET_MSP_PER_DAMAGE_RACE()
 * @method static RESET_MSP_PER_DAMAGE_RACE()
 * @method static GET_AGIT_ECONOMY()
 * @method static INC_AGIT_ECONOMY()
 * @method static DEC_AGIT_ECONOMY()
 * @method static EQ_MAKE()
 * @method static EQ_CHECK_USER()
 * @method static EQ_NOTIFY_ADMISSION()
 * @method static EQ_IS_READY()
 * @method static EQ_MOVE_TO_ROOM()
 * @method static EQ_RETURN_TO_PRE_SPACE()
 * @method static EQ_MOVE_TO_LOBBY()
 * @method static LOBBY_MAKE()
 * @method static LOBBY_USER_COUNT()
 * @method static LOBBY_NOTIFY_ADMISSION()
 * @method static LOBBY_IS_READY()
 * @method static LOBBY_MOVE_TO_ROOM()
 * @method static LOBBY_RETURN_TO_PRE_SPACE()
 * @method static LOBBY_INIT()
 * @method static CLEARDIALOG()
 * @method static NAVIGATION_ACTIVE()
 * @method static ACTIVE_MONTRANSFORM()
 * @method static DEACTIVE_MONTRANSFORM()
 * @method static SETAUTOSPELL2()
 * @method static SETAUTOSPELL2_MLEATKED()
 * @method static SETAUTOSPELL2_MLEATKED_SHORT()
 * @method static SETAUTOSPELL2_MLEATKED_LONG()
 * @method static SETAUTOSPELL2_MAGATKED()
 * @method static SETAUTOSPELL2_MLEATK()
 * @method static SETAUTOSPELL2_RANGEATK()
 * @method static RESETAUTOSPELL2()
 * @method static RESETAUTOSPELL2_MLEATKED()
 * @method static RESETAUTOSPELL2_MLEATKED_SHORT()
 * @method static RESETAUTOSPELL2_MLEATKED_LONG()
 * @method static RESETAUTOSPELL2_MAGATKED()
 * @method static RESETAUTOSPELL2_MLEATK()
 * @method static RESETAUTOSPELL2_RANGEATK()
 * @method static PAUSEAUTOSPELL2()
 * @method static PAUSEAUTOSPELL2_MLEATKED()
 * @method static PAUSEAUTOSPELL2_MLEATKED_SHORT()
 * @method static PAUSEAUTOSPELL2_MLEATKED_LONG()
 * @method static PAUSEAUTOSPELL2_MAGATKED()
 * @method static PAUSEAUTOSPELL2_MLEATK()
 * @method static PAUSEAUTOSPELL2_RANGEATK()
 * @method static RESUMEAUTOSPELL2()
 * @method static RESUMEAUTOSPELL2_MLEATKED()
 * @method static RESUMEAUTOSPELL2_MLEATKED_SHORT()
 * @method static RESUMEAUTOSPELL2_MLEATKED_LONG()
 * @method static RESUMEAUTOSPELL2_MAGATKED()
 * @method static RESUMEAUTOSPELL2_MLEATK()
 * @method static RESUMEAUTOSPELL2_RANGEATK()
 * @method static CHECK_JOB_GROUP()
 * @method static GETPREMIUMCAMPAIGN_GRADE()
 * @method static GET_BASE_JOB()
 * @method static GET_CATEGORY_JOB()
 * @method static EQ_JOB_CONTROL()
 * @method static ADDMDAMAGE_PROPERTY()
 * @method static SUBMDAMAGE_PROPERTY()
 * @method static LOBBY_CAMPCODE()
 * @method static INSERT_ALIVE_MEMBER()
 * @method static EQ_USERINFO_INIT()
 * @method static ADDQUESTINFO()
 * @method static ADDQUESTINFO2()
 * @method static SETQUESTJOB()
 * @method static SETQUESTSEX()
 * @method static SETQUESTLEVEL()
 * @method static SETQUESTJOBLEVEL()
 * @method static SETQUESTITEM()
 * @method static SETQUESTHOMUNLEVEL()
 * @method static SETQUESTHOMUNTYPE()
 * @method static SETQUESTQUESTITEM()
 * @method static IS_SET_QUEST()
 * @method static IS_LOWLEVEL_SIEGE_JOB()
 * @method static RESET_BUFF_LOWLEVEL_SIEGE()
 * @method static EQ_MOVE_TO_LOBBY_JOB_MATCHING()
 * @method static GET_YEAR()
 * @method static GET_MONTH()
 * @method static GET_DAY()
 * @method static GET_WEEK_DAY()
 * @method static MERGE_ITEM()
 * @method static DIALOG2()
 * @method static WAITDIALOG2()
 * @method static STORE_V2()
 * @method static CALLFALCON()
 * @method static SETPRNPCWINNER()
 * @method static PD_SUBSCRIPTION()
 * @method static PD_ENTER()
 * @method static SETQUESQUEST()
 * @method static TALK_SHOW()
 * @method static RENT_ITEM()
 * @method static PROVIDE_EXP()
 * @method static GET_MONSTER_HP()
 * @method static MONSTER_TALK_SHOW()
 * @method static CHECKJOBGROUP2()
 * @method static JOIN_CLAN()
 * @method static LEAVE_CLAN()
 * @method static SET_MONSTER_HP()
 * @method static ITEMDOWN2()
 * @method static GET_ELAPSED_TIMER()
 * @method static IS_ENABLE_NPC()
 * @method static GET_NPC_MONSTER_X()
 * @method static GET_NPC_MONSTER_Y()
 * @method static DELAY_TIME()
 * @method static SET_CLAN_MASTER()
 * @method static SAVEPOSITION()
 * @method static MOVETOSAVEPOSITION()
 * @method static MDUNGEON_TRADER()
 * @method static SET_MONSTER_HP2()
 * @method static GET_RANKING_POINT()
 * @method static INC_RANKING_POINT()
 * @method static DEC_RANKING_POINT()
 * @method static NPC_MONTRANSFORM()
 * @method static PARTY_MAP_MOVE()
 * @method static IS_PREMIUM()
 * @method static SETSKILL()
 * @method static ONUSE()
 * @method static ONSUCCESS()
 * @method static SKILLTYPE()
 * @method static SKILLMAXLV()
 * @method static SKILLPATTERN()
 * @method static SKILLSPLASH()
 * @method static SKILLHITPERCENT()
 * @method static SKILLATKPERCENT()
 * @method static SKILLDATA1()
 * @method static SKILLDATA2()
 * @method static SKILLFLAG()
 * @method static SKILLRANGE()
 * @method static SKILLPROPERTY()
 * @method static SKILLHANDICAP()
 * @method static SKILLKNOCKBACK()
 * @method static SKILLNODAMAGE()
 * @method static SKILLSP()
 * @method static SKILLPREDELAYTIME()
 * @method static SKILLPOSTDELAYTIME()
 * @method static SKILLRESETHANDICAP()
 * @method static SKILLDRAINHP()
 * @method static SKILLDRAINSP()
 * @method static SKILLEVENT()
 * @method static SKILLCASTFIXEDDELAY()
 * @method static SKILLCASTSTATDELAY()
 * @method static SKILLGLOBALPOSTDELAY()
 * @method static SKILLSINGLEPOSTDELAY()
 * @method static SKILLDATA3()
 * @method static SKILLDATA4()
 * @method static SKILLDATA5()
 * @method static SKILLDATA6()
 * @method static SKILLDATASTRING()
 */
final class ScriptFunc extends Enum
{
    private const RETURN = 0x0;
    private const ITEM = 0x1;
    private const EVENT = 0x2;
    private const GET = 0x3;
    private const INC = 0x4;
    private const DEC = 0x5;
    private const ENABLESKILL = 0x6;
    private const DISABLESKILL = 0x7;
    private const SUMMON = 0x8;
    private const SKILL = 0x9;
    private const CREATEITEM = 0xa;
    private const LOSTITEM = 0xb;
    private const HEALHP = 0xc;
    private const HEALSP = 0xd;
    private const HEALALL = 0xe;
    private const CONDITION = 0xf;
    private const CURE = 0x10;
    private const ADDEXTPARAM = 0x11;
    private const SUBEXTPARAM = 0x12;
    private const SETMAKABLE_WPNLIST = 0x13;
    private const SETMAKABLE_MTLLIST = 0x14;
    private const RACE_ADD_DAMAGE = 0x15;
    private const RACE_SUB_DAMAGE = 0x16;
    private const ATTR_ADD_TOLERACE = 0x17;
    private const ATTR_SUB_TOLERACE = 0x18;
    private const RACE_ADD_TOLERACE = 0x19;
    private const RACE_SUB_TOLERACE = 0x1a;
    private const ADDDAMAGE_PROPERTY = 0x1b;
    private const SUBDAMAGE_PROPERTY = 0x1c;
    private const ADDDAMAGE_SIZE = 0x1d;
    private const SUBDAMAGE_SIZE = 0x1e;
    private const ADDDAMAGE_NAME = 0x1f;
    private const SUBDAMAGE_NAME = 0x20;
    private const ADDDAMAGE_SKID = 0x21;
    private const SUBDAMAGE_SKID = 0x22;
    private const ADDDAMAGE_CRI = 0x23;
    private const SUBDAMAGE_CRI = 0x24;
    private const ADDSTATE_MLEATK = 0x25;
    private const SUBSTATE_MLEATK = 0x26;
    private const ADDSTATE_MLEATTACKED = 0x27;
    private const SUBSTATE_MLEATTACKED = 0x28;
    private const ADDHEALPERCENT_ITEM = 0x29;
    private const SUBHEALPERCENT_ITEM = 0x2a;
    private const ADDHEALAMOUNT_KILL = 0x2b;
    private const SUBHEALAMOUNT_KILL = 0x2c;
    private const INDESTRUCTIBLE_ARMOR = 0x2d;
    private const INDESTRUCTIBLE_WEAPON = 0x2e;
    private const ADD_STATE_TOLERACE = 0x2f;
    private const SUB_STATE_TOLERACE = 0x30;
    private const BODY_ATTRIBUTE = 0x31;
    private const SUB_RANGE_ATTACKDAMAGE = 0x32;
    private const ADD_RANGE_ATTACKDAMAGE = 0x33;
    private const NODISPELL = 0x34;
    private const ADD_HPDRAIN = 0x35;
    private const SUB_HPDRAIN = 0x36;
    private const MAGICIMMUNE = 0x37;
    private const NOJAMSTONE = 0x38;
    private const ADD_SPDRAIN = 0x39;
    private const SUB_SPDRAIN = 0x3a;
    private const ADD_MELEEATTACK_REFLECT = 0x3b;
    private const SUB_MELEEATTACK_REFLECT = 0x3c;
    private const PERFECT_DAMAGE = 0x3d;
    private const REINCARNATION = 0x3e;
    private const SUB_SPELLCASTTIME = 0x3f;
    private const ADD_SPELLCASTTIME = 0x40;
    private const SPLASH_ATTACK = 0x41;
    private const SUB_SPCONSUMPTION = 0x42;
    private const ADD_SPCONSUMPTION = 0x43;
    private const START_CAPTURE = 0x44;
    private const INCUBATION = 0x45;
    private const ADDATK_DAMAGEWEAPON = 0x46;
    private const SUBATK_DAMAGEWEAPON = 0x47;
    private const ADDATK_DAMAGEARMOR = 0x48;
    private const SUBATK_DAMAGEARMOR = 0x49;
    private const ADDRECEIVEITEM_RACE = 0x4a;
    private const SUBRECEIVEITEM_RACE = 0x4b;
    private const SETIGNOREDEFRACE = 0x4c;
    private const RESETIGNOREDEFRACE = 0x4d;
    private const SETIGNOREDEFCLASS = 0x4e;
    private const RESETIGNOREDEFCLASS = 0x4f;
    private const SET_DISAPPEARHPAMOUNT = 0x50;
    private const RESET_DISAPPEARHPAMOUNT = 0x51;
    private const SET_DISAPPEARSPAMOUNT = 0x52;
    private const RESET_DISAPPEARSPAMOUNT = 0x53;
    private const SETAUTOSPELL = 0x54;
    private const ADDATK_COMARACE = 0x55;
    private const SUBATK_COMARACE = 0x56;
    private const ADDATK_COMA = 0x57;
    private const SUBATK_COMA = 0x58;
    private const SUBSPAMOUNT_ACTION = 0x59;
    private const ADDSPAMOUNT_ACTION = 0x5a;
    private const SUBSPAMOUNT_ATTACK = 0x5b;
    private const ADDSPAMOUNT_ATTACK = 0x5c;
    private const ADDSPAMOUNT_ATKRACE = 0x5d;
    private const SUBSPAMOUNT_ATKRACE = 0x5e;
    private const ADDDESTROYSP_ATTACK = 0x5f;
    private const SUBDESTROYSP_ATTACK = 0x60;
    private const ADDSPAMOUNT_KILLRACE = 0x61;
    private const SUBSPAMOUNT_KILLRACE = 0x62;
    private const ADDEXPPERCENT_KILLRACE = 0x63;
    private const SUBEXPPERCENT_KILLRACE = 0x64;
    private const CLASSADDDAMAGE = 0x65;
    private const CLASSSUBDAMAGE = 0x66;
    private const RACEADDDAMAGESELF = 0x67;
    private const RACESUBDAMAGESELF = 0x68;
    private const SETINVESTIGATE = 0x69;
    private const RESETINVESTIGATE = 0x6a;
    private const MODIFYDEF_FRACTION = 0x6b;
    private const ADDGUIDEATTACK = 0x6c;
    private const SUBGUIDEATTACK = 0x6d;
    private const ADDCHANGEMONSTER = 0x6e;
    private const SUBCHANGEMONSTER = 0x6f;
    private const ADDCRIPERCENT_RACE = 0x70;
    private const SUBCRIPERCENT_RACE = 0x71;
    private const SETPUSHINGSKILL = 0x72;
    private const RESETPUSHINGSKILL = 0x73;
    private const GETZENY_RANGE = 0x74;
    private const MULTICASTEFFECT = 0x75;
    private const SETEXTPARAMTIME = 0x76;
    private const SETAUTOSPELL_TARGET = 0x77;
    private const SETAUTOSPELL_SELF = 0x78;
    private const SETRECEIVEZENY_KILL = 0x79;
    private const ADDCRI_RANGEATK = 0x7a;
    private const SUBCRI_RANGEATK = 0x7b;
    private const ADDSTATE_RANGEATK = 0x7c;
    private const SUBSTATE_RANGEATK = 0x7d;
    private const ADDGUILDEXP = 0x7e;
    private const SETRECEIVEITEM_GROUP = 0x7f;
    private const ADDREFLECT_MAGIC = 0x80;
    private const SUBREFLECT_MAGIC = 0x81;
    private const ITEMCREATE = 0x82;
    private const ISCOMPLETECOMBINATION = 0x83;
    private const ISBREAKCOMBINATION = 0x84;
    private const SETAUTOSPELL_MLEATKED = 0x85;
    private const GETREFINELEVEL = 0x86;
    private const GETSKILLLEVEL = 0x87;
    private const GETPUREJOB = 0x88;
    private const ADDGETITEM_KILL = 0x89;
    private const SUBGETITEM_KILL = 0x8a;
    private const ADDPARAMETERVALUE = 0x8b;
    private const EVOLUTION_HOMUN = 0x8c;
    private const ADDPARAMTIME = 0x8d;
    private const SUBPARAMTIME = 0x8e;
    private const ADDDAMAGETM_PROP = 0x8f;
    private const SUBSAMAGETM_PROP = 0x90;
    private const COOKING = 0x91;
    private const SUMMONNPC = 0x92;
    private const TRACE = 0x93;
    private const VAR = 0x94;
    private const LOCALVAR = 0x95;
    private const NPCVAR = 0x96;
    private const INCLOCALVAR = 0x97;
    private const DECLOCALVAR = 0x98;
    private const SETLOCALVAR = 0x99;
    private const GETEQUIP_IS_IDENTIFY = 0x9a;
    private const GETEQUIP_REFINERYCNT = 0x9b;
    private const GETEQUIP_PERCENTREFINERY = 0x9c;
    private const GETEQUIP_GETEQUIPREFINERYCOST = 0x9d;
    private const GETEQUIP_IS_SUCCESSREFINERY = 0x9e;
    private const GETEQUIP_NAME = 0x9f;
    private const GETEQUIP_ITEMIDX = 0xa0;
    private const GETEQUIP_WEAPONLV = 0xa1;
    private const GETEQUIP_IS_ENABLEREF = 0xa2;
    private const LASTNPCNAME = 0xa3;
    private const PCNAME = 0xa4;
    private const ONINIT = 0xa5;
    private const ONCLICK = 0xa6;
    private const ONTOUCH = 0xa7;
    private const ONMYMOBDEAD = 0xa8;
    private const ONTIMER = 0xa9;
    private const ONCOMMAND = 0xaa;
    private const ONSTARTARENA = 0xab;
    private const RAND = 0xac;
    private const LOT = 0xad;
    private const GETPCCOUNT = 0xae;
    private const ONTOUCHNPC = 0xaf;
    private const ONTOUCH2 = 0xb0;
    private const GUIDE = 0xb1;
    private const NPC = 0xb2;
    private const MOB = 0xb3;
    private const GATE = 0xb4;
    private const TRADER = 0xb5;
    private const ARENAGUIDE = 0xb6;
    private const HIDDENWARP = 0xb7;
    private const EFFECT = 0xb8;
    private const DIALOG = 0xb9;
    private const PUTMOB = 0xba;
    private const MOVETO = 0xbb;
    private const SAY = 0xbc;
    private const MENU = 0xbd;
    private const GETMONEY = 0xbe;
    private const DROPMONEY = 0xbf;
    private const GETITEM = 0xc0;
    private const DROPITEM = 0xc1;
    private const SETITEM = 0xc2;
    private const WAITDIALOG = 0xc3;
    private const CLOSEDIALOG = 0xc4;
    private const HPFULLHEAL = 0xc5;
    private const SPFULLHEAL = 0xc6;
    private const HPHEAL = 0xc7;
    private const SPHEAL = 0xc8;
    private const POISONHEAL = 0xc9;
    private const STONEHEAL = 0xca;
    private const CURSEHEAL = 0xcb;
    private const FREEZINGHEAL = 0xcc;
    private const SILENCEHEAL = 0xcd;
    private const CONFUSIONHEAL = 0xce;
    private const SELLITEM = 0xcf;
    private const BUYITEM = 0xd0;
    private const JOBCHANGE = 0xd1;
    private const EXCHANGEITEM = 0xd2;
    private const ERROR = 0xd3;
    private const CHECKPOINT = 0xd4;
    private const STORE = 0xd5;
    private const CART = 0xd6;
    private const DLGWRITE = 0xd7;
    private const INPUT = 0xd8;
    private const INPUTSTR = 0xd9;
    private const COMPASS = 0xda;
    private const NUDE = 0xdb;
    private const SHOW_IMAGE = 0xdc;
    private const CHANGE_PALLETE = 0xdd;
    private const CALLMONSTER = 0xde;
    private const ADDSKILL = 0xdf;
    private const OTHERNPC_CMD = 0xe0;
    private const STRLOCALVAR = 0xe1;
    private const INITTIMER = 0xe2;
    private const SETARENAEVENTSIZE = 0xe3;
    private const MAKEWAITINGROOM = 0xe4;
    private const ENABLEARENA = 0xe5;
    private const DISABLEARENA = 0xe6;
    private const WARPWAITINGPCTOARENA = 0xe7;
    private const RESETMYMOB = 0xe8;
    private const WARPALLPCINTHEMAP = 0xe9;
    private const BROADCASTINTHEMAP = 0xea;
    private const STOPTIMER = 0xeb;
    private const ADDNPCTIMER = 0xec;
    private const SUBNPCTIMER = 0xed;
    private const ENABLENPC = 0xee;
    private const DISABLENPC = 0xef;
    private const CALLNPC = 0xf0;
    private const SETFEEZENY = 0xf1;
    private const SETFEEITEM = 0xf2;
    private const SETREQLEVEL = 0xf3;
    private const SETTEXJOB = 0xf4;
    private const DISABLEITEMMOVE = 0xf5;
    private const ENABLEITEMMOVE = 0xf6;
    private const SUCCESSREFITEM = 0xf7;
    private const FAILEDREFITEM = 0xf8;
    private const SETEFFECTSTATUS = 0xf9;
    private const RESETSTAT = 0xfa;
    private const RESETSKILL = 0xfb;
    private const MENU2 = 0xfc;
    private const SHOWDIGIT = 0xfd;
    private const EVENT_ADD_SKILL = 0xfe;
    private const EVENT_DEL_SKILL = 0xff;
    private const SETPARAMETER = 0x100;
    private const EMOTION = 0x101;
    private const CHANGESPR = 0x102;
    private const GETEQUIPCOUNT = 0x103;
    private const AGITGET = 0x104;
    private const AGITGET2 = 0x105;
    private const AGITSET = 0x106;
    private const AGITFUNC = 0x107;
    private const AGITNPCGET = 0x108;
    private const AGITNPCSET = 0x109;
    private const ISGUILDMASTER = 0x10a;
    private const PUTMOB2 = 0x10b;
    private const AGITREGISTER = 0x10c;
    private const BROADCASTINMAP2 = 0x10d;
    private const BROADCASTSERVER = 0x10e;
    private const SOUND = 0x10f;
    private const DLGWRITESTR = 0x110;
    private const GETDAMAGEDITEMNUM = 0x111;
    private const REPAIRDAMAGEDITEM = 0x112;
    private const SAVEPPL = 0x113;
    private const AGITEMBLEMFLAG = 0x114;
    private const HPDRAIN = 0x115;
    private const SPDRAIN = 0x116;
    private const GETEXP = 0x117;
    private const DROPEXP = 0x118;
    private const SERVERTIME = 0x119;
    private const GETLOCALVARNAME = 0x11a;
    private const GETMARRIED = 0x11b;
    private const COUNTDOWN = 0x11c;
    private const GETGUILDNAME = 0x11d;
    private const GETNEIGHBORPCNUMBER = 0x11e;
    private const GETNAMEDITEM = 0x11f;
    private const DROPNAMEDITEM = 0x120;
    private const GETCOUNTFOMYNAMEITEM = 0x121;
    private const CHANGE_HAIRSTYLE = 0x122;
    private const SHOWEFFECT = 0x123;
    private const FALCON = 0x124;
    private const PECO = 0x125;
    private const CHECK_MAXWEIGHT = 0x126;
    private const CHECK_MAXCOUNT = 0x127;
    private const CHECK_MAXZENY = 0x128;
    private const MGETEVENT_ZENYNAME = 0x129;
    private const MGETEVENT_ITEMNAME = 0x12a;
    private const MGETEVENT_ZENY = 0x12b;
    private const MGETEVENT_ITEM = 0x12c;
    private const CREATE_GUILD = 0x12d;
    private const CREATEGLOBALVAR = 0x12e;
    private const GETGLOBALVAR = 0x12f;
    private const SETGLOBALVAR = 0x130;
    private const STRCMP = 0x131;
    private const STRSTR = 0x132;
    private const UPGRADEGUILDLEVEL = 0x133;
    private const CREATEGLOBALSTR = 0x134;
    private const GETGLOBALSTR = 0x135;
    private const SETGLOBALSTR = 0x136;
    private const DIVORCE = 0x137;
    private const GET_TABLEPOINT = 0x138;
    private const ADD_TABLEPOINT = 0x139;
    private const DEL_TABLEPOINT = 0x13a;
    private const PCDATA_MOVE_TO_PVP = 0x13b;
    private const GETPVPPOINT = 0x13c;
    private const SETPVPPOINT = 0x13d;
    private const MGETEVENT_ITEMID = 0x13e;
    private const GETJOBEXP = 0x13f;
    private const DROPJOBEXP = 0x140;
    private const CHKSKILL = 0x141;
    private const GETBODYSTATE = 0x142;
    private const GETHEALTHSTATE = 0x143;
    private const USESKILL_TO_PC = 0x144;
    private const ISBABY = 0x145;
    private const GETLOTTO = 0x146;
    private const SETLOTTO = 0x147;
    private const INITLOTTO = 0x148;
    private const OPENAUCTION = 0x149;
    private const OPENMAILING = 0x14a;
    private const STRIPTPC = 0x14b;
    private const PUSHPC = 0x14c;
    private const ITEMDOWN = 0x14d;
    private const SETNUMARRAY = 0x14e;
    private const GETNUMARRAY = 0x14f;
    private const SHUFFLENUMBERS = 0x150;
    private const PARTYNAME = 0x151;
    private const CHANGESPEED = 0x152;
    private const ISSIEGETIME = 0x153;
    private const CONSUMESPECIALITEM = 0x154;
    private const SETITEMPARTYINMAP = 0x155;
    private const ISHUNTINGLISTFULL = 0x156;
    private const FINDHUNTINGLIST = 0x157;
    private const ADDHUNTINGLIST = 0x158;
    private const DELETEHUNTINGLIST = 0x159;
    private const DISPLAYHUNTINGLIST = 0x15a;
    private const DISPLAYHUNTINGLIST2 = 0x15b;
    private const SETHUNTINGLIST = 0x15c;
    private const CLEARHUNTINGLIST = 0x15d;
    private const ISTIMELISTFULL = 0x15e;
    private const FINDTIMELIST = 0x15f;
    private const ADDTIMELIST = 0x160;
    private const DELETETIMELIST = 0x161;
    private const DISPLAYTIMELIST = 0x162;
    private const SETTIMELIST = 0x163;
    private const CLEARTIMELIST = 0x164;
    private const CHANGECELLTYPE = 0x165;
    private const GETCELLTYPE = 0x166;
    private const ISPCCAFE = 0x167;
    private const GETPAYTYPE = 0x168;
    private const GETCONNECTIONAREA = 0x169;
    private const GETCONNECTIONSTATE = 0x16a;
    private const GETPETEGG = 0x16b;
    private const CASHTRADER = 0x16c;
    private const CASHSHOP = 0x16d;
    private const ADDEXPTIME = 0x16e;
    private const SUBEXPTIME = 0x16f;
    private const ADDRECEIVEITEM = 0x170;
    private const ADVEQUIP_ISSUCCESSREFINERY = 0x171;
    private const PARTYCALL = 0x172;
    private const PUTBOSS = 0x173;
    private const USEBOSSPERCEPTION = 0x174;
    private const SKILLTOME = 0x175;
    private const UseHuntingList = 0x176;
    private const CheckHuntingList = 0x177;
    private const ITEM2 = 0x178;
    private const WEAPONPROPERTY = 0x179;
    private const READBOOK = 0x17a;
    private const MERCENARY_SUMMON = 0x17b;
    private const MERCENARY_HEALHP = 0x17c;
    private const MERCENARY_HEALSP = 0x17d;
    private const GETMERCENARYTOTALSUMMONNUM = 0x17e;
    private const GETMERCENARYFAITH = 0x17f;
    private const SUBMERCENARYFAITH = 0x180;
    private const ADDMERCENARYFAITH = 0x181;
    private const NPC2 = 0x182;
    private const ONMOVENPCCMD = 0x183;
    private const MOVEPOS = 0x184;
    private const MOVEWAIT = 0x185;
    private const REFINERY = 0x186;
    private const MERCENARY_CONDITION = 0x187;
    private const ADDNEVERKNOCKBACK = 0x188;
    private const GETADVEQUIPPERCENTREFINERY = 0x189;
    private const RELOAD_MOBILE_EVENT = 0x18a;
    private const SETTEMPTIME = 0x18b;
    private const ELAPSETEMPTIME = 0x18c;
    private const SUBNEVERKNOCKBACK = 0x18d;
    private const GETWEAPONCLASS = 0x18e;
    private const GETEQUIPSLOTAMOUNT = 0x18f;
    private const ADDHEALVALUE = 0x190;
    private const SUBHEALVALUE = 0x191;
    private const SETAUTOSPELL_MAGATKED = 0x192;
    private const ADDHPAMOUNT_MAGKILL = 0x193;
    private const SUBHPAMOUNT_MAGKILL = 0x194;
    private const ADDSPAMOUNT_MAGKILL = 0x195;
    private const SUBSPAMOUNT_MAGKILL = 0x196;
    private const ADDSPELLDELAY = 0x197;
    private const SUBSPELLDELAY = 0x198;
    private const ADDSPECIFICSPELLCASTTIME = 0x199;
    private const SUBSPECIFICSPELLCASTTIME = 0x19a;
    private const ADDMDAMAGE_RACE = 0x19b;
    private const SUBMDAMAGE_RACE = 0x19c;
    private const ADDMDAMAGE_CLASS = 0x19d;
    private const SUBMDAMAGE_CLASS = 0x19e;
    private const SETIGNOREMDEFRACE = 0x19f;
    private const RESETIGNOREMDEFRACE = 0x1a0;
    private const SETIGNOREMDEFCLASS = 0x1a1;
    private const RESETIGNOREMDEFCLASS = 0x1a2;
    private const SETHPAMOUNTTIME = 0x1a3;
    private const RESETHPAMOUNTTIME = 0x1a4;
    private const SETSPAMOUNTTIME = 0x1a5;
    private const RESETSPAMOUNTTIME = 0x1a6;
    private const SETIGNOREDEFRACE_PERCENT = 0x1a7;
    private const RESETIGNOREDEFRACE_PERCENT = 0x1a8;
    private const SETIGNOREDEFCLASS_PERCENT = 0x1a9;
    private const RESETIGNOREDEFCLASS_PERCENT = 0x1aa;
    private const GETINVENTORYREMAINCOUNT = 0x1ab;
    private const SETAUTOWEAPON = 0x1ac;
    private const RESETAUTOWEAPON = 0x1ad;
    private const SETAUTOMWEAPON = 0x1ae;
    private const RESETAUTOMWEAPON = 0x1af;
    private const SETAUTOATTACKED = 0x1b0;
    private const RESETAUTOATTACKED = 0x1b1;
    private const SETAUTOMATTACKED = 0x1b2;
    private const RESETAUTOMATTACKED = 0x1b3;
    private const RESETRECEIVEITEM_GROUP = 0x1b4;
    private const BUFF = 0x1b5;
    private const SETBUFF = 0x1b6;
    private const GETBUFFVALUE = 0x1b7;
    private const ADDHEALMODIFYPERCENT = 0x1b8;
    private const SUBHEALMODIFYPERCENT = 0x1b9;
    private const SETHPPERCENTTIME = 0x1ba;
    private const RESETHPPERCENTTIME = 0x1bb;
    private const SETSPPERCENTTIME = 0x1bc;
    private const RESETSPPERCENTTIME = 0x1bd;
    private const LOUDSPEAKER = 0x1be;
    private const SETTARGETATTACKED_BUFF = 0x1bf;
    private const RESETTARGETATTACKED_BUFF = 0x1c0;
    private const GETMAPNAME = 0x1c1;
    private const SETQUEST = 0x1c2;
    private const CHANGQUEST = 0x1c3;
    private const ERASEQUEST = 0x1c4;
    private const COMPLATEQUEST = 0x1c5;
    private const COMPLETEQUEST_BETWEEN = 0x1c6;
    private const RECALL_COMPLETEQUEST = 0x1c7;
    private const ISBEGINQUEST = 0x1c8;
    private const CHECKQUEST_HUNTING = 0x1c9;
    private const CHECKQUEST_PLAYTIME = 0x1ca;
    private const GETLASTSIEGEMSG = 0x1cb;
    private const PCDATA_MOVE_TO_SAKRAY = 0x1cc;
    private const CALLGUARDIAN = 0x1cd;
    private const ISPREMIUMPCCAFE = 0x1ce;
    private const MDUNGEON_SUBSCRIPTION = 0x1cf;
    private const MDUNGEON_ENTER = 0x1d0;
    private const CAMPCODE = 0x1d1;
    private const GETCAMPCODE = 0x1d2;
    private const CALLCAMPMOB = 0x1d3;
    private const SETRP2 = 0x1d4;
    private const MASSSETRP2 = 0x1d5;
    private const PLAYERTORP = 0x1d6;
    private const MGETEVENT_ZENYNAME_EXCULUSEIVE = 0x1d7;
    private const MGETEVENT_ITEMNAME_EXCULUSEIVE = 0x1d8;
    private const MGETEVENT_ZENY_EXCULUSEIVE = 0x1d9;
    private const MGETEVENT_ITEM_EXCULUSEIVE = 0x1da;
    private const MGETEVENT_ITEMID_EXCULUSEIVE = 0x1db;
    private const MGETEVENT_RELOAD_EXCULUSEIVE = 0x1dc;
    private const MDUNGEON_LIST = 0x1dd;
    private const MDUNGEON_DESTROY = 0x1de;
    private const PLUSCAMPPOINT = 0x1df;
    private const MINUSCAMPPOINT = 0x1e0;
    private const GETCAMPPOINT = 0x1e1;
    private const RESETCAMPPOINT = 0x1e2;
    private const CHECKSIEGETIME = 0x1e3;
    private const ADDSTATE_MATK = 0x1e4;
    private const SUBSTATE_MATK = 0x1e5;
    private const ADDSTATE_MATTACKED = 0x1e6;
    private const SUBSTATE_MATTACKED = 0x1e7;
    private const MDUNGEON_MAPNAME = 0x1e8;
    private const MDUNGEON_NPCNAME = 0x1e9;
    private const MDUNGEON_NPC = 0x1ea;
    private const MDUNGEON_WAP = 0x1eb;
    private const MDUNGEON_HIDDENWAP = 0x1ec;
    private const MDUNGEON_PUTMOB = 0x1ed;
    private const MYMOBSKILLUSE = 0x1ee;
    private const WHEREMYMOB_X = 0x1ef;
    private const WHEREMYMOB_Y = 0x1f0;
    private const WIDEMOBSKILLUSE = 0x1f1;
    private const WIDEMOBSKILLUSE2 = 0x1f2;
    private const MDUNGEON_OPENSTATE = 0x1f3;
    private const ADDHPDRAIN_100 = 0x1f4;
    private const SUBHPDRAIN_100 = 0x1f5;
    private const CREATEPACKAGE = 0x1f6;
    private const ISBENDER = 0x1f7;
    private const MOVERESUME = 0x1f8;
    private const MOVERETURN = 0x1f9;
    private const MOVESPEED = 0x1fa;
    private const GETNPCPOSITION = 0x1fb;
    private const ADDRECEIVEITEM_EQUIP = 0x1fc;
    private const SUBRECEIVEITEM_EQUIP = 0x1fd;
    private const ADDONLYJOBEXPTIME = 0x1fe;
    private const PROGRESS_BAR = 0x1ff;
    private const MERCENARYCHECK = 0x200;
    private const MERCENARYTYPE = 0x201;
    private const ISFREESERVER = 0x202;
    private const SETSKILLAUTOSPELL = 0x203;
    private const RESETSKILLAUTOSPELL = 0x204;
    private const SETSKILLACTIVATE = 0x205;
    private const RESETSKILLACTIVATE = 0x206;
    private const GETACTIVATED_SKILL = 0x207;
    private const GETDEACTIVATED_SKILL = 0x208;
    private const ADDSTATE_SKID = 0x209;
    private const SUBSTATE_SKID = 0x20a;
    private const SETAUTOSPELLMAGIC = 0x20b;
    private const RESETAUTOSPELLMAGIC = 0x20c;
    private const RUN_NPC = 0x20d;
    private const RUN_NPCTRADER = 0x20e;
    private const ONCAMPCOMMAND = 0x20f;
    private const CHECKPARTYJOB = 0x210;
    private const CLEARBUFF = 0x211;
    private const CHECKALIVE = 0x212;
    private const KVM_NPC = 0x213;
    private const KVM_START_ALIVECHECK = 0x214;
    private const PVPRANKCHECK = 0x215;
    private const SETAUTOSPELL_MLEATK = 0x216;
    private const SETAUTOSPELL_RANGEATK = 0x217;
    private const KVM_NPC_NOTI = 0x218;
    private const GETARENAEVENTSIZE = 0x219;
    private const SETMAKABLERUNEITEMLIST = 0x21a;
    private const ISSUCCESSRUNEUSE = 0x21b;
    private const SETMAKABLENEWPOISONLIST = 0x21c;
    private const GETITEMSOCKET = 0x21d;
    private const GETPETRELATIONSHIP = 0x21e;
    private const ADDMDAMAGE_NAME = 0x21f;
    private const SUBMDAMAGE_NAME = 0x220;
    private const GETNONSLOTITEMSOCK = 0x221;
    private const ISEFFECTSTATUS = 0x222;
    private const GETMYMERCENARY = 0x223;
    private const ERRORLOG = 0x224;
    private const ADDRECEIVEITEM_NAME = 0x225;
    private const SUBRECEIVEITEM_NAME = 0x226;
    private const ISMADOGEAR = 0x227;
    private const SETMADOGEAR = 0x228;
    private const ADD_SFCT_CONSUME_AMOUNT = 0x229;
    private const SUB_SFCT_CONSUME_AMOUNT = 0x22a;
    private const ADD_SFCT_CONSUME_PERMILL = 0x22b;
    private const SUB_SFCT_CONSUME_PERMILL = 0x22c;
    private const ADD_SFCT_EQUIP_AMOUNT = 0x22d;
    private const SUB_SFCT_EQUIP_AMOUNT = 0x22e;
    private const ADD_SFCT_EQUIP_PERMILL = 0x22f;
    private const SUB_SFCT_EQUIP_PERMILL = 0x230;
    private const ADDATTRTOLERACE_DISREGARD = 0x231;
    private const SUBATTRTOLERACE_DISREGARD = 0x232;
    private const GETEXDEQUIPISSUCCESSREFINERY = 0x233;
    private const GETEXDADVEQUIPISSUCCESSREFINERY = 0x234;
    private const DOWNREFITEM = 0x235;
    private const DOWNADVREFITEM = 0x236;
    private const SUCCESSRANDREFITEM = 0x237;
    private const SUCCESSRANDADVREFITEM = 0x238;
    private const FINAL_BATTLE_PLAY = 0x239;
    private const ONCAMPCOMMAND2 = 0x23a;
    private const INTEROTHER = 0x23b;
    private const SETAUTOSPELL_MLEATKED_V2 = 0x23c;
    private const SETAUTOSPELL_RANGEATKED_V2 = 0x23d;
    private const ADDSKILLSP = 0x23e;
    private const SUBSKILLSP = 0x23f;
    private const ADDSKILLDELAY = 0x240;
    private const SUBSKILLDELAY = 0x241;
    private const PLAYBGM = 0x242;
    private const STRIPTPC2 = 0x243;
    private const ADDSTATECASTTIME = 0x244;
    private const SUBSTATECASTTIME = 0x245;
    private const ADDSKILLMDAMAGE = 0x246;
    private const SUBSKILLMDAMAGE = 0x247;
    private const GETLOCATION = 0x248;
    private const BROADCAST_IN_ZONE = 0x249;
    private const BROADCAST_IN_MAP = 0x24a;
    private const SET_ITEM_EFFECT = 0x24b;
    private const RESET_ITEM_EFFECT = 0x24c;
    private const SETREADYMUTATIONHOMUN = 0x24d;
    private const MUTATIONHOMUN = 0x24e;
    private const MODIFYMDEF_FRACTION = 0x24f;
    private const ADDALLDEF_FLUCTUATION = 0x250;
    private const SUBALLDEF_FLUCTUATION = 0x251;
    private const ADDALLMDEF_FLUCTUATION = 0x252;
    private const SUBALLMDEF_FLUCTUATION = 0x253;
    private const OPEN_SEARCH_STORE_INFO = 0x254;
    private const CHANGERP = 0x255;
    private const ISALLIANCEGUILD = 0x256;
    private const OPENHOUR_SIEGEMODE = 0x257;
    private const CHECKHOMUNLEVEL = 0x258;
    private const CHECKHOMUNMUTATION = 0x259;
    private const CHECKHOMUNCALL = 0x25a;
    private const SETBATTLEFIELDMOVEPOSTION = 0x25b;
    private const GETEQUIPCOMPOSITIONTYPE = 0x25c;
    private const GETEQUIPDBNAME = 0x25d;
    private const GETEQUIPCARDID = 0x25e;
    private const DROPEQUIPITEM = 0x25f;
    private const GETREFITEM = 0x260;
    private const RESETBUFF = 0x261;
    private const ADDSTATE_EFSTATK = 0x262;
    private const SUBSTATE_EFSTATK = 0x263;
    private const GETNONSLOTITEMSOCK2 = 0x264;
    private const GETITEM2 = 0x265;
    private const SHOWSCRIPT = 0x266;
    private const SHOWEMOTION = 0x267;
    private const START_COLLECTION = 0x268;
    private const MONTRANSFORM = 0x269;
    private const DELETE_ALL_BODY_ITEM = 0x26a;
    private const ONAGITINVEST = 0x26b;
    private const INC_GLOBALVAR = 0x26c;
    private const DEC_GLOBALVAR = 0x26d;
    private const GET_AGITINVEST_MSG = 0x26e;
    private const ADD_HANDICAPSTATE_RACE = 0x26f;
    private const SUB_HANDICAPSTATE_RACE = 0x270;
    private const SET_NUM_DEF_RACE = 0x271;
    private const RESET_NUM_DEF_RACE = 0x272;
    private const SET_NUM_MDEF_RACE = 0x273;
    private const RESET_NUM_MDEF_RACE = 0x274;
    private const SET_PER_DEF_SELF = 0x275;
    private const SET_PER_MDEF_SELF = 0x276;
    private const SET_MHP_PER_DAMAGE_RACE = 0x277;
    private const RESET_MHP_PER_DAMAGE_RACE = 0x278;
    private const SET_MSP_PER_DAMAGE_RACE = 0x279;
    private const RESET_MSP_PER_DAMAGE_RACE = 0x27a;
    private const GET_AGIT_ECONOMY = 0x27b;
    private const INC_AGIT_ECONOMY = 0x27c;
    private const DEC_AGIT_ECONOMY = 0x27d;
    private const EQ_MAKE = 0x27e;
    private const EQ_CHECK_USER = 0x27f;
    private const EQ_NOTIFY_ADMISSION = 0x280;
    private const EQ_IS_READY = 0x281;
    private const EQ_MOVE_TO_ROOM = 0x282;
    private const EQ_RETURN_TO_PRE_SPACE = 0x283;
    private const EQ_MOVE_TO_LOBBY = 0x284;
    private const LOBBY_MAKE = 0x285;
    private const LOBBY_USER_COUNT = 0x286;
    private const LOBBY_NOTIFY_ADMISSION = 0x287;
    private const LOBBY_IS_READY = 0x288;
    private const LOBBY_MOVE_TO_ROOM = 0x289;
    private const LOBBY_RETURN_TO_PRE_SPACE = 0x28a;
    private const LOBBY_INIT = 0x28b;
    private const CLEARDIALOG = 0x28c;
    private const NAVIGATION_ACTIVE = 0x28d;
    private const ACTIVE_MONTRANSFORM = 0x28e;
    private const DEACTIVE_MONTRANSFORM = 0x28f;
    private const SETAUTOSPELL2 = 0x290;
    private const SETAUTOSPELL2_MLEATKED = 0x291;
    private const SETAUTOSPELL2_MLEATKED_SHORT = 0x292;
    private const SETAUTOSPELL2_MLEATKED_LONG = 0x293;
    private const SETAUTOSPELL2_MAGATKED = 0x294;
    private const SETAUTOSPELL2_MLEATK = 0x295;
    private const SETAUTOSPELL2_RANGEATK = 0x296;
    private const RESETAUTOSPELL2 = 0x297;
    private const RESETAUTOSPELL2_MLEATKED = 0x298;
    private const RESETAUTOSPELL2_MLEATKED_SHORT = 0x299;
    private const RESETAUTOSPELL2_MLEATKED_LONG = 0x29a;
    private const RESETAUTOSPELL2_MAGATKED = 0x29b;
    private const RESETAUTOSPELL2_MLEATK = 0x29c;
    private const RESETAUTOSPELL2_RANGEATK = 0x29d;
    private const PAUSEAUTOSPELL2 = 0x29e;
    private const PAUSEAUTOSPELL2_MLEATKED = 0x29f;
    private const PAUSEAUTOSPELL2_MLEATKED_SHORT = 0x2a0;
    private const PAUSEAUTOSPELL2_MLEATKED_LONG = 0x2a1;
    private const PAUSEAUTOSPELL2_MAGATKED = 0x2a2;
    private const PAUSEAUTOSPELL2_MLEATK = 0x2a3;
    private const PAUSEAUTOSPELL2_RANGEATK = 0x2a4;
    private const RESUMEAUTOSPELL2 = 0x2a5;
    private const RESUMEAUTOSPELL2_MLEATKED = 0x2a6;
    private const RESUMEAUTOSPELL2_MLEATKED_SHORT = 0x2a7;
    private const RESUMEAUTOSPELL2_MLEATKED_LONG = 0x2a8;
    private const RESUMEAUTOSPELL2_MAGATKED = 0x2a9;
    private const RESUMEAUTOSPELL2_MLEATK = 0x2aa;
    private const RESUMEAUTOSPELL2_RANGEATK = 0x2ab;
    private const CHECK_JOB_GROUP = 0x2ac;
    private const GETPREMIUMCAMPAIGN_GRADE = 0x2ad;
    private const GET_BASE_JOB = 0x2ae;
    private const GET_CATEGORY_JOB = 0x2af;
    private const EQ_JOB_CONTROL = 0x2b0;
    private const ADDMDAMAGE_PROPERTY = 0x2b1;
    private const SUBMDAMAGE_PROPERTY = 0x2b2;
    private const LOBBY_CAMPCODE = 0x2b3;
    private const INSERT_ALIVE_MEMBER = 0x2b4;
    private const EQ_USERINFO_INIT = 0x2b5;
    private const ADDQUESTINFO = 0x2b6;
    private const ADDQUESTINFO2 = 0x2b7;
    private const SETQUESTJOB = 0x2b8;
    private const SETQUESTSEX = 0x2b9;
    private const SETQUESTLEVEL = 0x2ba;
    private const SETQUESTJOBLEVEL = 0x2bb;
    private const SETQUESTITEM = 0x2bc;
    private const SETQUESTHOMUNLEVEL = 0x2bd;
    private const SETQUESTHOMUNTYPE = 0x2be;
    private const SETQUESTQUESTITEM = 0x2bf;
    private const IS_SET_QUEST = 0x2c0;
    private const IS_LOWLEVEL_SIEGE_JOB = 0x2c1;
    private const RESET_BUFF_LOWLEVEL_SIEGE = 0x2c2;
    private const EQ_MOVE_TO_LOBBY_JOB_MATCHING = 0x2c3;
    private const GET_YEAR = 0x2c4;
    private const GET_MONTH = 0x2c5;
    private const GET_DAY = 0x2c6;
    private const GET_WEEK_DAY = 0x2c7;
    private const MERGE_ITEM = 0x2c8;
    private const DIALOG2 = 0x2c9;
    private const WAITDIALOG2 = 0x2ca;
    private const STORE_V2 = 0x2cb;
    private const CALLFALCON = 0x2cc;
    private const SETPRNPCWINNER = 0x2cd;
    private const PD_SUBSCRIPTION = 0x2ce;
    private const PD_ENTER = 0x2cf;
    private const SETQUESQUEST = 0x2d0;
    private const TALK_SHOW = 0x2d1;
    private const RENT_ITEM = 0x2d2;
    private const PROVIDE_EXP = 0x2d3;
    private const GET_MONSTER_HP = 0x2d4;
    private const MONSTER_TALK_SHOW = 0x2d5;
    private const CHECKJOBGROUP2 = 0x2d6;
    private const JOIN_CLAN = 0x2d7;
    private const LEAVE_CLAN = 0x2d8;
    private const SET_MONSTER_HP = 0x2d9;
    private const ITEMDOWN2 = 0x2da;
    private const GET_ELAPSED_TIMER = 0x2db;
    private const IS_ENABLE_NPC = 0x2dc;
    private const GET_NPC_MONSTER_X = 0x2dd;
    private const GET_NPC_MONSTER_Y = 0x2de;
    private const DELAY_TIME = 0x2df;
    private const SET_CLAN_MASTER = 0x2e0;
    private const SAVEPOSITION = 0x2e1;
    private const MOVETOSAVEPOSITION = 0x2e2;
    private const MDUNGEON_TRADER = 0x2e3;
    private const SET_MONSTER_HP2 = 0x2e4;
    private const GET_RANKING_POINT = 0x2e5;
    private const INC_RANKING_POINT = 0x2e6;
    private const DEC_RANKING_POINT = 0x2e7;
    private const NPC_MONTRANSFORM = 0x2e8;
    private const PARTY_MAP_MOVE = 0x2e9;
    private const IS_PREMIUM = 0x2ea;
    private const SETSKILL = 0x2710;
    private const ONUSE = 0x2711;
    private const ONSUCCESS = 0x2712;
    private const SKILLTYPE = 0x2713;
    private const SKILLMAXLV = 0x2714;
    private const SKILLPATTERN = 0x2715;
    private const SKILLSPLASH = 0x2716;
    private const SKILLHITPERCENT = 0x2717;
    private const SKILLATKPERCENT = 0x2718;
    private const SKILLDATA1 = 0x2719;
    private const SKILLDATA2 = 0x271a;
    private const SKILLFLAG = 0x271b;
    private const SKILLRANGE = 0x271c;
    private const SKILLPROPERTY = 0x271d;
    private const SKILLHANDICAP = 0x271e;
    private const SKILLKNOCKBACK = 0x271f;
    private const SKILLNODAMAGE = 0x2720;
    private const SKILLSP = 0x2721;
    private const SKILLPREDELAYTIME = 0x2722;
    private const SKILLPOSTDELAYTIME = 0x2723;
    private const SKILLRESETHANDICAP = 0x2724;
    private const SKILLDRAINHP = 0x2725;
    private const SKILLDRAINSP = 0x2726;
    private const SKILLEVENT = 0x2727;
    private const SKILLCASTFIXEDDELAY = 0x2728;
    private const SKILLCASTSTATDELAY = 0x2729;
    private const SKILLGLOBALPOSTDELAY = 0x272a;
    private const SKILLSINGLEPOSTDELAY = 0x272b;
    private const SKILLDATA3 = 0x272c;
    private const SKILLDATA4 = 0x272d;
    private const SKILLDATA5 = 0x272e;
    private const SKILLDATA6 = 0x272f;
    private const SKILLDATASTRING = 0x2730;
}