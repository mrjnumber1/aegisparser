<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static ItemType HEAL()
 * @method static ItemType SCHANGE()
 * @method static ItemType SPECIAL()
 * @method static ItemType EVENT()
 * @method static ItemType ARMOR()
 * @method static ItemType WEAPON()
 * @method static ItemType CARD()
 * @method static ItemType QUEST()
 * @method static ItemType BOW()
 * @method static ItemType BOTHHAND()
 * @method static ItemType ARROW()
 * @method static ItemType ARMORTM()
 * @method static ItemType ARMORTB()
 * @method static ItemType ARMORMB()
 * @method static ItemType ARMORTMB()
 * @method static ItemType GUN()
 * @method static ItemType AMMO()
 * @method static ItemType THROWWEAPON()
 * @method static ItemType CASHPOINTITEM()
 * @method static ItemType CANNONBALL()
 */
final class ItemType extends Enum
{
    private const HEAL = 0;
    private const SCHANGE = 1;
    private const SPECIAL = 2;
    private const EVENT = 3;
    private const ARMOR = 4;
    private const WEAPON = 5;
    private const CARD = 6;
    private const QUEST = 7;
    private const BOW = 8;
    private const BOTHHAND = 9;
    private const ARROW = 10;
    private const ARMORTM = 11;
    private const ARMORTB = 12;
    private const ARMORMB = 13;
    private const ARMORTMB = 14;
    private const GUN = 15;
    private const AMMO = 16;
    private const THROWWEAPON = 17;
    private const CASHPOINTITEM = 18;
    private const CANNONBALL = 19;
}