<?php
/** @noinspection PhpUnusedPrivateFieldInspection */


namespace AegisParser;


use MyCLabs\Enum\Enum;

/**
 * @method static MobSkillCondition IF_HP()
 * @method static MobSkillCondition IF_CONDITION()
 * @method static MobSkillCondition IF_SLAVENUM()
 * @method static MobSkillCondition IF_MAGICLOCKED()
 * @method static MobSkillCondition IF_RANGEATTACKED()
 * @method static MobSkillCondition IF_COMRADECONDITION()
 * @method static MobSkillCondition IF_ENEMYCOUNT()
 * @method static MobSkillCondition IF_SKILLUSE()
 * @method static MobSkillCondition IF_HIDING()
 * @method static MobSkillCondition IF_COMRADEHP()
 * @method static MobSkillCondition IF_RUDEATTACK()
 * @method static MobSkillCondition IF_ALCHEMIST()
 * @method static MobSkillCondition IF_TRICKCASTING()
 * @method static MobSkillCondition IF_MASTERATTACKED()
 * @method static MobSkillCondition IF_MASTERHP()
 */

final class MobSkillCondition extends Enum
{
    private const IF_HP = 0;
    private const IF_CONDITION = 1;
    private const IF_SLAVENUM = 2;
    private const IF_MAGICLOCKED = 3;
    private const IF_RANGEATTACKED = 4;
    private const IF_COMRADECONDITION = 5;
    private const IF_ENEMYCOUNT = 6;
    private const IF_SKILLUSE = 7;
    private const IF_HIDING = 8;
    private const IF_COMRADEHP = 9;
    private const IF_RUDEATTACK = 10;
    private const IF_ALCHEMIST = 11;
    private const IF_TRICKCASTING = 12;
    private const IF_MASTERATTACKED = 13;
    private const IF_MASTERHP = 14;

    public static function HasParameter(MobSkillCondition $cond) : bool {
        switch ($cond->getValue()) {
            case self::IF_HP:
            case self::IF_CONDITION:
            case self::IF_SLAVENUM:
            case self::IF_COMRADECONDITION:
            case self::IF_ENEMYCOUNT:
            case self::IF_SKILLUSE:
            case self::IF_COMRADEHP:
            case self::IF_MASTERHP:
                return true;
        }

        return false;
    }
    public static function ValidateParameter(MobSkillCondition $cond, mixed $value) : mixed {
        switch ($cond->getValue()) {
            case self::IF_SLAVENUM:
            case self::IF_ENEMYCOUNT:
            assert(is_numeric($value), "MobSkillCondition::ValidateParameter Invalid value $value for param {$cond->getKey()}");
                return (int)$value;
            case self::IF_MASTERHP:
            case self::IF_COMRADEHP:
            case self::IF_HP:
                assert(is_numeric($value), "MobSkillCondition::ValidateParameter Invalid value $value for param {$cond->getKey()}");
                $value = (int)$value;
                assert($value > 0 && $value <= 100, "MobSkillCondition::ValidateParameter {$cond->getKey()} value $value must be in range(0, 100]!");
                return $value;
            case self::IF_CONDITION:
            case self::IF_COMRADECONDITION:
                assert(Condition::isValidKey($value) || $value === "BODY_ALL", "MobSkillCondition::ValidateParameter {$cond->getKey()} unknown condition `$value`!");
                if (Condition::isValidKey($value)) {
                    return Condition::$value();
                }
                return "BODY_ALL";
            case self::IF_SKILLUSE:
                assert(SkillID::isValidKey($value), "MobSkillCondition::ValidateParameter {$cond->getKey()} unknown skill `$value` to counter!");
                return SkillID::$value();
        }

        assert(false, "MobSkillCondition::ValidateParameter Unhandled cond {$cond->getKey()} and unknown value `$value`!");
    }
}