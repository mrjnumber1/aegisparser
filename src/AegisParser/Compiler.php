<?php

namespace AegisParser;

use Ds\Map;
use Ds\Pair;
use Ds\Stack;
use Ds\Vector;

class Compiler extends ScriptHandler
{
    private int $switch;
    private bool $isCase;
    private Block $block;
    private Script $script;
    private Asm $asm;
    private Bin $bin;
    private Vector $gotoInfo;
    private Map $blockCheckMap;
    private Stack $whileStack;
    private int $if;
    private array $switchInIf;
    private int $whileBlock;
    private Cmd $cmd;

    private const WHITESPACE = " \t";
    private const MAX_STRING_LENGTH = 250;
    private const MAX_SWITCH_IN_IF = 30;

    public function __construct() {
        $this->tokenMap = new TokenMap();
        $this->block = new Block();
        $this->script = new Script();
        $this->asm = new Asm();
        $this->bin = new Bin();
        $this->gotoInfo = new Vector();
        $this->blockCheckMap = new Map();
        $this->whileStack = new Stack();

        $this->script->RegisterHandler($this);

        $this->SetCmd(Cmd::BLOCKBREAK(), '__block');
        $this->SetCmd(Cmd::END(), 'end');
        $this->SetCmd(Cmd::VAR(), 'var');
        $this->SetCmd(Cmd::IF(), 'if');
        $this->SetCmd(Cmd::ELSEIF(), 'elseif');
        $this->SetCmd(Cmd::ELSE(), 'else');
        $this->SetCmd(Cmd::ENDIF(), 'endif');
        $this->SetCmd(Cmd::DECLARE(), 'declare');
        $this->SetCmd(Cmd::DEFINE(), 'define');
        $this->SetCmd(Cmd::SWITCH(), 'choose');
        $this->SetCmd(Cmd::CASE(), 'case');
        $this->SetCmd(Cmd::BREAK(), 'break');
        $this->SetCmd(Cmd::DEFAULT(), 'default');
        $this->SetCmd(Cmd::ENDSWITCH(), 'endchoose');
        $this->SetCmd(Cmd::DEFCMD(), 'defcmd');
        $this->SetCmd(Cmd::WHILE(), 'while');
        $this->SetCmd(Cmd::ENDWHILE(), 'endwhile');
        $this->SetCmd(Cmd::EXITWHILE(), 'exitwhile');

        $this->whileBlock = $this->switch = 0;
    }

    private function SetCmd(Cmd $cmd, string $key): void {
        $this->tokenMap->Set($key, TokenType::COMMAND(), $cmd->getValue(), '');
    }

    private function QueryToken(string $key, TokenType $expectedType): TokenInfo {
        $t = $this->tokenMap->GetToken($key);
        assert(!is_null($t), "QueryToken key not found `$key`");
        assert($t->GetType()->equals($expectedType), "QueryToken searched for non-{$expectedType->getKey()} key `$key`");
        return $t;
    }

    public function GetCmdValue(string $key): int {
        return $this->QueryToken($key, TokenType::COMMAND())->GetNum();
    }

    public function SetDefine(string $key, int $value): void {
        $this->tokenMap->Set($key, TokenType::DEFINE(), 1, $value);
    }

    public function GetDefine(string $key): TokenInfo {
        return $this->QueryToken($key, TokenType::DEFINE());
    }

    public function GetDefineValue(string $key): int {
        return $this->GetDefine($key)->GetStrAsInt();
    }

    public function LoadEnums(array $lines, int $version): bool {
        $iter = 0;
        $date = 0;

        foreach ($lines as $line) {
            $line = trim($line);

            if ($line === '' || str_starts_with($line, ';') || str_starts_with($line, '//')) {
                continue;
            }

            $line = preg_replace('/\s+/', ' ', $line);

            if (str_contains($line, '//')) {
                $line = substr($line, 0, strpos($line, '//'));
            }

            if (!$date) {
                $date = (int)$line;
                if ($date !== $version) {
                    return false;
                }
                continue;
            }

            if ($line[0] === '#') {
                $iter = (int)str_replace('#', '', $line);
                continue;
            }

            $data = preg_split("/[\s,]+/", $line);

            foreach ($data as $key) {
                $key = trim($key);
                if (empty($key) || str_starts_with($key, '//')) {
                    continue;
                }

                $this->SetDefine($key, $iter);
                ++$iter;
            }
        }

        return true;
    }

    public function LoadEnum(string $filename, int $version): bool {
        assert(file_exists($filename), "Compiler::LoadEnum file `$filename` not found!");
        $data = file_get_contents($filename);
        assert($data !== false, "LoadEnum fail for `$filename`");

        return $this->LoadEnums(explode("\n", $data), $version);
    }

    public function LoadDefs(array $lines): bool {
        foreach ($lines as $line) {
            $line = trim($line);
            if ($line === '') {
                continue;
            }

            $line = preg_replace('/\s+/', ' ', $line);

            $data = explode(' ', $line);
            assert(count($data) === 2, "LoadDefs invalid definitions on `$line`");

            [$name, $value] = $data;

            $this->tokenMap->Set($name, TokenType::DEFINE(), 1, $value);
        }

        return true;
    }

    public function LoadDef(string $filename): bool {
        assert(file_exists($filename), "Compiler::LoadDef file `$filename` not found!");
        $data = file_get_contents($filename);
        assert($data !== false, "LoadDef fail for `$filename`");

        return $this->LoadDefs(explode("\n", $data));
    }

    public function SetBin(string $filename): bool {
        return $this->bin->Set($filename);
    }

    public function SetAsm(string $filename): bool {
        return $this->asm->Set($filename);
    }

    public function OnControl(ScriptLine $line, Cmd $cmd): bool {
        switch ($cmd) {
            default:
                return true;
            case Cmd::IF():
            case Cmd::SWITCH():
            case Cmd::WHILE():
                $started = $this->block->Start($this->bin->GetPos());
                assert($started, "Compiler::OnControl: if/switch/while too many block number");
                break;
            case Cmd::BREAK():
                $this->Goto($this->block->GetEndId());
                return true;
            case Cmd::CASE():
            case Cmd::DEFAULT():
                $linked = $this->block->Link($this->bin->GetPos());
                assert($linked, "Compiler::OnControl: case/default too many block number.");
                break;
            case Cmd::ELSE():
            case Cmd::ELSEIF():
                $this->goto($this->block->GetEndId());
                $linked = $this->block->Link($this->bin->GetPos());
                assert($linked, "Compiler::OnControl: else/elseif too many linked block number.");
                break;
            /** @noinspection PhpMissingBreakStatementInspection */
            case Cmd::ENDWHILE():
                $this->goto($this->block->GetStartId());
            // fallthrough
            case Cmd::ENDIF():
            case Cmd::ENDSWITCH():
                $linked = $this->block->Link($this->bin->GetPos());
                assert($linked, "Compiler::OnControl: endif/endswitch too many linked blocked number");

                $pos = $this->bin->GetPos();
                $id = $this->block->GetCurId();
                $this->asm->Comment("block " . ($id >> 8) . "-".($id&0xFF)." id$id  addr hex[$pos]");
                $this->asm->Comment("block " . ($id >> 8) . "-255 id" . ($id | 255) . "  addr hex[$pos]");

                $ended = $this->block->End($pos);
                assert($ended, "Compiler::OnControl: endif/endswitch block {} not match number.");
                return true;
        }

        $pos = $this->bin->GetPos();
        $id = $this->block->GetCurId();
        $this->asm->Comment("block " . ($id >> 8) . "-".($id&0xFF)." id$id  addr hex[$pos]");

        return true;
    }

    public function OnVar(ScriptLine $line, string $name): bool {
        $line->Skip(self::WHITESPACE);

        $chars = $line->GetOperator("=+-*/%");
        $op = match ($chars) {
            "=" => Code::MOV(),
            "+", "+=" => Code::ADD(),
            "-", "-=" => Code::SUB(),
            "*", "*=" => Code::MUL(),
            "/", "/=" => Code::DIV(),
            "%", "%=" => Code::MOD(),
            "++" => Code::INC(),
            "--" => Code::DEC(),
            default => null
        };
        assert(!is_null($op), "Compiler::OnVar 1: [$op] is an invalid operator.");

        $this->writeCode($op);
        $this->writeVar($name);

        /** @noinspection TypeUnsafeComparisonInspection */
        /** @noinspection PhpNonStrictObjectEqualityInspection */
        if ($op == Code::INC() || $op == Code::DEC()) {
            return true;
        }

        $ok = $this->value($line);
        assert($ok, "Compiler::OnVar 2: [$op] is invalid operator.");

        return true;
    }

    public function OnFunc(ScriptLine $line, ScriptFunc $func, string $params) : bool {
        if (!$this->checkSwitchBlock()) {
            return false;
        }

        if (!$this->block->IsComplete() && $this->blockCheckMap->hasKey($func->getValue())) {
            return false;
        }
        $this->writeCode(Code::FUNC());
        $this->writeFunc($func);

        // "." should only be valid as the first option however
        if (!$params[0] || $params[0] === '.') {
            $this->writeType(';');
            return true;
        }

        $i=0;
        // WARNING: $i is manually incremented!
        $last = strlen($params);
        $p = '';
        while (($i < $last) && ($p !== '.'))
        {
            $p = $params[$i];
            $strict = $p !== "?";

            if ($p === 't')
            {
                $line->Skip(self::WHITESPACE);
                $line->GetWord(self::WHITESPACE);
                ++$i;
                continue;
            }

            if ($strict) {
                ++$i;
            }

            $this->writeType($p);

            if (!$this->value($line)) {
                assert(!$strict, "Compiler::OnFunc: {$func->getKey()} param error `$params` [$p]");
                $this->writeVar('NULL');
                $this->writeOp(';');
                break;
            }

        }

        $this->writeType(';');
        return true;
    }

    public function OnCommand(ScriptLine $line, Cmd $cmd) : bool {
        $this->cmd = $cmd;

        switch ($cmd) {
            case Cmd::END():
                return $this->cmdEnd($line);
            case Cmd::VAR():
                return $this->cmdVar($line);
            case Cmd::IF():
                return $this->cmdIf($line);
            case Cmd::ELSEIF():
                return $this->cmdElseIf($line);
            case Cmd::ELSE():
            case Cmd::DEFAULT():
                return $this->cmdElse($line);
            case Cmd::ENDIF():
                return $this->cmdEndIf($line);
            case Cmd::DECLARE():
                return $this->cmdDeclare($line);
            case Cmd::DEFINE():
                return $this->cmdDefine($line);
            case Cmd::SWITCH():
                return $this->cmdSwitch($line);
            case Cmd::CASE():
                return $this->cmdCase($line);
            case Cmd::BREAK():
                return $this->cmdBreak($line);
            case Cmd::ENDSWITCH():
                return $this->cmdEndSwitch($line);
            case Cmd::DEFCMD():
                return $this->cmdDefCmd($line);
            case Cmd::BLOCKBREAK():
                return $this->cmdBlockBreak($line);
            case Cmd::WHILE():
                return $this->cmdWhile($line);
            case Cmd::ENDWHILE():
                return $this->cmdEndWhile($line);
            case Cmd::EXITWHILE():
                return $this->cmdExitWhile($line);
        }
        assert(false, "Unhandled Cmd {$cmd->getKey()}:{$cmd->getValue()}");
    }

    private function checkSwitchBlock(): bool {
        /** @noinspection TypeUnsafeComparisonInspection */
        /** @noinspection PhpNonStrictObjectEqualityInspection */
        return ($this->switch <= 0 || $this->isCase || $this->cmd == Cmd::ENDIF());
    }

    private function writeOp(string $str): void {
        $op = match ($str) {
            ";" => ScriptOp::END(),
            "==" => ScriptOp::EQUAL(),
            "!=", "<>" => ScriptOp::NOTEQUAL(),
            ">=", "=>" => ScriptOp::LARGE_OR_EQUAL(),
            "<=", "=<" => ScriptOp::SMALL_OR_EQUAL(),
            ">" => ScriptOp::LARGE(),
            "<" => ScriptOp::SMALL(),
            "&&", "&" => ScriptOp::AND(),
            "||", "|" => ScriptOp::OR(),
            "+" => ScriptOp::ADD(),
            "-" => ScriptOp::SUB(),
            "*" => ScriptOp::MUL(),
            "/" => ScriptOp::DIV(),
            "%" => ScriptOp::MOD(),
            default => null
        };
        assert(!is_null($op), "Compiler::writeOp: `$str` is invalid operator.");

        $this->asm->Put($str);

        $this->bin->WriteByte($op->getValue());
    }

    private function writeCall(string $func): void {
        // would assert, but, the func names/values are inconsistent
        $this->asm->Put("call $func");
        $this->bin->WriteByte(ord('f'));
    }

    private function writeStr(string $s): void {
        $this->asm->Put("\"$s\" ");

        $this->bin->WriteByte(ord('s'));
        $this->bin->WriteString($s);
    }

    private function writeNum(int $n): void {
        $this->asm->Put("[$n " . dechex($n) . "h]");

        $this->bin->WriteByte(ord('n'));
        $this->bin->WriteNum($n);
    }

    private function writeCode(Code $code): void {
        $this->asm->Line($this->bin->GetPos());

        $this->asm->Put(strtolower($code->getKey()) . " ");

        $this->bin->WriteByte(ord('c'));
        $this->bin->WriteByte($code->getValue());
    }

    private function writeFunc(ScriptFunc $func) : void {
        $n = (int)$func->getValue();
        $this->asm->Put("[$n " . dechex($n) . "h] ");
        $this->bin->WriteWord($n);

    }

    private function writeGotoBlock(int $block): void
    {
        $pos = $this->bin->GetPos();
        $info = new GotoInfo($pos, $block);
        $this->gotoInfo->push($info);


        $this->asm->Put("[addr " . dechex($pos) . "] goto " . ($block >> 8) . "-".($block&0xFF)."\tid");
        $this->writeNum($block);
    }

    private function writeType(string $type): void
    {
        assert(strlen($type) === 1, "Compiler::writeType expects a single character!");

        $names = [
            ';' => 'end',
            'f' => 'float:',
            'n' => 'num:',
            's' => 'str:',
            '?' => ''
        ];

        assert(array_key_exists($type, $names), "Compiler::writeType invalid key $type!");

        if ($type !== '?') {
            $this->asm->Put($names[$type]);
        }
        $this->bin->WriteByte(ord($type));
    }

    private function writeVar(string $name): void
    {
        assert($name !== '', "Compiler::writeVar: empty var name passed!");
        $this->asm->Put("v[$name] ");
        $this->bin->WriteByte(ord('v'));
        $this->bin->WriteString($name);
    }

    private function value(ScriptLine $line): bool
    {
        if (str_contains($line->GetBase(), "ShowEffect")) {
            echo '';
        }
        while (true) {
            $line->Skip(self::WHITESPACE . "[");

            if (([$str, $found] = $line->GetParse('"')) && $found) {
                assert(strlen($str) < self::MAX_STRING_LENGTH, "Compiler::value: string `$str` exceeds max chars");
                $this->writeStr($str);

            } else if (([$str, $found] = $line->GetParse('#')) && $found) {
                $token = $this->tokenMap->GetToken($str);
                assert(!is_null($token), "Compiler::value: GetParse `$str` failed, not in a token map.");

                if ($token->GetNum()) {
                    $this->writeNum($token->GetStrAsInt());
                } else {
                    $this->writeStr($token->GetStr());
                }
            } else {
                $str = $line->GetWord("%!=+-/*&|>< \t[],");
                if ($str === '') {
                    return false;
                }
                if ($this->isNum($str)) {
                    $this->writeNum((int)$str);
                } else {
                    $token = $this->tokenMap->GetToken($str);
                    assert(!is_null($token), "Compiler::value: GetParse `$str` is not in a token map.");

                    switch ($token->GetType()) {
                        case TokenType::FUNC():
                            $this->writeCall($str);
                            $func = ScriptFunc::from($token->GetNum());
                            $params = $token->GetStr();
                            $ok = $this->OnFunc($line, $func, $params);
                            assert($ok, "Compiler::value: func {$func->getKey()} error.");
                            break;
                        case TokenType::VAR():
                            $this->writeVar($str);
                            break;
                        case TokenType::DEFINE():
                            if ($token->GetNum()) {
                                $this->writeNum($token->GetStrAsInt());
                            } else {
                                $this->writeStr($token->GetStr());
                            }
                            break;
                        default:
                            assert(false, "Compiler::value: token type for `$str` is invalid. Line:{$line->GetBase()}");

                    }


                }

            }

            $line->Skip(self::WHITESPACE . ",");

            $op = $line->GetOperator('%=+-/*&|><!');
            if ($op === '') {
                $line->Skip(']');
                $this->writeOp(';');
                return true;
            }
            $this->writeOp($op);
        }

    }

    private function isNum(string $str): bool
    {
        return is_numeric($str);
    }

    private function goto(int $block): void
    {
        $this->writeCode(Code::GOTO());
        $this->writeGotoBlock($block);
    }

    private function getDefVar(ScriptLine $line): ?Pair
    {
        if (!$this->checkSwitchBlock()) {
            return null;
        }

        $name = $line->GetWord(self::WHITESPACE . "+-");
        assert($name !== '', "Compiler::GetDefVar: There are no define or number");

        if (is_numeric($name)) {
            return new Pair($name, 1);
        }

        $info = $this->tokenMap->GetToken($name);
        assert(!is_null($info), "Compiler::GetDefVar: $name is not defined token.");

        $str = $info->GetStr();
        $num = $info->GetNum();

        $op = $line->GetOperator("+-");

        if (!$op || !$info->GetNum()) {
            return new Pair($str, $num);
        }
        $result = new Pair($str, $num);

        $val = (int)$str;
        if ($op === "++") {
            $val++;
        } else if ($op === "--") {
            $val--;
        } else {
            assert(false, "Compiler::GetDefVar: $op cannot use define operator!");
        }

        $val .= "\r\n";
        $this->tokenMap->Set($name, $info->GetType(), $num, $val);

        return $result;

    }

    /** @noinspection PhpUnusedParameterInspection */
    private function cmdBlockBreak(ScriptLine $line): bool
    {
        //$this->Error("current block info - ".$this->block->GetStrInfo());
        return false;
    }

    private function cmdDeclare(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }

        $line->Skip(self::WHITESPACE);

        $func = $line->GetWord(self::WHITESPACE);
        assert($func !== '', "Compiler::Declare: no func name!");

        $line->Skip(self::WHITESPACE);

        $params = $line->GetWord(self::WHITESPACE);
        assert($params !== '', "Compiler::Declare: '$func' no parameter info!");

        $line->Skip(self::WHITESPACE);

        $pair = $this->getDefVar($line);

        assert(!is_null($pair), "Compiler::Declare: there are problem func code part");

        $func_id = $pair->key;
        $isNum = $pair->value;

        $line->Skip(self::WHITESPACE);
        $bc = $line->GetWord(self::WHITESPACE);
        assert($isNum, "Compiler::Declare: invalid number at func code");

        $this->tokenMap->Set($func, TokenType::FUNC(), $func_id, $params);
        $this->asm->Comment("declare $func $params $func_id");

        if ($bc === "blockcheck") {
            $this->blockCheckMap[$func_id] = true;
            $this->asm->Comment("block check func");
        }

        return true;
    }

    private function cmdDefine(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }

        $line->Skip(self::WHITESPACE);
        $name = $line->GetWord(self::WHITESPACE);
        assert($name !== '', "no define name");

        $line->Skip(self::WHITESPACE);

        $isNum = 1;
        [$data,] = $line->GetParse("'");

        if ($data) {
            $isNum = 0;
        } else {
            $pair = $this->getDefVar($line);
            assert (!is_null($pair), "problem of define value");

            $isNum = $pair->value;
            $data = $pair->key;
        }


        $this->tokenMap->Set($name, TokenType::DEFINE(), $isNum, $data);
        $this->asm->Comment("define $name $data");
        return true;
    }

    private function cmdDefCmd(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }

        $line->Skip(self::WHITESPACE);

        $name = $line->GetWord(self::WHITESPACE);
        assert($name !== '', "DefCmd there are no define command.");

        $line->Skip(self::WHITESPACE);

        $data = $line->GetWord(self::WHITESPACE);
        assert($data !== '', "DefCmd there are no base command.");

        $info = $this->tokenMap->GetToken($data);

        assert(!is_null($info), "DefCmd token `$data` not exist.");

        $str = $info->GetStr();
        $this->tokenMap->Set($name, $info->GetType(), $info->GetNum(), $str);
        return true;
    }

    /** @noinspection PhpUnusedParameterInspection */
    private function cmdEnd(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }

        $this->writeCode(Code::END());
        return true;
    }

    private function cmdIf(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }
        $this->writeCode(Code::CMP());

        $ok = $this->value($line);
        assert($ok, "if error");

        $this->asm->Put("not ");
        ++$this->if;
        $this->writeGotoBlock($this->block->GetNextId());

        return true;
    }

    private function cmdElseIf(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }

        $this->writeCode(Code::CMP());

        $ok = $this->value($line);
        assert($ok, "elseif error");

        $this->asm->Put("not ");
        $this->writeGotoBlock($this->block->GetNextId());

        return true;
    }

    /** @noinspection PhpUnusedParameterInspection */
    private function cmdElse(ScriptLine $line): bool
    {
        return $this->checkSwitchBlock() !== false;
    }

    /** @noinspection PhpUnusedParameterInspection */
    private function cmdEndIf(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }

        assert(!$this->switchInIf[$this->if], "endif there are not endchoose commanded in if block!");

        --$this->if;
        return true;
    }

    private function cmdVar(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }

        $line->Skip(self::WHITESPACE);

        $var = $line->GetWord("=" . self::WHITESPACE);
        assert($var !== '', "var no value name");

        $this->tokenMap->Set($var, TokenType::VAR(), 0, '');
        if (!$this->OnVar($line, $var)) {
            return false;
        }

        $this->asm->Comment("int $var");
        return true;
    }

    private function cmdWhile(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }

        $this->whileStack->push($this->whileBlock);
        $this->writeCode(Code::CMP());

        $ok = $this->value($line);
        assert($ok, "if error");

        $this->asm->Put("not ");
        $this->whileBlock = $this->block->GetEndId();
        $this->writeGotoBlock($this->whileBlock);

        return true;

    }

    /** @noinspection PhpUnusedParameterInspection */
    private function cmdEndWhile(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }
        assert($this->whileStack->count() > 0, "while-endwhile mismatch!");

        $this->whileBlock = $this->whileStack->pop();
        return true;
    }

    /** @noinspection PhpUnusedParameterInspection */
    private function cmdExitWhile(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }

        $this->goto($this->whileBlock);
        return true;
    }

    private function cmdSwitch(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }

        $this->writeCode(Code::PUSH());
        $this->writeVar('$case');
        $this->writecode(Code::MOV());
        $this->writeVar('$case');

        $ok = $this->value($line);
        assert($ok, "Compiler::Switch: switch error");

        $if_count = $this->if;
        ++$this->switch;
        if ($if_count > 0) {
            assert($this->switchInIf[$if_count] < self::MAX_SWITCH_IN_IF, "Too many choosemenu command in if block");
            $this->switchInIf[$if_count]++;
        }

        return true;
    }

    private function cmdCase(ScriptLine $line): bool
    {
        $this->writeCode(Code::CASE());
        $ok = $this->value($line);
        assert($ok, "case error");

        $this->asm->Put("not ");

        $next = $this->block->GetNextId();
        $this->writeGotoBlock($next);

        $this->isCase = true;
        return true;
    }

    /** @noinspection PhpUnusedParameterInspection */
    private function cmdBreak(ScriptLine $line): bool
    {
        if (!$this->checkSwitchBlock()) {
            return false;
        }
        if ($this->switch) {
            $this->isCase = false;
        }
        return true;
    }

    /** @noinspection PhpUnusedParameterInspection */
    private function cmdEndSwitch(ScriptLine $line): bool
    {
        assert($this->switch > 0, "none choosemenu commanded before!");

        --$this->switch;
        if ($this->if) {
            --$this->switchInIf[$this->if];
        }

        $this->writeCode(Code::POP());
        $this->writeVar('$case');

        return true;
    }

    public function optimize() : void {
        $this->writeCode(Code::END());

        $this->asm->Put("\n\n\n");
        $this->asm->Comment("optimize");
        $this->asm->Comment("===================================================================");

        $count = $this->gotoInfo->count();

        for ($i = 0; $i < $count; ++$i) {
            $item = $this->gotoInfo->get($i);
            $pos = $item->GetPos();
            $id = $item->GetId();
            $this->bin->Seek($pos);
            $addr = $this->block->GetInfo($id);
            assert(!is_null($addr), "bad block info or something i dunno.");

            $this->asm->Comment("optimize: move ".dechex($pos)." - block $id -> addr hex ".dechex($addr).":dec ");
            $this->writeNum($addr);
        }
    }

    public function Release() : void {
        $this->asm->Release();
        $this->bin->Release();
        $this->gotoInfo->clear();
    }

    public function Run(string $filename, int $date=0) : bool {
        $this->isCase = false;
        $this->if = $this->switch = 0;
        $this->switchInIf = [];
        for ($i=0; $i < self::MAX_SWITCH_IN_IF; ++$i) {
            $this->switchInIf[$i] = 0;
        }
        unset($this->cmd);

        $this->asm->Comment("===================================================================\n");
        $this->asm->Comment("Load $filename");

        $loaded = $this->script->LoadFile($filename, $date);
        assert($loaded, "Compiler::Run: Load error for $filename");

        $this->asm->Comment("===================================================================\n");
        $line = 0;

        if (!$this->script->GetLineNum()) {
            return true;
        }

        while ($this->script->Analyze($line)) {
            ++$line;
            if ($line >= $this->script->GetLineNum()) {
                return true;
            }
        }

        $str = $this->script->GetLine($line);
        assert(false, "Compiler::Run: file $filename line $line: $str");
    }

}