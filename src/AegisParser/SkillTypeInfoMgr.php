<?php

namespace AegisParser;

use Ds\Map;

class SkillTypeInfoMgr
{
    private Map $nameMap; // map<string (skname), int(skid)>
    private Map $infoMap; // map<SkillID, SkillTypeInfo>
	
	private Map $pcSkTreeMap; // map<JobType, Map<SkillID, Map<SkillID, level>>>
	private Map $homSkTreeMap; // map<JobType, Map<SkillID, Map<SkillID, level>>>
	private Map $gdSkTreeMap; // Map<SkillID, Map<SkillID, level>>
	private Map $merSkTreeMap;  // Map<JobType, Map<SkillID, level>>
	

    public function __construct(string $zone_path) {
        $this->nameMap = new Map();
        $this->infoMap = new Map();
		
		$this->pcSkTreeMap = new Map();
		$this->homSkTreeMap = new Map();
		$this->merSkTreeMap = new Map();
		$this->gdSkTreeMap = new Map();
		
        $this->loadSkeleton();
		
		$this->loadSkillTree($zone_path."SkillTree.txt");
		$this->loadGuildSkillTree($zone_path."GDSkillTree.txt");
		$this->loadSkillTree($zone_path."SKTree_Homun.txt");
		$this->loadMerSkillTree($zone_path."SKTree_Merce.txt");
    }

    private function loadSkeleton() : void {
        foreach (SkillID::toArray() as $name => $entry) {
            $name = trim($name);
            $this->nameMap[$name] = (int)$entry;
            $this->infoMap[(int)$entry] = new SkillTypeInfo(SkillID::$name());
        }
    }

    public function Save(string $path) : void {
        assert(!$this->nameMap->isEmpty(), "SkillTypeInfoMgr::Save: nameMap empty!!");

        $this->nameMap->sort();
        $it = $this->nameMap->getIterator();
        $str = "";

        foreach ($it as $name => $id) {
            $str .= $name." ".$id."\r\n";
        }

        file_put_contents($path, $str);
    }
	
	private function loadFile(string $filename) : array {
		assert(file_exists($filename), "SkillTypeInfoMgr::loadSkillTree: required file `$filename` not found!");
		$contents = file_get_contents($filename);
		assert($contents !== false, "SkillTypeInfoMgr::loadSkillTree: Failed to read `$filename`.");
		
		$lines = [];
		
		foreach (explode("\n", $contents) as $line)
		{
			$line = trim($line);
			if ($line === '' || str_starts_with($line, "//"))
            {
                continue;
            }
			if (str_contains($line, "//"))
            {
                $line = substr($line, 0, strpos($line, "//"));
            }
			$line = str_replace("\t", " ", $line);
			$line = preg_replace("/\s+/", " ", $line);
			$line = trim($line);
            if ($line !== '')
            {
                $lines[] = $line;
            }

		}
		
		return $lines;
	}
	
	private function inheritFirstJobSkill(JobType $child, JobType $parent) : void {
		$this->pcSkTreeMap[$child->getValue()] = $this->pcSkTreeMap[$parent->getValue()];
	}
	
	private function inheritPreJobSkill_HO(JobType $child, JobType $parent) : void {
		$this->homSkTreeMap[$child->getValue()] = $this->homSkTreeMap[$parent->getValue()];
	}
	
	private function loadSkillTree(string $path) : void {
		$lines = $this->loadFile($path);
		
		$skillTree = new Map();
		$lowerSkill = new Map();

		$jt = null;
		$skid = null;

		foreach ($lines as $line)
		{
			if (str_starts_with(strtolower($line), "inherit"))
			{
				$value = preg_match("/inherit ([A-Z_]+) ([A-Z_]+)/i", $line, $matches);
				assert($value !== false, "SkillTypeInfoMgr::loadSkillTree: Failed to match jobs for line `$line`.");
                assert(count($matches) === 3, "SkillTypeInfoMgr::loadSkillTree: didn't get enough matches. `$line`.");

				for ($i=1; $i < 3; ++$i)
				{
					assert(JobType::HasKey($matches[$i]), "SkillTypeInfoMgr::loadSkillTree: invalid inherit job `$matches[$i]`!");
				}
				[,$jt_child,$jt_parent] = $matches;

				$jt_child = JobType::$jt_child();
				$jt_parent = JobType::$jt_parent();
				
				if (JobType::IsPC($jt_child))
                {
                    $this->inheritFirstJobSkill($jt_child, $jt_parent);
                }
				else
                {
                    $this->inheritPreJobSkill_HO($jt_child, $jt_parent);
                }
				continue;
			}

            if (is_null($jt))
			{
                if (strtolower($line) === "#end")
                {
                    return;
                }
				assert(JobType::HasKey($line), "SkillTypeInfoMgr::loadSkillTree: invalid job `$line`!");
				$jt = JobType::$line();
			}
 			else if ($jt)
			{
				if ($line === "{")
				{
					continue;
				}

                if ($line === "}")
				{	
					if (JobType::IsPC($jt))
                    {
                        $this->pcSkTreeMap[$jt->getValue()] = clone $skillTree;
                    }
					else if (JobType::IsHom($jt))
                    {
                        $this->homSkTreeMap[$jt->getValue()] = clone $skillTree;
                    }
					else
					{
						assert(false, "Unknown job shit!! oh no!! JT:{$jt->getValue()}");
					}

					$jt = null; $skid = null; $skillTree = new Map();
					continue;
				}
				if (!$skid)
				{
					$skid = SkillID::$line();
					continue;
				}

                if ($line === "begin")
                {
                    $lowerSkill = new Map();
                    continue;
                }

                if ($line === "end")
                {
                    $skillTree[$skid->getValue()] = clone $lowerSkill;
                    $skid = null;
                    continue;
                }

                if (str_starts_with($line, "JOBLEVEL"))
                {
                    // this is, of course, only used for LK_BERSERK. weird
                    [$req_lvl] = sscanf($line, "JOBLEVEL %u");
                    $this->infoMap[$skid->getValue()]->SetAvailableJobLevel($jt, $req_lvl);
                    continue;
                }

                [$req_skid, $req_lvl] = sscanf($line, "%s %u");
                $lowerSkill[SkillID::$req_skid()->getValue()] = $req_lvl;
			}
		}
	}
	
	private function loadMerSkillTree(string $path) : void {
		$lines = $this->loadFile($path);

		$skillTree = new Map();
		$jt = null;
		
		foreach ($lines as $line)
		{
			if (!$jt)
			{
				assert(JobType::HasKey($line), "SkillTypeInfoMgr::loadSkillTree: invalid job `$line`!");
				$jt = JobType::$line();
			}
			else
			{
				if ($line === "{") // start with this class
				{
					continue;
				}

                if ($line === "}") // done with this class
				{
					$this->merSkTreeMap[$jt->getValue()] = clone $skillTree;
					$jt = null; $skillTree->clear();
					continue;
				}
				
				[$skid, $level] = sscanf($line, "%s %u");
				$skillTree[SkillID::$skid()->getValue()] =  $level;
			}
		}
	}
	
	private function loadGuildSkillTree(string $path) : void {
		$lines = $this->loadFile($path);
		
		$skillTree = new Map();
		
		$skid = null;
		foreach ($lines as $line)
		{
			if (!$skid)
			{
				$skid = SkillID::$line();
				continue;
			}

            if ($line === "begin") // start with this skill
            {
                continue;
            }

            if ($line === "end") // done with this skill
            {
                $this->gdSkTreeMap[$skid->getValue()] = clone $skillTree;
                $skid = null; $skillTree->clear();
                continue;
            }

            [$req_skid, $req_lvl] = sscanf($line, "%s %u");
            $skillTree[SkillID::$req_skid()->getValue()] =  $req_lvl;

        }
		
	}
	
	public function SearchSkillTypeInfo(SkillID $skid) : SkillTypeInfo {
		return $this->infoMap[$skid->getValue()];
	}


}