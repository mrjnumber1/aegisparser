<?php


namespace AegisParser;


final class BinBuf
{
	private string $path;
	private string $buf;
	
	public function Load(string $path) : bool {
		$this->path = $path;
		$this->buf = file_get_contents($this->path);
		
		return $this->buf !== false;
		//assert($this->buf !== false, "BinBuf::Load: failed to read `$path`");
	}
	
	public function Clear() : void {
		$this->path = $this->buf = null;
	}
	
	public function GetSize() : int {
		return strlen($this->buf);
	}
}