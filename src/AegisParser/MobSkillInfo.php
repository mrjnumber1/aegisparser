<?php


namespace AegisParser;


final class MobSkillInfo
{
    public FSMState $State;
    public SkillID $SKID;
    public int $Level;
    public int $Rate;
    public int $CastTime;
    public int $Cooldown;
    public bool $Breakable;
    public ?NpcAI $NewAI;
    public ?Emotion $Emotion;
    public ?MonChat $Chat;
    public ?MobSkillCondition $Condition;
    public mixed $Condition_Value;
}