<?php


namespace AegisParser;


class ITPEquippable extends ITP
{
    protected int $maxSlot;
    protected int $strValue;
    protected int $intValue;
    protected int $vitValue;
    protected int $dexValue;
    protected int $agiValue;
    protected int $lukValue;
    protected Sex $sex;
    protected bool $refinable;
    protected bool $damagable;
    protected int $availableJob;

    public function __construct() {
        parent::__construct();

        $this->SetRefinable(false);
        $this->SetDamagable(false);

        $this->SetAvailableJobCode(0);
    }
    public function IsRefinable() : bool { return $this->refinable; }
    public function SetRefinable(bool $v) : void { $this->refinable = $v;}
    public function IsDamagable() : bool { return $this->damagable; }
    public function SetDamagable(bool $v) : void { $this->damagable = $v; }
    public function GetAvailableJobCode() : int { return $this->availableJob; }
    public function SetAvailableJobCode(int $jc) : void { $this->availableJob = $jc; }

    /**
     * @return int
     */
    public function GetMaxSlot(): int
    {
        return $this->maxSlot;
    }

    /**
     * @param int $maxSlot
     */
    public function SetMaxSlot(int $maxSlot): void
    {
        $this->maxSlot = $maxSlot;
    }

    /**
     * @return int
     */
    public function GetStrValue(): int
    {
        return $this->strValue;
    }

    /**
     * @param int $strValue
     */
    public function SetStrValue(int $strValue): void
    {
        $this->strValue = $strValue;
    }

    /**
     * @return int
     */
    public function GetIntValue(): int
    {
        return $this->intValue;
    }

    /**
     * @param int $intValue
     */
    public function SetIntValue(int $intValue): void
    {
        $this->intValue = $intValue;
    }

    /**
     * @return int
     */
    public function GetVitValue(): int
    {
        return $this->vitValue;
    }

    /**
     * @param int $vitValue
     */
    public function SetVitValue(int $vitValue): void
    {
        $this->vitValue = $vitValue;
    }

    /**
     * @return int
     */
    public function GetDexValue(): int
    {
        return $this->dexValue;
    }

    /**
     * @param int $dexValue
     */
    public function SetDexValue(int $dexValue): void
    {
        $this->dexValue = $dexValue;
    }

    /**
     * @return int
     */
    public function GetAgiValue(): int
    {
        return $this->agiValue;
    }

    /**
     * @param int $agiValue
     */
    public function SetAgiValue(int $agiValue): void
    {
        $this->agiValue = $agiValue;
    }

    /**
     * @return int
     */
    public function GetLukValue(): int
    {
        return $this->lukValue;
    }

    /**
     * @param int $lukValue
     */
    public function SetLukValue(int $lukValue): void
    {
        $this->lukValue = $lukValue;
    }

    /**
     * @return Sex
     */
    public function GetSex(): Sex
    {
        return $this->sex;
    }

    /**
     * @param Sex $sex
     */
    public function SetSex(Sex $sex): void
    {
        $this->sex = $sex;
    }

}