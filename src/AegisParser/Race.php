<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static Race NOTHING()
 * @method static Race UNDEAD()
 * @method static Race ANIMAL()
 * @method static Race PLANT()
 * @method static Race INSECT()
 * @method static Race FISHS()
 * @method static Race DEVIL()
 * @method static Race HUMAN()
 * @method static Race ANGEL()
 * @method static Race DRAGON()
 * @method static Race PLAYER_HUMAN()
 * @method static Race PLAYER_DORAM()
 */
final class Race extends Enum
{
    private const NOTHING = 0;
    private const UNDEAD = 1;
    private const ANIMAL = 2;
    private const PLANT = 3;
    private const INSECT = 4;
    private const FISHS = 5;
    private const DEVIL = 6;
    private const HUMAN = 7;
    private const ANGEL = 8;
    private const DRAGON = 9;

    private const PLAYER_HUMAN = 11;
    private const PLAYER_DORAM = 12;
}