<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static WeaponType NONE()
 * @method static WeaponType SHORTSWORD()
 * @method static WeaponType SWORD()
 * @method static WeaponType TWOHANDSWORD()
 * @method static WeaponType SPEAR()
 * @method static WeaponType TWOHANDSPEAR()
 * @method static WeaponType AXE()
 * @method static WeaponType TWOHANDAXE()
 * @method static WeaponType MACE()
 * @method static WeaponType TWOHANDMACE()
 * @method static WeaponType ROD()
 * @method static WeaponType BOW()
 * @method static WeaponType KNUKLE()
 * @method static WeaponType INSTRUMENT()
 * @method static WeaponType WHIP()
 * @method static WeaponType BOOK()
 * @method static WeaponType CATARRH()
 * @method static WeaponType GUN_HANDGUN()
 * @method static WeaponType GUN_RIFLE()
 * @method static WeaponType GUN_GATLING()
 * @method static WeaponType GUN_SHOTGUN()
 * @method static WeaponType GUN_GRANADE()
 * @method static WeaponType SYURIKEN()
 * @method static WeaponType TWOHANDROD()
 */

final class WeaponType extends Enum
{
    private const NONE = 0x0;
    private const SHORTSWORD = 0x1;
    private const SWORD = 0x2;
    private const TWOHANDSWORD = 0x3;
    private const SPEAR = 0x4;
    private const TWOHANDSPEAR = 0x5;
    private const AXE = 0x6;
    private const TWOHANDAXE = 0x7;
    private const MACE = 0x8;
    private const TWOHANDMACE = 0x9;
    private const ROD = 0xa;
    private const BOW = 0xb;
    private const KNUKLE = 0xc;
    private const INSTRUMENT = 0xd;
    private const WHIP = 0xe;
    private const BOOK = 0xf;
    private const CATARRH = 0x10;
    private const GUN_HANDGUN = 0x11;
    private const GUN_RIFLE = 0x12;
    private const GUN_GATLING = 0x13;
    private const GUN_SHOTGUN = 0x14;
    private const GUN_GRANADE = 0x15;
    private const SYURIKEN = 0x16;
    private const TWOHANDROD = 0x17;

}