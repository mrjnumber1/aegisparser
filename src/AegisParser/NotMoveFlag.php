<?php


namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static NotMoveFlag DROP()
 * @method static NotMoveFlag EXCHANGE()
 * @method static NotMoveFlag STORE()
 * @method static NotMoveFlag CART()
 * @method static NotMoveFlag SELL_NPC()
 * @method static NotMoveFlag MAIL()
 * @method static NotMoveFlag AUCTION()
 * @method static NotMoveFlag GUILD_STORE()
 */
final class NotMoveFlag extends Enum
{
    private const DROP = 1;
    private const EXCHANGE = 2;
    private const STORE = 4;
    private const CART = 8;
    private const SELL_NPC = 0x10;
    private const MAIL = 0x20;
    private const AUCTION = 0x40;
    private const GUILD_STORE = 0x80;
}