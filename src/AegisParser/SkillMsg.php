<?php
namespace AegisParser;


use MyCLabs\Enum\Enum;

/**
 * Class SkillMsg
 * @package AegisParser
 * @method static SkillMsg REQ_EXCHANGE_ITEM()
 * @method static SkillMsg REQ_EMOTION()
 * @method static SkillMsg REQ_SIT()
 * @method static SkillMsg REQ_CHATROOM()
 * @method static SkillMsg REQ_MAKEPARTY()
 * @method static SkillMsg REQ_SHOUT()
 * @method static SkillMsg REQ_PK()
 * @method static SkillMsg REQ_GIVE_MANNERPOINT()
 * @method static SkillMsg GET_PLUSATTPOWER()
 * @method static SkillMsg LEVEL_UP()
 * @method static SkillMsg GET_PLUSHEALVELOCITY()
 * @method static SkillMsg GET_PLUSSPVELOCITY()
 * @method static SkillMsg USE_SKILL()
 * @method static SkillMsg RESET_SKILL()
 * @method static SkillMsg GETINFO_SKILL()
 * @method static SkillMsg GETINFO_SKILL1()
 * @method static SkillMsg GETINFO_SKILL2()
 * @method static SkillMsg GET_POSTDELAY()
 * @method static SkillMsg JOINPARTY()
 */
final class SkillMsg extends Enum
{
	private const REQ_EXCHANGE_ITEM = 0;
	private const REQ_EMOTION = 1;
	private const REQ_SIT = 2;
	private const REQ_CHATROOM = 3;
	private const REQ_MAKEPARTY = 4;
	private const REQ_SHOUT = 5;
	private const REQ_PK = 6;
	private const REQ_GIVE_MANNERPOINT = 7;
	private const GET_PLUSATTPOWER = 8;
	private const LEVEL_UP = 9;
	private const GET_PLUSHEALVELOCITY = 10;
	private const GET_PLUSSPVELOCITY = 11;
	private const USE_SKILL = 12;
	private const RESET_SKILL = 13;
	private const GETINFO_SKILL = 14;
	private const GETINFO_SKILL1 = 15;
	private const GETINFO_SKILL2 = 16;
	private const GET_POSTDELAY = 17;
	private const JOINPARTY = 18;
}