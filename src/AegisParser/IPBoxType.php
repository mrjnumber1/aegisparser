<?php

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * Class IPBoxType
 * @package AegisParser
 * @method static IPBoxType UNKNOWN()
 * @method static IPBoxType MUST()
 * @method static IPBoxType RANDOM()
 */
final class IPBoxType extends Enum
{
    private const UNKNOWN = 0;
    private const MUST = 1;
    private const RANDOM = 2;
}