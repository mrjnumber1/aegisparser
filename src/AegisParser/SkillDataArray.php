<?php


namespace AegisParser;


final class SkillDataArray
{
    private int $max_lv;
    private array $data;

    public function __construct(array $data, int $max_lv = SkillTypeInfo::MAX_LEVEL) {
        $this->max_lv = $max_lv;
        $this->SetValues($data);
    }

    public function SetValues(array $data) : void {
        assert(!empty($data), "SkillDataArray::SetValues: Empty array passed!");
        $i = 0;

        $this->data = [];
        foreach ($data as $value) {
            assert(is_int($value), "SkillDataArray::SetValues: Got non-integer entry!");
            $this->data[$i] = $value;
            ++$i;
        }

        if ($i === $this->max_lv) {
            return;
        }

        for (; $i < $this->max_lv; ++$i) {
            $this->data[$i] = $this->data[$i-1];
        }
    }
    public function Get(int $lv) : int {
        return $this->data[$lv-1];
    }
}