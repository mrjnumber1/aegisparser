<?php


namespace AegisParser;


use Ds\Set;

final class IPBox
{
    private Set $itemList; // Set<IPBoxItem>
    private IPBoxType $type;

    public function __construct(IPBoxType $type)  {
        $this->itemList = new Set();
        $this->Clear();
        $this->type = $type;
    }

    public function SetType(IPBoxType $type) : void {
        $this->type = $type;
    }
    public function GetType() : IPBoxType {
        return $this->type;
    }
    public function Insert(IPBoxItem $item) : void {
        $this->itemList->add($item);
    }

    public function Clear() : void {
        $this->type = IPBoxType::UNKNOWN();
        $this->itemList->clear();
    }

}
