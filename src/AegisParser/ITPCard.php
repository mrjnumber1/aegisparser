<?php


namespace AegisParser;


final class ITPCard extends ITP
{
    public function __construct() { parent::__construct(); $this->type = ItemType::SPECIAL(); }
    public function IsCompositionable(ITP $gear) : bool { return $this->GetLocation()->Contains($gear->GetLocation()); }
}