<?php


namespace AegisParser;


final class ITPArrow extends ITPAmmo
{
    public function __construct() { parent::__construct(); $this->type = ItemType::ARROW(); }
    public function GetAvailableJobCode() : int { return 0; } // ARCHER_GR + THIEF_GR which is hardcoded
}