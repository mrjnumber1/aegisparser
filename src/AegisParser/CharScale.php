<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static CharScale SMALL()
 * @method static CharScale MEDIUM()
 * @method static CharScale LARGE()
 */
final class CharScale extends Enum
{
    private const SMALL = 0;
    private const MEDIUM = 1;
    private const LARGE = 2;
}