<?php


namespace AegisParser;


final class MonMakingItem
{
    public const MAX_DROPS = 8;
    public const MAX_MVP_DROPS = 3;
    private array $Drops;

    public function SetDrops(array $drops) : void {
        $this->Drops = [];

        foreach ($drops as $drop) {
            assert (is_a($drop, MonItemInfo::class, false), "Invalid item passed !".var_export($drop, true));

            if ($drop->Rate === 0) {
                continue;
            }
            $this->Drops[] = $drop;

        }

        assert(count($this->Drops) <= $this::MAX_DROPS, "MonMakingItem::SetDrops: Too many drops passed! Got ".count($drops)." drops.");
    }

    public function GetDrops() : array {
        return $this->Drops;
    }

}
