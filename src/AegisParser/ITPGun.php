<?php


namespace AegisParser;


final class ITPGun extends ITPWeapon
{
    public function __construct() { parent::__construct(); $this->type = ItemType::GUN(); }
}