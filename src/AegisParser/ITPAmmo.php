<?php

namespace AegisParser;

class ITPAmmo extends ITP
{
    protected ElementProperty $property;
    protected int $attackValue;
    protected int $availableJobCode;
    public function __construct() {
        parent::__construct();
        $this->SetAvailableJobCode(0);
        $this->SetAttackValue(0);
        $this->SetProperty(ElementProperty::NOTHING());
        $this->SetType(ItemType::AMMO());
    }

    public function SetAvailableJobCode(int $num) : void { $this->availableJobCode = $num; }
    public function GetAvailableJobCode() : int { return $this->availableJobCode; }

    /**
     * @return int
     */
    public function GetAttackValue(): int
    {
        return $this->attackValue;
    }

    /**
     * @param int $atk
     */
    public function SetAttackValue(int $atk): void
    {
        $this->attackValue = $atk;
    }

    /**
     * @return ElementProperty
     */
    public function GetProperty(): ElementProperty
    {
        return $this->property;
    }

    /**
     * @param ElementProperty $property
     */
    public function SetProperty(ElementProperty $property): void
    {
        $this->property = $property;
    }
    public function GetLocation() : EquipPosBitfield { return new EquipPosBitfield([EquipPosFlag::AMMO()]); }
}