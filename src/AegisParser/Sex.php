<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static Sex FEMALE()
 * @method static Sex MALE()
 * @method static Sex ANY()
 */
final class Sex extends Enum
{
    private const FEMALE = 0;
    private const MALE = 1;
    private const ANY = 2;
}