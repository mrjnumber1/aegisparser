<?php


namespace AegisParser;


use Ds\Map;
use Exception;
use MyCLabs\Enum\Enum;

final class RandomSummonList
{
    private ItemID $item;
    private Map $list; // map<int, count>
    private int $entries;
    private string $type;

    public function __construct(ItemID $container, string $type) {
        $this->list = new Map();
        $this->item = $container;
        $this->entries = 0;
        $this->type = $type;

        assert(is_subclass_of($this->type, Enum::class, true), "RandomSummonList::ctor: Passed a non-Enum type $this->type!");
    }

    public function GetITID() : ItemID {
        return $this->item;
    }

    public function GetEntryType() : string {
        return $this->type;
    }

    public function AddEntry(mixed $id, int $count) : void {
        assert(is_a($id, $this->type, false), "RandomSummonList::AddEntry: Invalid entry type $id");
        assert($count > 0, "RandomSummonList::AddEntry: {$this->item->getKey()} received an invalid number of reward '{$id->getKey()}!'");
        assert(!$this->list->hasKey($id->getValue()), "RandomSummonList::AddEntry: {$this->item->getKey()} received duplicate key '{$id->getKey()}'was added!");

        $this->entries += $count;
        $this->list->put($id->getValue(), $count);
    }

    public function GetUniqueEntryCount() : int {
        return $this->list->count();
    }

    public function GetTotalEntryCount() : int {
        return $this->entries;
    }

    public function GetEntryCount(mixed $entry) : int {
        assert($this->list->hasKey($entry->getValue()), "RandomSummonList::GetEntryCount: {$this->GetITID()->getKey()} does not have entry `$entry`");
        return $this->list[$entry->getValue()];
    }

    public function GetEntryPercent(mixed $entry) : float {
        assert($this->list->hasKey($entry->getValue()), "RandomSummonList::GetEntryProbabilityNum: {$this->GetITID()->getKey()} does not have entry `$entry`");
        return round($this->GetEntryCount($entry)*100/$this->GetTotalEntryCount(), 2);
    }

    public function GetRandomEntry(int $rnd = -1) : mixed {
        if ($rnd === -1) {
            try {
                $rnd = random_int(0, $this->GetTotalEntryCount() - 1);
            } catch (Exception $e) {
                assert(false, "RandomSummonList::GetRandomEntry: " . $e->getMessage());
            }
        }

        $i=0;

        foreach ($this->list as $entry => $count)  {
            $i += $count;
            if ($i > $rnd)  {
                return new $this->type($entry);
            }
        }

        assert(false, "RandomSummonList::GetRandomEntry: {$this->item->getKey()} wasn't able to find a proper entry! rnd:$rnd entries:{$this->GetTotalEntryCount()}");
    }
}