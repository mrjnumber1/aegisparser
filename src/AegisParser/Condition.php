<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
* @method static Condition BODY_STONECURSE()
* @method static Condition BODY_FREEZING()
* @method static Condition BODY_STUN()
* @method static Condition BODY_SLEEP()
* @method static Condition BODY_UNDEAD()
* @method static Condition HEALTH_POISON()
* @method static Condition HEALTH_CURSE()
* @method static Condition HEALTH_SILENCE()
* @method static Condition HEALTH_CONFUSION()
* @method static Condition HEALTH_BLIND()
* @method static Condition HEALTH_HEAVYPOISON()
* @method static Condition HEALTH_BLOODING()
* @method static Condition EFFECT_ENDURE()
* @method static Condition EFFECT_HASTE()
* @method static Condition EFFECT_HASTEATTACK()
* @method static Condition EFFECT_SLOW_POTION()
* @method static Condition EFFECT_HASTE_POTION()
* @method static Condition EFFECT_SANTA()
* @method static Condition EFFECT_ANGELUS()
* @method static Condition EFFECT_PUSHCART()
* @method static Condition EFFECT_CONCENTRATE()
* @method static Condition EFFECT_HIDE()
* @method static Condition EFFECT_WEDDING()
* @method static Condition EFFECT_PLUSATTACKPOWER()
* @method static Condition EFFECT_PLUSMAGICPOWER()
* @method static Condition EFFECT_CLAIRVOYANCE()
* @method static Condition EFFECT_HASTE_HORSE()
* @method static Condition EFFECT_SUMMER()
* @method static Condition HEALTH_FEAR()
* @method static Condition BODY_BURNNING()
* @method static Condition BODY_IMPRISON()
* @method static Condition HANDICAPSTATE_DEEPSLEEP()
* @method static Condition HANDICAPSTATE_FROSTMISTY()
* @method static Condition HANDICAPSTATE_COLD()
* @method static Condition HANDICAPSTATE_NORECOVER()
* @method static Condition EFFECT_HASTEATTACK_CASH()
* @method static Condition HANDICAPSTATE_ICEEXPLO()
* @method static Condition HANDICAPSTATE_ILLUSION()
* @method static Condition EFFECT_HANBOK()
*/

final class Condition extends Enum
{
    private const BODY_STONECURSE = 0x1;
    private const BODY_FREEZING = 0x2;
    private const BODY_STUN = 0x3;
    private const BODY_SLEEP = 0x4;
    private const BODY_UNDEAD = 0x5;
    private const HEALTH_POISON = 0x6;
    private const HEALTH_CURSE = 0x7;
    private const HEALTH_SILENCE = 0x8;
    private const HEALTH_CONFUSION = 0x9;
    private const HEALTH_BLIND = 0xa;
    private const HEALTH_HEAVYPOISON = 0xb;
    private const HEALTH_BLOODING = 0xc;
    private const EFFECT_ENDURE = 0xd;
    private const EFFECT_HASTE = 0xe;
    private const EFFECT_HASTEATTACK = 0xf;
    private const EFFECT_SLOW_POTION = 0x10;
    private const EFFECT_HASTE_POTION = 0x11;
    private const EFFECT_SANTA = 0x12;
    private const EFFECT_ANGELUS = 0x13;
    private const EFFECT_PUSHCART = 0x14;
    private const EFFECT_CONCENTRATE = 0x15;
    private const EFFECT_HIDE = 0x16;
    private const EFFECT_WEDDING = 0x17;
    private const EFFECT_PLUSATTACKPOWER = 0x18;
    private const EFFECT_PLUSMAGICPOWER = 0x19;
    private const EFFECT_CLAIRVOYANCE = 0x1a;
    private const EFFECT_HASTE_HORSE = 0x1b;
    private const EFFECT_SUMMER = 0x1c;
    private const HEALTH_FEAR = 0x1d;
    private const BODY_BURNNING = 0x1e;
    private const BODY_IMPRISON = 0x1f;
    private const HANDICAPSTATE_DEEPSLEEP = 0x20;
    private const HANDICAPSTATE_FROSTMISTY = 0x21;
    private const HANDICAPSTATE_COLD = 0x22;
    private const HANDICAPSTATE_NORECOVER = 0x23;
    private const EFFECT_HASTEATTACK_CASH = 0x24;
    private const HANDICAPSTATE_ICEEXPLO = 0x25;
    private const HANDICAPSTATE_ILLUSION = 0x26;
    private const EFFECT_HANBOK = 0x27;

    public static function IsBuff(Condition $cond) : bool {
        $v = $cond->getValue();
        return (
            ($v >= self::EFFECT_ENDURE && $v <= self::EFFECT_SUMMER)
            || ($v === self::EFFECT_HASTEATTACK_CASH)
            || ($v === self::EFFECT_HANBOK)
        );
    }
    public static function IsHandicap(Condition $cond) : bool
    {
        return !self::IsBuff($cond);
    }
}