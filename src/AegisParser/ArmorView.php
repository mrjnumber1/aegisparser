<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static ArmorView NONE()
 */
class ArmorView extends Enum
{
    protected const NONE = 0;
}
