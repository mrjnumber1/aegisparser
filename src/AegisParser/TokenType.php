<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static TokenType NONE()
 * @method static TokenType COMMAND()
 * @method static TokenType FUNC()
 * @method static TokenType VAR()
 * @method static TokenType DEFINE()
 */
final class TokenType extends Enum
{
    private const NONE = 0;
    private const COMMAND = 1;
    private const FUNC = 2;
    private const VAR = 3;
    private const DEFINE = 4;
}