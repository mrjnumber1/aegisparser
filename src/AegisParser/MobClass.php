<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static MobClass NONE()
 * @method static MobClass BOSS()
 * @method static MobClass GUARDIAN()
 * @method static MobClass UNUSED__INVINCIBLE()
 * @method static MobClass BATTLEFIELD()
 * @method static MobClass FIXED_ITEMDROP()
 */
final class MobClass extends Enum
{
    private const NONE = 0;
    private const BOSS = 1;
    private const GUARDIAN = 2;
    private const UNUSED__INVINCIBLE = 3;
    private const BATTLEFIELD = 4;
    private const FIXED_ITEMDROP = 5;
}