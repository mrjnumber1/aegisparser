<?php


namespace AegisParser;


final class MonChat
{
    private int $color;
    private string $text;
    public function __construct(int $color, string $text) {
        $this->SetColor($color);
        $this->SetText($text);
    }

    /**
     * @return int
     */
    public function GetColor(): int
    {
        return $this->color;
    }

    /**
     * @param int $color
     */
    public function SetColor(int $color): void
    {
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function GetText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function SetText(string $text): void
    {
        $this->text = $text;
    }


}