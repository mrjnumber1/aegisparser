<?php

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static Code END()
 * @method static Code MOV()
 * @method static Code ADD()
 * @method static Code SUB()
 * @method static Code MUL()
 * @method static Code DIV()
 * @method static Code MOD()
 * @method static Code INC()
 * @method static Code DEC()
 * @method static Code CMP()
 * @method static Code GOTO()
 * @method static Code FUNC()
 * @method static Code CASE()
 * @method static Code PUSH()
 * @method static Code POP()
 */
final class Code extends Enum
{
    private const END = 0;
    private const MOV = 1;
    private const ADD = 2;
    private const SUB = 3;
    private const MUL = 4;
    private const DIV = 5;
    private const MOD = 6;
    private const INC = 7;
    private const DEC = 8;
    private const CMP = 9;
    private const GOTO = 10;
    private const FUNC = 11;
    private const CASE = 12;
    private const PUSH = 13;
    private const POP = 14;
}