<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static NpcAI WARP_NPC()
 * @method static NpcAI GUIDE_NPC()
 * @method static NpcAI TALK_NPC()
 * @method static NpcAI TRADER_NPC()
 * @method static NpcAI EFFECT_NPC()
 * @method static NpcAI ARENA_GUIDE_NPC()
 * @method static NpcAI MONSTER_TYPE_01()
 * @method static NpcAI MONSTER_TYPE_02()
 * @method static NpcAI MONSTER_TYPE_03()
 * @method static NpcAI MONSTER_TYPE_04()
 * @method static NpcAI MONSTER_TYPE_05()
 * @method static NpcAI MONSTER_TYPE_06()
 * @method static NpcAI MONSTER_TYPE_07()
 * @method static NpcAI MONSTER_TYPE_08()
 * @method static NpcAI MONSTER_TYPE_09()
 * @method static NpcAI MONSTER_TYPE_10()
 * @method static NpcAI MONSTER_TYPE_11()
 * @method static NpcAI MONSTER_TYPE_12()
 * @method static NpcAI MONSTER_TYPE_13()
 * @method static NpcAI MONSTER_TYPE_14()
 * @method static NpcAI MONSTER_TYPE_15()
 * @method static NpcAI MONSTER_TYPE_16()
 * @method static NpcAI MONSTER_TYPE_17()
 * @method static NpcAI MONSTER_TYPE_18()
 * @method static NpcAI MONSTER_TYPE_19()
 * @method static NpcAI MONSTER_TYPE_20()
 * @method static NpcAI MONSTER_TYPE_21()
 * @method static NpcAI MONSTER_TYPE_22()
 * @method static NpcAI MONSTER_TYPE_23()
 * @method static NpcAI MONSTER_TYPE_24()
 * @method static NpcAI MONSTER_TYPE_25()
 * @method static NpcAI MONSTER_TYPE_26()
 * @method static NpcAI MONSTER_TYPE_27()
 * @method static NpcAI HIDDEN_WARP_NPC()
 * @method static NpcAI MERCENARY_TYPE_01()
 * @method static NpcAI MERCENARY_TYPE_02()
 * @method static NpcAI CASHTRADER_NPC()
 * @method static NpcAI MOVE_NPC_TYPE01()
 * @method static NpcAI MOVE_NPC_TRADER()
 * @method static NpcAI LUA_AI_TYPE()
 * @method static NpcAI MONSTER_TYPE_28()
 * @method static NpcAI LUA_ELEMENTAL_TYPE()
 * @method static NpcAI ELEMENTAL_TYPE_WAIT()
 * @method static NpcAI ELEMENTAL_TYPE_PASSIVE()
 * @method static NpcAI ELEMENTAL_TYPE_DEFENSIVE()
 * @method static NpcAI ELEMENTAL_TYPE_OFFENSIVE()
 * @method static NpcAI MOB_TOMB_NPC()
 * @method static NpcAI MONSTER_TYPE_29()
 */

final class NpcAI extends Enum
{
    private const WARP_NPC = 0x0;
    private const GUIDE_NPC = 0x1;
    private const TALK_NPC = 0x2;
    private const TRADER_NPC = 0x3;
    private const EFFECT_NPC = 0x4;
    private const ARENA_GUIDE_NPC = 0x5;
    private const MONSTER_TYPE_01 = 0x6;
    private const MONSTER_TYPE_02 = 0x7;
    private const MONSTER_TYPE_03 = 0x8;
    private const MONSTER_TYPE_04 = 0x9;
    private const MONSTER_TYPE_05 = 0xa;
    private const MONSTER_TYPE_06 = 0xb;
    private const MONSTER_TYPE_07 = 0xc;
    private const MONSTER_TYPE_08 = 0xd;
    private const MONSTER_TYPE_09 = 0xe;
    private const MONSTER_TYPE_10 = 0xf;
    private const MONSTER_TYPE_11 = 0x10;
    private const MONSTER_TYPE_12 = 0x11;
    private const MONSTER_TYPE_13 = 0x12;
    private const MONSTER_TYPE_14 = 0x13;
    private const MONSTER_TYPE_15 = 0x14;
    private const MONSTER_TYPE_16 = 0x15;
    private const MONSTER_TYPE_17 = 0x16;
    private const MONSTER_TYPE_18 = 0x17;
    private const MONSTER_TYPE_19 = 0x18;
    private const MONSTER_TYPE_20 = 0x19;
    private const MONSTER_TYPE_21 = 0x1a;
    private const MONSTER_TYPE_22 = 0x1b;
    private const MONSTER_TYPE_23 = 0x1c;
    private const MONSTER_TYPE_24 = 0x1d;
    private const MONSTER_TYPE_25 = 0x1e;
    private const MONSTER_TYPE_26 = 0x1f;
    private const MONSTER_TYPE_27 = 0x20;
    private const HIDDEN_WARP_NPC = 0x21;
    private const MERCENARY_TYPE_01 = 0x22;
    private const MERCENARY_TYPE_02 = 0x23;
    private const CASHTRADER_NPC = 0x24;
    private const MOVE_NPC_TYPE01 = 0x25;
    private const MOVE_NPC_TRADER = 0x26;
    private const LUA_AI_TYPE = 0x27;
    private const MONSTER_TYPE_28 = 0x28;
    private const LUA_ELEMENTAL_TYPE = 0x29;
    private const ELEMENTAL_TYPE_WAIT = 0x2a;
    private const ELEMENTAL_TYPE_PASSIVE = 0x2b;
    private const ELEMENTAL_TYPE_DEFENSIVE = 0x2c;
    private const ELEMENTAL_TYPE_OFFENSIVE = 0x2d;
    private const MOB_TOMB_NPC = 0x2e;
    private const MONSTER_TYPE_29 = 0x2f;
}