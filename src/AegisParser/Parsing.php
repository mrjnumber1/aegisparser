<?php


namespace AegisParser;


use Ds\Deque;
use Ds\Stack;

final class Parsing
{
    private Deque $phase;

    public function __construct() {
        $this->phase = new Deque();
    }
    public function Run(string $line, string $reg="$") : bool {
        assert(strlen($reg) === 1, "Reg str MUST be a single character! line: `".$line."`");

        $phase = new Stack();
        $cur = "";
        $flag = false;
        $regPos = 0;
        $depth = 0;

        $this->phase->clear();

        // Don't bother searching the entire thing if there's no quotes/parenthesis
        if (!str_contains($line, '"') && !str_contains($line, '(') && !str_contains($line, ')')) {
            $this->phase->push($line);
            return true;
        }

        $characters = str_split($line);

        foreach ($characters as $c)
        {
            if ($flag)
            {
                if ($c === '"') {
                    $flag = false;
                }
                $cur .= $c;
                continue;
            }

            switch ($c) {
                case '"':
                    $flag = true;
                    $cur .= $c;
                    break;
                case '(':
                    $cur .= ' '.$reg.$regPos;
                    $phase->push($cur);

                    $cur = $reg.$regPos.'=';
                    ++$regPos;
                    ++$depth;
                    break;
                case ')':
                    if (!$depth) {
                        return false;
                    }
                    $this->phase->push($cur);
                    $cur = $phase->pop();

                    --$depth;
                    break;
                default:
                    $cur .= $c;
            }
        }

        $this->phase->push($cur);

        return !($depth > 0);
    }

    public function Get() : string {
        if ($this->phase->isEmpty()) {
            return '';
        }

        return $this->phase->remove(0); // pop front
    }


}