<?php


namespace AegisParser;


final class MonItemInfo
{
    public const DEFAULT_GROUP = "G0";
    public ItemID $ITID;
    public int $Rate = 0;
    public ?string $Group = "G0";
}