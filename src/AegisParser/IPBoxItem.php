<?php


namespace AegisParser;


final class IPBoxItem
{
    private ItemID $id;
    private int $count;
    private int $hours;
    private int $factor;

    public function __construct(ItemID $id, int $count, int $hours, int $factor) {
        $this->id = $id;
        $this->count = $count;
        $this->hours = $hours;
        $this->factor = $factor;
    }


    /**
     * @return ItemID
     */
    public function GetID(): ItemID
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function GetCount(): int
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function GetHours(): int
    {
        return $this->hours;
    }

    /**
     * @return int
     */
    public function GetRandomFactor(): int
    {
        return $this->factor;
    }
}