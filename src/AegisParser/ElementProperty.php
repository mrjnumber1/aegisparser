<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static ElementProperty NOTHING()
 * @method static ElementProperty WATER()
 * @method static ElementProperty GROUND()
 * @method static ElementProperty FIRE()
 * @method static ElementProperty WIND()
 * @method static ElementProperty POISON()
 * @method static ElementProperty SAINT()
 * @method static ElementProperty DARKNESS()
 * @method static ElementProperty TELEKINESIS()
 * @method static ElementProperty UNDEAD()
 * @method static ElementProperty NOTHING1()
 * @method static ElementProperty WATER1()
 * @method static ElementProperty GROUND1()
 * @method static ElementProperty FIRE1()
 * @method static ElementProperty WIND1()
 * @method static ElementProperty POISON1()
 * @method static ElementProperty SAINT1()
 * @method static ElementProperty DARKNESS1()
 * @method static ElementProperty TELEKINESIS1()
 * @method static ElementProperty UNDEAD1()
 * @method static ElementProperty NOTHING2()
 * @method static ElementProperty WATER2()
 * @method static ElementProperty GROUND2()
 * @method static ElementProperty FIRE2()
 * @method static ElementProperty WIND2()
 * @method static ElementProperty POISON2()
 * @method static ElementProperty SAINT2()
 * @method static ElementProperty DARKNESS2()
 * @method static ElementProperty TELEKINESIS2()
 * @method static ElementProperty UNDEAD2()
 * @method static ElementProperty NOTHING3()
 * @method static ElementProperty WATER3()
 * @method static ElementProperty GROUND3()
 * @method static ElementProperty FIRE3()
 * @method static ElementProperty WIND3()
 * @method static ElementProperty POISON3()
 * @method static ElementProperty SAINT3()
 * @method static ElementProperty DARKNESS3()
 * @method static ElementProperty TELEKINESIS3()
 * @method static ElementProperty UNDEAD3()
 * @method static ElementProperty NOTHING4()
 * @method static ElementProperty WATER4()
 * @method static ElementProperty GROUND4()
 * @method static ElementProperty FIRE4()
 * @method static ElementProperty WIND4()
 * @method static ElementProperty POISON4()
 * @method static ElementProperty SAINT4()
 * @method static ElementProperty DARKNESS4()
 * @method static ElementProperty TELEKINESIS4()
 * @method static ElementProperty UNDEAD4()
 */
final class ElementProperty extends Enum
{
    private const NOTHING = 0x0;
    private const WATER = 0x1;
    private const GROUND = 0x2;
    private const FIRE = 0x3;
    private const WIND = 0x4;
    private const POISON = 0x5;
    private const SAINT = 0x6;
    private const DARKNESS = 0x7;
    private const TELEKINESIS = 0x8;
    private const UNDEAD = 0x9;
    private const NOTHING1 = 0x14;
    private const WATER1 = 0x15;
    private const GROUND1 = 0x16;
    private const FIRE1 = 0x17;
    private const WIND1 = 0x18;
    private const POISON1 = 0x19;
    private const SAINT1 = 0x1a;
    private const DARKNESS1 = 0x1b;
    private const TELEKINESIS1 = 0x1c;
    private const UNDEAD1 = 0x1d;
    private const NOTHING2 = 0x28;
    private const WATER2 = 0x29;
    private const GROUND2 = 0x2a;
    private const FIRE2 = 0x2b;
    private const WIND2 = 0x2c;
    private const POISON2 = 0x2d;
    private const SAINT2 = 0x2e;
    private const DARKNESS2 = 0x2f;
    private const TELEKINESIS2 = 0x30;
    private const UNDEAD2 = 0x31;
    private const NOTHING3 = 0x3c;
    private const WATER3 = 0x3d;
    private const GROUND3 = 0x3e;
    private const FIRE3 = 0x3f;
    private const WIND3 = 0x40;
    private const POISON3 = 0x41;
    private const SAINT3 = 0x42;
    private const DARKNESS3 = 0x43;
    private const TELEKINESIS3 = 0x44;
    private const UNDEAD3 = 0x45;
    private const NOTHING4 = 0x50;
    private const WATER4 = 0x51;
    private const GROUND4 = 0x52;
    private const FIRE4 = 0x53;
    private const WIND4 = 0x54;
    private const POISON4 = 0x55;
    private const SAINT4 = 0x56;
    private const DARKNESS4 = 0x57;
    private const TELEKINESIS4 = 0x58;
    private const UNDEAD4 = 0x59;
}