<?php

namespace AegisParser;

use ErrorException;
use RuntimeException;

abstract class ScriptHandler
{
    protected TokenMap $tokenMap;

    /**
     * @param string $msg
     * @throws ErrorException
     */
    public function Error(string $msg) : void {
        if ($msg === 'base') {
            throw new RuntimeException('must call Compiler::' . __FUNCTION__ . ' this is the pseudo-trait!!');
        }

        $msg = __CLASS__."::".$msg;
        throw new ErrorException($msg);

    }

    abstract public function OnCommand(ScriptLine $line, Cmd $cmd) : bool;
    abstract public function OnVar(ScriptLine $line, string $name) : bool;
    abstract public function OnControl(ScriptLine $line, Cmd $cmd) : bool;
    abstract public function OnFunc(ScriptLine $line, ScriptFunc $func, string $params) : bool;

    public function AnalyzeParse(string $str) : bool {
        $line = new ScriptLine($str);
        $line->Skip(" \t");

        $word = $line->GetWord(" \t(=+-*/%[");
        if ($word === '') {
            return true;
        }

        $token_info = $this->tokenMap->GetToken($word);
        if (is_null($token_info))
        {
            assert($word[0] === '$', "AnalyzeParse - 1:[$word] is not in a tokenMap");
            $this->tokenMap->Set($word, TokenType::VAR(), 0, '');

            $token_info = $this->tokenMap->GetToken($word);
            assert(!is_null($token_info), "AnalyzeParse - 2:[$word] is not in a tokenMap");
        }

        switch ($token_info->GetType())
        {
            case TokenType::COMMAND():
                return $this->OnCommand($line, Cmd::from($token_info->GetNum()));
            case TokenType::FUNC():
                return $this->OnFunc($line, ScriptFunc::from($token_info->GetNum()), $token_info->GetStr());
            case TokenType::VAR():
                return $this->OnVar($line, $word);
        }

        assert(false, "[$word] has unknown type ".$token_info->GetType()->getValue());
    }

    public function AnalyzeLine(string $str) : bool {
        $line = new ScriptLine($str);
        $line->Skip(" \t");

        $word = $line->GetWord(" \t(=+-*/[");

        if ($word === '') {
            // nothing to parse, we're good
            return true;
        }

        $info = $this->tokenMap->GetToken($word);
        assert(!is_null($info), "Unknown token `$word` line: `$str`");
        /** @noinspection TypeUnsafeComparisonInspection */
        /** @noinspection PhpNonStrictObjectEqualityInspection */
        if ($info->GetType() == TokenType::COMMAND())
        {
            $ok = $this->OnControl($line, Cmd::from($info->GetNum()));
            assert($ok);
        }

        $parse = new Parsing();
        $base = $line->GetBase();

        $complete = $parse->Run($base);
        assert($complete, "parenthesis number is not correct");

        if (($buf = $parse->Get()) !== '') {
            while ($this->AnalyzeParse($buf)) {
                if (!($buf = $parse->Get())) {
                    return true;
                }
            }
        }

        return true;
    }

    public function SetTokenMap(TokenMap $map) : void {
        $this->tokenMap = $map;
    }
}