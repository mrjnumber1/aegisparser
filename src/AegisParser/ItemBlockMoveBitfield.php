<?php

namespace AegisParser;

use bitfield\Bitfield;

final class ItemBlockMoveBitfield
{
    private Bitfield $field;
    private const LIST = ['DROP', 'EXCHANGE', 'STORE', 'CART', 'SELL_NPC', 'MAIL', 'AUCTION', 'GUILDSTORE'];

    public function __construct(int $num) {
        $this->field = new Bitfield(self::LIST);
        $this->SetFromNum($num);
    }

    public function GetItemMoveInfoString(int $max_columns = 7) : string {
        $str = '';
        $i = 0;
        foreach (self::LIST as $type) {
            if ($i >= $max_columns) {
                break;
            }
            if ($i > 0) {
                $str .= ' ';
            }
            $i++;
            $str .= $this->field->isOn($type) ? '1' : '0';

        }
        return trim($str);
    }

    public function IsBlockSellNPC() : bool {
        return $this->field->isOn('SELL_NPC');
    }
    public function IsBlockMail() : bool {
        return $this->field->isOn('MAIL');
    }
    public function IsBlockAuction() : bool {
        return $this->field->isOn('AUCTION');
    }
    public function IsBlockGuildStorage() : bool {
        return $this->field->isOn('GUILDSTORE');
    }
    public function IsBlockDrop() : bool {
        return $this->field->isOn('DROP');
    }
    public function IsBlockTrade() : bool {
        return $this->field->isOn('EXCHANGE');
    }
    public function IsBlockStorage() : bool {
        return $this->field->isOn('STORE');
    }
    public function IsBlockCart() : bool {
        return $this->field->isOn('CART');
    }

    public function GetMovableNum() : int {
        return $this->field->getValue();
    }

    public function IsFlagged(NotMoveFlag $f) : bool {
        return ($this->GetMovableNum() & $f->getValue()) !== 0;
    }

    public function SetFromNum(int $num) : void {
        $this->checkValidNum($num, __FUNCTION__);
        $this->field->setValue($num);
    }

    public function SetFromItemMoveInfo(string $txt) : void {
        $txt = trim($txt);

        assert($txt !== '', "ItemBlockMoveBitfield::SetFromItemMoveInfo A blank line was passed");
        assert(!str_starts_with($txt, '//'), "ItemBlockMoveBitfield::SetFromItemMoveInfo A commented line was passed");

        // simplify whitespace between fields
        $txt = preg_replace('/\s+/',' ', $txt);
        // remove comments
        if (str_contains($txt, '//')) {
            $txt = substr($txt, 0, strpos($txt,'//'));
            $txt = trim($txt);
        }

        $data = explode(' ', $txt);
        assert(count($data) > 0, "ItemBlockMoveBitfield::SetFromItemMoveInfo did not receive any fields");

        // discard the item ID
        array_shift($data);

        $this->SetFromNum(0);

        foreach ($data as $index => $val) {
            assert(is_numeric($val), "ItemBlockMoveBitfield::SetFromItemMoveInfo invalid field $index value:$val");
            if ($val) {
                $this->field->on(self::LIST[$index]);
            }
        }
    }

    private function checkValidNum(int $num, string $caller) : void {
        assert($num < 2 ** count(self::LIST), "ItemBlockMoveBitfield::$caller invalid num $num");
    }
}