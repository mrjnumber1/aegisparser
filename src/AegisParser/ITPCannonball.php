<?php


namespace AegisParser;


final class ITPCannonball extends ITPAmmo
{
    public function __construct() { parent::__construct(); $this->type = ItemType::CANNONBALL(); }

    public function GetAvailableJobCode() : int { return JobType::MECHANIC()->getValue(); }
}