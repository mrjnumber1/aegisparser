<?php


namespace AegisParser;


final class ITPBothHand extends ITPWeapon
{
    public function __construct() { parent::__construct(); $this->type = ItemType::BOTHHAND(); }
}