<?php


namespace AegisParser;


final class ScriptLine
{
    private string $line;
    private string $cur;

    public function __construct(string $line) {
        $this->line = $this->cur = $line;
    }

    public function GetBase() : string { return $this->line; }
    public function GetCurrent() : string { return $this->cur; }

    // Strip any char from $filters from the beginning of $this->cur
    public function Skip(string $blacklist) : void {
        if ($this->cur === '') {
            return;
        }
        $this->cur = ltrim($this->cur, $blacklist);
    }

    // Return string from $this->cur to the first char in $blacklist
    public function GetWord(string $blacklist) : string {
        if ($this->cur === '') {
            return '';
        }

        $word = strtok($this->cur, $blacklist);
        if ($word === false) {
            $word = $this->cur;
        }

        // Advance $this->cur the length of $word
        $this->cur = substr($this->cur, strlen($word));
        return $word;
    }

    // Return slice of $this->cur until one not in the $whitelist
    public function GetOperator(string $whitelist) : string {
        if ($this->cur === '') {
            return '';
        }

        $gap_length = strlen($this->cur) - strlen(ltrim($this->cur, $whitelist));

        // fail if the string didn't start with anything from $filters
        if ($gap_length <= 0) {
            return '';
        }

        $op = substr($this->cur, 0, $gap_length);
        $this->cur = substr($this->cur, $gap_length);

        return $op;
    }

    // return the string in $this->cur bounded by $separator, or '' if not found
    public function GetParse(string $separator) : array {
        assert(strlen($separator) === 1, "Too many separators passed to GetParse. Got `".$separator."`");
        if ($this->cur === '' || $this->cur[0] !== $separator) {
            return ['', false];
        }
        $old = $this->cur;
        $this->cur = ltrim($this->cur, $separator);
        $removed_count = strlen($old) - strlen($this->cur);

        if (!str_contains($this->cur, $separator)) {
            return ['', $removed_count > 1];
        }

        $position = strpos($this->cur, $separator);
        $result = substr($this->cur, 0, $position);

        // Advance cur by length of $result and the final separator
        $this->cur = substr($this->cur, strlen($result)+1);

        return [$result, true];
    }
}