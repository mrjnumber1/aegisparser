<?php


namespace AegisParser;

use Ds\Pair;
use Ds\Set;

class SkillTypeInfo
{
    public const MAX_LEVEL = 10;

    protected SkillID $skid;
    protected string $name;
    protected SkillTargetType $targets;
    protected int $max_level;
    protected SkillPattern $pattern;
    protected SkillSplash $splash;
    protected SkillDataArray $hitPercent;
    protected SkillDataArray $atkPercent;
    protected array $data; // [data1 => SkillDataArray, data2 => SkillDataArray...]
    protected SkillFlagBitfield $flag;
    protected SkillDataArray $range;
    protected ElementProperty $element;
    protected Pair $handicap; // Pair<Condition, SkillDataArray> (time in ms)
    protected SkillDataArray $knockback;
    protected bool $isNoDamage;
    protected SkillDataArray $SPCost;
    protected Set $resetHandicapList; // list of Conditions
    protected SkillDataArray $drainHPRate;
    protected SkillDataArray $drainSPRate;
    protected bool $isQuestSkill;
    protected SkillDataArray $fixedCastTime;
    protected SkillDataArray $castTime;
    protected SkillDataArray $globalCooldown;
    protected SkillDataArray $singleCooldown;
	protected int $gndRange;
	protected int $jobLevel;
	protected JobType $jobLevel_validJob;
    protected array $dataString;
    protected array $weapons;

    public function __construct(SkillID $skid, int $max_level = SkillTypeInfo::MAX_LEVEL) {
        $this->skid = $skid;
        $this->name = $skid->getKey();
        $this->targets = SkillTargetType::PASSIVE();
        $this->max_level = $max_level;
        $this->pattern = SkillPattern::NONE();
        $this->splash = new SkillSplash(SkillSplashType::NONE(), [], $this->max_level);
        $this->hitPercent = new SkillDataArray([100], $max_level);
        $this->atkPercent = new SkillDataArray([100], $max_level);
        $this->data = [];
        $this->flag = new SkillFlagBitfield();
        $this->range = new SkillDataArray([0], $max_level);
        $this->element = ElementProperty::NOTHING();
        $this->handicap = new Pair(null, null);
        $this->knockback = new SkillDataArray([0], $max_level);
        $this->isNoDamage = true;
        $this->SPCost = new SkillDataArray([0], $max_level);
        $this->resetHandicapList = new Set();
        $this->drainHPRate = new SkillDataArray([0], $max_level);
        $this->drainSPRate = new SkillDataArray([0], $max_level);
        $this->isQuestSkill = false;
        $this->fixedCastTime = new SkillDataArray([0], $max_level);
        $this->castTime = new SkillDataArray([0], $max_level);
        $this->globalCooldown = new SkillDataArray([0], $max_level);
        $this->singleCooldown = new SkillDataArray([0], $max_level);
        $this->dataString = [];
		$this->jobLevel = $this->gndRange = 0;
		
    }

    public function GetSKID() : SkillID {
        return $this->skid;
    }

    public function GetName() : string {
        return $this->name;
    }
    public function GetMaxLevel() : int {
        return $this->max_level;
    }
    public function SetTargetType(SkillTargetType $type) : void {
        $this->targets = $type;
    }
    public function GetTargetType() : SkillTargetType {
        return $this->targets;
    }
    public function SetPattern(SkillPattern $pattern) : void {
        $this->pattern = $pattern;
    }
    public function GetPattern() : SkillPattern {
        return $this->pattern;
    }
    public function SetElement(ElementProperty $element) : void {
        assert($element->getValue() < ElementProperty::NOTHING1()->getValue(), "SkillTypeInfo::SetElement: {$this->GetName()} received invalid element property!");
        $this->element = $element;
    }
    public function GetElement() : ElementProperty {
        return $this->element;
    }
    public function SetFlag(SkillFlagBitfield $flag) : void {
        $this->flag = $flag;
    }
    public function GetFlag() : SkillFlagBitfield {
        return $this->flag;
    }
    public function HasFlag(SkillFlag $flag) : bool {
        return $this->flag->bf->HasFlag($flag);
    }
    public function SetNoDamage(bool $value) : void {
        $this->isNoDamage = $value;
    }
    public function IsNoDamage() : bool {
        return $this->isNoDamage;
    }
    public function SetQuestSkill(bool $value) : void {
        $this->isQuestSkill = $value;
    }
    public function IsQuestSkill() : bool {
        return $this->isQuestSkill;
    }
    public function SetResetHandicap(array $conditions) : void {
        $this->resetHandicapList->clear();
        foreach ($conditions as $cond) {
            assert(is_a($cond, Condition::class, false), "SkillTypeInfo::SetResetHandicap: $this->name Non-condition type passed!");
            assert(Condition::IsHandicap($cond), "SkillTypeInfo::SetResetHandicap: $this->name Passed a buff type {$cond->getKey()}!");

            $this->resetHandicapList->add($cond);
        }
    }
    public function GetResetHandicap() : Set {
        return $this->resetHandicapList;
    }
    public function ResetsHandicap(Condition $cond) : bool {
        return $this->resetHandicapList->contains($cond);
    }


    public function SetSplashData(SkillSplashType $type, array $values) : void {
        $this->splash = new SkillSplash($type, $values, $this->max_level);
    }
    public function GetSplashAt(int $level) : Pair {
        return $this->splash->GetSplash($level);
    }

    public function SetHitPercent(array $values) : void {
        $this->hitPercent->SetValues($values);
    }
    public function GetHitPercentAt(int $skill_lv) : int {
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetHitPercentAt: $this->name invalid level $skill_lv!");
        return $this->hitPercent->Get($skill_lv);
    }

    public function SetAtkPercent(array $values) : void {
        $this->atkPercent->SetValues($values);
    }
    public function GetAtkPercentAt(int $skill_lv) : int {
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetAtkPercentAt: $this->name invalid level $skill_lv!");
        return $this->atkPercent->Get($skill_lv);
    }

    public function SetRange(array $values) : void {
        $this->range->SetValues($values);
    }
    public function GetRangeAt(int $skill_lv) : int {
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetRangeAt: $this->name invalid level $skill_lv!");
        return $this->range->Get($skill_lv);
    }

    public function SetPushback(array $values) : void {
        $this->knockback->SetValues($values);
    }
    public function GetPushbackAt(int $skill_lv) : int {
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetPushbackAt: $this->name invalid level $skill_lv!");
        return $this->knockback->Get($skill_lv);
    }

    public function SetSPCost(array $values) : void {
        $this->SPCost->SetValues($values);
    }
    public function GetSPCostAt(int $skill_lv) : int {
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetSPCostAt: $this->name invalid level $skill_lv!");
        return $this->SPCost->Get($skill_lv);
    }

    public function SetDrainHP(array $values) : void {
        $this->drainHPRate->SetValues($values);
    }
    public function GetDrainHPAt(int $skill_lv) : int {
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetDrainHPAt: $this->name invalid level $skill_lv!");
        return $this->drainHPRate->Get($skill_lv);
    }

    public function SetDrainSP(array $values) : void {
        $this->drainSPRate->SetValues($values);
    }
    public function GetDrainSPAt(int $skill_lv) : int {
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetDrainSPAt: $this->name invalid level $skill_lv!");
        return $this->drainSPRate->Get($skill_lv);
    }

    public function SetFixedCast(array $values) : void {
        $this->fixedCastTime->SetValues($values);
    }
    public function GetFixedCastAt(int $skill_lv) : int {
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetFixedCastAt: $this->name invalid level $skill_lv!");
        return $this->fixedCastTime->Get($skill_lv);
    }


    public function SetCast(array $values) : void {
        $this->castTime->SetValues($values);
    }
    public function GetCastAt(int $skill_lv) : int {
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetCastAt: $this->name invalid level $skill_lv!");
        return $this->castTime->Get($skill_lv);
    }

    public function SetGlobalCD(array $values) : void {
        $this->globalCooldown->SetValues($values);
    }
    public function GetGlobalCDAt(int $skill_lv) : int {
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetGlobalCDAt: $this->name invalid level $skill_lv!");
        return $this->globalCooldown->Get($skill_lv);
    }

    public function SetSingleCD(array $values) : void {
        $this->singleCooldown->SetValues($values);
    }
    public function GetSingleCDAt(int $skill_lv) : int {
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetSingleCDAt: $this->name invalid level $skill_lv!");
        return $this->singleCooldown->Get($skill_lv);
    }

    public function SetStrings(array $values) : void {
        $this->dataString = [];

        foreach ($values as $str) {
            assert(is_string($str), "SkillTypeInfo::SetStrings: $this->name non string passed!");
            $this->dataString[] = $str;
        }
    }
    public function GetString(int $index) : string {
        assert($index < count($this->dataString), "SkillTypeInfo::GetString: $this->name invalid index!");
        return $this->dataString[$index];
    }

    public function SetExtraData(array $arrays) : void {
        $this->data = [];
        foreach ($arrays as $index => $array) {
            $this->data[$index] = new SkillDataArray($array, $this->max_level);
        }
    }
    public function GetExtraDataAt(int $index, int $skill_lv) : int {
        assert($index < count($this->data), "SkillTypeInfo::GetExtraDataAt: $this->name invalid index $index!");
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetExtraDataAt: $this->name invalid level $skill_lv!");
        return $this->data[$index]->Get($skill_lv);
    }

    public function SetHandicap(Condition $cond, array $values) : void {
        assert(Condition::IsHandicap($cond), "SkillTypeInfo::SetHandicap: $this->name Passed a buff type $cond!");
        $this->handicap = new Pair($cond, new SkillDataArray($values, $this->max_level));
    }

    public function GetHandicapType() : Condition {
        assert(!is_null($this->handicap->key), "SkillTypeInfo::GetHandicapType: $this->name has no handicap!");
        return $this->handicap->key;
    }
    public function GetHandicapRateAt(int $skill_lv) : int {
        assert(!is_null($this->handicap->key), "SkillTypeInfo::GetHandicapType: $this->name has no handicap!");
        assert($skill_lv <= $this->max_level, "SkillTypeInfo::GetExtraDataAt: $this->name invalid level $skill_lv!");


        return $this->handicap->value[$skill_lv];
    }

    public function SetRequiredWeapon(array $weapons) : void {
        $this->weapons = [];
        foreach ($weapons as $type) {
            assert(is_a($type, WeaponType::class, false), "SkillTypeInfo::SetRequiredWeapon: $this->name invalid weapon type received.");
            $this->weapons[] = $type;
        }
    }
    public function IsRequiredWeapon(WeaponType $type) : bool {
        return in_array($type, $this->weapons, false);
    }
	
	public function GetGNDRange(int $skill_lv) : int {
		return $this->gndRange;
	}
	
	public function IsAvailableJob(JobType $jt) : bool {
		return false;
	}
	
	public function IsEnableIgnoreMagicImmune(int $skill_lv) : bool {
		return $this->GetPattern() !== SkillPattern::ATTACKSPELL();
	}
	
	public function SetAvailableJobLevel(JobType $jt, int $lvl) : void {
		$this->jobLevel = $lvl;
		$this->jobLevel_validJob = $jt;
	}
	
	public function OnMsg(Character $sender, SkillID $receiver, SkillMsg $msg, int $skill_lv, int $targetAID, array $par3) : int {
		//if ($msg == SkillMsg::LEVELUP())
		//{
			//charmgr->SendMsg(null, $sender, CharMsg::SKILL_LEVEL(), $receiver);
		//}
		return 0;
	}
}
