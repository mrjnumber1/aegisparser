<?php


namespace AegisParser;


use Application\Log;
use Ds\Map;
use Ds\Set;

final class ITPMgr
{
    private Map $itpList; // map<item_id, ITP>
    private Map $nameList; // map<item_name, item_id> (also combo_name, combo_id)
    private Map $combiList; // map<combo_name, array<item_id>>
    private Set $scrollList; // set<item_id>
    private Set $not_disappearList; // set<item_id>

    public function __construct(string $zone_path, array $DB) {
        $this->itpList = new Map();
        $this->nameList = new Map();
        $this->combiList = new Map();
        $this->scrollList = new Set();
        $this->not_disappearList = new Set();

        $this->init($zone_path, $DB);
    }

    private function init(string $zone_path, array $DB) : void {

        Log::info("ITPMgr::init start...");

        $this->loadITP($DB);
        Log::info("ITPMgr::loadITP complete! {$this->itpList->count()} entries loaded.");
        $this->loadCombiItemList($zone_path);
        Log::info("ITPMgr::loadCombiItemList complete! {$this->combiList->count()} entries loaded.");
        $num = $this->loadItemMoveInfo($zone_path);
        Log::info("ITPMgr::loadItemMoveInfo complete! $num entries loaded.");
        $num = $this->loadCashItemList($zone_path);
        Log::info("ITPMgr::loadCashItemList complete! $num entries loaded.");
        $this->loadScrollItemList($zone_path);
        Log::info("ITPMgr::loadScrollItemList complete! {$this->scrollList->count()} entries loaded.");
        $num = $this->loadItemBindOnEquip($zone_path);
        Log::info("ITPMgr::itemBindOnEquip complete! $num entries loaded.");
        $this->loadNotDisappearAfterUsingItemListLua($zone_path);
        Log::info("ITPMgr::loadNotDisappearAfterUsingItemListLua complete! {$this->not_disappearList->count()} entries loaded.");
        $num = $this->loadItemPackage($zone_path);
        Log::info("ITPMgr::loadItemPackage complete! $num packages loaded.");

        $num = $this->loadSummonList($zone_path);
        Log::info("ITPMgr::loadSummonList complete! $num items with summons loaded.");
    }

    public function Save(string $path) : void
    {
        assert(!$this->nameList->isEmpty(), "ITPMgr::Save: empty item list!");

        $this->nameList->sort();
        $it = $this->nameList->getIterator();
        $str = "";

        foreach ($it as $name => $id) {
            $str .= $name ." ".$id."\r\n";
        }

        file_put_contents($path, $str);
    }


    public function HasName(string $name) : int {
        return $this->nameList->hasKey($name);
    }

    public function GetITID(string $name) : int {
        assert($this->HasName($name), "ITPMgr::GetITID invalid name search '$name'");
        return $this->nameList[$name];
    }

    public function HasITP(int $id) : bool {
        return $this->itpList->hasKey($id);
    }

    public function GetITP(int $id) : ITP {
        assert($this->HasITP($id), "IIPMgr::GetITP: Invalid item id '$id'");
        return $this->itpList->get($id);
    }

    private function replaceITP(ITP $itp) : void {
        assert($this->HasITP($itp->GetITID()), "ITPMgr::replaceITP: Invalid ITP ID {$itp->GetITID()}");
        $this->itpList->put($itp->GetITID(), $itp);
    }

    private function loadFileContents(string $filename, bool $isRequiredFile = false) : array {
        if (!file_exists($filename)) {
            assert(!$isRequiredFile, "ITPMgr::loadFileContents: required file `$filename` not found!");
            // if we reached here, it's not an important file or it doesn't exist on this episode
            return [];
        }

        $contents = file_get_contents($filename);
        assert ($contents !== false, "IIPMgr::loadFileContents: Failed to read `$filename`.");

        $lines = [];
        foreach (explode("\n", $contents) as $line) {
            // remove all comments and trailing/leading whitespace
            $line = trim($line);
            if ($line === '' || str_starts_with($line, "//")) {
                continue;
            }
            if (str_contains($line, "//")) {
                $line = substr($line, 0, strpos($line, "//"));
            }

            // replace all tabs with normal spaces
            $line = str_replace("\t", " ", $line);
            // remove multiple whitespace with single whitespace
            $line = preg_replace("/\s+/", " ", $line);
            // trim starting/ending whitespace again for the end
            $line = trim($line);

            // add to our return :)
            $lines[] = $line;
        }

        return $lines;
    }

    private function loadSummonList(string $zone_path) : int {
        $files = [ItemID::class => "ItemSummonList.txt", JobType::class => "MonsterSummonList.txt"];

        $count = 0;
        foreach ($files as $type => $file) {
            $filename = $zone_path . $file;
            $lines = $this->loadFileContents($filename, true);

            if (empty($lines)) {
                continue;
            }

            $items = [];

            foreach ($lines as $line) {
                [$container, $entry] = explode(" ", $line);
                if (!array_key_exists($container, $items)) {
                    $items[$container] = [];
                }
                // MonsterSummonList stores by name, ItemSummonList stores by id. Convert for usage
                if (!is_numeric($entry)) {
                    assert($type === JobType::class, "ITPMgr::loadSummonList: Non numeric entry `$entry` in $file!");
                    $entry = JobType::$entry()->getValue();
                }
                if (!array_key_exists($entry, $items[$container])) {
                    $items[$container][$entry] = 0;
                }
                $items[$container][$entry]++;
            }

            foreach ($items as $container => $contents) {
                assert(ItemID::IsValidKey($container), "ITPMgr::loadSummonList: Invalid container item `$container`");

                $list = new RandomSummonList(ItemID::$container(), $type);


                foreach ($contents as $entry => $count) {
                    $class = new ($list->GetEntryType())($entry);
                    assert($class, "ITPMgr::loadSummonList: Invalid entry ID $entry!");

                    $list->AddEntry($class::from($entry), $count);
                 }

                assert($list->GetTotalEntryCount() > 0, "ITPMgr::loadSummonList: List {$list->GetITID()} has 0 entries!");

                $container_id = $this->nameList[$container];
                $item = $this->itpList->get($container_id);

                $item->SetSummonList($list);

                $count++;
            }
        }

        return $count;
    }

    private function loadItemPackage(string $zone_path) : int {
        $filename = $zone_path."ItemPackage.txt";
        $lines = $this->loadFileContents($filename);
        if (empty($lines)) {
            return 0;
        }

        $count = 0;
        $pack_type = IPBoxType::UNKNOWN();
        $indent_level = 0;
        $package = null;

        foreach ($lines as $line) {
            $line = trim($line);
            switch ($indent_level)
            {
            case 0:
                preg_match('/PACKAGE "([^\"]+)"{/', $line, $matches);
                assert(count($matches), "ITPMgr::loadItemPackage: No PACKAGE name found!. line:`$line`");
                [, $name] = $matches;
                assert($this->nameList->hasKey($name), "ITPMgr::loadItemPackage: Package ItemID for `$name` not found! line:`$line`");

                $package = new ItemPackage(ItemID::$name());

                $indent_level++;
                break;
            case 1:
                if (str_contains($line, 'MUST')) {
                    $pack_type = IPBoxtype::MUST();
                } else if (str_contains($line, 'RANDOM')) {
                    $pack_type = IPBoxType::RANDOM();
                } else if (str_contains($line, '}')) {

                    $itp = $this->itpList[$package->GetITID()->getValue()];
                    $itp->SetPackage($package);
                    $this->itpList->put($itp->GetITID(), $itp);
                    $itp = $package = null;
                    $count++;

                    $indent_level--;
                    break;
                } else {
                    assert(false, "ITPMgr::loadItemPackage: Invalid pack type! line: $line");
                }
                $indent_level++;
                break;
            case 2:
                $entry = null;
                $matches = [];
                $entry_rate = 1000;

                if (str_contains($line, '}'))  {
                    $pack_type = IPBoxType::UNKNOWN();
                    $indent_level--;
                    break;
                }

                if ($pack_type == IPBoxType::MUST()) {
                    preg_match('/"([^\"]+)",(\d+),(\d+)/', $line, $matches);
                    assert(count($matches) === 4, "ITPMgr::loadItemPackage: more data must be available! `$line`");

                    array_shift($matches);
                }  else if ($pack_type == IPBoxType::RANDOM()) {
                    preg_match('/"([^\"]+)",(\d+),(\d+)/', $line, $matches);
                    assert(count($matches) === 5, "ITPMgr::loadItemPackage: random pack entry didn't contain enough data! line `$line`");

                    array_shift($matches);
                    $entry_rate = array_shift($matches);

                } else {
                    assert(false, "ITPMgr::loadItemPackage: Unhandled line type! $line");
                }

                $entry_name = array_shift($matches);
                $entry_count = (int)array_shift($matches);
                $entry_time = (int)array_shift($matches);

                assert($this->nameList->hasKey($entry_name), "ITPMgr::loadItemPackage: No ItemID found for item `$entry_name`. line `$line`");
                assert($entry_count > 0, "ITPMgr::loadItemPackage: No count for entry `$entry_name`. line `$line`");

                $entry = new IPBoxItem(ItemID::$entry_name(), $entry_count, $entry_time, $entry_rate);
                $package->AddBoxItem($pack_type, $entry);

                $entry = null;
                break;
            }

        }



        return $count;
    }


    private function loadCombiItemList(string $zone_path) : void {
        $filename = $zone_path."itemdata".DIRECTORY_SEPARATOR."combiItem.sc";
        $lines = $this->loadFileContents($filename, true);

        if (empty($lines)) {
            return;
        }

        $in_block = false;
        $combo_id = 30000;
        $combo_name = '';
        $items = [];

        foreach ($lines as $line) {
            if ($line === '{') {
                assert($combo_name !== '', "ITPMgr::loadCombiItemList: Encountered open brace but no combo name set!");

                continue;
            }

            if ($line === '}') {
                assert($in_block, "ITPMgr::loadCombiItemList: Encountered close brace but wasn't in a block!");

                sort($items);
                $this->combiList->put($combo_name, $items);
                $this->nameList->put($combo_name, $combo_id);
                $combo_id++;

                $in_block = false;
                $items = [];
                $combo_name = '';

                continue;
            }

            if ($in_block) {
                $name = $line;
                assert($this->HasName($name), "ITPMgr::loadCombiItemList: Invalid item name '$name'");

                $id = $this->GetITID($name);
                $items[] = $id;

                continue;
            }

            // some lines are "item COMBITEM_xxx" so we strip that space..
            if (str_contains($line, " ")) {
                $line = substr($line, strpos($line, " "));
            }

            $combo_name = $line;
            $in_block = true;
        }

    }

    private function loadItemMoveInfo(string $zone_path) : int {
        $names = ["ItemMoveInfo.txt", "ItemMoveInfoV4.txt", "ItemMoveInfoV5.txt"];
        $lines = [];
        foreach ($names as $name) {
            $lines = $this->loadFileContents($zone_path.$name);
            if (!empty($lines)) {
                break;
            }
        }
        if (empty($lines)) {
            return 0;
        }

        $i=0;
        foreach ($lines as $line) {
            $data = explode(" ", $line);
            assert(count($data) > 5, "ITPMgr::loadItemMoveInfo: line lacked data! line: `$line`");

            $id = (int)array_shift($data);
            if (!$this->HasITP($id)) {
                Log::error("ITPMgr::loadItemMoveInfo attempted to set moveinfo for invalid item $id");
                continue;
            }

            $itp = $this->GetITP($id);

            $txt = implode(" ", $data);
            $movable = new ItemBlockMoveBitfield(0);
            $movable->SetFromItemMoveInfo($txt);

            $itp->SetMovable($movable);
            $i++;

            $this->replaceITP($itp);
        }
        return $i;
    }

    private function loadCashItemList(string $zone_path) : int {
        $filename = $zone_path."CashItemList.txt";
        $lines = $this->loadFileContents($filename);
        if (empty($lines)) {
            return 0;
        }

        $i = 0;
        foreach ($lines as $line) {
            $name = str_replace("\"", "", $line);
            assert($this->HasName($name), "ITPMgr::loadCashItemList: Invalid item name '$name'");

            $itp = $this->GetITP($this->GetITID($name));

            $itp->SetCashItem(true);
            $i++;
            $this->replaceITP($itp);
        }

        return $i;
    }

    private function loadScrollItemList(string $zone_path) : void {
        $filename = $zone_path."ScrollItemList.txt";
        $lines = $this->loadFileContents($filename);
        if (empty($lines)) {
            return;
        }
        foreach ($lines as $line) {
            $id = (int)$line;
            assert($this->HasITP($id), "ITPMgr::loadScrollItemList: Invalid item ID $id");

            $this->scrollList->add($id);
        }

    }

    private function loadItemBindOnEquip(string $zone_path) : int {
        $filename = $zone_path."BindOnEquip.txt";
        $lines = $this->loadFileContents($filename);
        if (empty($lines)) {
            return 0;
        }

        $i = 0;
        foreach ($lines as $line) {
            $name = $line;
            assert($this->nameList->hasKey($name), "ITPMgr::loadItemBindOnEquip: invalid name '$name'");
            $id = $this->GetITID($name);
            $itp = $this->GetITP($id);

            $itp->SetBindOnEquip(true);
            $i++;
            $this->replaceITP($itp);
        }

        return $i;
    }

    private function loadNotDisappearAfterUsingItemListLua(string $zone_path) : void {
        $filename = $zone_path."NotDisappearAfterUsingItemList.lua";
        $lines = $this->loadFileContents($filename);
        if (empty($lines)) {
            return;
        }

        $in_block = false;
        foreach ($lines as $line) {
            if (str_starts_with($line, "--")) { // lua comments weren't stripped before
                continue;
            }
            if (!$in_block) {
                if (str_contains($line, "{")) {
                    $in_block = true;
                }
                continue;
            }
            if (str_contains($line, "};")) { // reached the end..
                break;
            }

            $name = trim($line, "\",");
            assert($this->HasName($name), "ITPMgr::loadNotDisappearAfterUsingItemListLua invalid name '$name'");

            $this->not_disappearList->add($this->GetITID($name));
        }

    }

    // Populate $this->>itpMap with contents from $DB
    private function loadITP(array $DB) : void {
        $this->loadHeal($DB['heal']);
        $this->loadGeneric($DB['special'], ItemType::SPECIAL());
        $this->loadGeneric($DB['event'], ItemType::EVENT());
        $this->loadCard($DB['card']);
        $this->loadArmor($DB['armor'], ItemType::ARMOR());
        $this->loadArmor($DB['armorTB'], ItemType::ARMORTB());
        $this->loadArmor($DB['armorTM'], ItemType::ARMORTM());
        $this->loadArmor($DB['armorTMB'], ItemType::ARMORTMB());
        $this->loadArmor($DB['armorMB'], ItemType::ARMORMB());
        $this->loadWeapon($DB['weapon'], ItemType::WEAPON());
        if (array_key_exists('quest', $DB)) {
            $this->loadGeneric($DB['quest'], ItemType::QUEST());
        } else if (array_key_exists('guest', $DB)) {
            $this->loadGeneric($DB['guest'], ItemType::QUEST());
        }
        $this->loadAmmo($DB['arrow'], ItemType::ARROW());
        $this->loadWeapon($DB['bothhand'], ItemType::BOTHHAND());
        $this->loadWeapon($DB['bow'], ItemType::BOW());

        // expanded classes
        if (array_key_exists('ammo', $DB)) {
            $this->loadAmmo($DB['ammo'], ItemType::AMMO());
        }
        if (array_key_exists('gun', $DB)) {
            $this->loadWeapon($DB['gun'], ItemType::GUN());
        }
        if (array_key_exists('ThrowWeapon', $DB)) {
            $this->loadAmmo($DB['ThrowWeapon'], Itemtype::THROWWEAPON());
        }
        // cash point updates
        if (array_key_exists('CashPointItem', $DB)) {
            $this->loadCashPointItem($DB['CashPointItem']);
        }

        // renewal stuff
        if (array_key_exists('cannonball', $DB)) {
            $this->loadAmmo($DB['cannonball'], ItemType::CANNONBALL());
        }
    }

    private function addITP(ITP $itp) : void {
        $id = $itp->GetITID();
        $name = $itp->GetName();

        assert($id > 500, "ITPMgr::addITP Invalid item id! nameid: $id");
        assert(!$this->itpList->hasKey($id), "ITPMgr::addITP duplicate key added! id: $id");
        assert($name !== '' && strlen($name) <= 23,"ITPMgr::addITP invalid name for $id name: '$name'" );

        $this->itpList->put($id, $itp);
        $this->nameList[$name] = $id;
    }

    private function setItemBasics(array $data, ITP $itp) : void {
        $itp->SetITID($data['ID']);

        $name_keys = ['NAME', 'Name'];
        foreach ($name_keys as $key) {
            if (array_key_exists($key, $data)) {
                $itp->SetName($data[$key]);
                break;
            }
        }

        $price_keys = ['PRICE', 'Price'];
        foreach ($price_keys as $key) {
            if (array_key_exists($key, $data)) {
                $itp->SetPrice($data[$key]);
                break;
            }
        }

        $weight_keys = ['WEIGHT', 'Weight'];
        foreach ($weight_keys as $key) {
            if (array_key_exists($key, $data)) {
                $itp->SetWeight($data[$key]);
                break;
            }
        }

        if (array_key_exists('equipableLevel', $data)) {
            $itp->SetMinLevel($data['equipableLevel']);
            $itp->SetMaxLevel(99);
        }
        if (array_key_exists('MinLevel', $data)) {
            $itp->SetMinLevel($data['MinLevel']);
        }
        if (array_key_exists('MaxLevel', $data)) {
            $itp->SetMaxLevel($data['MaxLevel']);
        }
    }

    private function setGearBasics(array $data, ITPEquippable $itp) : void {
        $itp->SetRefinable(($data['IsRefinable']===1));
        $itp->SetDamagable(($data['IsDamagable']===1));
        $itp->SetAvailableJobCode($data['EQUIP']);
        $itp->SetSex(Sex::From($data['SEX']));
        $itp->SetMaxSlot($data['SLOT']);

        $pos = new EquipPosBitfield();
        $pos->FromNum($data['LOCA']);
        $itp->SetLocation($pos);

        $itp->SetStrValue($data['STR']);
        $itp->SetAgiValue($data['AGI']);
        $itp->SetVitValue($data['VIT']);
        $itp->SetDexValue($data['DEX']);
        $itp->SetIntValue($data['INT']);
        $itp->SetLukValue($data['LUK']);
    }

    private function setArmorBasics(array $data, ITPArmor $itp) : void {
        $itp->SetDefense($data['DEF']);
        $itp->SetMagicDefense($data['MDEF']);
        $itp->SetMaxHp($data['MHP']);
        $itp->SetMaxSp($data['MSP']);

        $c = $data['CLASS'];
        if ($itp->GetLocation()->IsHeadPos() || $itp->GetLocation()->IsCostumeHeadPos()) {
            $itp->SetClass(AccessoryView::from($c));
        } else if ($itp->GetLocation()->EqualsFlag(EquipPosFlag::LARM())) {
            $itp->SetClass(ShieldView::from($c));
        } else {
            $itp->SetClass(ArmorView::NONE());
        }
    }

    private function setWeaponBasics(array $data, ITPWeapon $itp) : void {
        $itp->SetAttackValue($data['ATK']);
        $itp->SetAttRange($data['AR']);
        $itp->SetItemLevel($data['level']);
        $itp->SetClass(WeaponType::from($data['CLASS']));
        $itp->SetProperty(ElementProperty::from($data['property']));
        if (array_key_exists('MATK', $data)) {
            $itp->SetMATK($data['MATK']);
        }
    }

    private function setAmmoBasics(array $data, ITPAmmo $itp) : void {
        $itp->SetLocation(new EquipPosBitfield([EquipPosflag::AMMO()]));

        $itp->SetAttackValue($data['ATK']);
        $itp->SetProperty(ElementProperty::from($data['PROPERTY']));

        if (array_key_exists('EQUIP', $data)) {
            $itp->SetAvailableJobCode($data['EQUIP']);
        } else {
            if ($itp instanceof ITPThrow) {
                $itp->SetAvailableJobCode(74);
            } else if ($itp instanceof ITPCannonball) {
                $itp->SetAvailableJobCode(JobType::MECHANIC()->getValue());
            } else if (!($itp instanceof ITPArrow)) {
                $itp->SetAvailableJobCode(73); // (pre-RE hardcoded)
            } else {
                $itp->SetAvailableJobCode(0); // arrows hardcoded for ARCHER_GR+THIEF_GR
            }

        }
    }

    private function loadGeneric(array $entries, ItemType $type) : void {
        foreach ($entries as $data) {
            $itp = null;
            switch ($type) {
                case ItemType::SPECIAL(): $itp = new ITPSpecial(); break;
                case ItemType::EVENT(): $itp = new ITPEvent(); break;
                case ItemType::QUEST(): $itp = new ITPQuest(); break;
            }
            assert(!is_null($itp), "ITPMgr::loadGeneric: unknown type {$type->getValue()}");

            $this->setItemBasics($data, $itp);

            $this->addITP($itp);
        }
    }

    private function loadCashPointItem(array $entries) : void {
        foreach ($entries as $data) {
            $itp = new ITPCashPointItem();
            $this->setItemBasics($data, $itp);

            $itp->SetChangeItemID($data['ChangeItemID']);
            $itp->SetCount($data['Count']);

            $this->addITP($itp);
        }
    }

    private function loadHeal(array $entries) : void {
        foreach ($entries as $data) {
            $itp = new ITPHeal();
            $this->setItemBasics($data, $itp);

            $itp->SetMinHp($data['HPMIN']);
            $itp->SetMaxHp($data['HPMAX']);
            $itp->SetMinSp($data['SPMIN']);
            $itp->SetMaxSp($data['SPMAX']);
            $itp->SetStatusNum($data['STATUS']);

            $this->addITP($itp);
        }
    }

    private function loadCard(array $entries) : void {
        foreach ($entries as $data) {
            $itp = new ITPCard();
            $this->setItemBasics($data, $itp);

            if ($data['compositionType'] === 1) {
                $itp->SetLocation(new EquipPosBitfield([EquipPosFlag::RARM()]));
            }
            else {
                $f = new EquipPosBitfield();
                $f->FromNum($data['compositionPos']);
                $itp->SetLocation($f);
            }

            $this->addITP($itp);
        }
    }

    private function loadArmor(array $entries, ItemType $type) : void {

        foreach ($entries as $data) {
            $itp = null;
            switch ($type) {
                case ItemType::ARMOR(): $itp = new ITPArmor(); break;
                case ItemType::ARMORTB(): $itp = new ITPArmorTB(); break;
                case ItemType::ARMORTM(): $itp = new ITPArmorTM(); break;
                case ItemType::ARMORTMB(): $itp = new ITPArmorTMB(); break;
                case ItemType::ARMORMB(): $itp = new ITPArmorMB(); break;
            }
            assert(!is_null($itp), "ITPMgr::loadArmor Invalid type passed {$type->getValue()}");

            $this->setItemBasics($data, $itp);
            $this->setGearBasics($data, $itp);

            $this->setArmorBasics($data, $itp);

            $this->addITP($itp);
        }
    }

    private function loadWeapon(array $entries, ItemType $type) : void {
        foreach ($entries as $data) {
            $itp = null;
            switch($type) {
                case ItemType::BOTHHAND(): $itp = new ITPBothHand(); break;
                case ItemType::BOW(): $itp = new ITPBow(); break;
                case ItemType::GUN(): $itp = new ITPGun(); break;
                case ItemType::WEAPON(): $itp = new ITPWeapon(); break;
            }

            assert(!is_null($itp), "ITPMgr::loadWeapon Invalid type passed {$type->getValue()}");

            $this->setItemBasics($data, $itp);

            $this->setGearBasics($data, $itp);
            $this->setWeaponBasics($data, $itp);

            $this->addITP($itp);
        }
    }

    private function loadAmmo(array $entries, ItemType $type) : void {
        foreach ($entries as $data) {
            $itp = null;
            switch ($type) {
                case ItemType::AMMO(): $itp = new ITPAmmo(); break;
                case ItemType::ARROW(): $itp = new ITPArrow(); break;
                case ItemType::THROWWEAPON(): $itp = new ITPThrow(); break;
                case ItemType::CANNONBALL(): $itp = new ITPCannonball(); break;
            }
            assert(!is_null($itp), "ITPMgr::loadAmmo: unknown type {$type->getValue()}");

            $this->setItemBasics($data, $itp);

            $this->setAmmoBasics($data, $itp);

            $this->addITP($itp);
        }
    }

}