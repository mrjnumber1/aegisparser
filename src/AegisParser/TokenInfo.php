<?php


namespace AegisParser;


final class TokenInfo
{
    private int $num;
    private string $str;
    private TokenType $type;

    // do we need to pass isNum i don't think so, will know later
    public function __construct(TokenType $type, int $num, string $str) {
        $this->Set($type, $num, $str);
    }
    public function GetType() : TokenType {
        return $this->type;
    }
    public function GetStr() : string {
        return $this->str;
    }
    public function GetNum() : int {
        return $this->num;
    }

    public function IsStrType() : bool {
        assert($this->GetStr() !== '', "IsStrType for an un-set token");
        return $this->num === 1;
    }
    public function IsIntType() : bool {
        $s = $this->GetStr();
        return is_numeric($s) && ctype_digit($s);
    }

    public function GetStrAsInt() : int {
        $value = $this->GetStr();

        assert($this->IsIntType(), "TokenInfo::GetStrAsInt must be int convertible:$this->str");

        return (int)$value;
    }

    public function Set(TokenType $type, int $num, string $str) : void {
        $this->type = $type;
        $this->num = $num;
        $this->str = $str;
    }

}