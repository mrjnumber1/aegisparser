<?php


namespace AegisParser;


class ITP
{
    protected int $ITID;
    protected int $weight;
    protected int $price;
    protected EquipPosBitfield $location;
    protected string $name;
    protected ItemBlockMoveBitfield $movable;
    protected bool $cashItem;
    protected bool $useEvent;
    protected bool $bindOnEquip;
    protected int $minLevel;
    protected int $maxLevel;
    protected ItemType $type;
    protected ItemPackage $package;
    protected RandomSummonList $summonList;

    public function __construct() {
        $this->type = ItemType::HEAL();
        $this->Init();
    }

    public function Init() : void {
        $this->ITID = 0;
        $this->weight = 0;
        $this->price = 0;
        $this->location = new EquipPosBitfield([]);
        $this->name = '';
        $this->movable = new ItemBlockMoveBitfield(0);
        $this->cashItem = false;
        $this->useEvent = false;
        $this->bindOnEquip = false;
        $this->minLevel = 1;
        $this->maxLevel = 200;
        if ($this->GetType() === ItemType::QUEST()){
            $this->movable->SetFromItemMoveInfo("1 1 1 1 1 1 1 1");
        }
    }

    public function GetLocation() : EquipPosBitfield { return $this->location; }
    public function SetLocation(EquipPosBitfield $l) : void { $this->location = $l; }
    public function IsApparel() : bool {
        // must use switch here, because of type issues
        switch ($this->GetType()) {
            case ItemType::ARMOR():
            case ItemType::ARMORMB():
            case ItemType::ARMORTB():
            case ITemType::ARMORTM():
            case ItemType::ARMORTMB():
            case ItemType::BOTHHAND():
            case ItemType::BOW();
            case ItemType::GUN();
            case ItemType::THROWWEAPON();
            case ItemType::WEAPON();
            return true;
        }
        return false;
    }
    public function GetType() : ItemType { return $this->type; }
    public function SetType(ItemType $t) : void { $this->type = $t; }
    public function IsUsableLevel(int $level) : bool { return ($level >= $this->minLevel && $level <= $this->maxLevel); }
    public function SetUsableLevel(int $min, int $max) : void { assert($max >= $min); $this->minLevel = $min; $this->maxLevel = $max; }
    public function SetMinLevel(int $min) : void { $this->minLevel = $min; }
    public function GetMinLevel() : int { return $this->minLevel; }
    public function SetMaxLevel(int $max) : void { $this->maxLevel = $max; }
    public function GetMaxLevel() : int { return $this->maxLevel; }
    public function IsCashItem() : bool { return $this->cashItem; }
    public function SetCashItem(bool $v) : void { $this->cashItem = $v; }
    public function MovableNum() : int { return $this->movable->GetMovableNum(); }
    public function IsUnmovable(NotMoveFlag $flag) : bool { return $this->movable->IsFlagged($flag); }
    public function SetMovable(ItemBlockMoveBitfield $f) : void { $this->movable = $f; }
    public function IsPickupNotifyParty() : bool { return ($this->IsApparel() || $this->GetType() === ItemType::CARD());}

    public function IsWeapon() : bool
    {
        switch ($this->GetType()) {
            case ItemType::WEAPON():
            case ItemType::BOW():
            case ItemType::GUN():
            case ItemType::BOTHHAND():
                return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function GetITID(): int
    {
        return $this->ITID;
    }

    /**
     * @param int $ITID
     */
    public function SetITID(int $ITID): void
    {
        $this->ITID = $ITID;
    }

    /**
     * @return int
     */
    public function GetWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function SetWeight(int $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function GetPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function SetPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function GetName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function SetName(string $name): void
    {
        // i love gravity
        // assert (strlen($name) <= 24, "Too long of name given to {$this->GetITID()}");
        if (strlen($name) > 23) {
            $name = substr($name, 0, 23);
        }
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function IsUseEvent(): bool
    {
        return $this->useEvent;
    }

    /**
     * @param bool $useEvent
     */
    public function SetUseEvent(bool $useEvent): void
    {
        $this->useEvent = $useEvent;
    }

    /**
     * @return bool
     */
    public function IsBindOnEquip(): bool
    {
        return $this->bindOnEquip;
    }

    /**
     * @param bool $bindOnEquip
     */
    public function SetBindOnEquip(bool $bindOnEquip): void
    {
        $this->bindOnEquip = $bindOnEquip;
    }

    public function GetPackage() : ItemPackage {
        return $this->package;
    }

    public function SetPackage(ItemPackage $package) : void {
        $this->package = $package;
    }

    public function GetSummonList() : RandomSummonList {
        return $this->summonList;
    }
    public function SetSummonList(RandomSummonList $l) : void {
        $this->summonList = $l;
    }
}