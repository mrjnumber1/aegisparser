<?php


namespace AegisParser;


final class ITPThrow extends ITPAmmo
{
    public function __construct() { parent::__construct(); $this->type = ItemType::THROWWEAPON(); }
    public function GetAvailableJobCode() : int { return 74; }
}