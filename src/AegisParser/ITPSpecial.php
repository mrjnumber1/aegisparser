<?php


namespace AegisParser;


final class ITPSpecial extends ITP
{
    public function __construct() { parent::__construct(); $this->type = ItemType::SPECIAL(); }
}