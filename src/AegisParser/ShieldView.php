<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

/**
 * @method static ShieldView GUARD()
 * @method static ShieldView BUCKLER()
 * @method static ShieldView SHIELD()
 * @method static ShieldView MIRRORSHIELD()
 * @method static ShieldView BOOK()
 */
final class ShieldView extends ArmorView
{
    private const GUARD = 1;
    private const BUCKLER = 2;
    private const SHIELD = 3;
    private const MIRRORSHIELD = 4;
    private const BOOK = 5;
}