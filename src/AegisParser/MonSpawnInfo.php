<?php


namespace AegisParser;


final class MonSpawnInfo
{
    private JobType $master;
    private JobType $slave;
    private int $slaveCount;

    public function __construct(JobType $master, JobType $slave, int $num) {
        $this->SetMaster($master);
        $this->SetSlave($slave);
        $this->SetSlaveCount($num);
    }


    /**
     * @return JobType
     */
    public function GetMaster(): JobType
    {
        return $this->master;
    }

    /**
     * @param JobType $master
     */
    public function SetMaster(JobType $master): void
    {
        $this->master = $master;
    }

    /**
     * @return JobType
     */
    public function GetSlave(): JobType
    {
        return $this->slave;
    }

    /**
     * @param JobType $slave
     */
    public function SetSlave(JobType $slave): void
    {
        $this->slave = $slave;
    }

    /**
     * @return int
     */
    public function GetSlaveCount(): int
    {
        return $this->slaveCount;
    }

    /**
     * @param int $slaveCount
     */
    public function SetSlaveCount(int $slaveCount): void
    {
        $this->slaveCount = $slaveCount;
    }
}