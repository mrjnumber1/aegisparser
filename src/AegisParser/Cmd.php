<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static Cmd END()
 * @method static Cmd VAR()
 * @method static Cmd IF()
 * @method static Cmd ELSEIF()
 * @method static Cmd ELSE()
 * @method static Cmd ENDIF()
 * @method static Cmd DECLARE()
 * @method static Cmd DEFINE()
 * @method static Cmd SWITCH()
 * @method static Cmd CASE()
 * @method static Cmd BREAK()
 * @method static Cmd DEFAULT()
 * @method static Cmd ENDSWITCH()
 * @method static Cmd DEFCMD()
 * @method static Cmd BLOCKBREAK()
 * @method static Cmd WHILE()
 * @method static Cmd ENDWHILE()
 * @method static Cmd EXITWHILE()
 */
final class Cmd extends Enum
{
    private const END = 0;
    private const VAR = 1;
    private const IF = 2;
    private const ELSEIF = 3;
    private const ELSE = 4;
    private const ENDIF = 5;
    private const DECLARE = 6;
    private const DEFINE = 7;
    private const SWITCH = 8;
    private const CASE = 9;
    private const BREAK = 10;
    private const DEFAULT = 11;
    private const ENDSWITCH = 12;
    private const DEFCMD = 13;
    private const BLOCKBREAK = 14;
    private const WHILE = 15;
    private const ENDWHILE = 16;
    private const EXITWHILE = 17;

}