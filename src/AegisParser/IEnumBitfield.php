<?php

namespace AegisParser;
use MyCLabs\Enum\Enum;

interface IEnumBitfield
{
    public function __construct(string $enum_class, bool $is_zero_start=false);

    public function AddFlag(Enum $flag) : void;
    public function AddFlags(array $flags) : void;
    public function ClearFlags();
    public function GetFlags() : array;
    public function RemoveFlag(Enum $flag) : void;
    public function RemoveFlags(array $flags) : void;
    public function ToggleFlag(Enum $flag) : void;
    public function SetFlag(Enum $flag) : void;
    public function SetFlags(array $flags) : void;

    public function HasFlag(Enum $flag) : bool;

    public function FromNum(int $num) : void;
    public function GetNum() : int;

    public function Equals(IEnumBitfield $rhs) : bool;
    public function EqualsFlag(Enum $flag) : bool;
    public function Contains(IEnumBitfield $rhs) : bool;

}