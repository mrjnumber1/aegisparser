<?php


namespace AegisParser;


use MyCLabs\Enum\Enum;

/**
 * Class SkillSplash
 * @package AegisParser
 * @method static SkillSplashType NONE()
 * @method static SkillSplashType SQUARE()
 * @method static SkillSplashType LINE()
 */
final class SkillSplashType extends Enum
{
    private const NONE = 0;
    private const SQUARE = 1;
    private const LINE = 2;
}