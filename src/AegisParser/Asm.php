<?php


namespace AegisParser;


final class Asm
{
    private mixed $fp;

    public function __construct() {
        $this->fp = null;
    }
    public function __destruct() {
        $this->Release();
    }
    public function Set(string $filename) : bool {
        $this->Release();

        if (!file_exists($filename)) {
            $result = touch($filename);
            assert($result === true, 'unable to make file '.$filename);
        }

        $this->fp = fopen($filename, 'wb');

        return $this->fp !== false;
    }

    private function fileOpened() : bool {
        return (isset($this->fp) && is_resource($this->fp) && get_resource_type($this->fp) === 'stream');
    }
    public function Release() : void {
        if ($this->fileOpened()) {
            fclose($this->fp);
        }
    }

    public function Put(string $str) : void {
        assert($this->fileOpened(), "Call to CAsm::Put but it's not valid yet! str: '$str'");
        $result = fwrite($this->fp, $str);
        assert($result !== false, "fwrite() failed on {stream_get_meta_data($this->fp)['uri']}!");
    }

    public function Line(int $addr) : void {
        $line = sprintf("\r\n%08x: ", $addr);
        $this->Put($line);
    }

    public function Comment(string $str) : void {
        $this->Put("\r\n// ".$str);
    }

    public function Error(string $str) : void {
        $this->Comment("Error: ".$str);
    }

}