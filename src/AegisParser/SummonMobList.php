<?php


namespace AegisParser;


final class SummonMobList
{
    private JobType $boss;
    private int $maxSlaves = 0;
    private array $list = [];

    public function __construct(JobType $boss, int $maxSlaves, array $list) {
        $this->SetBoss($boss);
        $this->SetMaxSlaves($maxSlaves);
        $this->SetList($list);
    }

    public function GetBoss() : JobType {
        return $this->boss;
    }
    public function SetBoss(JobType $boss) : void {
        $this->boss = $boss;
    }
    /**
     * @return int
     */
    public function GetMaxSlaves(): int
    {
        return $this->maxSlaves;
    }

    /**
     * @param int $maxSlaves
     */
    public function SetMaxSlaves(int $maxSlaves): void
    {
        $this->maxSlaves = $maxSlaves;
    }

    /**
     * @return array
     */
    public function GetList(): array
    {
        return $this->list;
    }

    /**
     * @param array $list
     */
    public function SetList(array $list): void
    {
        foreach ($list as $entry) {
            assert(is_a($entry, JobType::class, false), "SummonMobList:SetList: Received non-JobType entry!");
        }
        $this->list = $list;

    }
}