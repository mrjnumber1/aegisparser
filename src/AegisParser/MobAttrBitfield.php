<?php


namespace AegisParser;


final class MobAttrBitfield
{
    public EnumBitfield $bf;

    public function __construct() {
        $this->bf = new EnumBitfield(MobAttrFlag::class);
    }
}