<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static MobAttrFlag MUST_DAMAGE_ONE_SHORT_RNG_PHYSICAL_ATK()
 * @method static MobAttrFlag MUST_DAMAGE_ONE_SPELLATK()
 * @method static MobAttrFlag MUST_DAMAGE_ONE_LONG_RNG_PHYSICAL_ATK()
 * @method static MobAttrFlag MVP()
 * @method static MobAttrFlag MUST_DAMAGE_ONE_SKILLPATT_NONE()
 * @method static MobAttrFlag KNOCKBACK_BLOCK()
 * @method static MobAttrFlag TELEPORT_BLOCK()
 * @method static MobAttrFlag ITEMDROP_BY_OPTION_BLOCK()
 * @method static MobAttrFlag REFLECT_BLOCK()
 * @method static MobAttrFlag DAMAGE_REDUCTION_10()
 * @method static MobAttrFlag DAMAGE_REDUCTION_100()
 * @method static MobAttrFlag DAMAGE_REDUCTION_1000()
 */
final class MobAttrFlag extends Enum
{
    private const MUST_DAMAGE_ONE_SHORT_RNG_PHYSICAL_ATK = 0x1;
    private const MUST_DAMAGE_ONE_SPELLATK = 0x2;
    private const MUST_DAMAGE_ONE_LONG_RNG_PHYSICAL_ATK = 0x4;
    private const MVP = 0x8;
    private const MUST_DAMAGE_ONE_SKILLPATT_NONE = 0x10;
    private const KNOCKBACK_BLOCK = 0x20;
    private const TELEPORT_BLOCK = 0x40;
    private const ITEMDROP_BY_OPTION_BLOCK = 0x80;
    private const REFLECT_BLOCK = 0x100;
    private const DAMAGE_REDUCTION_10 = 0x200;
    private const DAMAGE_REDUCTION_100 = 0x400;
    private const DAMAGE_REDUCTION_1000 = 0x800;

}