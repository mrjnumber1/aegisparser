<?php
/** @noinspection PhpUnusedPrivateFieldInspection */

namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * @method static FSMState IDLE_ST()
 * @method static FSMState RMOVE_ST()
 * @method static FSMState FOLLOW_ST()
 * @method static FSMState ANGRY_ST()
 * @method static FSMState RUSH_ST()
 * @method static FSMState FOLLOW_SEARCH_ST()
 * @method static FSMState SEARCH_ST()
 * @method static FSMState BERSERK_ST()
 * @method static FSMState FIGHT_ST()
 * @method static FSMState MOVEENEMY_ST()
 * @method static FSMState MOVEHELP_ST()
 * @method static FSMState DEAD_ST()
 * @method static FSMState MOVEITEM_ST()
 * @method static FSMState ABNORMAL_ST()
 */
final class FSMState extends Enum
{
    private const IDLE_ST = 0;
    private const RMOVE_ST = 1;
    private const FOLLOW_ST = 2;
    private const ANGRY_ST = 3;
    private const RUSH_ST = 4;
    private const FOLLOW_SEARCH_ST = 5;
    private const SEARCH_ST = 6;
    private const BERSERK_ST = 7;
    private const FIGHT_ST = 8;
    private const MOVEENEMY_ST = 9;
    private const MOVEHELP_ST = 10;
    private const DEAD_ST = 11;
    private const MOVEITEM_ST = 12;
    private const ABNORMAL_ST = 13;
}