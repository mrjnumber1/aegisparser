<?php


namespace AegisParser;


use Ds\Map;

final class ItemPackage
{
    private ItemID $id;
    private Map $boxList; // map<IPBoxType, IPBox>

    public function __construct(ItemID $id) {
        $this->boxList = new Map();
        $this->id = $id;
    }

    public function GetITID() : ItemID {
        return $this->id;
    }

    public function AddBoxItem(IPBoxType $type, IPBoxItem $item) : void {
        $box = null;
        if ($this->boxList->hasKey($type)) {
            $box = $this->boxList[$type];
        } else {
            $box = new IPBox($type);
        }
        $box->Insert($item);
        $this->boxList->put($type, $box);
    }


}