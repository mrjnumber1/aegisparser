<?php


namespace AegisParser;


use Ds\Deque;

final class Script
{
    private Deque $line;
    private ScriptHandler $handler;
    private TokenMap $tokenMap;

    public function __construct() {
        $this->Clear();
        $this->tokenMap = new TokenMap();
    }

    public function __destruct() {
        $this->Clear();
    }

    public function Clear() : void {
        if (isset($this->line)) {
            $this->line->clear();
        }
    }

    public function GetLineNum() : int {
        assert(isset($this->line), "GetLineNum before loading script.");
        return $this->line->count();
    }

    public function GetLine(int $num) : string {
        assert(isset($this->line), "GetLine($num) before loading script.");
        assert($this->line->count() > $num, "GetLine($num) past script end ({$this->line->count()}).");

        return $this->line->get($num);
    }

    // extracted to make the class actually testable
    // Returns if it managed to populate the line deck
    public function LoadText(string $text) : bool {
        $this->Clear();
        if ($text === '') {
            return false;
        }

        $this->line = $this->divideLine($text);
        return $this->line->count() > 0;
    }

    public function LoadFile(string $filename, int $version = 0) : bool {
        $fp = fopen($filename, 'rb');
        if ($fp === false) {
            return false;
        }
        $date = null;

        if ($version) {
            $date = fscanf($fp, "%d")[0];
        }
        fseek($fp, 0, SEEK_END);
        $length = ftell($fp);
        fseek($fp, 0, SEEK_SET);

        if ($version) {
            $date_length = strlen("20210312");
            fseek($fp, $date_length, SEEK_CUR);
            $length -= $date_length;
        }

        // php should handle pre-allocation of the buffer quickly enough
        $text= fread($fp, $length);

        assert($text !== false, "fread failed for `$filename`.");

        return $this->LoadText($text);
    }

    private function divideLine(string $text) : Deque {
        $lines = explode("\n", $text);

        foreach ($lines as &$line)
        {
            $line = trim($line);

            // we cheat, and remove line comments here first
            if ($line === '' || str_starts_with($line, '//')) {
                $line = '';
                continue;
            }
            if (str_contains($line, '//')) {
                $comment_pos = strpos($line, '//');
                $line = substr($line, 0, $comment_pos);
            }
        }

        return $this->removeComment($lines);
    }

    // the ZoneServer version turns commented parts into space, we strip from the string
    private function removeComment(array $lines) : Deque {

        $block_flag = false;

        foreach ($lines as &$line) {
            // first try to strip out ones all on the same line
            if (str_contains($line, "/*") && str_contains($line, "*/")
                && !str_contains($line, "/*/") && !str_contains($line, '"')) {
                $start_pos = strpos($line, "/*");
                $end_pos = strpos($line, "*/") + 2;
                $comment_len = $end_pos - $start_pos;
                $comment_text = substr($line, $start_pos, $comment_len);

                $line = str_replace($comment_text, '', $line);
            }

            // in block_flag we only need to be searching for the end comment
            if ($block_flag) {
                if (str_contains($line, "*/"))
                {
                    $block_flag = false;
                    $end_pos = strpos($line, "*/")+2;
                    // Set $line to what's after the */
                    $line = substr($line, $end_pos);
                    continue;
                }
                // didn't find the end, so clear this line out
                $line = '';
            }

            // luckily, multi-line quotes are not supported
            $quote_flag = str_contains($line, '"');

            if (str_contains($line, "/*"))
            {
                $start_pos = 0;
                if ($quote_flag) {
                    // we are now required to navigate as long as there are quotes...
                    while (($value = strpos($line, '"', $start_pos)) !== false) {
                        $start_pos = $value+1;
                    }
                }
                $comment_pos = strpos($line, "/*", $start_pos);
                if ($comment_pos !== false) {
                    $block_flag = true;
                    // now remove everything from $comment_pos to EOL
                    // this is safe because we already handled single line comments
                    $line = substr($line, 0, $comment_pos);
                }
            }

        }
        unset($line);

        $deck = new Deque();
        foreach ($lines as $line) {
            $deck->push($line);
        }

        return $deck;
    }


    public function RegisterHandler(ScriptHandler $handler) : void {
        $this->handler = $handler;
        $this->handler->SetTokenMap($this->tokenMap);
    }

    public function Analyze(int $line) : bool {
        return $this->handler->AnalyzeLine($this->line[$line]);
    }


}