<?php


namespace AegisParser;


final class Bin
{
    private mixed $fp;
    public function __construct() {
        $this->fp = null;
    }

    public function __destruct() {
        $this->Release();
    }

    private function fileOpened() : bool {
        return (isset($this->fp) && is_resource($this->fp) && get_resource_type($this->fp) === 'stream');
    }

    public function Release() : void {
        if ($this->fileOpened()) {
            fclose($this->fp);
        }
    }

    public function Set(string $filename) : bool {
        $this->Release();

        if (!file_exists($filename)) {
            $result = touch($filename);
            assert($result, 'unable to make file '.$filename);
        }

        $this->fp = fopen($filename, 'wb');

        return $this->fp !== false;
    }

    public function Seek(int $pos) : void {
        assert($this->fileOpened(), "Trying to seek to $pos on an unopened bin!");

        $result = fseek($this->fp, $pos, SEEK_SET);
        assert($result === 0, "fseek() failed on {stream_get_meta_data($this->fp)['uri']}");
    }

    public function GetPos() : int {
        assert($this->fileOpened(), "Trying to GetPos on an unopened bin!");

        $position = ftell($this->fp);
        assert($position !== false, "ftell failed for {stream_get_meta_data($this->fp)['uri']}");

        return $position;
    }

    public function WriteNum(int $num, int $size = 4) : void {
        assert($this->fileOpened(), "Trying to WriteNum on an unopened bin!");
        assert($size !== 3, "Sorry theres no 3 byte value for now");
        assert($size <= 4, "Sorry only 4 bytes at a time please");

        $pack_type = [1=>'c', 2=>'s', 4=>'l'];

        $result = fwrite($this->fp, pack($pack_type[$size], $num), $size);

        assert($result === $size, "WriteNum didn't write $size bytes!");
    }

    public function WriteByte(int $num) : void {
        $this->WriteNum($num, 1);
    }

    public function WriteWord(int $num) : void {
        $this->WriteNum($num, 2);
    }

    public function WriteString(string $str) : void {
        assert($this->fileOpened(), "Trying to WriteStr on an unopened bin!");

        $length = strlen($str)+1;
        $this->WriteWord($length);
        $written = fwrite($this->fp, $str."\x00", $length);

        assert ($written === $length, "WriteStr Not enough bytes written!");
    }
}