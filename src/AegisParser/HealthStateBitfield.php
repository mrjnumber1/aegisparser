<?php

namespace AegisParser;

use bitfield\Bitfield;

final class HealthStateBitfield
{
    private Bitfield $field;

    public function __construct() {
        $options = HealthStateFlag::keys();
        array_shift($options);
        $this->field = new Bitfield($options);
    }

    public function ToggleFlag(HealthStateFlag $f) : void {
        /** @noinspection TypeUnsafeComparisonInspection */
        /** @noinspection PhpNonStrictObjectEqualityInspection */
        assert($f != HealthStateFlag::NORMAL(), "HealthStateBitfield attempted to toggle normal flag!");
        $str = $f->getKey();
        if ($this->field->isOn($str)) {
            $this->field->off($str);
        } else {
            $this->field->on($str);
        }
    }

    public function ClearFlags() : void { $this->field->setValue(0); }

    public function SetFlags(array $flags) : void {
        $this->ClearFlags();

        foreach ($flags as $flag) {
            assert(is_a($flag, HealthStateFlag::class, false), "HealthStateBitfield::SetFlags received non-flag type!");
            $this->AddFlag($flag);
        }
    }

    public function GetFlags() : array {
        if ($this->field->getValue() === 0) {
            return [];
        }

        $fields = HealthStateFlag::keys();
        array_shift($fields);
        $flags = [];
        foreach ($fields as $i => $k) {
            if ($this->field->isOn($k)) {
                $flags[] = HealthStateFlag::from(1<<$i);
            }
        }
        return $flags;
    }

    public function AddFlag(HealthStateFlag $f) : void {
        /** @noinspection TypeUnsafeComparisonInspection */
        /** @noinspection PhpNonStrictObjectEqualityInspection */
        assert($f != HealthStateFlag::NORMAL(), "AddFlag for NORMAL!");
        $this->field->on($f->getKey());
    }

    public function RemoveFlag(HealthStateFlag $f) : void {
        /** @noinspection TypeUnsafeComparisonInspection */
        /** @noinspection PhpNonStrictObjectEqualityInspection */
        assert($f != HealthStateFlag::NORMAL(), "Attempted to remove normal HealthStateFlag!");
        assert($this->field->isOn($f->getKey()), "Attempted to remove a non-set HealthStateFlag!");
        $this->field->off($f->getKey());
    }

    public function HasFlag(HealthStateFlag $f) : bool {
        /** @noinspection TypeUnsafeComparisonInspection */
        /** @noinspection PhpNonStrictObjectEqualityInspection */
        if ($f == HealthStateFlag::NORMAL()) {
            return $this->field->getValue() === 0;
        }
        return $this->field->isOn($f->getKey());
    }

    public function GetNum() : int {
        return $this->field->getValue();
    }
    public function FromNum(int $num) : void {
        $this->ClearFlags();
        if ($num === 0) {
            return;
        }

        $keys = HealthStateFlag::keys();
        array_shift($keys);

        assert($num < (1<<count($keys)), "HealthStateBitfield::FromNum num `$num` received was too high!");

        foreach ($keys as $i => $name) {
            if (((1<<$i) & $num) !== 0) {
                $this->field->on($name);
            }
        }

        assert($this->field->getValue() === $num, "HealthStateBitfield::FromNum mismatch! requested `$num` got `{$this->field->getValue()}`");
    }

}