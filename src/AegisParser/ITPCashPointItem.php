<?php


namespace AegisParser;


final class ITPCashPointItem extends ITP
{
    protected int $changeItemID;
    protected int $count;
    public function __construct() { parent::__construct(); $this->type = ItemType::CASHPOINTITEM(); }


    /**
     * @return int
     */
    public function GetChangeItemID(): int
    {
        return $this->changeItemID;
    }

    /**
     * @param int $changeItemID
     */
    public function SetChangeItemID(int $changeItemID): void
    {
        $this->changeItemID = $changeItemID;
    }

    /**
     * @return int
     */
    public function GetCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function SetCount(int $count): void
    {
        $this->count = $count;
    }

}