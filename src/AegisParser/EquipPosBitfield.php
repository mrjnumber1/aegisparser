<?php


namespace AegisParser;


use bitfield\Bitfield;

final class EquipPosBitfield
{
    private Bitfield $field;

    public function __construct(array $flags = []) {

        $options = EquipPosFlag::keys();
        $this->field = new Bitfield($options);
        if (!empty($flags)) {
            $this->SetFlags($flags);
        }
    }

    public function IsHeadPos() : bool {
        return ($this->HasFlag(EquipPosFlag::HEAD())
            || $this->HasFlag(EquipPosFlag::HEAD2())
            || $this->HasFlag(EquipPosFlag::HEAD3())
        );
    }

    public function IsSingleHeadPos() : bool {
        return ($this->IsHeadPos() && count($this->GetFlags()) === 1);
    }

    public function IsCostumeHeadPos() : bool {
        return ($this->HasFlag(EquipPosFlag::COSTUME_HEAD())
            || $this->HasFlag(EquipPosFlag::COSTUME_HEAD2())
            || $this->HasFlag(EquipPosFlag::COSTUME_HEAD3())
        );
    }

    public function IsCostumePos() : bool {
        return ($this->IsCostumeHeadPos()
            || $this->HasFlag(EquipPosFlag::COSTUME_ROBE())
            || $this->HasFlag(EquipPosFlag::COSTUME_FLOOR())
        );
    }

    public function IsShadowPos() : bool {
        return ($this->HasFlag(EquipPosFlag::ARMOR_SHADOW())
            || $this->HasFlag(EquipPosFlag::WEAPON_SHADOW())
            || $this->HasFlag(EquipPosFlag::SHIELD_SHADOW())
            || $this->HasFlag(EquipPosFlag::SHOES_SHADOW())
            || $this->HasFlag(EquipPosFlag::R_ACCESSORY_SHADOW()) || $this->HasFlag(EquipPosFlag::L_ACCESSORY_SHADOW())
        );
    }

    public function IsTwoHandedPos() : bool {
        return ($this->IsWeaponPos() && $this->HasFlag(EquipPosFlag::LARM()));
    }
    public function IsWeaponPos() : bool {
        return $this->HasFlag(EquipPosFlag::RARM());
    }

    public function IsSingleAccessoryPos() : bool {
        return ($this->IsAccessoryPos() && count($this->GetFlags()) === 1);
    }

    public function IsAccessoryPos() : bool {
        return ($this->HasFlag(EquipPosFlag::ACCESSORY1()) && $this->HasFlag(EquipPosFlag::ACCESSORY2()));
    }
    public function IsArmorPos() : bool {
        return !$this->IsWeaponPos();
    }


    public function AddFlag(EquipPosFlag $f) : void {
        $this->field->on($f->getKey());
    }

    public function RemoveFlag(EquipPosFlag $f) : void {
        $this->field->off($f->getKey());
    }

    public function ClearFlags() : void {
        $this->field->setValue(0);
    }

    public function ToggleFlag(EquipPosFlag $f) : void {
        if ($this->HasFlag($f)) {
            $this->RemoveFlag($f);
        } else {
            $this->AddFlag($f);
        }
    }

    public function SetFlag(EquipPosFlag $f) : void {
        $this->ClearFlags();
        $this->AddFlag($f);
    }

    public function SetFlags(array $list) : void {
        $this->ClearFlags();

        foreach ($list as $flag) {
            assert(is_a($flag, EquipPosFlag::class, false), "EquipPosBitfield::SetFlags non flag passed $flag");
            $this->AddFlag($flag);
        }
    }

    public function GetFlags() : array {
        if ($this->field->getValue() === 0) {
            return [];
        }

        $fields = EquipPosFlag::keys();
        $flags = [];
        foreach ($fields as $i => $k) {
            if ($this->field->isOn($k)) {
                $flags[] = EquipPosFlag::from(1<<$i);
            }
        }

        return $flags;
    }

    public function HasFlag(EquipPosFlag $f) : bool {
        return $this->field->isOn($f->getKey());
    }

    public function EqualsFlag(EquipPosFlag $f) : bool { return $this->GetPosNum() === $f->getValue(); }
    public function EqualsFlags(array $data) : bool { return $this->Equals(new EquipPosBitfield($data)); }
    public function Equals(EquipPosBitfield $rhs) : bool { return $this->GetPosNum() === $rhs->GetPosNum(); }
    public function Contains(EquipPosBitfield $rhs) : bool { return ($this->GetPosNum() & $rhs->GetPosNum()) !== 0; }

    public function FromNum(int $num) : void {
        $this->ClearFlags();
        if ($num === 0) {
            return;
        }

        $keys = EquipPosFlag::keys();
        assert($num < (1<<count($keys)), "EquipPosBitfield::FromNum num `$num` received was too high!");

        foreach ($keys as $i => $name) {
            if (((1<<$i) & $num) !== 0) {
                $this->field->on($name);
            }
        }

        assert($this->GetPosNum() === $num, "EquipPosBitfield::FromNum mismatch! requested `$num` got `{$this->field->getValue()}`");
    }

    public function GetPosNum() : int {
        //$v = ;
        // this turns out to be totally valid for a lot of items.. annoying!
        //assert($v !== 0, "GetPosNum used for empty EquipPosBitfield!");
        return $this->field->getValue();
    }


}