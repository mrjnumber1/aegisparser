<?php


namespace AegisParser;

use MyCLabs\Enum\Enum;

/**
 * Class ScriptOp
 * @package AegisParser
 * @method static ScriptOp END()
 * @method static ScriptOp EQUAL()
 * @method static ScriptOp NOTEQUAL()
 * @method static ScriptOp LARGE()
 * @method static ScriptOp SMALL()
 * @method static ScriptOp AND()
 * @method static ScriptOp OR()
 * @method static ScriptOp ADD()
 * @method static ScriptOp SUB()
 * @method static ScriptOp MUL()
 * @method static ScriptOp DIV()
 * @method static ScriptOp MOD()
 * @method static ScriptOp LARGE_OR_EQUAL()
 * @method static ScriptOp SMALL_OR_EQUAL()
 */

final class ScriptOp extends Enum
{
    private const END = 0;
    private const EQUAL = 1;
    private const NOTEQUAL = 2;
    private const LARGE = 3;
    private const SMALL = 4;
    private const AND = 5;
    private const OR = 6;
    private const ADD = 7;
    private const SUB = 8;
    private const MUL = 9;
    private const DIV = 10;
    private const MOD = 11;
    private const LARGE_OR_EQUAL = 12;
    private const SMALL_OR_EQUAL = 13;
}