<?php


namespace AegisParser;


class ITPArmor extends ITPEquippable
{
    protected int $defense;
    protected int $magicDefense;
    protected int $maxHp;
    protected int $maxSp;
    protected ArmorView $class;

    public function __construct() { parent::__construct(); $this->type = ItemType::ARMOR(); }

    /**
     * @return int
     */
    public function GetDefense(): int
    {
        return $this->defense;
    }

    /**
     * @param int $defense
     */
    public function SetDefense(int $defense): void
    {
        $this->defense = $defense;
    }

    /**
     * @return int
     */
    public function GetMagicDefense(): int
    {
        return $this->magicDefense;
    }

    /**
     * @param int $magicDefense
     */
    public function SetMagicDefense(int $magicDefense): void
    {
        $this->magicDefense = $magicDefense;
    }

    /**
     * @return int
     */
    public function GetMaxHp(): int
    {
        return $this->maxHp;
    }

    /**
     * @param int $maxHp
     */
    public function SetMaxHp(int $maxHp): void
    {
        $this->maxHp = $maxHp;
    }

    /**
     * @return int
     */
    public function GetMaxSp(): int
    {
        return $this->maxSp;
    }

    /**
     * @param int $maxSp
     */
    public function SetMaxSp(int $maxSp): void
    {
        $this->maxSp = $maxSp;
    }

    /**
     * @return ArmorView
     */
    public function GetClass(): ArmorView
    {
        return $this->class;
    }

    /**
     * @param ArmorView $class
     */
    public function SetClass(ArmorView $class): void
    {
        $this->class = $class;
    }


}