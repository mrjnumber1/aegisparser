<?php


namespace AegisParser;


final class TokenMap
{
    private array $tokens;

    public function Set(string $key, TokenType $type, int $num, string $str) : void {
        $this->tokens[$key] = new TokenInfo($type, $num, $str);
    }

    public function GetToken(string $key) : ?TokenInfo {
        return $this->tokens[$key] ?? null;
    }

}