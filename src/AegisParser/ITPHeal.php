<?php


namespace AegisParser;


final class ITPHeal extends ITP
{
    protected int $minHp;
    protected int $maxHp;
    protected int $minSp;
    protected int $maxSp;
    protected HealthStateBitfield $status;
    public function __construct() {
        parent::__construct();
        $this->SetType(ItemType::HEAL());
        $this->status = new HealthStateBitfield();
    }


    /**
     * @return int
     */
    public function GetMinHp(): int
    {
        return $this->minHp;
    }

    /**
     * @param int $minHp
     */
    public function SetMinHp(int $minHp): void
    {
        $this->minHp = $minHp;
    }

    /**
     * @return int
     */
    public function GetMaxHp(): int
    {
        return $this->maxHp;
    }

    /**
     * @param int $maxHp
     */
    public function SetMaxHp(int $maxHp): void
    {
        $this->maxHp = $maxHp;
    }

    /**
     * @return int
     */
    public function GetMinSp(): int
    {
        return $this->minSp;
    }

    /**
     * @param int $minSp
     */
    public function SetMinSp(int $minSp): void
    {
        $this->minSp = $minSp;
    }

    /**
     * @return int
     */
    public function GetMaxSp(): int
    {
        return $this->maxSp;
    }

    /**
     * @param int $maxSp
     */
    public function SetMaxSp(int $maxSp): void
    {
        $this->maxSp = $maxSp;
    }

    /**
     * @return HealthStateBitfield
     */
    public function GetStatus(): HealthStateBitfield
    {
        return $this->status;
    }

    /**
     * @param HealthStateBitfield $status
     */
    public function SetStatus(HealthStateBitfield $status): void
    {
        $this->status = $status;
    }

    public function GetStatusNum() : int { return $this->status->GetNum(); }
    public function SetStatusNum(int $num) : void { $this->status->FromNum($num); }

}