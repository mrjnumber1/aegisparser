<?php

declare(strict_types=1);


error_reporting(E_ALL);

ini_set("display_errors", "1");
ini_set("data.timezone", "America/New_York");
ini_set("zend.assertions", "1");
ini_set("assert.exception", "1");

if (version_compare('8.0', PHP_VERSION, '>')) {
    fwrite(STDERR,"Requires PHP >= 8 dude. Running: {PHP_VERSION} {PHP_BINARY}");
    die(1);
}

define("START_TIME", microtime(true));
define("START_MEM", memory_get_usage());

define("AUTOLOADER", __DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");


require AUTOLOADER;

use Application\AegisParser;
use Application\FileLogger;
use Application\Cli;
use Application\Log;

Cli::_init();
Log::_init(new FileLogger());

AegisParser::Main();