<?php
namespace Application;

use DateTime;
use Exception;
use RuntimeException;

interface Logger
{
	public function emergency($message, array $context=null) : void;
	public function alert($message, array $context=null) : void;
    public function critical($message, array $context=null) : void;
	public function error($message, array $context=null) : void;
	public function warning($message, array $context=null) : void;
	public function info($message, array $context=null) : void;
    public function notice($message, array $context=null) : void;
	public function debug($message, array $context=null) : void;
	public function log($level, $message, array $context=null) : void;
}

class LogLevel
{
	public const EMERGENCY = 'emergency';
	public const ALERT =     'alert';
	public const CRITICAL =  'critical error';
	public const ERROR	=     'error';
	public const WARNING =   'warning';
	public const NOTICE =    'notice';
	public const INFO =      'info';
	public const DEBUG =     'DEBUG';
}

abstract class AbstractLogger implements Logger
{
	public function emergency($message, array $context=null) : void
	{
		$this->log(LogLevel::EMERGENCY, $message, $context);
	}
	public function alert($message, array $context=null) : void
	{
		$this->log(LogLevel::ALERT, $message, $context);
	}
	public function critical($message, array $context=null) : void
	{
		$this->log(LogLevel::CRITICAL, $message, $context);
	}
	public function error($message, array $context=null) : void
	{
		$this->log(LogLevel::ERROR, $message, $context);
	}
	public function warning($message, array $context=null) : void
	{
		$this->log(LogLevel::WARNING, $message, $context);
	}
	public function info($message, array $context=null) : void
	{
		$this->log(LogLevel::INFO, $message, $context);
	}
	public function debug($message, array $context=null) : void
	{
		$this->log(LogLevel::DEBUG, $message, $context);
	}
    public function notice($message, array $context=null) : void
    {
        $this->log(LogLevel::NOTICE, $message, $context);
    }
}

class Log
{
	private static Logger $logger;
	
	public static function _init(Logger $logger) : void
	{
		static::$logger = $logger;
	}
	
	public static function emergency($message, array $context=null) : void
	{
		Cli::write('EMERGENCY: '.$message);
		Cli::error('EMERGENCY: '.$message);
		static::$logger->emergency($message, $context);
	}
	public static function alert($message, array $context=null) : void
	{
		Cli::write('ALERT: '.$message);
		static::$logger->alert($message, $context);
	}
	public static function critical($message, array $context=null) : void
	{
		Cli::write('CRITICAL ERROR: '.$message);
		Cli::error('CRITICAL ERROR: '.$message);
		static::$logger->critical($message, $context);
	}
	public static function error($message, array $context=null) : void
	{
		Cli::write('ERROR: '.$message);
		Cli::error('ERROR: '.$message);
		static::$logger->error($message, $context);
	}
	public static function warning($message, array $context=null) : void
	{
		Cli::write('warning: '.$message);
		static::$logger->warning($message, $context);
	}
	public static function info($message, array $context=null) : void
	{
		Cli::write($message);
		static::$logger->info($message, $context);
	}
	public static function debug($message, array $context=null) : void
	{
		Cli::write('DEBUG: '.$message);
		static::$logger->debug($message, $context);
	}
}

class FileLogger extends AbstractLogger
{
	private const LOGDIR = 'logs';
	private const PATH = self::LOGDIR.DIRECTORY_SEPARATOR;
	private string|null $filename;
	private string|null $full_filename;
	private $fp;
	
	public function __construct(string $filename = null)
	{	
		if (strpbrk($filename, "\"\\?*:/@|<>") !== false)
		{
			echo 'unable to use logfile `'.$filename.'`, using default';
			$filename = null;
		}
		
		if (!file_exists(self::LOGDIR) && !mkdir($concurrentDirectory = self::LOGDIR) && !is_dir($concurrentDirectory)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

		$full_filename = $filename;
		if (is_null($filename))
		{
			$time = new DateTime();
			$filename = $time->format('YmdHis').'.log';
			$full_filename = $this::PATH . $filename;
		}
		
		if (($this->fp = fopen($full_filename, 'ab')) === false)
		{
			echo 'failed to open logfile `'.$full_filename.'`, abort!!!';
			die();
		}
		
		$this->filename = $filename;
		$this->full_filename = $full_filename;
	}
	
	public function __destruct()
	{
		$this->filename = null;
		$this->full_filename = null;
		fclose($this->fp);
	}
	
	public function log($level, $message, array $context=null) : void
	{
		$time = new DateTime();
		$time_str = $time->format('[His.u]');
		$msg = $time_str.'[';
		$exception_msg = null;
		$e = null;
		
		if (!is_null($context) && array_key_exists('exception', $context))
		{
			$e = $context['exception'];
			
			if ($e instanceof Exception)
			{
				$exception_msg = get_class($e).'(code '.$e->getCode().') at '.$e->getFile().':'.$e->getLine().' uncaught!! msg: '
					.$e->getMessage()
					.PHP_EOL.'stack: '.PHP_EOL
					.$e->getTraceAsString();
			}
			else
			{
				$exception_msg = 'Unknown exception `'.$e.'`';
			}
		}
				
		switch ($level)
		{
			default:
				$this->notice('Unknown log type `'.$level.'`, converting to info!', $context);
				$this->info($message, $context);
				break;
			case LogLevel::INFO:
			case LogLevel::NOTICE:
			case LogLevel::DEBUG:
			case LogLevel::ALERT:
			case LogLevel::ERROR:
			case LogLevel::WARNING:
			case LogLevel::CRITICAL:
			case LogLevel::EMERGENCY:
				$msg .= $level."] ".$message;
				break;
		}
		
		fwrite($this->fp, $msg.PHP_EOL);
		if (!is_null($e)) {
            fwrite($this->fp, $exception_msg . PHP_EOL);
        }
		
		switch ($level)
		{
			case LogLevel::CRITICAL:
			case LogLevel::EMERGENCY:
			die();
		}
	}
}

class NullLogger extends AbstractLogger
{
	public function log($level, $message, array $context=null) : void
	{
		switch($level)
		{
			case LogLevel::CRITICAL:
			case LogLevel::EMERGENCY:
				echo 'a '.$level.' emergency was encountered. why the fuck are error msges off though? aborting script anyways';
				die;
		}
		// do nothing. that's the point, after all!
	}
}