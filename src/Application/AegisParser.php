<?php
namespace Application;

use AegisParser\ITPMgr;
use AegisParser\MonParameterMgr;
use AegisParser\ScriptCompiler;
use AegisParser\SkillTypeInfoMgr;
use RuntimeException;
use Throwable;

class AegisParser {

    private bool $running = true;
	
	private string $server_dir;
	private string $cache_dir;
	private string $client_dir;
	private string $db_dir;
	private string $zone_dir;
	
	private array $script;
	private ITPMgr $itp_mgr;
    private MonParameterMgr $mp_mgr;
    private SkillTypeInfoMgr $stp_mgr;

	public static function Main() : int {
		try {
			return (new static)->run($_SERVER['argv']);
		} catch (Throwable $t) {
			//Log::critical($t->getMessage());
			throw new RuntimeException($t->getMessage(), (int)$t->getCode(), $t);
		}
	}

	private function run(array $args) : int {
	    $path = getopt("", ["server:"])["server"];

	    if (!$this->init($path)) {
	        return 1;
        }

	    while ($this->running) {
            $this->onLoop();
        }

		return $this->exit();
	}

	private function init(string $server_path) : bool {
        $this->initPaths($server_path);

        $this->initZoneFiles();

        return true;
    }


    private function onLoop() : void {
	    // todo: this!
        $this->running = false;
    }

	private function exit() : int {
	    $filename = $this->cache_dir."itp_mgr.cache";
        if (!file_exists($filename)) {
            Log::info("Saving itp mgr to cache at $filename..");
            file_put_contents($filename, serialize($this->itp_mgr));
        }
        $filename = $this->cache_dir."mp_mgr.cache";
        if (!file_exists($filename)) {
            Log::info("Saving mp mgr to cache at $filename.");
            file_put_contents($filename, serialize($this->mp_mgr));
        }
        $filename = $this->cache_dir."stp_mgr.cache";
        if (!file_exists($filename)) {
            Log::info("Saving stp mgr to cache at $filename.");
            file_put_contents($filename, serialize($this->stp_mgr));
        }

	    $time = $this->formatTime((microtime(true) - START_TIME));
        $mem = $this->formatMemory(memory_get_usage());
        $peak = $this->formatMemory(memory_get_peak_usage());

        Log::info("AegisParser complete! Runtime: $time, Mem:$mem, Peak Mem:$peak.");

        return 0;
    }

	private function formatTime(float $time) : string {
	    $num = (int)$time;
	    $divisor = [60, 60];
	    $suffix = ["s", "m"];
	    $i=0;
	    while (($num / $divisor[$i]) >= 1) {
	        $num /= $divisor[$i];
	        $i++;
        }
        return $num." ".$suffix[$i];
    }

	private function formatMemory(int $memory) : string {
	    $num = $memory;
	    $i=0;
	    $suffix = ['B', 'KB', 'MB', 'GB'];
	    while (($num / 1024) >= 1 && $i < 3) {
	        $num /= 1024;
	        ++$i;
        }
        return sprintf("%.2f $suffix[$i]", $num);
    }

    private function initPaths(string $server_path) : void {

        $this->server_dir = 'data' . DIRECTORY_SEPARATOR . 'servers' . DIRECTORY_SEPARATOR . $server_path . DIRECTORY_SEPARATOR;

        $this->zone_dir = $this->server_dir . "ZoneServer" . DIRECTORY_SEPARATOR;
		$this->db_dir = $this->server_dir . "DB" . DIRECTORY_SEPARATOR;
        $this->client_dir = $this->server_dir . "Client" . DIRECTORY_SEPARATOR;
        $this->cache_dir = $this->server_dir."cache".DIRECTORY_SEPARATOR;

        $dirs = ["base" => $this->server_dir, "DB" => $this->db_dir, "ZoneServer" => $this->zone_dir, "Client" => $this->client_dir];
        foreach ($dirs as $type => $path) {
            assert(file_exists($path), "AegisParser::Run: Declared $type directory `$path` not found!");
            assert(is_dir($path), "AegisParser::Run: Declared $type directory `$path` was not a directory!");
            assert(count(scandir($path)) > 2, "AegisParser::Run: Declared $type `$path` is empty!");
        }

        if (!file_exists($this->cache_dir) && !mkdir($concurrentDirectory = $this->cache_dir) && !is_dir($concurrentDirectory)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        $script_path = $this->db_dir."script.dat";
        $result = unserialize(file_get_contents($script_path), ['allowed_classes' => false]);
        assert(is_array($result), "Failed to unserialize $script_path!");

        $this->script = $result;
    }

    private function initZoneFiles() : void {

        $filename = $this->cache_dir."itp_mgr.cache";
        if (file_exists($filename) && (time() - filemtime($filename)) < 6000000 ) {
            Log::info("Loading ITP mgr from cache.");
            $this->itp_mgr = unserialize(file_get_contents($filename), ["allowed_classes"=>true]);
        } else {
            $this->itp_mgr = new ITPMgr($this->zone_dir, $this->script);
        }
        Log::info("ITPMgr init complete..");

        $filename = $this->cache_dir."mp_mgr.cache";
        if (file_exists($filename) && (time() - filemtime($filename)) < 6000000 ) {
            Log::info("Loading MP mgr from cache.");
            $this->mp_mgr = unserialize(file_get_contents($filename), ["allowed_classes"=>true]);
        } else {
            $this->mp_mgr = new MonParameterMgr($this->zone_dir, $this->script);
        }
        Log::info("MonParameterMgr init complete...");

        $this->stp_mgr = new SkillTypeInfoMgr($this->zone_dir);
        $this->scriptCompile();
        Log::info("Script Compile complete...");
    }

    private function scriptCompile() : void {

        $path = $this->zone_dir."scriptdata".DIRECTORY_SEPARATOR;
        $defs = [
            'itp_mgr' => 'itp.def',
            'mp_mgr' => 'npcspr.def',
            'stp_mgr' => 'skill.def'
        ];

        foreach ($defs as $type => $filename) {
            $filename = $path.$filename;
            if (file_exists($filename) && time() - filemtime($filename) < 60000) {
                continue;
            }
            Log::info("ScriptCompile: Rebuilding $filename.");
            $this->$type->Save($filename);
        }

        $compiler = new ScriptCompiler($this->zone_dir);
        $ok = $compiler->Init();
        assert($ok, "ScriptCompile failed!");
    }

}