<?php

// this is a clone of php fuel's cli class

namespace Application;

use BadFunctionCallException;
use Exception;
use OutOfBoundsException;

/**
 * Cli class
 *
 * Interact with the command line by accepting input options, parameters and output text
 *
 * @package		Fuel
 * @category	Core
 * @author		Phil Sturgeon
 * @link		http://docs.fuelphp.com/classes/cli.html
 */
class Cli
{
    public static string $wait_msg = 'Press any key to continue...';
    public static bool $no_color = false;
    protected static array $args = [];

    protected static array $foreground_colors = [
        'black'			=> '0;30',
        'dark_gray'		=> '1;30',
        'blue'			=> '0;34',
        'dark_blue'		=> '1;34',
        'light_blue'	=> '1;34',
        'green'			=> '0;32',
        'light_green'	=> '1;32',
        'cyan'			=> '0;36',
        'light_cyan'	=> '1;36',
        'red'			=> '0;31',
        'light_red'		=> '1;31',
        'purple'		=> '0;35',
        'light_purple'	=> '1;35',
        'light_yellow'	=> '0;33',
        'yellow'		=> '1;33',
        'light_gray'	=> '0;37',
        'white'			=> '1;37',
    ];

    protected static array $background_colors = [
        'black'			=> '40',
        'red'			=> '41',
        'green'			=> '42',
        'yellow'		=> '43',
        'blue'			=> '44',
        'magenta'		=> '45',
        'cyan'			=> '46',
        'light_gray'	=> '47',
    ];

    protected static mixed $stdout;
    protected static mixed $stderr;

    /**
     * Static constructor.    Parses all the CLI params.
     * @throws Exception
     */
    public static function _init() : void
    {
        if (!defined("STDIN"))
        {
            throw new BadFunctionCallException('Cli class cannot be used outside of the command line.');
        }
        for ($i = 1; $i < $_SERVER['argc']; $i++)
        {
            $arg = explode('=', $_SERVER['argv'][$i]);

            static::$args[$i] = $arg[0];

            if (count($arg) > 1 || strncmp($arg[0], '-', 1) === 0)
            {
                static::$args[ltrim($arg[0], '-')] = ($arg[1] ?? true);
            }
        }


        static::$stderr = STDERR;
        static::$stdout = STDOUT;
    }

    /**
     * Returns the option with the given name.	You can also give the option
     * number.
     *
     * Named options must be in the following formats:
     * php index.php user -v --v -name=John --name=John
     *
     * @param   string|int  $name     the name of the option (int if unnamed)
     * @param   mixed       $default  value to return if the option is not defined
     * @return  mixed
     */
    public static function option(string|int $name, mixed $default = null) : mixed
    {
        return static::$args[$name] ?? $default;
    }

    /**
     * Allows you to set a commandline option from code
     *
     * @param   string|int  $name   the name of the option (int if unnamed)
     * @param   mixed|null  $value  value to set, or null to delete the option
     * @return  void
     */
    public static function set_option(string|int $name, mixed $value = null) : void
    {
        if ($value === null)
        {
            if (isset(static::$args[$name]))
            {
                unset(static::$args[$name]);
            }
        }
        else
        {
            static::$args[$name] = $value;
        }
    }

    /**
     * Get input from the shell, using readline or the standard STDIN
     *
     * Named options must be in the following formats:
     * php index.php user -v --v -name=John --name=John
     *
     * @param	string|int	$prefix	the name of the option (int if unnamed)
     * @return	string
     */
    public static function input(string|int $prefix = '') : string
    {
        echo $prefix;
        return fgets(STDIN);
    }

    /**
     * Asks the user for input.  This can have either 1 or 2 arguments.
     *
     * Usage:
     *
     * // Waits for any key press
     * CLI::prompt();
     *
     * // Takes any input
     * $color = CLI::prompt('What is your favorite color?');
     *
     * // Takes any input, but offers default
     * $color = CLI::prompt('What is your favourite color?', 'white');
     *
     * // Will only accept the options in the array
     * $ready = CLI::prompt('Are you ready?', array('y','n'));
     *
     * @return	string	the user input
     */
    public static function prompt() : string
    {
        $args = func_get_args();

        $options = [];
        $output = '';
        $default = null;

        // How many we got
        $arg_count = count($args);

        // Is the last argument a boolean? True means required
        $required = end($args) === true;

        // Reduce the argument count if required was passed, we don't care about that anymore
        $required === true && --$arg_count;

        // This method can take a few crazy combinations of arguments, so lets work it out
        switch ($arg_count)
        {
            case 2:

                // E.g: $ready = CLI::prompt('Are you ready?', array('y','n'));
                if (is_array($args[1]))
                {
                    [$output, $options] = $args;
                }

                // E.g: $color = CLI::prompt('What is your favourite color?', 'white');
                elseif (is_string($args[1]))
                {
                    [$output, $default] = $args;
                }

                break;

            case 1:

                // No question (probably been asked already) so just show options
                // E.g: $ready = CLI::prompt(array('y','n'));
                if (is_array($args[0]))
                {
                    $options = $args[0];
                }

                // Question without options
                // E.g: $ready = CLI::prompt('What did you do today?');
                elseif (is_string($args[0]))
                {
                    $output = $args[0];
                }

                break;
        }

        // If a question has been asked with the read
        if ($output !== '')
        {
            $extra_output = '';

            if ($default !== null)
            {
                $extra_output = ' [ Default: "'.$default.'" ]';
            }

            elseif ($options !== array())
            {
                $extra_output = ' [ '.implode(', ', $options).' ]';
            }

            fwrite(static::$stdout, $output.$extra_output.': ');
        }

        // Read the input from keyboard.
        $input = trim(static::input()) ?: $default;

        // No input provided and we require one (default will stop this being called)
        if (empty($input) && $required)
        {
            static::write('This is required.');
            static::new_line();

            $input = forward_static_call_array(array(__CLASS__, 'prompt'), $args);
        }

        // If options are provided and the choice is not in the array, tell them to try again
        if ( !empty($options) && !in_array($input, $options, true))
        {
            static::write('This is not a valid option. Please try again.');
            static::new_line();

            $input = forward_static_call_array(array(__CLASS__, 'prompt'), $args);
        }

        return $input;
    }

    /**
     * Outputs a string to the cli.     If you send an array it will implode them
     * with a line break.
     *
     * @param string|array $text the text to output, or array of lines
     * @param string|null $foreground the foreground color
     * @param string|null $background the foreground color
     */
    public static function write(string|array $text = '', string $foreground = null, string $background = null) : void
    {
        if (is_array($text))
        {
            $text = implode(PHP_EOL, $text);
        }

        if ($foreground || $background)
        {
            $text = static::color($text, $foreground, $background);
        }

        fwrite(static::$stdout, $text.PHP_EOL);
    }

    /**
     * Outputs an error to the CLI using STDERR instead of STDOUT
     *
     * @param string|array	$text		the text to output, or array of errors
     * @param string	$foreground	the foreground color
     * @param string|null		$background	the foreground color
     */
    public static function error(string|array $text = '', string $foreground = 'light_red', string $background = null) : void
    {
        if (is_array($text))
        {
            $text = implode(PHP_EOL, $text);
        }

        if ($foreground || $background)
        {
            $text = static::color($text, $foreground, $background);
        }

        fwrite(static::$stderr, $text.PHP_EOL);
    }

    /**
     * Beeps a certain number of times.
     *
     * @param	int $num	the number of times to beep
     */
    public static function beep(int $num = 1) : void
    {
        echo str_repeat("\x07", $num);
    }

    /**
     * Waits a certain number of seconds, optionally showing a wait message and
     * waiting for a key press.
     *
     * @param	int		$seconds	number of seconds
     * @param	bool	$countdown	show a countdown or not
     */
    public static function wait(int $seconds = 0, bool $countdown = false) : void
    {
        if ($countdown)
        {
            $time = $seconds;

            while ($time > 0)
            {
                fwrite(static::$stdout, $time.'... ');
                sleep(1);
                $time--;
            }
            static::write();
        }

        else
        {
            if ($seconds)
            {
                sleep($seconds);
            }
            else
            {
                static::write(static::$wait_msg);
                static::input();
            }
        }
    }

    /**
     * if operating system === windows
     */
    public static function is_windows() : bool
    {
        return DIRECTORY_SEPARATOR === "\\";
    }

    /**
     * Enter a number of empty lines
     *
     * @param	integer	Number of lines to output
     * @return	void
     */
    public static function new_line(int $num = 1) : void
    {
        // Do it once or more, write with empty string gives us a new line
        for($i = 0; $i < $num; $i++)
        {
            static::write();
        }
    }

    /**
     * Clears the screen of output
     *
     * @return	void
     */
    public static function clear_screen() : void
    {
        static::is_windows()

            // Windows is a bit crap at this, but their terminal is tiny so shove this in
            ? static::new_line(40)

            // Anything with a flair of Unix will handle these magic characters
            : fwrite(static::$stdout, chr(27)."[H".chr(27)."[2J");
    }

    /**
     * Returns the given text with the correct color codes for a foreground and
     * optionally a background color.
     *
     * @param	string	$text		the text to color
     * @param	string	$foreground the foreground color
     * @param	string|null	$background the background color
     * @param	string|null	$format		other formatting to apply. Currently only 'underline' is understood
     * @return	string	the color coded string
     */
    public static function color(string $text, string $foreground, string $background = null, string $format=null) : string
    {
        if (!$_SERVER['ANSICON'] && static::is_windows())
        {
            return $text;
        }

        if (static::$no_color)
        {
            return $text;
        }

        if (!array_key_exists($foreground, static::$foreground_colors))
        {
            throw new OutOfBoundsException('Invalid CLI foreground color: '.$foreground);
        }

        if ($background !== null && !array_key_exists($background, static::$background_colors))
        {
            throw new OutOfBoundsException('Invalid CLI background color: '.$background);
        }

        $string = "\033[".static::$foreground_colors[$foreground]."m";

        if (!is_null($background))
        {
            $string .= "\033[".static::$background_colors[$background]."m";
        }

        if ($format === 'underline')
        {
            $string .= "\033[4m";
        }

        $string .= $text."\033[0m";

        return $string;
    }

    /**
     * Spawn Background Process
     *
     * Launches a background process (note, provides no security itself, $call must be sanitised prior to use)
     * @param string $call the system call to make
     * @param string $output
     * @return void
     * @author raccettura
     * @link http://robert.accettura.com/blog/2006/09/14/asynchronous-processing-with-php/
     */
    public static function spawn(string $call, string $output = '/dev/null') : void
    {
        // Windows
        if(static::is_windows())
        {
            pclose(popen('start /b '.$call, 'r'));
        }

        // Some sort of UNIX
        else
        {
            pclose(popen($call.' > '.$output.' &', 'r'));
        }
    }

    /**
     * Redirect STDERR writes to this file or fh
     *
     * Call with no argument to retrieve the current file handle.
     *
     * Is not smart about opening the file if it's a string. Existing files will be truncated.
     *
     * @param  resource|string  $fh  Opened file handle or string filename.
     *
     * @return resource
     */
    public static function stderr(mixed $fh = null) : mixed
    {
        $orig = static::$stderr;

        if (!is_null($fh)) {
            $fh = fopen($fh, "wb");

            static::$stderr = $fh;
        }

        return $orig;
    }

    /**
     * Redirect STDOUT writes to this file or fh
     *
     * Call with no argument to retrieve the current file handle.
     *
     * Is not smart about opening the file if it's a string. Existing files will be truncated.
     *
     * @param  resource|string|null  $fh  Opened file handle or string filename.
     *
     * @return resource
     */
    public static function stdout(mixed $fh = null) : mixed
    {
        $orig = static::$stdout;

        if (!is_null($fh)) {
            $fh = fopen($fh, "wb");

            static::$stdout = $fh;
        }

        return $orig;
    }
}